﻿namespace ProcessMAP.DataAccess.Common
{
    /// <summary>
    /// 
    /// </summary>
    internal interface IDataAccess<T>
    {
        T OpenDatabase();
    }
}