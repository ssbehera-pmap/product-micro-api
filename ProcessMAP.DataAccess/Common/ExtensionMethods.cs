﻿using FastMember;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;

namespace ProcessMAP.DataAccess.Common
{
    /// <summary>
    /// Extension methods for the Data Access Layer.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        [ExcludeFromCodeCoverage]
        //public static bool HasColumn(this SqlDataReader dr, string columnName)
        //{
        //    try
        //    {
        //        var quiz = dr[columnName];
        //        return true;
        //    }
        //    catch (IndexOutOfRangeException) { return false; }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> collection, string tableName = null)
        {
            DataTable table = new DataTable(tableName);

            using (var reader = ObjectReader.Create(collection))
            {
                table.Load(reader);
            }

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TVal"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<TKey, TVal>(this IDictionary<TKey, TVal> dictionary, string tableName = null)
        {
            var colKey = new DataColumn("Key", typeof(TKey));
            var colVal = new DataColumn("Value", typeof(TVal));

            DataTable table = new DataTable(tableName);
            table.Columns.Add(colKey);
            table.Columns.Add(colVal);

            if (dictionary == null)
                return table;

            foreach(var entry in dictionary)
            {
                var newRow = table.NewRow();
                newRow[colKey.ColumnName] = entry.Key;
                newRow[colVal.ColumnName] = entry.Value;

                table.Rows.Add(newRow);
            }

            return table;
        }
    }
}