﻿namespace ProcessMAP.DataAccess.Common
{
    public class TenantAllowedEndpoint
    {
        public int ConsumerId { get; set; }

        public string Path { get; set; }
    }
}