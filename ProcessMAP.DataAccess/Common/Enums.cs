﻿namespace ProcessMAP.DataAccess.Common
{
    /// <summary>
    /// 
    /// </summary>
    public enum OutputType
    {
        /// <summary>
        /// 
        /// </summary>
        DataReader,

        /// <summary>
        /// 
        /// </summary>
        DataSet,

        /// <summary>
        /// 
        /// </summary>
        Nonquery,

        /// <summary>
        /// 
        /// </summary>
        Scalar,

        /// <summary>
        /// 
        /// </summary>
        XmlReader
    }
}
