﻿using ProcessMAP.API.Common.Filters;
using ProcessMAP.Common.Utilities;
using System.IO.Compression;

namespace ProcessMAP.API.Common.Controllers
{
    public abstract class PmapApiStartup
    {
        protected PmapApiStartup(IConfiguration configuration)
        {
            //
            // Load API Configuration Settings
            //
            Configuration = configuration;
            AppConfig = Configuration.GetSection(CommonConstants.AppConfig).Get<AppConfig>();
            ApiInformation = Configuration.GetSection(CommonConstants.AppConfig).Get<AppConfig>().ApiInformation;
        }

        /// <summary>
        /// The application settings.
        /// </summary>
        protected IConfiguration Configuration { get; }

        /// <summary>
        /// The API configuration settings.
        /// </summary>
        protected AppConfig AppConfig { get; private set; }
        protected ApiInformation ApiInformation { get; private set; }

        // This method gets called by the runtime.
        // Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
            .AddNewtonsoftJson(options =>
            {
                WebApiConfig.ConfigureMvcJsonOptions(options, AppConfig);
            });

            services.AddResponseCaching();

            services.AddMvcCore(options =>
            {
                options.Filters.Add(new TokenAuthorizeFilter());
                options.Filters.Add(new EndpointValidationActionFilter());
                options.Filters.Add(new RequiredHeadersFilter());
                options.Filters.Add(new EndpointLoggerActionFilter());
                options.Filters.Add(new EntityValidationActionFilter());
                options.Filters.Add(new ApiExceptionFilterAttribute());
            })
            .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Latest)
            .AddXmlSerializerFormatters()
            .AddControllersAsServices();

            //
            // GZip Compression
            //
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);
            services.AddResponseCompression();

            //
            // Make specific sections of the Configuration object available for injection.
            //
            services.AddOptions();
            services.Configure<AppConfig>(Configuration.GetSection(CommonConstants.AppConfig));
            services.Configure<ApiInformation>(Configuration.GetSection("ApiInformation"));
            services.Configure<AppConfig>(x => Configuration.GetSection(CommonConstants.AppConfig).Bind(x));
            services.Configure<ApiInformation>(x => Configuration.GetSection("ApiInformation").Bind(x));
            services.AddSingleton(Configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime.
        // Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            WebApiConfig.ConfigureMvcJsonOptions(WebApiConfig.mvcJsonOptions, AppConfig);
            app.UseMiddleware<BeginRequestMessageHandler>();
            app.UseMiddleware<RequestIdentifierMessageHandler>();
            // todo:           app.UseMiddleware<LoggerRequestParamsConfigurationMiddleware>();
            app.UseMiddleware<TenantCacheMessageHandler>();
            app.Use(next => context =>
            {
                context.Request.EnableBuffering(); // DM: to Core 3 changed. Core 2.2: .EnableRewind();
                return next(context);
            });

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseResponseCaching();
            //app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}