﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using ProcessMAP.API.AppSettings;
using ProcessMAP.Business;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Common.Entities.Interfaces;
using ProcessMAP.Common.Enumerations;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authentication;
using ProcessMAP.Security.Authorization;
using System;
using System.Net;
using System.Reflection;

namespace ProcessMAP.API.Common.Controllers.DAP
{
    /// <summary>
    /// Base Controller
    /// </summary>
    public class PmapApiController : ControllerBase
    {
        public AppConfig AppConfig { get; set; }
        public ApiInformation ApiInformation { get; set; }

        public PmapApiController(IOptions<AppConfig> appConfig)
        {
            AppConfig = appConfig.Value;
            ApiInformation = AppConfig.ApiInformation;
        }

        #region Properties
        /// <summary>
        /// The UserAuthorization object defining all the user permissions.
        /// </summary>
        public UserAuthorization UserAuthorization
        {
            get
            {
                if (HttpContext.Items.Keys.Contains(PmapClaimNames.Authorization))
                {
                    return HttpContext.Items[PmapClaimNames.Authorization] as UserAuthorization;
                }
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ConsumerContext ConsumerContext
        {
            get
            {
                var context = new ConsumerContext()
                {
                    DatabaseInfo = DatabaseInfo,
                    ConsumerId = ConsumerId,
                    Consumer = Consumer
                };

                return context;
            }
        }

        private IDatabaseInfo databaseInfo = null;
        /// <summary>
        /// Gets the consumer's database information.
        /// </summary>
        public IDatabaseInfo DatabaseInfo
        {
            get
            {
                if (databaseInfo != null)
                    return databaseInfo;

                var dbInfo = CachedConsumers.GetConsumerDatabaseInfo(ConsumerId);

                if (dbInfo == null)
                    throw new WebApiException(HttpStatusCode.InternalServerError, "The consumer's database information could not be found.");

                return dbInfo;
            }
            set { databaseInfo = value; }
        }

        /// <summary>
        /// Gets a value representing the tenant connection string.
        /// </summary>
        public string TenantConnectionString
        {
            get
            {
                //var connString = CachedConsumers.GetConsumerConnectionString(ConsumerId);

                //if (string.IsNullOrWhiteSpace(connString))
                //    throw new WebApiException(HttpStatusCode.InternalServerError, ErrorMessages.ERR_1032);

                //return connString;
                return string.Empty;
            }
        }

        private Consumer consumer = null;

        /// <summary>
        /// Gets a value representing the tenant connection string.
        /// </summary>
        public Consumer Consumer
        {
            get
            {
                if (consumer != null)
                    return consumer;

                if (CachedConsumers.Contains(ConsumerId))
                {
                    return CachedConsumers.Elements[ConsumerId] as Consumer;
                }

                return null;
            }
            set { consumer = value; }
        }

        /// <summary>
        /// Gets a value representing the connection string to the TenantSettings database.
        /// </summary>
        public static string FederatedConnectionString
        {
            get
            {
                throw new Exception("DM: You should setup FederatedConnectionString ai AppConfig");
                // return ConfigurationManager.ConnectionStrings["TennantSettings"].ConnectionString;
            }
        }

        /// <summary>
        /// Gets a value representing the integrations connection string.
        /// </summary>
        public string IntegrationsConnectionString
        {
            get
            {
                throw new Exception("DM: You should setup IntegrationsConnectionString ai AppConfig");
                // return ConfigurationManager.ConnectionStrings["Integrations"].ConnectionString;
            }
        }

        private int consumerId = 0;

        /// <summary>
        /// Gets a value from the request headers representing the consumer id to use for this service call.
        /// </summary>
        public int ConsumerId
        {
            get
            {
                if (consumerId != 0)
                    return consumerId;

                return Request.Headers.GetValue<int>(PmapRequestHeaders.ConsumerId);
            }
            set { consumerId = value; }
        }

        /// <summary>
        /// Gets a value from the request headers representing the location id to use for this service call.
        /// </summary>
        public int LocationId
        {
            get
            {
                return Request.Headers.GetValue<int>(PmapRequestHeaders.LocationId);
            }
        }

        /// <summary>
        /// Gets a value from the request headers representing the user id to use for this service call.
        /// </summary>
        public int UserId
        {
            get
            {
                return Request.Headers.GetValue<int>(PmapRequestHeaders.UserId);
            }
        }

        /// <summary>
        /// Gets a value from the request headers representing the level id to use for this service call.
        /// </summary>
        public int LevelId
        {
            get
            {
                return Request.Headers.GetValue<int>(PmapRequestHeaders.LevelId);
            }
        }

        /// <summary>
        /// Gets a value from the request headers representing the module id to use for this service call.
        /// </summary>
        public int ModuleId
        {
            get
            {
                return Request.Headers.GetValue<int>(PmapRequestHeaders.ModuleId);
            }
        }

        /// <summary>
        /// Gets a value from the request headers representing the time-zone id to use for this service call.
        /// </summary>
        public string TimeZone
        {
            get
            {
                return Request.Headers.GetValue<string>(PmapRequestHeaders.TimeZone);
            }
        }

        /// <summary>
        /// Gets a value from the request headers representing the language code to use for this service call.
        /// </summary>
        public string Language
        {
            get
            {
                string lang = Request.Headers[PmapRequestHeaders.AcceptLanguage];

                if (string.IsNullOrWhiteSpace(lang))
                {
                    lang = WebApiConfig.DEFAULT_LANGUAGE;
                }
                else if (lang.Length > 2)
                {
                    lang = lang.Substring(0, 2);
                }

                return lang;
            }
        }
        
        /// <summary>
        /// Gets a value from the request headers representing the application type of the consumer.
        /// E.g.: iOS, Android, Web, etc.
        /// </summary>
        public ApplicationType ApplicationType
        {
            get
            {
                HttpRequest r = Request;
                IHeaderDictionary d = r.Headers;
                var appType = d.GetValue<ApplicationType>(PmapRequestHeaders.ApplicationType);
                return appType == ApplicationType.None ? ApplicationType.Web : appType;
            }
        }

        /// <summary>
        /// Represents the unique identifier of the source.
        /// </summary>
        public string SourceId
        {
            get
            {
                return Request.Headers.GetValue<string>(PmapRequestHeaders.SourceId);
            }
        }

        /// <summary>
        /// Represents the unique identifier of the type of the source.
        /// </summary>
        public EntityType SourceTypeId
        {
            get
            {
                return Request.Headers.GetValue<EntityType>(PmapRequestHeaders.SourceTypeId);
            }
        }

        /// <summary>
        /// Represents the unique identifier of the type of the source of the parent.
        /// </summary>
        public string ParentSourceId
        {
            get
            {
                return Request.Headers.GetValue<string>(PmapRequestHeaders.ParentSourceId);
            }
        }

        /// <summary>
        /// Represents the name with extension of the file to be uploaded.
        /// </summary>
        public string FileName
        {
            get
            {
                return Request.Headers.GetValue<string>(PmapRequestHeaders.FileName);
            }
        }

        /// <summary>
        /// Sets the File upload path if provided. Eg : For calendarModule: "locationId\MODULES\ACTIONITEM\SourceId". Sample :"8790\MODULES\ACTIONITEM\US-Westlake-16-CA-1973".
        /// </summary>
        public string DestinationFolder
        {
            get
            {
                return Request.Headers.GetValue<string>(PmapRequestHeaders.DestinationFolder);
            }
        }

        /// <summary>
        /// Sets the File Category /parent. Eg: Form/ActionItem/ControlNAme etc.
        /// </summary>
        public string Source
        {
            get
            {
                return Request.Headers.GetValue<string>(PmapRequestHeaders.Source);
            }
        }

        /// <summary>
        /// AuthoAuthorizationCode
        /// </summary>
        public string AuthorizationCode
        {
            get
            {
                return Request.Headers.GetValue<string>(PmapRequestHeaders.Authorization);
            }
        }

        /// <summary>
        /// RestServiceHostUrl
        /// </summary>
        protected string RestServiceHostUrl
        {
            get { return AppConfig.ConnectionSettings.RestServiceHostUrl; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ApiRequestHeaders RequestHeaders
        {
            get
            {
                var requestHeaders = new ApiRequestHeaders()
                {
                    ApplicationType = ApplicationType,
                    AuthorizationCode = AuthorizationCode,
                    ConsumerId = ConsumerId,
                    FileName = FileName,
                    Language = Language,
                    LevelId = LevelId,
                    LocationId = LocationId,
                    ModuleId = ModuleId,
                    ParentSourceId = ParentSourceId,
                    SourceId = SourceId,
                    SourceTypeId = SourceTypeId,
                    TimeZone = TimeZone,
                    UserId = UserId
                };

                return requestHeaders;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="paramName"></param>
        /// <param name="methodName"></param>
        public void ValidateGuidInputParameter<T>(Guid guid, string paramName, string methodName)
        {
            if (guid != Guid.Empty) return;

            PmapLogger<T>.DebugFormat("{0} failed due to invalid input parameter '{1}={2}'", methodName, paramName, guid);
            throw new InvalidApiRequestException(ErrorMessages.ERR_1026, HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="item"></param>
        /// <param name="paramName"></param>
        /// <param name="methodName"></param>
        /// <param name="errorMessage"></param>
        public void ValidateEnumInputParameter<TClass, TEnum>(TEnum item, string paramName, string methodName, string errorMessage = null)
        {
            if (Enum.IsDefined(typeof(TEnum), item)) return;

            LogInvalidInputParameter<TClass>(item, paramName, methodName);
            throw new InvalidApiRequestException(errorMessage, HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param"></param>
        /// <param name="paramName"></param>
        /// <param name="methodName"></param>
        private void LogInvalidInputParameter<T>(object param, string paramName, string methodName)
        {
            PmapLogger<T>.InfoFormat("{0} failed due to invalid parameter '{1}={2}'", methodName, paramName, param);
        }
        #endregion

        public IActionResult GetVersion<T>(string controllerName)
        {
            PmapLogger<T>.Info(CommonConstants.EnteringIntoThe + controllerName +
                nameof(GetVersion) + CommonConstants.Endpoint);
            string version = ApiInformation.CurrentVersion.ToString();
            if (string.IsNullOrWhiteSpace(version))
            {
                version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
            PmapLogger<T>.Info(CommonConstants.LeavingThe + controllerName +
                nameof(GetVersion) + CommonConstants.Endpoint);
            return Ok(version);
        }

        /// <summary>
        /// Gets the appsettings.json of the current microservice.
        /// </summary>
        /// <returns></returns>
        public IActionResult GetAppSettings<T>(string controllerName)
        {
            PmapLogger<T>.Info(CommonConstants.EnteringIntoThe + controllerName +
                nameof(GetAppSettings) + CommonConstants.Endpoint);
            JObject jsonObject = JObject.Parse(System.IO.File.ReadAllText("./conf/appsettings.json"));
            PmapLogger<T>.Info(CommonConstants.LeavingThe + controllerName +
                nameof(GetAppSettings) + CommonConstants.Endpoint);
            return Ok(jsonObject);
        }
    }
}