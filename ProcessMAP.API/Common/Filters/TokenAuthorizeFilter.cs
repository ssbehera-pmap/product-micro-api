﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using ProcessMAP.Business.Authentication;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authentication;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace ProcessMAP.API.Common.Filters
{
    /// <summary>
    /// Custom HTTP Message Handler that satisfies the need for sliding JWT expirations.
    /// </summary>
    public class TokenAuthorizeFilter : PmapActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (CanSkipAuthorization(actionContext))
            {
                // Intercept only secured endpoints
                // Exit
                PmapLogger<TokenAuthorizeFilter>.DebugFormat("OnAuthorization will skip this API: '{0}'", actionContext.HttpContext.Request.Host);
                return;
            }

            var request = actionContext.HttpContext.Request;
            var consumerId = request.Headers.GetValue<int>(PmapRequestHeaders.ConsumerId);

            if (CachedConsumers.Contains(consumerId) == false)
            {
                PmapLogger<TokenAuthorizeFilter>.InfoFormat("OnAuthorization failed because the tenant with id '{0}' was not found in memory.", consumerId);
                throw new InvalidApiRequestException(ErrorMessages.ERR_1035, HttpStatusCode.Unauthorized);
            }

            var tenant = CachedConsumers.Elements[consumerId];
            var claimSettings = tenant.ClaimSettings;
            if (claimSettings == null || claimSettings.Count == 0)
            {
                PmapLogger<TokenAuthorizeFilter>.InfoFormat("OnAuthorization failed because the tenant '{0}' found in memory does not have any claim settings.", consumerId);
                throw new InvalidApiRequestException(ErrorMessages.ERR_1036, HttpStatusCode.Unauthorized);
            }

            var token = GetToken(request);
            if (string.IsNullOrWhiteSpace(token))
            {
                PmapLogger<TokenAuthorizeFilter>.Info("OnAuthorization failed because a required security token was not found in the headers.");
                throw new WebApiException(HttpStatusCode.Unauthorized, ErrorMessages.ERR_1013);
            }

            var authGrantType = tenant.ClaimSettings.FirstOrDefault(x => x.Key.Equals(RequiredClaim.AuthGrantType, StringComparison.CurrentCultureIgnoreCase))?.Value?.ToString();

            if (string.IsNullOrWhiteSpace(authGrantType) == false)
            {
                if (token.StartsWith(authGrantType))
                {
                    token = token.Replace(authGrantType + CommonConstants.Space, string.Empty);
                }
            }

            var consumerClaim = new KeyValuePair<string, int>(PmapClaimNames.ConsumerId, consumerId);

            try
            {
                JWTManager.ValidateToken(token, tenant.ClaimSettings, consumerClaim, tenant.ApplicationId);
            }
            catch (InvalidTokenException ite)
            {
                PmapLogger<TokenAuthorizeFilter>.ErrorFormat("OnAuthorization failed while validating the web token. Error Message: {0}", ite.Message);
                throw new WebApiException(HttpStatusCode.Unauthorized, ite.Message);
            }

            var tokenClaims = JWTManager.GetTokenClaims(token);
            if (tokenClaims.ContainsKey(PmapClaimNames.Authorization))
            {
                var jwtJson = tokenClaims.SerializeToJson();
                var jwtObj = JsonConvert.DeserializeObject<JObject>(jwtJson);
                var userAuthorization = jwtObj[PmapClaimNames.Authorization].ToObject<UserAuthorization>();

                actionContext.HttpContext.Items.Add(PmapClaimNames.Authorization, userAuthorization);

                // APB-196: additional check for token's and header's LocationId equality
                int headerLocId = request.Headers.GetValue<int>(PmapRequestHeaders.LocationId);
                if (userAuthorization.LocationId != headerLocId)
                {
                    string msg = "OnAuthorization failed: Token and Header LocationId mismatch.";
                    PmapLogger<TokenAuthorizeFilter>.ErrorFormat(msg);
                    throw new WebApiException(HttpStatusCode.Unauthorized, msg);
                }
            }

            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(ActionExecutedContext actionContext)
        {
            base.OnActionExecuted(actionContext);

            // Intercept only secured endpoints
            if (CanSkipAuthorization(actionContext))
                return;

            // Intercept only 200 OK responses.
            if (actionContext.HttpContext.Response.StatusCode != (int)HttpStatusCode.OK)
                return;

            // If the original token header is not present, we can't proceed.
            if (actionContext.HttpContext.Request.Headers.Keys.Select(key => key.ToLower()).Contains(PmapRequestHeaders.Authorization.ToLower()) == false)
                return;

            var consumerId = actionContext.HttpContext.Request.Headers.GetValue<int>(PmapRequestHeaders.ConsumerId);
            if (CachedConsumers.Elements.ContainsKey(consumerId) == false)
            {
                // Without the tenant we can't proceed.
                // It contains claims settings and other properties needed to continue.
                // Exit.
                return;
            }

            // Get the Tenant information from memory.
            var consumer = CachedConsumers.Elements[consumerId];

            // We need to add the "Access-Control-Expose-Headers" header for access control.
            actionContext.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", PmapRequestHeaders.Authorization);

            var origToken = actionContext.HttpContext.Request.Headers.GetValue<string>(PmapRequestHeaders.Authorization);
            origToken = origToken.Split(' ').Length > 1 ? origToken.Split(' ')[1] : origToken;

            // Check if the Claim Settings dictate a sliding expiration.
            // If so, create a new token and put it in the response headers.
            var isSlidingClaim = consumer.ClaimSettings.FirstOrDefault(c => c.Key.Equals(RequiredClaim.IsSlidingExpiration, StringComparison.CurrentCultureIgnoreCase))?.Value?.ToString();
            bool.TryParse(isSlidingClaim, out bool isSliding);

            if (isSliding)
            {
                // Get payload from original token
                var origJwtPayload = JWTManager.GetTokenClaims(origToken);
                var origPayloadValues = origJwtPayload.ToNameValueCollection();
                origPayloadValues.Remove(RegisteredClaimNames.JWTID);
                origPayloadValues.Remove(PmapClaimNames.Authorization);

                var userAuthorization = actionContext.HttpContext.Items.ContainsKey(PmapClaimNames.Authorization) ?
                    actionContext.HttpContext.Items[PmapClaimNames.Authorization] as UserAuthorization : null;

                if (userAuthorization == null)
                {
                    if (origJwtPayload.ContainsKey(PmapClaimNames.Authorization))
                    {
                        var jwtJson = origJwtPayload.SerializeToJson();
                        var jwtObj = JsonConvert.DeserializeObject<JObject>(jwtJson);
                        userAuthorization = jwtObj[PmapClaimNames.Authorization].ToObject<UserAuthorization>();
                    }
                }

                string newToken = JWTManager.IssueToken(origPayloadValues, consumer.ClaimSettings, userAuthorization);

                if (newToken != null)
                {
                    actionContext.HttpContext.Response.Headers
                        .Add(PmapRequestHeaders.Authorization, newToken);
                    return;
                }
            }

            // No sliding expiration specified.
            // Use the original token for the response headers.
            if (string.IsNullOrWhiteSpace(origToken) == false)
            {
                actionContext.HttpContext.Response.Headers.Add(PmapRequestHeaders.Authorization, new string[] { origToken });
            }
        }

        internal static bool CanSkipAuthorization(FilterContext filterContext)
        {
            return filterContext.ActionDescriptor.EndpointMetadata.Any(f => f is AllowAnonymousAttribute);
        }

        private static string GetToken(HttpRequest request)
        {
            var headerValue = request.Headers.GetValue<string>(PmapRequestHeaders.Authorization);
            var parts = headerValue.ToString().Split(' ');

            return parts.Length == 1 ? parts[0] : parts[1];
        }
    }
}