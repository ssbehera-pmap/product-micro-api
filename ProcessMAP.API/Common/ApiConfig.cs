﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using ProcessMAP.API.AppSettings;
using ProcessMAP.Common.Utilities;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProcessMAP.API.Common
{
    /// <summary>
    /// Provides an enumeration of the supported API versions.
    /// </summary>
    public enum ApiVersion : int
    {
        /// <summary>
        /// Version 1
        /// </summary>
        v1 = 1,

        /// <summary>
        /// Latest version
        /// </summary>
        Latest = v1,

        /// <summary>
        /// Earliest Version
        /// </summary>
        Earliest = v1
    }

    /// <summary>
    /// 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class WebApiConfig
    {
        internal const string DEFAULT_LANGUAGE = "en";
        internal const string HELP_PREFIX = "Help";
        internal const string WEB_API_PREFIX = "papi";
        internal const string VERSION_TEMPLATE = "/v{version}";
        private const string API_VERSION_REGEX = @"(v\d+(.\d+)?)";
        internal const string WEB_API_VERSIONED_ROUTE_PREFIX = WEB_API_PREFIX + VERSION_TEMPLATE;
        internal const string HELP_VERSIONED_ROUTE_PREFIX = HELP_PREFIX + VERSION_TEMPLATE;

        internal static readonly PmapHeader[] REQUIRED_GLOBAL_API_HEADERS = {};

        internal static readonly PmapHeader[] OPTIONAL_GLOBAL_API_HEADERS =
        {
            Constants.PmapHeaders[PmapRequestHeaders.ApplicationType],
            Constants.PmapHeaders[PmapRequestHeaders.AcceptLanguage],
            Constants.PmapHeaders[PmapRequestHeaders.TimeZone]
        };

        internal static readonly PmapHeader[] REQUIRED_SECURED_API_HEADERS =
        {
            Constants.PmapHeaders[PmapRequestHeaders.Authorization],
            Constants.PmapHeaders[PmapRequestHeaders.ConsumerId]
        };

        public static int VR_counter = 0;
        public static string[] VR_array = new string[277];
        public static int RRH_counter = 0;
        public static string[] RRH_array = new string[209];

        public static MvcNewtonsoftJsonOptions mvcJsonOptions = new MvcNewtonsoftJsonOptions();

        public static void ConfigureMvcJsonOptions(MvcNewtonsoftJsonOptions options, AppConfig appConfig)
        {
            options.SerializerSettings.Converters.Add(new StringEnumConverter());
            options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            options.SerializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss.fffZ";
            options.SerializerSettings.Formatting = Formatting.Indented;
            options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            options.SerializerSettings.ObjectCreationHandling = ObjectCreationHandling.Replace;
            options.SerializerSettings.MissingMemberHandling = appConfig.SerializerSettings.IgnoreMissingMembers ?
                MissingMemberHandling.Ignore : MissingMemberHandling.Error;
            options.SerializerSettings.NullValueHandling = appConfig.SerializerSettings.IgnoreNullValues ?
                NullValueHandling.Ignore : NullValueHandling.Include;

            // Remove camelCasing
            if (options.SerializerSettings.ContractResolver is DefaultContractResolver resolver)
                resolver.NamingStrategy = null;
        }

        public static void CommonConfig(IApplicationBuilder app, AppConfig appConfig)
        {
            ConfigureMvcJsonOptions(mvcJsonOptions, appConfig);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal static string GetApiVersionRewrite(string path)
        {
            var apiVersion = GetApiVersionFromURL(path);

            if (apiVersion.HasValue == false)
            {
                var pathSections = path.Split('/').ToList();

                if (pathSections[0] == string.Empty)
                {
                    pathSections.RemoveAt(0);
                }

                var urlApiVersion = "v" + ((int) ApiVersion.Latest).ToString();
                int insertIndex;
                if (pathSections.Contains(WEB_API_PREFIX))
                {
                    insertIndex = pathSections.IndexOf(WEB_API_PREFIX);
                }
                else
                {
                    insertIndex = pathSections.IndexOf(HELP_PREFIX);
                }

                if (insertIndex == -1)
                    return null;

                pathSections.Insert(insertIndex + 1, urlApiVersion);
                var newPath = "/" + string.Join("/", pathSections.ToArray());

                return newPath;
            }

            return null;
        }

        internal static int? GetApiVersionFromURL(string path)
        {
            var urlApiVersion = Regex.Match(path, API_VERSION_REGEX).Value;

            if (string.IsNullOrWhiteSpace(urlApiVersion))
                return null;

            int.TryParse(urlApiVersion.Substring(1), out int version);

            if (version <= 0)
                return null;

            return version;
        }
    }
}