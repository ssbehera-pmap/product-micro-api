﻿using Newtonsoft.Json;
using ProcessMAP.API.AppSettings;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Services.REST.Common;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProcessMAP.Business.PubSub
{
    /// <summary>
    /// PublisherBroker
    /// </summary>
    public class PublisherBroker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="apiRequestHeader"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static async Task<MessageStatus> Publish(AppConfig appConfig, dynamic info, ApiRequestHeaders apiRequestHeader, string action)
        {
            var messageStatus = default(MessageStatus);
            //var publisherUri = new Uri(ConfigurationManager.AppSettings.Get("PublisherEndpoint"));
            var publisherUri = new Uri(appConfig.PubSubParam.PublisherEndpoint);
            //var appBuilderChannel = ConfigurationManager.AppSettings.Get("AppbuilderChannel");

            PmapLogger<PublisherBroker>.Info($"The subscriber '{publisherUri.ToString()}' ");
            var payload = new
            {
                //Channel = appBuilderChannel,
                Channel = appConfig.PubSubParam.AppbuilderChannel,
                Payload = new
                {
                    Information = info,
                    Action = action
                }
            };

            //var headers = new NameValueCollection()
            //{
            //    { PmapRequestHeaders.Authorization, apiRequestHeader.AuthorizationCode },
            //    { PmapRequestHeaders.ConsumerId, apiRequestHeader.ConsumerId.ToString() }
            //};
            try
            {
                using (var response = await Utility.Utility.CallExternalAPIAsync(publisherUri.ToString(), "", HttpMethod.Post, null, payload, false))//await HttpPostAsync(publisherUri, payload, headers)
                {
                    messageStatus = new MessageStatus()
                    {
                        ErrorMessage = response.ReasonPhrase,
                        StatusCode = response.StatusCode
                    };
                }
            }
            catch (Exception ex)
            {
                PmapLogger<Exception>.Error("An exception was thrown while posting a message to the publisher.", ex);
                messageStatus = new MessageStatus()
                {
                    ErrorMessage = ex.Message,
                    Exception = ex,
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }

            return messageStatus;
        }
        /// <summary>
        /// HttpPostAsync
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="payload"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> HttpPostAsync(Uri uri, dynamic payload, NameValueCollection headers)
        {
            using (var client = new HttpClient())
            {
                if (headers != null)
                {
                    foreach (string key in headers)
                    {
                        client.DefaultRequestHeaders.Add(key, headers[key]);
                    }
                }
                client.DefaultRequestHeaders.Add("Accept", "*/*");

                using (var content = new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, CommonConstants.ApplicationJson))
                {
                    return await client.PostAsync(uri, content);
                }
            }
        }
    }
}
