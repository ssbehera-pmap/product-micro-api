﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessMAP.Common.Entities.PubSub
{
    [Serializable]
    public class AsyncMessageContent
    {

        /// <summary>
        /// 
        /// </summary>
        public AsyncMessageContent() { }

        /// <summary>
        /// 
        /// </summary>
        //public int ConsumerId { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string RequestUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public string JobName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public dynamic Payload { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AlertMeOnChannel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime AlertMeOnDate { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// 
        /// </summary>
        public int? RepeatCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? RepeatUntilDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? RepeatIntervalInMinutes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string InvalidatingKey { get; set; }
    }



}

