﻿using System;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using ProcessMAP.API.AppSettings;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rebus.Activation;
using Rebus.Config;
using Rebus.Logging;
using Rebus.Serialization.Json;
using ProcessMAP.Common.PubSub;
using ProcessMAP.Business.DAP;
using ProcessMAP.Business.DAP.Subscribers;
using ProcessMAP.Common.Entities.PubSub;
using ProcessMAP.Common.Utilities;

namespace ProcessMAP.Business.PubSub
{
    public class SubscriberBroker
    {
        private static AppConfig Appconfig { get; set; }
        private static List<BuiltinHandlerActivator> _subscribers = new List<BuiltinHandlerActivator>();

        public SubscriberBroker(AppConfig appConfig)
        {
            Appconfig = appConfig;
        }
        public async Task StartSubscribers()
        {

            var config = new SubscriberConfiguration();
            config.Channel = Appconfig.PubSubParam.AppbuilderChannel;
            config.QueueName = Appconfig.PubSubParam.AppbuilderChannel;
            config.ConnectionString = Appconfig.ConnectionSettings.RabbitMQ;

            var subscriber = new BuiltinHandlerActivator();
            subscriber.Handle<dynamic>(async msg => { await ProcessIncomingMessage(msg, config); });

            var clientProperties = new Dictionary<string, string>() { { "connection_name", config.QueueName } };
            var settings = new JsonSerializerSettings { };
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            settings.TypeNameHandling = TypeNameHandling.None;

            Configure.With(subscriber)
                    .Logging(l => l.ColoredConsole())
                    .Transport(t => t.UseRabbitMq(config.ConnectionString, config.QueueName)
                    .AddClientProperties(clientProperties))
                    .Serialization(s => s.UseNewtonsoftJson(settings))
                    .Start();

            await subscriber.Bus.Advanced.Topics.Subscribe(config.Channel);
            _subscribers.Add(subscriber);



            PmapLogger<SubscriberBroker>.Info($"The subscriber '{config.QueueName}' has started listening on channel '{config.Channel}'.");

        }

        /// <summary>
        /// Processes the incoming messages.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        private static async Task ProcessIncomingMessage(dynamic message, SubscriberConfiguration config)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var json = JsonConvert.SerializeObject(message);
            PmapLogger<SubscriberBroker>.Debug($"Message: {json}.");
            JObject results = JObject.Parse(json);

            try
            {
                if (results != null)
                {
                    var channel = results["Channel"]?.ToString();
                    var payload = results["Payload"];

                    if (payload != null)
                    {
                        var messageAction = payload["Action"]?.ToString();
                        var messageContent = JsonConvert.DeserializeObject<AsyncMessageContent>(json);
                        var contentPayloadInit = messageContent != null ? ((AsyncMessageContent)messageContent).Payload : null;
                        var contentPayload = JsonConvert.SerializeObject(contentPayloadInit);
                        switch (messageAction)
                        {
                            case CommonConstants.AppBuilderNotification:
                                {
                                    var AppBuilderNotificationRequestData = payload["Information"]?.ToString();
                                    var NotificationRequest = JsonConvert.DeserializeObject<AppBuilderNotificationRequest>(AppBuilderNotificationRequestData);
                                    await AppBuilderNotificationJob.Execute(Appconfig, NotificationRequest);
                                    break;
                                }
                            case CommonConstants.ActionItemSync:
                                {
                                    var AppBuilderNotificationRequestData = payload["Information"]?.ToString();
                                    var ActionItemSyncRequest = JsonConvert.DeserializeObject<ActionItemDataSyncRequest>(AppBuilderNotificationRequestData);
                                    await ActionItemDataSyncJob.Execute(Appconfig, ActionItemSyncRequest);
                                    break;
                                }
                            case CommonConstants.DataSync:
                                {
                                    var AppBuilderNotificationRequestData = payload["Information"]?.ToString();
                                    var DataSyncRequest = JsonConvert.DeserializeObject<DataSyncRequest>(AppBuilderNotificationRequestData);
                                    await DataSyncJob.Execute(Appconfig, DataSyncRequest);
                                    break;
                                }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                PmapLogger<SubscriberBroker>.Fatal($" ProcessIncomingMessage threw an exception!", ex);
                throw;
            }

            //if (message.Channel != config.Channel)
            //    throw new InvalidOperationException($"Channel mismatch detected. The message was intended for channel: '{message.Channel}' but handled by channel: '{config.Channel}'.");

            //specific actions to be carriedout here



            // var messageHandlerName = config.MessageHandler.GetType().Name;
            // PmapLogger<SubscriberBroker>.Info($"{nameof(SubscriberBroker)} received a message with id '{message.RequestUid}' on channel '{config.Channel}'. Delegating it to '{messageHandlerName}'.");

            //try
            //{

            //    // here call specific action depending upon the type
            //    await config.MessageHandler.HandleMessage(message);
            //    PmapLogger<SubscriberBroker>.Info($"{messageHandlerName} handled the message with id '{message.RequestUid}' on channel '{config.Channel}'.");
            //}
            //catch (Exception ex)
            //{
            //    PmapLogger<SubscriberBroker>.Fatal($"{messageHandlerName} threw an exception!",ex);

            //}
        }
    }
}
