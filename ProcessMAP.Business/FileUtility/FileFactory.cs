﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Net.Http.Headers;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Business.Foundation;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;

namespace ProcessMAP.Business.FileUtility
{
    /// <summary>
    /// Represents the controller responsible for managing the files.
    /// </summary>
    public class FileFactory : DAPBroker
    {
        /// <summary>
        /// ExcelSheets info for Drill don
        /// </summary>
        internal Dictionary<string, string> SheetsInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public FileFactory(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        { }

        #region "Preview"
        /// <summary>
        /// Takes file path and converts into byte stream.
        /// </summary>
        /// <returns>file content.</returns>
        public ActionResult GetfileContent(HttpResponse response, string fileName,
            byte[] fileStream, string filePath = null, bool isCacheable = false)
        {
            ActionResult result = new OkResult();
            try
            {
                byte[] data = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
                if (filePath != null)
                {
                    var extension = System.IO.Path.GetExtension(filePath);
                    FileManager fileManager = new FileManager(AppConfig, ConsumerContext, RequestHeaders, UserAuthorization);
                    data = fileManager.GetFileContents(filePath);
                }
                else
                {
                    if (fileStream != null && fileStream.Length > 0)
                    {
                        data = fileStream;
                    }
                }
                var provider = new FileExtensionContentTypeProvider();
                if (!provider.TryGetContentType(fileName, out string contentType))
                {
                    contentType = "application/octet-stream";
                }
                string dispositionHeaderType = "attachment";
                if (contentType.Contains("application/pdf") || contentType.Contains("image/") ||
                    contentType.Contains("text/plain") || contentType.Contains("video/msvideo"))
                    dispositionHeaderType = "inline";
                if (data != null)
                {
                    result = new FileContentResult(data, new MediaTypeHeaderValue(contentType));
                    //EE
                    //var disposition = new ContentDispositionHeaderValue(dispositionHeaderType);
                    //disposition.FileName = fileName;
                    var disposition = new ContentDispositionHeaderValue(dispositionHeaderType)
                    {
                        FileName = fileName
                    };
                    response.Headers[HeaderNames.ContentDisposition] = disposition.ToString();
                    if (isCacheable)
                    {
                        response.Headers[HeaderNames.CacheControl] = "public, max-age=2592000‬";
                    }
                }
            }
            catch (Exception)
            {
                //EE: var errorMsg = "The document you have requested does not exist.";
                //result = GenerateMessageContent(response, errorMsg);
                result = GenerateMessageContent(response, CommonConstants.TheDocumentYouHaveRequestedDoesNotExist);
            }
            return result;
        }

        /// <summary>
        /// Generates message content.
        /// </summary>
        /// <returns>file content.</returns>
        public ActionResult GenerateMessageContent(HttpResponse response, string displayMessage)
        {
            var errResponse = "<html><body style='background-color:#e6f2ff;color:#3973ac;text-align: center;padding-top:80px;'><div><h1>" + displayMessage + "</h1></div></body></html>";
            Byte[] info = new System.Text.UTF8Encoding(true).GetBytes(errResponse);
            FileContentResult result = new FileContentResult(info, new MediaTypeHeaderValue("text/html"));
            var disposition = new ContentDispositionHeaderValue("inline");
            response.Headers[HeaderNames.ContentDisposition] = disposition.ToString();
            return result;
        }
        #endregion

        #region "Export For Excel"
        /// <summary>
        /// Sets the workbook properties and adds a default sheet.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        private static void SetWorkbookProperties(ExcelPackage p)
        {
            p.Workbook.Properties.Author = "ProcessMAP Corporation";
            p.Workbook.Properties.Title = "Export to Excel";
        }

        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName)
        {
            if (sheetName.Length > 30)
            {
                sheetName = sheetName.Substring(0, 25);
                sheetName = sheetName + "...";
            }
            if (HasSpecialChar(sheetName))
            {
                string specialChar = @"\/|!#$%&()=?»«@£§€{}.-;'<>_,";
                string newString = Regex.Replace(sheetName, specialChar, "");
                sheetName = newString;
            }
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[sheetName];
            ws.Name = sheetName;
            ws.Cells.Style.Font.Size = 11;
            ws.Cells.Style.Font.Name = "Calibri";
            return ws;
        }

        private static void CreateData(ExcelWorksheet ws, DataTable dt)
        {
            //int colIndex = 0, rowIndex = 0;
            int rowIndex = 0;
            foreach (DataRow dr in dt.Rows) // Adding Data into rows
            {
                int colIndex = 1;
                rowIndex++;

                foreach (DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting Value in cell
                    var v = dr[dc.ColumnName].ToString();
                    if (int.TryParse(v, out int i))
                    {
                        cell.Value = i;
                    }
                    else if (double.TryParse(v, out double d))
                    {
                        cell.Value = d;
                    }
                    else
                    {
                        cell.Value = v;
                    }

                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = border.Top.Style = border.Bottom.Style = ExcelBorderStyle.Thin;
                    //cell.Style.Indent = 10;
                    colIndex++;
                }
            }
        }

        /// <summary>
        /// This method is responsible to Format html to form excel and return as memorystream.
        /// </summary>
        public byte[] ExcelFromTable(string table, string sheetName)
        {
            var ms = new System.IO.MemoryStream();
            using (ExcelPackage p = new ExcelPackage(ms))
            {
                SetWorkbookProperties(p);
                ExcelWorksheet ws = CreateSheet(p, sheetName);
                DataTable dt = HtmlTableParser.ParseTable(table);
                CreateData(ws, dt);
                var fill = ws.Cells[1, 1, 1, dt.Columns.Count].Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.FromArgb(1, 0, 0, 102));
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Color.SetColor(Color.FromArgb(1, 255, 255, 255));
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                int i = 0;
                foreach (var c in dt.Columns)
                {
                    i++;
                    ws.Column(i).Width = 28;
                }
                p.Save();
            }
            return ms.ToArray();
        }

        /// <summary>
        /// This method is responsible to Format html to form excel and return as memorystream.
        /// eg  dataSheetName, sheetContent
        /// </summary>
        public byte[] ExcelwithMultipleSheets(Dictionary<string, StringBuilder> sheetsInfo)
        {
            var ms = new System.IO.MemoryStream();
            using (ExcelPackage p = new ExcelPackage(ms))
            {
                SetWorkbookProperties(p);
                //var count = 0;
                foreach (var sheet in sheetsInfo)
                {
                    ExcelWorksheet ws = CreateSheet(p, sheet.Key);

                    DataTable dt = HtmlTableParser.ParseTable(sheet.Value.ToString());
                    CreateData(ws, dt);
                    var fill = ws.Cells[1, 1, 1, dt.Columns.Count].Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(Color.FromArgb(1, 0, 0, 102));
                    ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Color.SetColor(Color.FromArgb(1, 255, 255, 255));
                    ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                    int i = 0;
                    foreach (var c in dt.Columns)
                    {
                        i++;
                        ws.Column(i).Width = 28;
                    }
                }
                p.Save();
            }
            return ms.ToArray();
        }
        public static bool HasSpecialChar(string input)
        {
            string specialChar = @"\/|!#$%&()=?»«@£§€{}.-;'<>_,";
            foreach (var item in specialChar)
            {
                if (input.Contains(item)) return true;
            }

            return false;
        }
        #endregion
    }
}