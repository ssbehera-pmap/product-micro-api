﻿using Microsoft.AspNetCore.WebUtilities;
using MongoDB.Bson;
using Newtonsoft.Json;
using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Business.Foundation;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Entities.DAP;
using ProcessMAP.Common.Entities.DAP.Caching;
using ProcessMAP.Common.Entities.FileUpload;
using ProcessMAP.Common.Enumerations;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.DataAccess.DAP;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.FileUtility
{
    /// <summary>
    /// 
    /// </summary>
    public class FileBroker : DAPBroker
    {
        /// <summary>
        /// Provides access to the Data Access.
        /// </summary>
        private IFileDataAccess DataAccess { get; set; }

        /// <summary>
        /// Represents an enumeration of the various Application Types.
        /// </summary>
        private ApplicationType ApplicationType { get; set; }

        private int LocationID { get; set; }

        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public FileBroker(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        {
            ApplicationType = requestHeaders.ApplicationType;
            UserId = requestHeaders.UserId;
            LocationID = requestHeaders.LocationId;

            switch (context.DatabaseInfo.DatabaseType)
            {
                case DatabaseType.MongoDb: DataAccess = new DataAccess.DAP.DatabaseProviders.MongoNoSQL.AttachmentDataAccess(AppConfig); break;
                default: DataAccess = new DataAccess.DAP.DatabaseProviders.MongoNoSQL.AttachmentDataAccess(AppConfig); break;
            }

            CachedDataSources.Instance.SetExpiration(ConsumerContext.Consumer.ApplicationCacheExpiration);
            TwoKeyCacheStore<int, Guid, Form>.Instance.SetExpiration(ConsumerContext.Consumer.ApplicationCacheExpiration);
        }

        /// <summary>
        /// Upload file represented by MultipartSection
        /// </summary>
        /// <param name="content">Represents File contents information</param>
        /// <param name="userId">Loggedin UserID</param>
        /// <param name="destinationFolder">Sets the File upload path if provided. Eg : For calendarModule: "locationId\MODULES\ACTIONITEM\SourceId". Sample :"8790\MODULES\ACTIONITEM\US-Westlake-16-CA-1973"</param>
        /// <param name="destination">determines the file saving destination . Possible values 1- mongo, Default is 0</param>
        /// <param name="Uid">[Optional] Represents the unique Id for the File.</param>
        /// <param name="fileName">[Optional] Represents the File Name.</param>
        /// <param name="IsPublic">[Optional] Represents the accessibility of the file.</param>
        /// <returns></returns>
        public async Task<Common.Entities.FileUpload.File> UploadMultipart(MultipartSection section, int userId, Guid? Uid = null,
            string fileName = "", string destinationFolder = "", int destination = 0, bool? IsPublic = false, string source = null)
        {
            byte[] buffer;
            using (var streamReader = new MemoryStream())
            {
                section.Body.Seek(0, SeekOrigin.Begin);
                section.Body.ReadByte(); //it's a magic: need to prevent data corruption in beginning of streams
                section.Body.Seek(0, SeekOrigin.Begin);
                await section.Body.CopyToAsync(streamReader);
                buffer = streamReader.ToArray();
            }

            if (fileName == "" || fileName == null)
            {
                var contentDisposition = section.GetContentDispositionHeader();
                fileName = contentDisposition.FileName.Value.Trim('\"');
            }

            return await UploadBinary(buffer, userId, Uid, fileName, destinationFolder, destination, IsPublic, source);
        }

        /// <summary>
        /// Upload file represented by HttpContent
        /// </summary>
        /// <param name="content">Represents File contents information</param>
        /// <param name="userId">Loggedin UserID</param>
        /// <param name="destinationFolder">Sets the File upload path if provided. Eg : For calendarModule: "locationId\MODULES\ACTIONITEM\SourceId". Sample :"8790\MODULES\ACTIONITEM\US-Westlake-16-CA-1973"</param>
        /// <param name="destination">determines the file saving destination . Possible values 1- mongo, Default is 0</param>
        /// <param name="Uid">[Optional] Represents the unique Id for the File.</param>
        /// <param name="fileName">[Optional] Represents the File Name.</param>
        /// <param name="IsPublic">[Optional] Represents the accessibility of the file.</param>
        /// <returns></returns>
        public async Task<Common.Entities.FileUpload.File> UploadHttpContent(HttpContent content, int userId, Guid? Uid = null,
            string fileName = "", string destinationFolder = "", int destination = 0, bool? IsPublic = false, string source = null)
        {
            byte[] buffer = await content.ReadAsByteArrayAsync();
            //PmapLogger<FileBroker>.InfoFormat("InsertNewDocument was able to extract the contents of the file. Content-Length: {0}.", buffer.Length);

            if (fileName == "" || fileName == null)
            {
                var contentDisposition = content.Headers.ContentDisposition;
                fileName = content.Headers.ContentDisposition.FileName.Trim('\"');
            }
            //PmapLogger<FileBroker>.InfoFormat("InsertNewDocument was able to extract the file name. FileName: {0}.", fileName);
            return await UploadBinary(buffer, userId, Uid, fileName, destinationFolder, destination, IsPublic, source);
        }

        private async Task<Common.Entities.FileUpload.File> UploadBinary(byte[] buffer, int userId, Guid? Uid = null,
            string fileName = "", string destinationFolder = "", int destination = 0, bool? IsPublic = false, string source = null)
        {
            var document = new Common.Entities.FileUpload.File
            {
                Uid = (Uid != null) ? Uid.GetValueOrDefault() : Guid.NewGuid(),
                Name = fileName,
                CreatedBy = userId,
                CreatedDate = DateTime.UtcNow,
                LocationId = LocationID,
                ConsumerId = ConsumerContext.ConsumerId,
                Stream = buffer,
                Type = System.IO.Path.GetExtension(fileName),
                Size = buffer.Length,
                IsPublic = IsPublic,
                Source = source,
                Destination = destination
            };

            var newFile = Foundation.File.CreateFile(document);
            var Attachment = new Common.Entities.FileUpload.File();
            try
            {
                if (destination == 0 || destination == 2)
                {
                    FileManager fileManager = new FileManager(AppConfig, ConsumerContext, RequestHeaders, UserAuthorization);

                    fileManager.UploadFile(newFile, ConsumerContext, out string filePath, destinationFolder: destinationFolder);

                    var uniqueFileName = newFile.FileName;
                    document.Location = (destinationFolder != null && destinationFolder.Length > 0) ? System.IO.Path.Combine(destinationFolder.Replace("/", "\\"), uniqueFileName) : System.IO.Path.Combine(uniqueFileName);
                    Attachment = document;
                }
            }
            catch (ApiException ae)
            {
                PmapLogger<FileBroker>.InfoFormat("Unable to upload- FileName: {0} , Reason : {1}.", fileName, ae);
            }

            // Incase of Mongo adds it to GridFS
            if (destination == 0 || destination == 1)
            {
                Attachment = await DataAccess.AddFileAsync(document);
            }
            return Attachment;
        }

        ///<summary>
        ///Represents asynchronous operation to save File data against Form or any parent record
        ///</summary>
        /// <param name="Uploads">List of uploaded files</param>
        /// <param name="ParentUid">unique ID of the parent. Eg: FormUID incase of Form</param>
        /// <param name="Source">Category the parent. Eg: Form/ActionItem/ControlNAme etc</param>
        /// <param name="destination">determines the file saving destination . Possible values 1- mongo, Default is 0</param>
        /// <param name="ControlUid">[Optional]unique ID of the control (Needed incase parent has multiple attachment control)</param>
        /// <returns>currently saved data</returns>
        public async Task<List<Common.Entities.FileUpload.File>> InsertUploadsAsync(IEnumerable<Common.Entities.FileUpload.File> Uploads, int destination = 0, Guid? ParentUid = null, string Source = "", Guid? ControlUid = null)
        {
            var UploadList = new List<Common.Entities.FileUpload.File>();

            if (Uploads != null && Uploads.ToList().Count() > 0)
            {
                MassageUploadFiles(Uploads.ToList(), UserId.Value, ConsumerContext.ConsumerId, ParentUid, Source, ControlUid);

                // To save data in Mongo
                UploadList = (await DataAccess.AddFilesAsync(Uploads.ToList())).ToList();
            }
            return UploadList;
        }

        ///<summary>
        ///Represents asynchronous operation to save File data against Form or any parent record
        ///</summary>
        /// <param name="Uploads">List of uploaded files</param>
        /// <param name="ParentUid">unique ID of the parent. Eg: FormUID incase of Form</param>
        /// <param name="Source">Category the parent. Eg: Form/ActionItem/ControlNAme etc</param>
        /// <param name="destination">determines the file saving destination . Possible values 1- mongo, Default is 0</param>
        /// <param name="ControlUid">[Optional]unique ID of the control (Needed incase parent has multiple attachment control)</param>
        /// <returns>currently saved data</returns>
        //EE public async Task<List<Common.Entities.FileUpload.File>> SaveUploadsAsync(IEnumerable<Common.Entities.FileUpload.File> Uploads, Guid? ParentUid, string Source = "", int destination = 0, Guid? ControlUid = null)
        public async Task<List<Common.Entities.FileUpload.File>> SaveUploadsAsync(IEnumerable<Common.Entities.FileUpload.File> Uploads, Guid? ParentUid, string Source = "", Guid? ControlUid = null)
        {
            var UploadList = new List<Common.Entities.FileUpload.File>();

            // Assign the parent/controluid if provided to save files against the respective control
            MassageUploadFiles(Uploads.ToList(), UserId.Value, ConsumerContext.ConsumerId, ParentUid, Source, ControlUid);

            if (Uploads != null && Uploads.Count() > 0)
            {
                // To save data in Mongo
                UploadList = (await DataAccess.EditFilesAsync(Uploads.ToList())).ToList();
            }
            return UploadList;
        }

        private void MassageUploadFiles(List<Common.Entities.FileUpload.File> files, int userId, int consumerId, Guid? ParentUid = null, string Source = "", Guid? ControlUid = null)
        {
            foreach (var file in files)
            {
                file.CreatedBy = userId;
                file.CreatedDate = DateTime.UtcNow;
                file.ConsumerId = consumerId;
                file.Stream = null;
                file.ParentUid = (file.ParentUid != null) ? file.ParentUid : ParentUid;
                file.Source = file.Source ?? Source;
                file.ControlUid = (file.ControlUid != null) ? file.ControlUid : ControlUid;
                file.FileId = file.FileId ?? ObjectId.GenerateNewId().ToString();// Added temporarily for PMP-61128 to support mobile feature. to be removed 
            }
        }

        /// <summary>
        /// Represents the asynchronous operation that returns multiple Uploads.
        /// </summary>
        /// <param name="ParentUid">Represents the parent unique identifier.</param>
        /// <returns>Returns a list of Uploads based on the supplied Consumer identifier.</returns>
        public async Task<IEnumerable<Common.Entities.FileUpload.File>> GetUploadsAsync(Guid ParentUid)
        {
            // Validations
            if (ParentUid == Guid.Empty) throw new ApiException(CommonConstants.InvalidInputParameter + $"'{nameof(ParentUid)}': {ParentUid}");
            IEnumerable<Common.Entities.FileUpload.File> files = await DataAccess.GetAttachmentListAsync(ConsumerContext.ConsumerId, ParentUid);

            return files;
        }

        /// <summary>
        /// Represents the asynchronous operation that deletes multiple Uploads.
        /// </summary>
        /// <param name="userId">Represents the unique identifier of the user.</param>
        /// <param name="uploadUids">Represents the unique identifier for the records to be deleted.</param>
        public async Task<bool> DeleteUploadsAsync(List<string> uploadUids, int userId)
        {
            // Validations
            if (userId <= 0) throw new ApiException(CommonConstants.InvalidInputParameter + $"'{nameof(userId)}': {userId}");
            if (uploadUids == null) throw new ApiException(CommonConstants.InvalidInputParameter + $"'{nameof(uploadUids)}': {uploadUids}");
            DeleteActionResultType result = await DataAccess.DeleteUploadsAsync(ConsumerContext.ConsumerId, uploadUids, userId);
            bool deleted = result == DeleteActionResultType.Deleted ? true : false;

            return deleted;
        }

        /// <summary>
        /// Represents the asynchronous operation that deletes Uploads for a particular record/parent.
        /// </summary>
        /// <param name="userId">Represents the unique identifier of the user.</param>
        /// <param name="ParentUid">Represents the unique identifier of the for which records to be deleted.</param>
        public async Task<bool> DeleteUploadAsync(Guid ParentUid, int userId)
        {
            // Validations
            if (userId <= 0) throw new ApiException(CommonConstants.InvalidInputParameter + $"'{nameof(userId)}': {userId}");
            if (ParentUid == Guid.Empty) throw new ApiException(CommonConstants.InvalidInputParameter + $"'{nameof(ParentUid)}': {ParentUid}");

            DeleteActionResultType result = await DataAccess.DeleteUploadAsync(ConsumerContext.ConsumerId, ParentUid, userId);
            bool deleted = result == DeleteActionResultType.Deleted ? true : false;

            return deleted;
        }

        /// <summary>
        /// Represents the cancel operation for a upload.
        /// </summary>
        /// <param name="userId">Represents the unique identifier of the user.</param>
        /// <param name="uploadUid">Represents the  identifier of the File file.</param>
        public async Task<bool> CancelUploadAsync(Guid uploadUid, int userId)
        {
            // Validations
            var DeleteList = new List<string>
            {
                uploadUid.ToString()
            };
            var result = await DataAccess.DeleteUploadsAsync(ConsumerContext.ConsumerId, DeleteList, userId);
            bool deleted = result == DeleteActionResultType.Deleted ? true : false;

            return deleted;
        }

        /// <summary>
        /// Represents the asynchronous operation that returns multiple Uploads.
        /// </summary>
        /// <param name="FileUid">Represents the  unique identifier of File.</param>
        /// <returns>Returns a file.</returns>
        public async Task<Common.Entities.FileUpload.File> GetFileAsync(Guid FileUid)
        {
            // Validations
            if (FileUid == Guid.Empty) throw new ApiException(CommonConstants.InvalidInputParameter + $"'{nameof(FileUid)}': {FileUid}");

            Common.Entities.FileUpload.File file = await DataAccess.GetAttachmentAsync(ConsumerContext.ConsumerId, FileUid);
            if (file != null)
            {
                if (file.Destination == 0 || file.Destination == 1)
                {
                    file.Stream = DataAccess.DownloadFile(fileId: file.FileId);
                }
                else
                {
                    file.Stream = GetFileContent(file.Location, file.Uid);
                }
            }
            else
                // Incase when file not found in Upload Collection
                file = await GetFileFromOtherSources(FileUid);
            return file;
        }


        /// <summary>
        /// Represents the asynchronous operation that updates annotation of Files.
        /// </summary>
        /// <param name="uploads">Represents the list of files.</param>
        /// <returns>Returns a list of Uploads based on the supplied Consumer identifier.</returns>
        public async void UpdateFileAnnotation(List<Common.Entities.FileUpload.File> uploads)
        {
            await DataAccess.UpdateFileAnnotationAsync(uploads);
        }


        /// <summary>
        /// Represents the asynchronous operation that returns multiple Uploads.
        /// </summary>
        /// <param name="uploads">Represents the list of files returned from sql.</param>
        /// <returns>Returns a list of Uploads based on the supplied Consumer identifier.</returns>
        public async Task<IEnumerable<Common.Entities.FileUpload.File>> SyncUploads(List<Common.Entities.FileUpload.File> uploads, Guid ParentUid, Guid? ControlUid = null)
        {
            // 1.Fetch all files present in Mongo for the ControlUID
            var result = await GetUploadsAsync(ParentUid);
            var mongoUploads = result.ToList();
            if (ControlUid != null)
            {
                mongoUploads = mongoUploads.Where(m => m.ControlUid.Equals(ControlUid)).ToList();
            }

            // 2.Read file stream of newly added files from File System.
            var freshUploads = new List<Common.Entities.FileUpload.File>();

            if (uploads.Count > 0)
            {
                foreach (var upload in uploads)
                {
                    if ((mongoUploads.ToList().Count < 1) || !(mongoUploads.Any(u => u.Uid.Equals(upload.Uid))))
                    {
                        var stream = GetFileContent(upload.Location, upload.Uid);
                        upload.Stream = stream;
                        upload.CreatedBy = UserId.GetValueOrDefault();
                        upload.CreatedDate = DateTime.UtcNow;
                        upload.LocationId = LocationID;
                        upload.ConsumerId = ConsumerContext.ConsumerId;
                        upload.Type = System.IO.Path.GetExtension(upload.Name);
                        upload.Size = stream?.Length;
                        upload.ParentUid = ParentUid;
                        upload.ControlUid = ControlUid;

                        var uploadedFile = upload.Stream != null ? await DataAccess.AddFileAsync(upload) : upload;
                        freshUploads.Add(uploadedFile);
                    }
                }
            }
            await InsertUploadsAsync(freshUploads);
            // 3.Incase no files are there in mongo, need to create control for newly added files in FileSystem
            // 4.Delete Files from mongo not present in files System
            if (mongoUploads.Count > 0)
            {
                foreach (var upload in mongoUploads)
                {
                    if ((uploads.ToList().Count < 1) || !(uploads.Any(u => u.Uid.Equals(upload.Uid))))
                    {
                        var deleteList = new List<string>
                        {
                            upload.Uid.ToString()
                        };
                        await DeleteUploadsAsync(deleteList, UserId.GetValueOrDefault());
                    }
                }
            }
            return uploads;
        }

        /// <summary>
        /// Represents the asynchronous operation that returns file from SQl.
        /// </summary>
        /// <param name="FileUid">Represents the  unique identifier of File.</param>
        /// <returns>Returns a file.</returns>
        public async Task<Common.Entities.FileUpload.File> GetFileFromOtherSources(Guid FileUid)
        {
            // Validations
            if (FileUid == Guid.Empty) throw new ApiException(CommonConstants.InvalidInputParameter + $"'{nameof(FileUid)}': {FileUid}");
            var file = default(Common.Entities.FileUpload.File);
            var datumBroker = new DatumBroker(AppConfig, ConsumerContext, RequestHeaders, UserAuthorization);

            using (var response = await datumBroker.CallExternalAPIAsync(RestServiceHostUrl, UriConstant.DataSource.GetFile + FileUid, HttpMethod.Get))
            {
                var json = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    var responsedata = JsonConvert.DeserializeObject<Attachments>(json);
                    if (responsedata != null)
                    {
                        file = new Common.Entities.FileUpload.File
                        {
                            ConsumerId = this.ConsumerContext.ConsumerId,
                            Location = responsedata.FileLocation,
                            Name = responsedata.Name,
                            Size = Convert.ToInt32(responsedata.Size),
                            Type = responsedata.Type,
                            Uid = responsedata.Uid.GetValueOrDefault(),
                            // Stream = FileManager.GetFileContents(ConsumerContext, responsedata.FileLocation+"\\"+ responsedata.Name,isActivePathAppened:true)
                            Stream = GetFileContent(responsedata.FileLocation + "\\" + responsedata.Name, responsedata.Uid.GetValueOrDefault(), isActivePathAppened: true)
                        };
                    }
                }
                else
                {
                    return null;
                }
            }
            return file;
        }

        /// <summary>
        /// Represents the method that returns byte stream from Physical file path.
        /// </summary>
        public byte[] GetFileContent(string filePath, Guid? fileUid = null, bool isActivePathAppened = false)
        {
            FileManager fileManager = new FileManager(AppConfig, ConsumerContext, RequestHeaders, UserAuthorization);
            //EE var stream = default(byte[]);
            //stream = fileManager.GetFileContents(filePath, isActivePathAppened);
            byte[] stream = fileManager.GetFileContents(filePath, isActivePathAppened);

            return stream;
        }

        /// <summary>
        /// Crops the specified image.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private static byte[] Crop(Common.Entities.FileUpload.File fileObj, FileCrop cropConfig)
        {
            try
            {
                var StreamObj = new System.IO.MemoryStream(fileObj.Stream);
                using (Image OriginalImage = Image.FromStream(StreamObj))
                {
                    var width = Convert.ToInt32(cropConfig.Configurations["width"]);
                    var height = Convert.ToInt32(cropConfig.Configurations["height"]);
                    var x = Convert.ToInt32(cropConfig.Configurations["x"]);
                    var y = Convert.ToInt32(cropConfig.Configurations["y"]);

                    using (Bitmap bmp = new Bitmap(width, height))
                    {
                        //bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);

                        using (Graphics Graphic = Graphics.FromImage(bmp))
                        {
                            Graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                            Graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            Graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                            //Graphic.DrawImage(OriginalImage, new Rectangle(0, 0, width, height), x, -y, width, height,GraphicsUnit.Pixel);
                            Graphic.DrawImage(OriginalImage, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
                            System.IO.MemoryStream ms = new System.IO.MemoryStream();
                            bmp.Save(ms, OriginalImage.RawFormat);

                            return ms.GetBuffer();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// End point to crop uploaded Images 
        /// </summary> 
        public async Task<Common.Entities.FileUpload.File> Cropfile(FileCrop cropConfig)
        {
            try
            {
                var fileObj = await GetFileAsync(cropConfig.Uid);
                // Crop file
                var StreamObj = new System.IO.MemoryStream(fileObj.Stream);
                byte[] CropImage = Crop(fileObj, cropConfig);

                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(CropImage, 0, CropImage.Length))
                {
                    ms.Write(CropImage, 0, CropImage.Length);

                    using (Image CroppedImage = Image.FromStream(ms, true))
                    {
                        CroppedImage.Save(StreamObj, CroppedImage.RawFormat);
                    }
                }

                // replace file in GridFS
                if (cropConfig.Destination == 0 || cropConfig.Destination == 1)
                {
                    fileObj.Stream = StreamObj.ToArray();
                    fileObj = await DataAccess.AddFileAsync(fileObj);
                    await DataAccess.UpdateFileAsync(fileObj);
                }
                return fileObj;
            }

            catch (Exception ex)
            {
                var errorMsg = $"The product endpoint cropfile responded unsuccessfully ({ex.Message}).";
                throw new ApiException(errorMsg);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileUid"></param>
        /// <param name="height">in px</param>
        /// <param name="width">in px</param>
        /// <returns></returns>
        public async Task<string> GetImageSrc(Guid fileUid, int height, int width)
        {
            var imageHtml = "";
            try
            {
                var returnFile = await GetFileAsync(fileUid);
                if (returnFile.Stream != null)
                {
                    imageHtml = "<img style='display: inline-block; width: auto; max-width: 100%; height: auto; max-height: 40px; padding-right: 2px; transition: all 0.5s ease 0s;' src ='data: image/jpeg; base64, " + Convert.ToBase64String(returnFile.Stream) + "'/>";
                }
            }
            catch (Exception)
            {
                imageHtml = "";
            }
            return imageHtml;
        }
    }
}