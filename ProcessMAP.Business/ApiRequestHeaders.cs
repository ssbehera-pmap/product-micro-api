﻿using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Common.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Business
{
    /// <summary>
    /// Represents the headers and their values as specified for the current API request.
    /// </summary>
    public class ApiRequestHeaders
    {
        //private int consumerId = 0;

        /// <summary>
        /// Gets a value from the request headers representing the consumer id to use for this API call.
        /// </summary>
        [Required]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Gets a value from the request headers representing the location id to use for this API call.
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        /// Gets a value from the request headers representing the user id to use for this API call.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets a value from the request headers representing the level id to use for this API call.
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Gets a value from the request headers representing the module id to use for this API call.
        /// </summary>
        public int ModuleId { get; set; }

        /// <summary>
        /// Gets a value from the request headers representing the time-zone id to use for this API call.
        /// </summary>
        public string TimeZone { get; set; }

        /// <summary>
        /// Gets a value from the request headers representing the language code to use for this API call.
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets a value from the request headers representing the application type of the consumer.
        /// E.g.: iOS, Android, Web, etc.
        /// </summary>
        public ApplicationType ApplicationType { get; set; }

        /// <summary>
        /// Represents the unique identifier of the source.
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// Represents the unique identifier of the type of the source.
        /// </summary>
        public EntityType SourceTypeId { get; set; }

        /// <summary>
        /// Represents the unique identifier of the type of the source of the parent.
        /// </summary>
        public string ParentSourceId { get; set; }

        /// <summary>
        /// Represents the name with extension of the file to be uploaded.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Authorization Code
        /// </summary>
        public string AuthorizationCode { get; set; }
    }
}