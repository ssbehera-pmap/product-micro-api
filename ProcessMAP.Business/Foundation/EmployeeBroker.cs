﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class EmployeeBroker : DAPBroker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public EmployeeBroker(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        public async Task<List<ProcessMAP.Common.Entities.Foundation.Employee>> GetByLocation(int locationId)
        {
            var list = new List<Common.Entities.Foundation.Employee>();
            var endpoint = UriConstant.Foundation.GetEmployeesByLocation + Convert.ToString(locationId);
            try
            {
                using (var response = await CallExternalAPIAsync(RestServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Common.Entities.Foundation.Employee>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<EmployeeBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }

            return list;
        }
    }
}