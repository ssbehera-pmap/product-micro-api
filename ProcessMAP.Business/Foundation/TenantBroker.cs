﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Foundation
{
    /// <summary>
    /// Used to broker the Tenant operations.
    /// </summary>
    public class TenantBroker : DAPBroker
    {
        /// <summary>
        /// 
        /// </summary>
        public TenantBroker(AppConfig appConfig)
            : base(appConfig)
        {
        }

        /// <summary>
        /// Gets a Tenant object based on its Id.
        /// </summary>
        /// <param name="tenantId">Represents the id of the tenant in question.</param>
        /// <returns>A Tenant object.</returns>
        public Tenant GetTenantById(int tenantId)
        {
            if (tenantId <= 0)
            {
                PmapLogger<TenantBroker>.InfoFormat("GetTenantByAccessCode cannot continue with invalid input parameter 'accessCode={0}'.", tenantId);
                throw new ArgumentException(CommonConstants.InvalidInputParameter, "tenantId");
            }

            Tenant tenant = null;

            try
            {
                var endpoint = UriConstant.Tenant.GetTalent + Convert.ToString(tenantId);
                using (var task = Task.Run(async () => await CallInternalAPIAsync(ProductInternalServiceHostUrl, endpoint, HttpMethod.Get, isGZip: false)))
                {
                    task.Wait();
                    var response = task.Result;
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = Task.Run(async () => await response.Content.ReadAsStringAsync()).Result;
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                tenant = Newtonsoft.Json.JsonConvert.DeserializeObject<Tenant>(json);
                            }
                        }
                    }
                }
                //if (tenant != null)
                //{
                //    if (tenant.CryptographyInfo == null)
                //    {
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tenant;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseAddress"></param>
        /// <param name="APIUrl"></param>
        /// <param name="method"></param>
        /// <param name="data"></param>
        /// <param name="isGZip"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> CallInternalAPIAsync(string BaseAddress, string APIUrl, HttpMethod method, object data = null, bool isGZip = false)
        {
            var response = default(HttpResponseMessage);
            var endpointUri = new Uri($"{BaseAddress}/{APIUrl}");
            HttpClient client = null;
            if (isGZip)
            {
                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
                };
                client = new HttpClient(handler);
            }
            else { client = new HttpClient(); }

            // To get the post object in JSOn format
            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            if (method == HttpMethod.Get)
            {
                response = await client.GetAsync(endpointUri);
            }
            else if (method == HttpMethod.Post)
            {
                response = await client.PostAsJsonAsync(endpointUri, data);
            }
            else if (method == HttpMethod.Put)
            {
                response = await client.PutAsJsonAsync(endpointUri, data);
            }
            else
            {
                throw new NotImplementedException($"The http method '{method.Method}' has not been implemented for this '{nameof(CallExternalAPIAsync)}' functionality.");
            }

            return response;
        }
    }
}