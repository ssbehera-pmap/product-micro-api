﻿using ProcessMAP.Common.Entities.Base;

namespace ProcessMAP.Business.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class File : UEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public byte[] RawStream { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public static File CreateFile(Common.Entities.FileUpload.File document)
        {
            int index = document.Name.LastIndexOf('.');
            var fileNamewithoutExt = index == -1 ? document.Name : document.Name.Substring(0, index);
            var extension = index == -1 ? "" : document.Name.Substring(index + 1);
            var fileName = string.Format("{0}_{1}.{2}",
                fileNamewithoutExt,
                //document.Uid,
                System.Guid.NewGuid(),
                extension
                );

            return new File()
            {
                CreatedBy = document.CreatedBy,
                //CreatedByName = document.CreatedByName,
                CreatedDate = document.CreatedDate,
                FileName = fileName,
                RawStream = document.Stream,
                Uid = document.Uid
            };
        }
    }
}
