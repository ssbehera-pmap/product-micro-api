﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Common.Entities.ScreenLayout;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.DataAccess.ScreenLayout;
using ProcessMAP.DataAccess.ScreenLayout.DatabaseProviders.MongoNoSQL;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class UserInfoBroker : DAPBroker
    {
        const string DefaultPatternConsumer = "consumerdefault";
        const string DefaultPatternSystem = "systemdefault";

        private readonly Lazy<IScreenLayoutDataAccess> dataAccess;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public UserInfoBroker(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        {
            dataAccess = new Lazy<IScreenLayoutDataAccess>(() => new ScreenLayoutDataAccess(AppConfig));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserInfo> GetUser(int userId)
        {
            UserInfo userInfo = null;
            var endpoint = UriConstant.Foundation.GetUser.Replace("{userId}", Convert.ToString(userId));

            if (TwoKeyCacheStore<int, int, UserInfo>.Instance.ContainsKey(ConsumerContext.ConsumerId, userId))
            {
                userInfo = TwoKeyCacheStore<int, int, UserInfo>.Instance.Get(ConsumerContext.ConsumerId, userId);
                if (userInfo != null)
                    return userInfo;
            }
            try
            {
                using (var response = await CallExternalAPIAsync(ProductInternalServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfo>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (userInfo != null)
            {
                TwoKeyCacheStore<int, int, UserInfo>.Instance.Set(ConsumerContext.ConsumerId, userId, userInfo);
                TwoKeyCacheStore<int, string, UserInfo>.Instance.Set(ConsumerContext.ConsumerId, userInfo.Username, userInfo);
            }

            return userInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserInfo>> GetUsersInformation(IEnumerable<int> userIds)
        {
            var usersInfo = new List<UserInfo>();
            var endpoint = UriConstant.Foundation.GetUsersInformation;
            foreach (var userId in userIds)
            {
                if (TwoKeyCacheStore<int, int, UserInfo>.Instance.ContainsKey(ConsumerContext.ConsumerId, userId))
                {
                    var userInfo = TwoKeyCacheStore<int, int, UserInfo>.Instance.Get(ConsumerContext.ConsumerId, userId);
                    usersInfo.Add(userInfo);
                    continue;
                }
            }

            var uncachedUserIds = userIds.TakeWhile(id => !usersInfo.Any(u => u.UserId == id));
            var uncachedUsersInfo = default(IEnumerable<UserInfo>);

            try
            {

                using (var response = await CallExternalAPIAsync(ProductInternalServiceHostUrl, endpoint, HttpMethod.Post, userIds, isGZip: false))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                uncachedUsersInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserInfo>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (uncachedUsersInfo != null)
            {
                usersInfo.AddRange(uncachedUsersInfo);

                foreach (var userInfo in uncachedUsersInfo)
                {
                    TwoKeyCacheStore<int, int, UserInfo>.Instance.Set(ConsumerContext.ConsumerId, userInfo.UserId, userInfo);
                }
            }

            return usersInfo;
        }

        /// <summary>
        /// Impacts DAP
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public async Task<List<UserInfo>> GetAllUsersByModule(int moduleId)
        {
            List<UserInfo> userLoggedInfo = null;
            var endpoint = UriConstant.Foundation.GetAllUsersByModule.Replace("{moduleId}", Convert.ToString(moduleId));
            try
            {
                using (var response = await CallExternalAPIAsync(ProductInternalServiceHostUrl, endpoint, HttpMethod.Get, isGZip: false))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                userLoggedInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserInfo>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<EmployeeBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }
            return userLoggedInfo;
        }

        /// <summary>
        /// Responsible for extracting the data out of the DataRow.
        /// </summary>
        /// <param name="roleNotificationInfos">Represents the dictionary to be filled.</param>
        /// <param name="row">Represents the Data Set Row.</param>
        /// <param name="key">Represents the key of the dictionary.</param>
        /// <param name="value">Represents the value of the dictionary.</param>
        private void ExtractDataRow(Dictionary<string, string> roleNotificationInfos, object row, string key, string value)
        {
            if (!roleNotificationInfos.ContainsKey(((DataRow)row)[key].ToString()))
                roleNotificationInfos.Add(((DataRow)row)[key].ToString(), ((DataRow)row)[value].ToString());
        }


        /// <summary>
        /// Returns user's homescreen layout (APB-128)
        /// </summary>
        /// <returns>User's homescreen layout.</returns>
        public async Task<IEnumerable<LayoutSection>> GetScreenLayout(string DefaultPattern = null)
        {
            int userId = UserId.Value;
            int consumerId = ConsumerId;
            bool bConsumerDefault = false;
            if (!String.IsNullOrEmpty(DefaultPattern))
            {
                if (DefaultPattern.Equals(DefaultPatternConsumer))
                {
                    userId = 0;
                    bConsumerDefault = true;
                }
                else if (DefaultPattern.Equals(DefaultPatternSystem))
                {
                    userId = 0;
                    consumerId = 0;
                }
                else throw new ApiException("AddScreenLayout: unknown default target pattern.");
            }

            Layout layout;
            if (userId != 0)
            {
                layout = await dataAccess.Value.GetScreenLayoutAsync(userId, consumerId);
                if (layout != null) return layout.Sections;
            }

            if (consumerId != 0)
            {
                // if there is no layout for specified user, try to get a default layout for specified CustomerId (UserId=0)
                layout = await dataAccess.Value.GetScreenLayoutAsync(0, consumerId);
                if (layout != null) return layout.Sections;
                if (bConsumerDefault) return null; // return if explicit request for consumer default and specified layout was not found
            }

            // if there is no default layout for specified CustomerId, try to get a system default layout (UserId=0, CustomerId=0)
            layout = await dataAccess.Value.GetScreenLayoutAsync(0, 0);
            if (layout != null) return layout.Sections;

            return null; // return null if there is no any layouts
        }


        /// <summary>
        /// Adds user's homescreen layout (APB-128)
        /// </summary>
        /// <returns>User's homescreen layout added.</returns>
        public async Task<IEnumerable<LayoutSection>> AddScreenLayout(IEnumerable<LayoutSection> sections, string DefaultPattern = null)
        {
            if (UserId == null) throw new ApiException("AddScreenLayout: UserId is not set.");
            int userId = UserId.Value;
            if (userId == 0) throw new ApiException("AddScreenLayout: UserId cannot be 0 for the user layout.");

            if (ConsumerId == 0) throw new ApiException("AddScreenLayout: ConsumerId cannot be 0 for the user layout.");
            int consumerId = ConsumerId;

            if (!String.IsNullOrEmpty(DefaultPattern))
            {
                if (DefaultPattern.Equals(DefaultPatternConsumer)) userId = 0;
                else if (DefaultPattern.Equals(DefaultPatternSystem))
                {
                    userId = 0;
                    consumerId = 0;
                }
                else throw new ApiException("AddScreenLayout: unknown default target pattern.");
            }

            var tl = await dataAccess.Value.GetScreenLayoutAsync(userId, consumerId);
            if (tl != null) throw new ApiException($"AddScreenLayout: ScreenLayout for userId='{userId}' and consumerId='{consumerId}' already exist.");

            Layout layout = new Layout
            {
                UserId = userId,
                ConsumerId = consumerId,
                Sections = sections,
                CreatedBy = userId,
                CreatedDate = DateTime.UtcNow,
            };
            await dataAccess.Value.AddScreenLayoutAsync(layout);
            return sections;
        }


        /// <summary>
        /// Updates user's homescreen layout (APB-128)
        /// </summary>
        /// <returns>User's homescreen layout updated.</returns>
        public async Task<IEnumerable<LayoutSection>> UpdateScreenLayout(IEnumerable<LayoutSection> sections, string DefaultPattern = null)
        {
            if (UserId == null) throw new ApiException("UpdateScreenLayout: UserId is not set.");
            var userId = UserId.Value;
            if (userId == 0) throw new ApiException("UpdateScreenLayout: UserId cannot be 0 for the user layout.");
            if (ConsumerId == 0) throw new ApiException("UpdateScreenLayout: ConsumerId cannot be 0 for the user layout.");
            int consumerId = ConsumerId;

            if (!String.IsNullOrEmpty(DefaultPattern))
            {
                if (DefaultPattern.Equals(DefaultPatternConsumer)) userId = 0;
                else if (DefaultPattern.Equals(DefaultPatternSystem))
                {
                    userId = 0;
                    consumerId = 0;
                }
                else throw new ApiException("AddScreenLayout: unknown default target pattern.");
            }

            var layout = await dataAccess.Value.GetScreenLayoutAsync(userId, consumerId);
            if (layout == null) throw new ApiException($"UpdateScreenLayout: ScreenLayout for userId='{userId}' and consumerId='{consumerId}' is not exist.");

            layout.UpdatedBy = userId;
            layout.UpdatedDate = DateTime.UtcNow;
            layout.Sections = sections;

            await dataAccess.Value.UpdateScreenLayoutAsync(layout);
            return sections;
        }

        /// <summary>
        /// Deletes user's homescreen layout (APB-128)
        /// </summary>
        /// <returns>User's homescreen layout updated.</returns>
        public async Task<bool> DeleteScreenLayout(string DefaultPattern = null)
        {
            if (UserId == null) throw new ApiException("UpdateScreenLayout: UserId is not set.");
            var userId = UserId.Value;
            if (userId == 0) throw new ApiException("UpdateScreenLayout: UserId cannot be 0 for the user layout.");
            if (ConsumerId == 0) throw new ApiException("UpdateScreenLayout: ConsumerId cannot be 0 for the user layout.");
            int consumerId = ConsumerId;

            if (!String.IsNullOrEmpty(DefaultPattern))
            {
                if (DefaultPattern.Equals(DefaultPatternConsumer)) userId = 0;
                else if (DefaultPattern.Equals(DefaultPatternSystem))
                {
                    userId = 0;
                    consumerId = 0;
                }
                else throw new ApiException("AddScreenLayout: unknown default target pattern.");
            }

            var layout = await dataAccess.Value.GetScreenLayoutAsync(userId, consumerId);
            if (layout == null) throw new ApiException($"UpdateScreenLayout: ScreenLayout for userId='{userId}' and consumerId='{consumerId}' is not exist.");

            layout.DeletedBy = userId;
            layout.DeletedDate = DateTime.UtcNow;

            return await dataAccess.Value.DeleteScreenLayoutAsync(layout);
        }
    }

    /// <summary>
    /// Responsible for managing the result of the sent message.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Responsible for the sent status of the result.
        /// </summary>
        public bool Sent { get; set; }
    }
}