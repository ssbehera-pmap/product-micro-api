﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomSettingsBroker : DAPBroker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public CustomSettingsBroker(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetCustomSettings(int moduleId)
        {
            Dictionary<string, string> setttings = null;

            var endpoint = UriConstant.Foundation.CustomSettings + "/" + Convert.ToString(moduleId);
            try
            {
                using (var response = Task.Run(async () => await CallExternalAPIAsync(ProductInternalServiceHostUrl, endpoint, HttpMethod.Get, null, isGZip: false)).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = Task.Run(async () => await response.Content.ReadAsStringAsync()).Result;
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                setttings = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return setttings;
        }
    }
}
