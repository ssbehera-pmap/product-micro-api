﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Enumerations;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using ProcessMAP.Security.Authorization;
using System;
using System.IO;

namespace ProcessMAP.Business.Foundation
{
    internal class FileManager : DAPBroker
    {
        /// <summary>
        /// 
        /// </summary>
        public const string COMPANY_ID_SETTING = "strCompanyID";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public FileManager(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="consumerContext"></param>
        /// <param name="filePath"></param>
        /// <param name="enforceTempFolderWrite"></param>
        /// <param name="destinationFolder"></param>
        /// <returns></returns>
        public bool UploadFile(File file, ConsumerContext consumerContext, out string filePath, bool enforceTempFolderWrite = false, string destinationFolder = "")
        {
            // Initial assignments
            filePath = null;

            var ACTIVE_PATH_SETTING = AppConfig.FileStoreSettings.ACTIVE_PATH;
           // var ARCHIVE_PATH_SETTING = AppConfig.FileStoreSettings.ARCHIVE_PATH;
            var TEMP_FOLDER_SETTING = AppConfig.FileStoreSettings.TEMP_FOLDER;

            // Get the tenant custom settings.
            var customSettingsBroker = new CustomSettingsBroker(AppConfig, consumerContext, RequestHeaders, UserAuthorization);
            var customSettings = customSettingsBroker.GetCustomSettings((int)Module.None);

            if (customSettings == null || customSettings.Count == 0)
            {
                var errorMsg = string.Format("No custom settings were found for Consumer: {0}. Without the consumer's custom settings we cannot figure out where to save the file.", ConsumerContext.ConsumerId);
                throw new ApiException(errorMsg);
            }
            if (string.IsNullOrWhiteSpace(ACTIVE_PATH_SETTING) || string.IsNullOrWhiteSpace(TEMP_FOLDER_SETTING))
            {
                var errorMsg = string.Format("No settings for File Store were found. Check the appsettings.json file.");
                throw new ApiException(errorMsg);
            }

            // Save the file to the temp folder
            var tempDir = TEMP_FOLDER_SETTING;
            if (!Directory.Exists(tempDir)) Directory.CreateDirectory(tempDir);
            var tempFullPath = Path.Combine(tempDir, file.FileName);


            PmapLogger<FileManager>.InfoFormat("FileManager: Saving file {0}.", tempFullPath);
            try
            {
                System.IO.File.WriteAllBytes(tempFullPath, file.RawStream);
            }
            catch (IOException ioex)
            {
                var errorMsg = string.Format("Could not save the file to the temp location '{0}'. Message: {1}.", tempFullPath, ioex.Message);
                PmapLogger<FileManager>.Error(errorMsg, ioex);

                if (enforceTempFolderWrite)
                    throw new ApiException(errorMsg, ioex);
            }

            // Copy the file to the active folder
            try
            {
                var activeDir = (destinationFolder != null && destinationFolder.Length > 0)
                    ? Path.Combine(ACTIVE_PATH_SETTING, customSettings[COMPANY_ID_SETTING], destinationFolder) // destinationFolder.Replace("/", "\\"))
                    : Path.Combine(ACTIVE_PATH_SETTING, customSettings[COMPANY_ID_SETTING]);
                if (!Directory.Exists(activeDir)) Directory.CreateDirectory(activeDir);
                
                filePath = Path.Combine(activeDir, file.FileName);

                // Check added Incase file exists,append new guid
                if (System.IO.File.Exists(filePath))
                {
                    int index = file.FileName.LastIndexOf('.');
                    var fileNamewithoutExt = index == -1 ? file.FileName : file.FileName.Substring(0, index);
                    var extension = index == -1 ? "" : file.FileName.Substring(index + 1);

                    var GeneratedfileName = string.Format("{0}_{1}.{2}",
                        fileNamewithoutExt,
                        Guid.NewGuid(),
                        extension
                        );

                    // filePath = filePath.Replace(file.Uid.ToString(), Guid.NewGuid().ToString());
                    filePath = Path.Combine(activeDir, GeneratedfileName);
                }

                PmapLogger<FileManager>.InfoFormat("FileManager: Saving file {0}.", filePath);
                if (System.IO.File.Exists(tempFullPath))
                {
                    System.IO.File.Copy(tempFullPath, filePath, true);
                }
                else
                {
                    System.IO.File.WriteAllBytes(filePath, file.RawStream);
                }
            }
            catch (IOException ioex)
            {
                var errorMsg = string.Format("Could not write the file to the archive location '{0}'. Message: {1}.", filePath, ioex.Message);
                PmapLogger<FileManager>.Error(errorMsg, ioex);
                throw new ApiException(errorMsg, ioex);
            }

            // Delete the file from the temp folder
            try
            {
                System.IO.File.Delete(tempFullPath);
            }
            catch (IOException ioex)
            {
                var errorMsg = string.Format("Could not delete the file from the temp location. Message: {0}.", ioex.Message);
                PmapLogger<FileManager>.Warning(errorMsg, ioex);
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public byte[] GetFileContents(string filePath, bool isActivePathAppened = false)
        {
            if (string.IsNullOrWhiteSpace(filePath))
                throw new ArgumentNullException("filePath");

            var ACTIVE_PATH_SETTING = AppConfig.FileStoreSettings.ACTIVE_PATH;

            // Get the tenant custom settings.
            var customSettingsBroker = new CustomSettingsBroker(AppConfig, ConsumerContext, RequestHeaders, UserAuthorization);
            var customSettings = customSettingsBroker.GetCustomSettings((int)Module.None);

            if (customSettings == null || customSettings.Count == 0)
            {
                var errorMsg = string.Format("No custom settings were found for Consumer: {0}. Without the consumer's custom settings we cannot figure out where to save the file.", ConsumerContext.ConsumerId);
                throw new ApiException(errorMsg);
            }
            if (string.IsNullOrWhiteSpace(ACTIVE_PATH_SETTING))
            {
                var errorMsg = string.Format("No settings for File Store were found. Check the appsettings.json file.");
                throw new ApiException(errorMsg);
            }

             
            var activeDir = Path.Combine(ACTIVE_PATH_SETTING, customSettings[COMPANY_ID_SETTING]);

            filePath = (!isActivePathAppened) ? Path.Combine(activeDir, filePath) : filePath;
            //filePath = filePath.Replace("\\", "/");
            if (!System.IO.File.Exists(filePath))
                return null;
            PmapLogger<FileManager>.InfoFormat("FileManager: Reading file {0}.", filePath);
            return System.IO.File.ReadAllBytes(filePath);
        }
    }
}
