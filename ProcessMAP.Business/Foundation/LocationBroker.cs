﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Foundation
{
    /// <summary>
    /// Serves as a broker for managing Location/Level operations.
    /// </summary>
    public class LocationBroker : DAPBroker 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public LocationBroker(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        { }

        /// <summary>
        /// Responsible for to get the Levels, Locations based on User Scopeed Levels and Locations and by Root Level Id.
        /// </summary>
        /// <param name="appTypeId">Represents the unique application type id. Ex: Web, Mobile, Mobile Plus, Mobile Pro.</param>
        /// <param name="langCode">Represents the 2 or 4 character language code. Ex: en or en-US.</param>
        /// <param name="userId">Represents the unique identifier for the user.</param>
        /// <returns>Returns a list of Levels and Locations.</returns>
        //EE public async Task<List<Navigator>> GetLevelLocationsByRootLevelId(int appTypeId, string langCode, int userId, int rootLevelId)
        public async Task<List<Navigator>> GetLevelLocationsByRootLevelId(int rootLevelId)
        {
            var list = new List<Navigator>();
            var endpoint = UriConstant.Foundation.GetLevelLocationsByRootLevelId.Replace("{rootLevelId}", Convert.ToString(rootLevelId));
            try
            {
                using (var response = await CallExternalAPIAsync(RestServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Navigator>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<EmployeeBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }

            return list;
        }

        /// <summary>
        /// Responsible for to get the Levels, Locations based on User Scopeed Levels and Locations and by Root Level Id.
        /// </summary>
        /// <param name="appTypeId">Represents the unique application type id. Ex: Web, Mobile, Mobile Plus, Mobile Pro.</param>
        /// <param name="langCode">Represents the 2 or 4 character language code. Ex: en or en-US.</param>
        /// <param name="userId">Represents the unique identifier for the user.</param>
        /// <param name="rootLevelId">Represents the root levelid</param>
        /// <param name="LocationId">Represents the selected locationid.</param>
        /// <param name="LevelId">Represents the selected levelid.</param>
        /// <returns>Returns a list of Levels and Locations.</returns>
        //EE public async Task<List<Navigator>> GetLevelLocationsNestedByRootLevelId(int appTypeId, string langCode, int userId, int rootLevelId, int LocationId, int LevelId)
        public async Task<List<Navigator>> GetLevelLocationsNestedByRootLevelId(int rootLevelId)
        {
            var list = new List<Navigator>();
            var endpoint = UriConstant.Foundation.GetLevelLocationsNestedByRootLevelId.Replace("{rootLevelId}", Convert.ToString(rootLevelId));
            try
            {
                using (var response = await CallExternalAPIAsync(RestServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Navigator>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<EmployeeBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }
            return list;
        }

        /// <summary>
        /// Returns a locations based on the supplied location id.
        /// </summary>
        /// <param name="locationId">Represents the ID of the Location.</param>
        /// <returns>A LocationInfo object.</returns>
        public async Task<LocationInfo> GetLocation(int locationId)
        {
            LocationInfo location = null;
            var endpoint = UriConstant.Foundation.GetLocation.Replace("{locationId}", Convert.ToString(locationId));
            try
            {
                using (var response = await CallExternalAPIAsync(RestServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                location = Newtonsoft.Json.JsonConvert.DeserializeObject<LocationInfo>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<EmployeeBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }
            return location;
        }

        /// <summary>
        /// Responsible for to get the top Levels
        /// </summary>
        /// <param name="appTypeId">Represents the unique application type id. Ex: Web, Mobile, Mobile Plus, Mobile Pro.</param>
        /// <param name="langCode">Represents the 2 or 4 character language code. Ex: en or en-US.</param>
        /// <param name="userId">Represents the unique identifier for the user.</param>
        /// <returns>Returns a list of Top Levels.</returns>
        //EE public async Task<List<LocationInfo>> GetTopLevels(int appTypeId, string langCode, int userId)
        public async Task<List<LocationInfo>> GetTopLevels()
        {
            List<LocationInfo> location = null;
            var endpoint = UriConstant.Foundation.GetTopLevels;
            try
            {
                using (var response = await CallExternalAPIAsync(RestServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                location = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LocationInfo>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<EmployeeBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }
            return location;
        }
    }
}