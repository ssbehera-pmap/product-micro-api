﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authorization;
using System;
using System.Reflection;

namespace ProcessMAP.Business
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class PmapBusiness
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="userId"></param>
        /// <param name="locationId"></param>
        /// <param name="authorizationCode"></param>
        /// <param name="levelId"></param>
        /// <param name="languageCode"></param>
        public PmapBusiness(AppConfig appConfig, int? userId = null, int locationId = 0,
            string authorizationCode = null, int levelId = 0, string languageCode = "en")
        {
            AppConfig = appConfig;
            ConnectionString = appConfig.ConnectionSettings.MongoDbConnectionString;
            RestServiceHostUrl = appConfig.ConnectionSettings.RestServiceHostUrl;
            DAPServiceHostUrl = appConfig.ConnectionSettings.DAPServiceHostUrl;
            ProductInternalServiceHostUrl = appConfig.ConnectionSettings.ProductInternalServiceHostUrl;
            UserId = userId;
            LocationId = locationId;
            AuthorizationCode = authorizationCode;
            LevelId = levelId;
            LanguageCode = languageCode;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="userId"></param>
        /// <param name="locationId"></param>
        /// <param name="userAuthz"></param>
        /// <param name="authorizationCode"></param>
        ///  <param name="levelId"></param>
        ///  <param name="languageCode"></param>
        public PmapBusiness(AppConfig appConfig, ConsumerContext context, int? userId = null,
            int locationId = 0, UserAuthorization userAuthz = null, string authorizationCode = null,
            int levelId = 0, string languageCode = "en")
            : this(appConfig, userId, locationId, authorizationCode, levelId, languageCode)
        {
            ConsumerContext = context;
            UserAuthorization = userAuthz;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public PmapBusiness(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : this(appConfig, requestHeaders.UserId, requestHeaders.LocationId, requestHeaders.AuthorizationCode)
        {
            ConsumerContext = context;
            RequestHeaders = requestHeaders;
            UserAuthorization = userAuthz;
        }


        /// <summary>
        /// Gets the connection string for the customer based on the request's URL.
        /// </summary>
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Gets the consumer id for this request.
        /// </summary>
        public int ConsumerId
        {
            get { return ConsumerContext != null ? ConsumerContext.ConsumerId : 0; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected ConsumerContext ConsumerContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected ApiRequestHeaders RequestHeaders { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected int? UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected int LocationId { get; set; }

        /// <summary>
        /// LangCode
        /// </summary>
        protected string LanguageCode { get; set; }

        /// <summary>
        /// The UserAuthorization object.
        /// </summary>
        protected UserAuthorization UserAuthorization { get; set; }

        /// <summary>
        /// AuthorizationCode
        /// </summary>
        protected string AuthorizationCode { get; set; }

        /// <summary>
        /// LevelId
        /// </summary>
        protected int LevelId { get; set; }

        /// <summary>
        /// RestServiceHostUrl
        /// </summary>
        private string rshu;
        protected string RestServiceHostUrl
        {
            get
            {
                if (String.IsNullOrEmpty(rshu))
                    throw new Exception(CommonConstants.RestServiceHostUrlIsNotSet);
                return rshu;
            }
            set
            { rshu = value; }
        }

        /// <summary>
        /// ProductInternalServiceHostUrl
        /// </summary>
        private string pishu;
        protected string ProductInternalServiceHostUrl
        {
            get
            {
                if (String.IsNullOrEmpty(pishu))
                    throw new Exception(CommonConstants.RestServiceHostUrlIsNotSet);
                return pishu;
            }
            set
            { pishu = value; }
        }

        /// <summary>
        /// DAPServiceHostUrl
        /// </summary>
        private string dshu;
        protected string DAPServiceHostUrl
        {
            get
            {
                if (String.IsNullOrEmpty(dshu))
                    throw new Exception("DAPServiceHostUrl is not set.");
                return dshu;
            }
            set
            { dshu = value; }
        }

        protected AppConfig AppConfig { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="Base"></param>
        /// <returns></returns>
        public T Construct<F, T>(F Base) where T : F, new()
        {
            // create derived instance
            T derived = new T();
            // get all base class properties
            PropertyInfo[] properties = typeof(F).GetProperties();
            foreach (PropertyInfo bp in properties)
            {
                // get derived matching property
                PropertyInfo dp = typeof(T).GetProperty(bp.Name, bp.PropertyType);

                // this property must not be index property
                if (
                    (dp != null)
                    && (dp.GetSetMethod() != null)
                    && (bp.GetIndexParameters().Length == 0)
                    && (dp.GetIndexParameters().Length == 0)
                )
                    dp.SetValue(derived, dp.GetValue(Base, null), null);
            }
            return derived;
        }
    }
}
