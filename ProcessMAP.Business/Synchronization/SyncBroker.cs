﻿using Newtonsoft.Json;
using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Entities.DAP;
using ProcessMAP.Common.Entities.Synchronization;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.DataAccess.DAP;
using ProcessMAP.DataAccess.DAP.DatabaseProviders.MongoNoSQL;
using ProcessMAP.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Synchronization
{
    /// <summary>
    /// 
    /// </summary>
    public class SyncBroker : PmapBusiness
    {
        #region Private Members
        private delegate T AddEntity<T>(T entity);
        private delegate void UpdateEntity<T>(T entity);
        private delegate T UpdateEntityResponse<T>(T entity);
        private delegate void DeleteEntity(int id, int userId);
        private delegate void DeleteEntityByGuid(Guid uid, int userId, Guid? FormUid, int LocationId);
        private delegate T AddChildEntity<T>(int id, int locationId, T entity);
        private delegate void DeleteChildEntity(int id, int userId, int locationId);

        //EE private SynchronizationBatchDataAccess syncBatchDataAccess;
        private readonly SynchronizationBatchDataAccess syncBatchDataAccess;
        //EE private JsonSerializerSettings serializerSettings;
        private readonly JsonSerializerSettings serializerSettings;
        private DatumBroker formDataBroker;
        private SyncBatchStatus syncBatchStatus;
        private SyncBatchResponse syncBatchResponse;
        #endregion

        #region Constructor
        public SyncBroker(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
          : base(appConfig, context, requestHeaders, userAuthz)
        {
            ConsumerContext = context;
            UserId = requestHeaders.UserId;
            LocationId = requestHeaders.LocationId;
            LevelId = requestHeaders.LevelId;
            AuthorizationCode = requestHeaders.AuthorizationCode;

            serializerSettings = new JsonSerializerSettings()
            {
                MissingMemberHandling = MissingMemberHandling.Ignore, // APB-169 (to ignore wrong IsSynced element in AI)
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ObjectCreationHandling = ObjectCreationHandling.Replace
            };

            syncBatchDataAccess = new DataAccess.DAP.DatabaseProviders.MongoNoSQL.SynchronizationBatchDataAccess(AppConfig);
        }
        #endregion

        #region Private Properties
        private DatumBroker FormDataBroker
        {
            get
            {
                if (formDataBroker == null)
                {
                    formDataBroker = new DatumBroker(AppConfig, ConsumerContext, RequestHeaders, UserAuthorization);
                }

                return formDataBroker;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="batchId"></param>
        /// <returns></returns>
        public async Task<SyncBatchStatus> GetBatchStatus(Guid batchId)
        {
            if (batchId == Guid.Empty)
            {
                throw new ArgumentException(CommonConstants.InvalidInputParameter, CommonConstants.BatchId);
            }

            return await syncBatchDataAccess.GetBatchStatusAsync(batchId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="batchId"></param>
        /// <returns></returns>
        public async Task<SyncBatchResponse> GetBatchResponse(Guid batchId)
        {
            if (batchId == Guid.Empty)
            {
                throw new ArgumentException(CommonConstants.InvalidInputParameter, CommonConstants.BatchId);
            }

            return await syncBatchDataAccess.GetBatchResponseAsync(batchId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerId"></param>
        /// <param name="batchId"></param>
        public async Task DeleteBatch(int consumerId, Guid batchId)
        {
            if (batchId == Guid.Empty)
            {
                throw new ArgumentException(CommonConstants.InvalidInputParameter, CommonConstants.BatchId);
            }
            var result = DeleteActionResultType.Deleted;
            result = await syncBatchDataAccess.DeleteBatchAsync(consumerId, batchId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="syncBatchRequest"></param>
        /// <returns></returns>
        public async Task<SyncBatchStatus> SynchronizeBatch(SyncBatchRequest syncBatchRequest)
        {
            // Assign the batch id value to all entries.
            syncBatchRequest.AssignBatchIdToEntries();

            // Insert Batch and Batch Entries into database.
            await InsertSyncBatchEntry(syncBatchRequest);

            // Send for processing on a separate thread.
            await Task.Factory.StartNew(() => StartSynchronizationProcess(syncBatchRequest));

            syncBatchStatus.Status = SyncStatus.Received;
            syncBatchStatus.ConsumerId = syncBatchRequest.ConsumerId;

            return syncBatchStatus;
        }

        public async Task<SyncBatchStatus> NewSynchronizeBatch(SyncBatchRequest syncBatchRequest)
        {
            // Parse FormData with embedded AIs
            List<SyncBatchEntryRequest> newBatchEntriesList = new List<SyncBatchEntryRequest>();
            IEnumerable<ActionItems> storedActionItems = null;

            foreach (SyncBatchEntryRequest entry in syncBatchRequest.Batch)
            {
                if (entry.Action == SyncAction.Delete) continue; // we will not process Delete action. It already works and deletes all depended AIs
                if (entry.EntityType != SyncEntity.FormData) continue; // we will process FormData entities only

                Data entity = JsonConvert.DeserializeObject<Data>(entry.Entity.ToString(), serializerSettings);

                if (entry.Action == SyncAction.Update || entry.Action == SyncAction.Delete)
                { // get list of stored AIs from the database
                    FormDataRequest fdr = BuildDataRequest();
                    fdr.FormUid = entity.FormUid;
                    fdr.DocumentUid = entity.Uid;
                    storedActionItems = await FormDataBroker.GetDocumentActionItemsAsync(fdr);
                }
                else 
                    if (entry.Action == SyncAction.Add)
                        storedActionItems = new List<ActionItems>(); // for newly added FormData there is no AIs in database
                    else continue; // skip unknown action

                // get list of ActionItems from the request's document
                IEnumerable<FormDocumentValue> reqAIControlValues = entity.Values.Where(c => c.ValueDataType == DataType.MultipleActionItem);
                if (!reqAIControlValues.Any()) continue; // skip if the document doesn't have any MultipleActionItems
                var reqAIControlValue = reqAIControlValues.FirstOrDefault();
                string reqAIstr = JsonConvert.SerializeObject(reqAIControlValue.Value);
                var reqActionItems = JsonConvert.DeserializeObject<IEnumerable<ActionItems>>(reqAIstr, serializerSettings);

                List<ActionItems> docActionItems = new List<ActionItems>();
                if (reqActionItems != null)
                {
                    foreach (var reqActionItem in reqActionItems)
                    {
                        // generate new batch entry for every AI from Doc of request
                        reqActionItem.ParentUid = entry.EntityId; //TODO: DM: //check if it is unnecessary (client sets it)
                        reqActionItem.ControlUid = reqAIControlValue.ControlUid; //TODO: DM: //check if it is unnecessary (client sets it)
                        SyncBatchEntryRequest newAIEntry = new SyncBatchEntryRequest()
                        {
                            EntryId = Guid.NewGuid(),
                            EntityId = entry.EntityId,
                            EntityType = SyncEntity.ActionItem,
                            Entity = JsonConvert.SerializeObject(reqActionItem),
                            LocationId = entry.LocationId
                        };

                        if (entry.Action == SyncAction.Add)
                            newAIEntry.Action = SyncAction.Add; // add new AI for new Doc
                        else
                        {
                            if (entry.Action == SyncAction.Update) // for existed Doc:
                                if (storedActionItems.Any(i => i.Uid == reqActionItem.Uid))
                                    newAIEntry.Action = SyncAction.Update; // if AI was stored in the database, update it
                                else
                                    newAIEntry.Action = SyncAction.Add; // if AI was not found in the database, add it
                        }
                        docActionItems.Add(reqActionItem);
                        newBatchEntriesList.Add(newAIEntry);
                    } //end of foreach (var reqActionItem in reqActionItems)
                } //end of if (reqActionItems != null)

                // now check the AIs to delete in updated Doc
                if (entry.Action == SyncAction.Update)
                {
                    foreach (ActionItems ai in storedActionItems)
                    {
                        if (docActionItems.Any(i => i.Uid == ai.Uid)) continue;  //skip if the doc have stored AI
                        // generate new batch entry for stored AI that not included in doc
                        SyncDeleteObject newAIDelObj = new SyncDeleteObject() { Uid = ai.Uid };
                        SyncBatchEntryRequest newAIEntry = new SyncBatchEntryRequest()
                        {
                            Action = SyncAction.Delete,
                            EntryId = Guid.NewGuid(),
                            EntityId = entry.EntityId,
                            EntityType = SyncEntity.ActionItem,
                            Entity = JsonConvert.SerializeObject(newAIDelObj)
                        };
                        newBatchEntriesList.Add(newAIEntry);
                    }
                }
            }
            syncBatchRequest.Batch.AddRange(newBatchEntriesList);
            // End of parse FormData with embedded AIs

            // Assign the batch id value to all entries.
            syncBatchRequest.AssignBatchIdToEntries();

            // Insert Batch and Batch Entries into database.
            await InsertSyncBatchEntry(syncBatchRequest);

            // Send for processing on a separate thread.
            await Task.Factory.StartNew(() => StartSynchronizationProcess(syncBatchRequest));

            syncBatchStatus.Status = SyncStatus.Received;
            syncBatchStatus.ConsumerId = syncBatchRequest.ConsumerId;

            return syncBatchStatus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="syncBatchRequest"></param>
        /// <returns></returns>
        public async Task<SyncBatchResponse> InsertSyncBatchEntry(SyncBatchRequest syncBatchRequest)
        {
            // Add Sync Batch Status with Received 
            syncBatchStatus = new SyncBatchStatus
            {
                BatchId = syncBatchRequest.BatchId,
                Status = SyncStatus.Received,
                ConsumerId = syncBatchRequest.ConsumerId
            };
            await syncBatchDataAccess.AddBatchStatusAsync(syncBatchStatus);

            // Add Sync Batch Entry
            syncBatchResponse = new SyncBatchResponse
            {
                BatchId = syncBatchRequest.BatchId,
                Status = SyncStatus.Received,
                ConsumerId = syncBatchRequest.ConsumerId,
                Batch = new List<SyncBatchEntryResponse>()
            };
            foreach (var entry in syncBatchRequest.Batch)
            {
                SyncBatchEntryResponse syncBatchEntryResponse = new SyncBatchEntryResponse
                {
                    EntityId = entry.EntityId,
                    EntryId = entry.EntryId
                };
                syncBatchResponse.Batch.Add(syncBatchEntryResponse);
            }
            return await syncBatchDataAccess.AddBatchEntryAsync(syncBatchResponse);
        }
        #endregion

        #region Private Methods
        private async void StartSynchronizationProcess(SyncBatchRequest syncBatchRequest)
        {
            PmapLogger<SyncBroker>.InfoFormat("Initializing synchronization process with a request containing {0} entries.", syncBatchRequest.Batch.Count);

            // Update Batch status to InProgress
            syncBatchStatus.Status = SyncStatus.InProgress;
            await syncBatchDataAccess.UpdateBatchStatusAsync(syncBatchStatus);

            PmapLogger<SyncBroker>.InfoFormat("Updated batch status to '{0}' for batch '{1}'.", SyncStatus.InProgress, syncBatchRequest.BatchId);

            // Temp variables
            var responseCode = HttpStatusCode.OK;
            var errorMessage = string.Empty;
            var entryResponse = new SyncBatchEntryResponse();
            syncBatchResponse.Batch = new List<SyncBatchEntryResponse>();
            syncBatchResponse.Status = SyncStatus.InProgress;

            // Sync each entity in the batch entries
            foreach (var entry in syncBatchRequest.Batch)
            {
                try
                {
                    switch (entry.EntityType)
                    {
                        case SyncEntity.FormData:
                            entryResponse = SynchronizeEntity<Data>(entry, FormDataBroker.AddData, FormDataBroker.UpdateData, deleteEntityByGuidDelegate: FormDataBroker.DeleteData);
                            PmapLogger<SyncBroker>.InfoFormat("DAP Form Document with id:'{0}' was synchronized.", entry.EntryId);
                            break;
                        case SyncEntity.ActionItem:
                            entryResponse = SynchronizeEntity<ActionItems>(entry, FormDataBroker.SyncMobileActionItem, updateEntityResponseDelegate: FormDataBroker.SyncMobileActionItemUpdate, deleteEntityByGuidDelegate: FormDataBroker.SyncDeleteActionItem);
                            PmapLogger<SyncBroker>.InfoFormat("Sync ActionItem with id:'{0}' was synchronized.", entry.EntryId);
                            break;
                        default:
                            errorMessage = string.Format("Unknown entity type '{0}'.", entry.EntityType);
                            responseCode = HttpStatusCode.NotImplemented;
                            PmapLogger<SyncBroker>.Info(errorMessage);
                            break;
                    }
                }
                catch (InvalidOperationException iopEx)
                {
                    PmapLogger<SyncBroker>.Error($"An {nameof(InvalidOperationException)} " + CommonConstants.WasThrownWhileTryingToSynchronizeAnEntity, iopEx);
                    errorMessage = iopEx.Message;
                    responseCode = HttpStatusCode.Gone;
                }
                catch (SecurityException secEx)
                {
                    PmapLogger<SyncBroker>.Error($"A {nameof(SecurityException)} " + CommonConstants.WasThrownWhileTryingToSynchronizeAnEntity, secEx);
                    errorMessage = secEx.Message;
                    responseCode = HttpStatusCode.Forbidden;
                }
                catch (JsonReaderException jsonReadEx)
                {
                    PmapLogger<SyncBroker>.Error($"A {nameof(JsonReaderException)} " + CommonConstants.WasThrownWhileTryingToSynchronizeAnEntity, jsonReadEx);
                    errorMessage = jsonReadEx.Message;
                    responseCode = HttpStatusCode.BadRequest;
                }
                catch (JsonSerializationException jsonEx)
                {
                    PmapLogger<SyncBroker>.Error($"A {nameof(JsonSerializationException)} " + CommonConstants.WasThrownWhileTryingToSynchronizeAnEntity, jsonEx);
                    errorMessage = jsonEx.Message;
                    responseCode = HttpStatusCode.BadRequest;
                }
                catch (Exception ex)
                {
                    PmapLogger<SyncBroker>.Error("An Exception  " + CommonConstants.WasThrownWhileTryingToSynchronizeAnEntity, ex);
                    errorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                    responseCode = HttpStatusCode.InternalServerError;
                }
                finally
                {
                    if (string.IsNullOrWhiteSpace(errorMessage) == false)
                    {
                        entryResponse = new SyncBatchEntryResponse(entry)
                        {
                            ResponseCode = (int)responseCode,
                            Message = errorMessage,
                            UpdatedJsonEntity = entry.JsonEntity
                        };
                    }

                    // Add entry response to response batch.
                    syncBatchResponse.Batch.Add(entryResponse);
                }
            }
            // Update the batch entry
            syncBatchResponse.Status = SyncStatus.Completed;
            await syncBatchDataAccess.UpdateBatchEntryAsync(this.syncBatchResponse);

            // Reset temp variables
            responseCode = HttpStatusCode.OK;
            errorMessage = string.Empty;

            // Update Batch Status with Completed
            syncBatchStatus.Status = SyncStatus.Completed;
            await syncBatchDataAccess.UpdateBatchStatusAsync(syncBatchStatus);
            PmapLogger<SyncBroker>.InfoFormat("Updated batch status to '{0}' for batch '{1}'.", SyncStatus.Completed, syncBatchRequest.BatchId);
        }

        private SyncBatchEntryResponse SynchronizeEntity<T>(
            SyncBatchEntryRequest entry,
            AddEntity<T> addEntityDelegate,
            UpdateEntity<T> updateEntityDelegate = null,
            DeleteEntity deleteEntityDelegate = null,
            AddChildEntity<T> addChildDelegate = null,
            DeleteChildEntity deleteChildDelegate = null,
            DeleteEntityByGuid deleteEntityByGuidDelegate = null,
            UpdateEntityResponse<T> updateEntityResponseDelegate = null)
        {
            var response = new SyncBatchEntryResponse(entry);

            if (entry.Action == SyncAction.None)
            {
                response.ResponseCode = (int)HttpStatusCode.NotImplemented;
                response.Message = string.Format("Unknown synchronization action '{0}'.", entry.Action);
                return response;
            }

            //EE
            //dynamic entity = null;
            dynamic entity;

            switch (entry.Action)
            {
                case SyncAction.Delete:
                    entity = JsonConvert.DeserializeObject<SyncDeleteObject>(entry.Entity.ToString(), serializerSettings);
                    break;
                default:
                    entity = JsonConvert.DeserializeObject<T>(entry.Entity.ToString(), serializerSettings);
                    break;
            }
            if (entry.LocationId != 0 && !(entry.Action == SyncAction.Delete))
                entity.LocationId = entry.LocationId;

            if (entity == null)
            {
                var errorMessage = string.Format("The entity to be synchronized could not be deserialized into a '{0}' object.", typeof(T));
                PmapLogger<SyncBroker>.Error(errorMessage);
                throw new JsonSerializationException(errorMessage);
            }

            switch (entry.Action)
            {
                case SyncAction.Add:
                    T ent = addEntityDelegate(entity);
                    response.UpdatedJsonEntity = CreateSyncedEntityInfo(ent);
                    break;
                case SyncAction.Update:
                    if (updateEntityDelegate != null)
                        updateEntityDelegate(entity);
                    else
                    {
                        if (updateEntityResponseDelegate != null)
                        {
                            T updatedEnt = updateEntityResponseDelegate(entity);
                            response.UpdatedJsonEntity = CreateSyncedEntityInfo(updatedEnt);
                        }
                    }
                    break;
                case SyncAction.Delete:
                    if (deleteEntityDelegate != null)
                    {
                        deleteEntityDelegate(entity.Id, UserId.Value);
                    }
                    else if (deleteEntityByGuidDelegate != null)
                    {
                        deleteEntityByGuidDelegate(entity.Uid, UserId.Value, entity.FormUid, entry.LocationId);
                    }
                    break;
                case SyncAction.AddChild:
                    ent = addChildDelegate((int)entity.Id, entity.LocationId, entity);
                    break;
                case SyncAction.DeleteChild:
                    deleteChildDelegate((int)entity.Id, entity.CreatedBy, entity.LocationId);
                    break;
            }
            response.ResponseCode = (int)HttpStatusCode.OK;
            return response;
        }

        private dynamic CreateSyncedEntityInfo<T>(T obj)
        {
            if (obj is Data)
            {
                var entity = obj as Data;
                return entity;
            }

            else if (obj is ActionItems)
            {
                var entity = obj as ActionItems;

                return entity;
            }
            return null;
        }

        private FormDataRequest BuildDataRequest()
        {
            var dataRequest = new FormDataRequest()
            {
                ConsumerId = ConsumerContext.ConsumerId,
                UserId = RequestHeaders.UserId,
                LocationIds = new int[] { RequestHeaders.LocationId },
                LevelId = RequestHeaders.LevelId,
                ApplicationType = RequestHeaders.ApplicationType
            };
            return dataRequest;
        }
        #endregion
    }
}