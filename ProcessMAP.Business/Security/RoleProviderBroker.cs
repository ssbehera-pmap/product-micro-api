﻿using ProcessMAP.API.AppSettings;
using ProcessMAP.Business.DAP;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authorization;
using ProcessMAP.Security.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Security
{
    /// <summary>
    /// Serves as a broker for managing Location/Level operations.
    /// </summary>
    public class RoleProviderBroker : DAPBroker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="userAuthz"></param>
        public RoleProviderBroker(AppConfig appConfig, ConsumerContext context, ApiRequestHeaders requestHeaders, UserAuthorization userAuthz = null)
            : base(appConfig, context, requestHeaders, userAuthz)
        { }

        /// <summary>
        /// Returns a list of all Roles in the system.
        /// </summary>
        /// <param></param>
        /// <returns>A list of Roles.</returns>
        public async Task<List<Role>> GetAllRoles()
        {
            var roleList = new List<Role>();
            var endpoint = UriConstant.Foundation.GetAllRoles;
            try
            {
                using (var response = await CallExternalAPIAsync(RestServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                roleList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Role>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<RoleProviderBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }

            return roleList;
        }

        /// <summary>
        /// Returns a list of all Roles in the system.
        /// </summary>
        /// <param></param>
        /// <returns>A list of Roles.</returns>
        //EE public async Task<List<Role>> GetUserRolesByModule(int userId, int moduleId)
        public async Task<List<Role>> GetUserRolesByModule(int moduleId)
        {
            var roleList = new List<Role>();
            var endpoint = UriConstant.Foundation.GetRolesByModule.ToString().Replace("{moduleId}", Convert.ToString(moduleId));
            try
            {
                using (var response = await CallExternalAPIAsync(RestServiceHostUrl, endpoint, HttpMethod.Get, isGZip: true))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content != null)
                        {
                            var json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                roleList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Role>>(json);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PmapLogger<RoleProviderBroker>.Error(ex.Message, ex);
                throw new ApiException(CommonConstants.ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest, ex);
            }

            return roleList;
        }
    }
}