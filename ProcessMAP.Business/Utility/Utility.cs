﻿using Newtonsoft.Json;
using ProcessMAP.Common.Utilities;
using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProcessMAP.Business.Utility
{
    /// <summary>
    /// Utility
    /// </summary>
    public class Utility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseAddress"></param>
        /// <param name="APIUrl"></param>
        /// <param name="method"></param>
        /// <param name="headers"></param>
        /// <param name="data"></param>
        /// <param name="isGZip"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> CallExternalAPIAsync(string BaseAddress, string APIUrl, HttpMethod method, NameValueCollection headers, object data = null, bool isGZip = false)
        {
            var response = default(HttpResponseMessage);
            var endpointUri = new Uri($"{BaseAddress}/{APIUrl}");
            HttpClient client = null;
            if (isGZip)
            {
                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
                };
                client = new HttpClient(handler);
            }
            else { client = new HttpClient(); }

            // To get the post object in JSOn format
            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            if (headers != null)
            {
                foreach (string key in headers)
                {
                    client.DefaultRequestHeaders.Add(key, headers[key]);
                }
            }

            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.LocationId, LocationId.ToString());
            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.UserId, UserId.ToString());
            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.ConsumerId, ConsumerId.ToString());
            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.Authorization, AuthorizationCode);
            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.ModuleId, Convert.ToString((int)Module.DAP));
            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.LevelId, LevelId.ToString());
            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.AcceptLanguage, LanguageCode);
            //client.DefaultRequestHeaders.Add(PmapRequestHeaders.ModuleId, Convert.ToString((int)Module.DAP));

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(CommonConstants.ApplicationJson));

            if (method == HttpMethod.Get)
            {
                response = await client.GetAsync(endpointUri);
            }
            else if (method == HttpMethod.Post)
            {
                response = await client.PostAsJsonAsyncNew(endpointUri, data);
            }
            else if (method == HttpMethod.Put)
            {
                response = await client.PutAsJsonAsyncNew(endpointUri, data);
            }
            else
            {
                throw new NotImplementedException($"The http method '{method.Method}' has not been implemented for this '{nameof(CallExternalAPIAsync)}' functionality.");
            }
            return response;
        }
    }

    #region HttpClientExtensions
    public static class HttpClientExtensions
    {
        public async static Task<HttpResponseMessage> PostAsJsonAsyncNew<T>(
            this HttpClient httpClient, Uri endpointUri, T data)
        {
            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var result = await httpClient.PostAsync(endpointUri, content);
            return result;
        }

        public static Task<HttpResponseMessage> PutAsJsonAsyncNew<T>(
            this HttpClient httpClient, Uri endpointUri, T data)
        {
            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            return httpClient.PutAsync(endpointUri, content);
        }

        public static async Task<T> ReadAsJsonAsyncNew<T>(this HttpContent content)
        {
            var dataAsString = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(dataAsString);
        }
    }
    #endregion

}
