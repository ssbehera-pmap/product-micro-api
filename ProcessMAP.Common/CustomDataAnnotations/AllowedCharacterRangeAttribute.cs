﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.CustomDataAnnotations
{
    /// <summary>
    /// It validates that the given string value is whithin the specified character count.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class AllowedCharacterRangeAttribute : ValidationAttribute
    {
        /// <summary>
        /// Creates an instance of the attribute.
        /// </summary>
        /// <param name="minCount"></param>
        /// <param name="maxCount"></param>
        public AllowedCharacterRangeAttribute(int minCount = 1, int maxCount = int.MaxValue)
            : base()
        {
            MinimumCount = minCount;
            MaximumCount = maxCount;
        }

        /// <summary>
        /// Minimum character count.
        /// </summary>
        public int MinimumCount { get; private set; }

        /// <summary>
        /// Maximum character count.
        /// </summary>
        public int MaximumCount { get; private set; }

        /// <summary>
        /// Validation execution.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            // If the value is null, then this validation doesn't apply.
            // Return true. If a required validation is needed, decorate
            // your property or field with the Required attribute.
            if (value == null) return true;

            var str = value as string;
            var isValid = !string.IsNullOrWhiteSpace(str);

            if (isValid)
            {
                isValid = str.Length >= MinimumCount && str.Length <= MaximumCount;
            }

            return isValid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override string FormatErrorMessage(string name)
        {
            return $"The '{name}' property is required and should contain between {MinimumCount} and {MaximumCount} characters.";
        }
    }
}