﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.CustomDataAnnotations
{
    /// <summary>
    /// 
    /// </summary>
    public class RequiredGuidAttribute : RequiredAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            //EE
            //var result = default(Guid);
            //var isValid = Guid.TryParse(value.ToString(), out result);
            var isValid = Guid.TryParse(value.ToString(), out Guid result);
            //EE
            //if (isValid) isValid = result != default(Guid);
            if (isValid) isValid = result != default;

            return isValid;
        }
    }
}