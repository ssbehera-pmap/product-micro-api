﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.CustomDataAnnotations
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class CollectionRangeAttribute : ValidationAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="minCount"></param>
        /// <param name="maxCount"></param>
        public CollectionRangeAttribute(int minCount = 1, int maxCount = int.MaxValue)
            : base()
        {
            Minimum = minCount;
            Maximum = maxCount;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Minimum { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public int Maximum { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            // If the value is null, then this validation doesn't apply.
            // Return true. If a required validation is needed, decorate
            // your property or field with the Required attribute.
            if (value == null) return true;

            var isValid = false;

            if (value is ICollection)
            {
                var count = (value as ICollection).Count;

                isValid = (count >= Minimum) && (count <= Maximum);
            }

            return isValid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override string FormatErrorMessage(string name)
        {
            return $"The '{name}' property is required and should contain between {Minimum} and {Maximum} elements.";
        }
    }
}