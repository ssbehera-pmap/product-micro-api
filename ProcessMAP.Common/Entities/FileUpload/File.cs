﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.FileUpload
{
    /// <summary>
    /// File File Entity
    /// </summary>
    [BsonIgnoreExtraElements]
    public class File : ProcessMAP.Common.Entities.DAP.DAPEntity
    {
        ///// <summary>
        ///// Gets or sets a unique identifier for the element in the tree.
        ///// </summary>
        //[BsonId]
        //[BsonRepresentation(BsonType.String)]
        //public Guid Uid { get; set; }

        /// <summary>
        /// Represents the GridFS Id the document.
        /// </summary>
        public string FileId { get; set; }

        /// <summary>
        /// Represents the full file name of the document.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the contents of the document ready to stream.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public byte[] Stream { get; set; }

        /// <summary>
        /// Represents the size of the file.
        /// </summary>
        public int? Size { get; set; }

        /// <summary>
        /// Represents the physical locationof the file,if available
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Represents the preview url of the file
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Represents the file extension.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Sets the File Category /parent. Eg: Form/ActionItem/ControlNAme etc
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Represents the Id of the parent ,the file is uploaded for.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? ParentUid { get; set; }

        /// <summary>
        /// Represents the unique ID of the control (Needed incase parent has multiple attachment control)
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? ControlUid { get; set; }

        /// <summary>
        /// Gets or sets the id of the tenant.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Sets or gets the location id for this element.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int? LocationId { get; set; }

        /// <summary>
        /// Represents the physical locationof the file,if available
        /// </summary>
        [System.ComponentModel.DefaultValue(2)]
        public int Destination { get; set; }

        /// <summary>
        /// Represents the acessibility of the file.
        /// </summary>
        public bool? IsPublic { get; set; }

        /// <summary>
        /// Represents the annotation of the file.
        /// </summary>
        public string Annotation { get; set; }
    }
}