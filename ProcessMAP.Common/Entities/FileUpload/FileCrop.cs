﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.FileUpload
{
    /// <summary>
    /// Serves as the Data Value entity.
    /// </summary>
    public class FileCrop
    {
        /// <summary>
        /// Represents the unique id of the file
        /// </summary>
        public Guid Uid { get; set; }
        
        /// <summary>
        /// Represents the crop configurations.Eg : {"x": "10","y":"20"}
        /// </summary>
    public IDictionary<string, string> Configurations { get; set; }

        /// <summary>
        /// determines the file destination.Possible values 1- mongo/2-FileSystem/0-Both.Default is 0
        /// </summary>
        public int? Destination { get; set; }
    }
}