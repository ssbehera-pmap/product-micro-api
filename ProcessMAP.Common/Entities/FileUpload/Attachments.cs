﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessMAP.Common.Entities.FileUpload
{
    /// <summary>
    /// grid Settings, this includes Column settings and View Settings
    /// </summary>
    public class Attachments
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public dynamic AttachmentSettings { get; set; }

        /// <summary>
        /// Gets or sets a unique identifier of the file.
        /// </summary>
        public Guid? Uid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int AttachmentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileLocation { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UploadedBy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime UploadedDate { get; set; }
    }
}
