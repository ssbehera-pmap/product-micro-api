﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class Navigator
    {
        /// <summary>
        /// Unique id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int parentId { get; set; }

        /// <summary>
        /// location/level name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// level/location
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// if the type is location, then it is represents locationid. if the type is level, it represents levelid
        /// </summary>
        public int typeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool selected { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool toggled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string hierarchy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Navigator> children { get; set; }
    }
}
