﻿using ProcessMAP.Common.Caching;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class LevelLocations : ICacheableEntity
    {
        /// <summary>
        /// The level id.
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// The collection of location ids in the current level.
        /// </summary>
        public IEnumerable<int> ChildLocationIds { get; set; }

        /// <summary>
        /// The datetime value of the last cache.
        /// </summary>
        public DateTime LastCached { get ; set; }
    }
}