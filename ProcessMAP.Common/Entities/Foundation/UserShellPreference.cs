﻿using System;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class UserShellPreference
    {
        /// <summary>
        /// 
        /// </summary>
        public UserShellPreference() { }

        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsShellDefault { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowShellRedirectionIcon { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastAccessedDate { get; set; }
    }
}
