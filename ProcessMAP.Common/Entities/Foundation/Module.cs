﻿namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents the Module.
    /// </summary>
    public class Module
    {
        /// <summary>
        /// Creates an instance of the Module class.
        /// </summary>
        public Module()
        {
            // Empty constructor needed for serialization
        }

        /// <summary>
        /// Represents the identifier of the Module.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Represents the Description of the Module.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Represents the abbreviated module name.
        /// </summary>
        public string AbbreviatedName { get; set; }
    }
}