﻿namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// This class represents an icon resource.
    /// </summary>
    public class Icon
    {
        /// <summary>
        /// Gets or sets the contents of the file.
        /// </summary>
        public byte[] Content { get; set; }

        /// <summary>
        /// Gets or sets a direct link to the file.
        /// </summary>
        public string Link { get; set; }
    }
}