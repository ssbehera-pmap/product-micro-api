﻿namespace ProcessMAP.Common.Entities.Foundation
{
    public class Employee
    {
        /// <summary>
        /// Gets or set a unique value representing the id of the Employee.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or set a value representing the Employee number.
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or set a value representing the Employee's first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or set a value representing  the Employee's last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or set a value representing the Employee's email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or set a value representing the type of Employee.
        /// </summary>
        public int EmployeeType { get; set; }

        /// <summary>
        /// [SUBJECT TO REVISION] Gets or set a value representing the employment status for the Employee.
        /// </summary>
        public string EmploymentStatus { get; set; }

        /// <summary>
        /// [SUBJECT TI REVISION] Gets or set a value representing the status of the Employee record in the database.
        /// </summary>
        public string Status { get; set; }
    }
}