﻿using Newtonsoft.Json;
using ProcessMAP.Common.Entities.Base;
using ProcessMAP.Common.Entities.DocumentManagement;
using ProcessMAP.Common.Entities.Synchronization;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents the class for managing attachments.
    /// </summary>
    public class Attachment : UEntity, ISynchronizable
    {
        /// <summary>
        /// 
        /// </summary>
        public EntityType EntityType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long EntityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long DocumentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid DocumentUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long Size { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Document Document { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SyncAction SyncAction { get; set; }

        /// <summary>
        /// Gets or sets the link to download the document.
        /// </summary>
        public string DownloadLink { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ContentType { get; set; }
    }
}