﻿using ProcessMAP.Common.Entities.Base;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents the Custom Setting entity.
    /// </summary>
    public class CustomSetting : Entity
    {
        /// <summary>
        /// Represents the Custom Setting Module unique identifier.
        /// </summary>
        public int ModuleId { get; set; }

        /// <summary>
        /// Represents the Custom Setting Key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Represents the Custom Setting Value.
        /// </summary>
        public string Value { get; set; }
    }
}