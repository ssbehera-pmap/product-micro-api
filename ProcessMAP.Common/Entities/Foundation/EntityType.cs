﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EntityType
    {
        /// <summary>
        /// Undefined sync entity.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The Observation entity type.
        /// </summary>
        BBSObservation = 1,

        /// <summary>
        /// The WellnessCheck entity type.
        /// </summary>
        WellnessCheck = 2,

        /// <summary>
        /// The Calendar Event entity type.
        /// </summary>
        CalendarEvent = 3,

        /// <summary>
        /// The Program - Finding entity type.
        /// </summary>
        AuditsFinding = 4,

        /// <summary>
        /// The Program Action Item entity type.
        /// </summary>
        AuditsActionItem = 5,

        /// <summary>
        /// The Document entity type.
        /// </summary>
        Document = 6,
        
        /// <summary>
        /// The Question Attachment entity type. decide the Attachment File locationPath
        /// </summary>
        QuestionAttachment = 7,
        
        /// <summary>
        /// The Questionnaire Finding Attachment entity type. decide the Attachment File locationPath.
        /// </summary>
        QuestionFinding = 8,
        
        /// <summary>
        /// The Program - Finding ActionItem Attachment entity type.decide the Attachment File locationPath
        /// </summary>
        FindingActionItem = 9,

        /// <summary>
        /// Represents the Question Finding Action Item Attachment
        /// </summary>
        QuestionFindingActionItem = 10,

        /// <summary>
        /// Represents the Question Action Item Attachment
        /// </summary>
        QuestionActionItem = 11
    }
}
