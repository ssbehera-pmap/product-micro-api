﻿using ProcessMAP.Common.Entities.Interfaces;
using ProcessMAP.Security.Authentication;
using ProcessMAP.Security.Cryptography;
using ProcessMAP.Security.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents an instance of a Consumer object.
    /// </summary>
    public class Consumer : ICacheableConsumer
    {
        #region Constructor
        /// <summary>
        /// Creates an instance of the Consumer class.
        /// </summary>
        public Consumer()
        {
            // Empty constructor needed for serialization.
        }
        #endregion

        #region Properties
        /// <summary>
        /// Represents the unique id that is associated with the Tenant.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Represents the name of the consumer.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents whether the consumer is active or not.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Represents the date the consumer was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Claim> ClaimSettings { get; set; }

        /// <summary>
        /// Gets or sets a collection of allowed endpoints.
        /// </summary>
        public List<string> AllowedEndpoints { get; set; }

        /// <summary>
        /// Gets or sets the list of authentication information for the consumer.
        /// </summary>
        public IEnumerable<IAuthenticationInfo> AuthenticationInfoList { get; set; }

        /// <summary>
        /// Gets or sets the database information for the consumer.
        /// </summary>
        public DatabaseInfo DatabaseInfo { get; set; }

        /// <summary>
        /// Gets or sets how long, in seconds, the consumer will remain cached.
        /// </summary>
        public int ApplicationCacheExpiration { get; set; }

        /// <summary>
        /// Gets or sets the last time the consumer was cached.
        /// </summary>
        public DateTime LastCached { get; set; }

        /// <summary>
        /// Gets or sets information about the consumer's encryption mechanisms.
        /// </summary>
        public ICryptographyInfo CryptographyInfo { get; set; }

        /// <summary>
        /// Represents the ApplicationId.
        /// </summary>
        public Guid ApplicationId { get; set; }

        /// <summary>
        /// Represents the Consumer Key.
        /// </summary>
        public Guid ConsumerKey { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get an authentication configuration that matches the specified domain.
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public IAuthenticationInfo MatchAuthenticationInfo(string domain)
        {
            if (AuthenticationInfoList == null || AuthenticationInfoList.Count() == 0)
                return null;

            return AuthenticationInfoList
                .Where(a => !string.IsNullOrWhiteSpace(a.DomainName) && domain.Equals(a.DomainName, StringComparison.CurrentCultureIgnoreCase))
                .FirstOrDefault();
        }
        #endregion

        #region NewtonSoft Serialization Methods
        /// <summary>
        /// This public method is needed by NewtonSoft to conditionally
        /// allow (or not) the serialization of the property.
        /// The naming convention is "ShouldSerialize[PropertyName]"
        /// In this case the property name is 'DatabaseInfo'.
        /// </summary>
        public bool ShouldSerializeDatabaseInfo()
        {
            /*  TODO:   Updated for the Tenant MicroService running on OWIN.
                // Register the ApplicationTypes that are allowed to serialize this property;
                var appTypes = new[] { ApplicationType.Integrations };

                // Check if it can be serialized.
                return appTypes.ContainsCurrentApplicationType();
            */

            return false;
        }
        #endregion
    }
}