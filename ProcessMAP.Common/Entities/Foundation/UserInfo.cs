﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Entities.Interfaces;
using ProcessMAP.Common.Utilities;
using ProcessMAP.Security.Authentication;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents an object containing the 
    /// </summary>
    public class UserInfo : IUserInfo, ICacheableEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public UserInfo() { }

        /// <summary>
        /// Gets or sets the user's id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the user's username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the user's first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the user's first last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets the user's full name.
        /// </summary>
        public string FullName
        {
            get { return FirstName + CommonConstants.Space + LastName; }
        }

        /// <summary>
        /// Gets or sets the user's email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the user's phone number.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the type of user.
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets the user's access level.
        /// </summary>
        public string AccessLevel { get; set; }

        /// <summary>
        /// Gets or sets the user's default location
        /// </summary>
        public LocationInfo DefaultLocation { get; set; }

        /// <summary>
        /// Indicates the type of authentication to be used for this user.
        /// This is used on hybrid configurations.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public AuthType AuthType { get; set; }

        /// <summary>
        /// When was the user last cached.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public DateTime LastCached { get; set; }
    }

    /// <summary>
    /// 
    /// Represents an object containing the 
    /// </summary>
    public class UserLoggedInfo
    {
        public string SystemAdminName { get; set; }
        public string SystemAdminTitle { get; set; }
        public string SystemAdminEmailID { get; set; }
        public string HelpLink { get; set; }
        public string ResourcesLink { get; set; }
        public string PswdExprNotfcdays { get; set; }
        public string SupportUrl { get; set; }
        public string SupportEmailId { get; set; }
        public string LastloginTime { get; set; }
        public string ExpireIndays { get; set; }
        public string LblChangePassword { get; set; }
        public string BtnLogout { get; set; }
        public string LblHelp { get; set; }
        public string LblEmailSupport { get; set; }
        public string LblSystemAdminDetails { get; set; }
        public string LblBrowserSetting { get; set; }
        public string LblUserGuide { get; set; }
        public string LblResources { get; set; }
        public string SpnStoreAndCorporateTicket { get; set; }
        public string SpnSubmitSparcTicket { get; set; }
        public string LanguagePreferenceUrl { get; set; }
        public string LblLanguagePreference { get; set; }
        public string LocationPreferenceUrl { get; set; }
        public string HlkUserPreferences { get; set; }
        public string SystemAdminPhone { get; set; }
        public string LblSystemAdminIsNotYetSetup { get; set; }
    }
}
