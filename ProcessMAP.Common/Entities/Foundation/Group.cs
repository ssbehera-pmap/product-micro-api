﻿namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents the Bond Group.
    /// </summary>
    public class Group
    {
        /// <summary>
        /// Represents the unique identifier of the Group.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Represents the Name of the Group.
        /// </summary>
        public string Name { get; set; }
    }
}