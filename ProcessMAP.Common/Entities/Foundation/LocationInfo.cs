﻿namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Navigation type. Value will be either 'Location' or 'Level'
        /// </summary>
        public string Type { get; set; }
    }
}