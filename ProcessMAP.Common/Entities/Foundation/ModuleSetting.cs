﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents the Custom Setting by Module entity.
    /// </summary>
    public class ModuleSetting : Module
    {
        /// <summary>
        /// Creates an instance of the Module Setting class.
        /// </summary>
        public ModuleSetting()
        {
            // Empty constructor needed for serialization
        }

        /// <summary>
        /// Represents a list of related Custom Setting per Module.
        /// </summary>
        public List<CustomSetting> Settings { get; set; }
    }
}