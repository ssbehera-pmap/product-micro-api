﻿using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Common.Entities.Foundation
{
    /// <summary>
    /// Represents a container for the data that defines the tenant settings in the system.
    /// </summary>
    public class Tenant : Consumer
    {
        /// <summary>
        /// Creates a new object instance of the Tenant class.
        /// </summary>
        public Tenant()
        {
            // Empty constructor needed for serialization
        }

        /// <summary>
        /// Creates an instance of a Tenant object based on the supplied Consumer object.
        /// </summary>
        /// <param name="consumer"></param>
        public Tenant(Consumer consumer)
        {
            AllowedEndpoints = consumer.AllowedEndpoints;
            ApplicationCacheExpiration = consumer.ApplicationCacheExpiration;
            AuthenticationInfoList = consumer.AuthenticationInfoList;
            ClaimSettings = consumer.ClaimSettings;
            CreatedDate = consumer.CreatedDate;
            CryptographyInfo = consumer.CryptographyInfo;
            DatabaseInfo = consumer.DatabaseInfo;
            Id = consumer.Id;
            IsActive = consumer.IsActive;
            LastCached = consumer.LastCached;
            Name = consumer.Name;
            ApplicationId = consumer.ApplicationId;
            ConsumerKey = consumer.ConsumerKey;
        }

        /// <summary>
        /// Represents the unique URL that is used for the Tenant.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Represents the registered Client Name of the Tenant.
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// Represents the registered Product Name of the Tenant.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets whether the tenant has SSO configured or not.
        /// </summary>
        public bool SSOEnabled
        {
            get
            {
                if (AuthenticationInfoList != null && AuthenticationInfoList.Count() > 0)
                    return AuthenticationInfoList.Any(a => a.SSOEnabled);

                return false;
            }
        }
    }
}