﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProcessMAP.Common.Entities.ScreenLayout
{
    public class LayoutSection
    {
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        [Required]
        public string Label { get; set; }

        [Required]
        [CollectionRange(minCount: 1)]
        public IEnumerable<Application> Applications { get; set; }
    }
}
