﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;


namespace ProcessMAP.Common.Entities.ScreenLayout
{
    public class Layout
    {
        public Layout()
        { }

        [JsonIgnore]
        [XmlIgnore]
        public int UserId { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public int ConsumerId { get; set; }

        public IEnumerable<LayoutSection> Sections { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that created the entity.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that updated the entity.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was updated.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Represents the optional user id of the user that [soft] deleted the record.
        /// </summary> 
        public int? DeletedBy { get; set; }

        /// <summary>
        /// Represents the optional date time when the record was [soft] deleted.
        /// </summary>
        public DateTime? DeletedDate { get; set; }

    }
}
