﻿using Newtonsoft.Json;
using System;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class Breadcrumb : ExtensionProperty<Guid, string>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "Uid")]
        public override Guid Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Breadcrumb CreateRoot()
        {
            return new Breadcrumb()
            {
                Description = Folder.DEFAULT_ROOT_FOLDER_TITLE,
                Id = Guid.Empty
            };
        }
    }
}