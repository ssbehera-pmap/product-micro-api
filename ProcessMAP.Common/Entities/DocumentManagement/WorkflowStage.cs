﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowStage
    {
        /// <summary>
        /// The description of the stage.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The WFE command which users in this stage would be executing.
        /// </summary>
        public string WorkflowCommand { get; set; }

        /// <summary>
        /// The number of users that should be in this stage.
        /// </summary>
        public int NumberOfUsers { get; set; }

        /// <summary>
        /// A collection of user approvals.
        /// </summary>
        public List<DocumentApproval> UserApprovals { get; set; }

        /// <summary>
        /// The due date for the stage.
        /// </summary>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// The unique identifier of the document version.
        /// </summary>
        public Guid DocumentVersionUid { get; set; }

        /// <summary>
        /// Determines if the stage is in progress.
        /// </summary>
        public bool IsInProgress { get; set; }

        /// <summary>
        /// Determines whether the stage has been completed.
        /// </summary>
        public bool IsCompleted
        {
            get { return UserApprovals != null && UserApprovals.All(u => u.IsApproved); }
        }
    }
}