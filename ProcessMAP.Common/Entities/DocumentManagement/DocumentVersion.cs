﻿using Newtonsoft.Json;
using ProcessMAP.Common.DocumentManagement;
using ProcessMAP.Common.Entities.Base;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentVersion : UEntity
    {
        /// <summary>
        /// The default version number when a new document is uploaded.
        /// </summary>
        public const string DEFAULT_VERSION_NUMBER = "1.0";

        /// <summary>
        /// The default version type when a new document is uploaded.
        /// </summary>
        public const DocumentVersionStatusType DEFAULT_VERSION_STATUS_TYPE = DocumentVersionStatusType.Upload;

        /// <summary>
        /// The default version type when a new document is uploaded.
        /// </summary>
        public const DocumentVersionType DEFAULT_VERSION_TYPE = DocumentVersionType.Major;

        /// <summary>
        /// An object structure that represents a particular version of the document.
        /// </summary>
        public DocumentVersion() { }

        /// <summary>
        /// Represents the unique identified of the document.
        /// </summary>
        public Guid DocumentUid { get; set; }

        /// <summary>
        /// Represents the document' version number.
        /// </summary>
        public string VersionNumber { get; set; }

        /// <summary>
        /// Represents the status of the document version. E.g.: Draft, Final.
        /// </summary>
        public DocumentVersionStatusType VersionStatusType { get; set; }

        /// <summary>
        /// Indicates the document version type. E.g.: Minor, Major.
        /// </summary>
        public DocumentVersionType VersionType { get; set; }

        /// <summary>
        /// The state of the version in regards to the WFE.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// The translated value of the state of the version in regards to the WFE.
        /// </summary>
        public string StateDescription { get; set; }

        /// <summary>
        /// Represents the physical file size in bytes.
        /// </summary>
        public long FileSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Determines whether the document version is the current version.
        /// </summary>
        public bool IsCurrent { get; set; }

        /// <summary>
        /// Determines if the document version is in the Final state.
        /// </summary>
        public bool IsFinal
        {
            get { return State != null && State.Equals(KnownWorkflowStates.Final.ToString(), StringComparison.CurrentCultureIgnoreCase); }
        }

        /// <summary>
        /// Determines whether the release of the document version has been scheduled for a future date.
        /// </summary>
        public bool IsReleaseScheduled
        {
            get
            {
                return ReleaseDate.HasValue;
            }
        }

        /// <summary>
        /// Represents whether the document version needs to be acknowledged.
        /// </summary>
        public bool IsAcknowledgementRequired { get; set; }

        /// <summary>
        /// Represents whether the document version has been acknowledged by current user.
        /// </summary>
        public bool IsAcknowledged { get; set; }

        /// <summary>
        /// Represents the date at which the document version needs to be acknowledged, if required.
        /// </summary>
        public DateTime? AcknowledgementDueDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string Comment { get; set; }

        /// <summary>
        /// Indicates the number of revisions that this document version has had.
        /// </summary>
        public short RevisionCount { get; set; }

        /// <summary>
        /// Represents the date at which the document was finalized.
        /// </summary>
        public DateTime? FinalizedDate { get; set; }

        /// <summary>
        /// Represents the date at which the document is scheduled for release as determined by the ReleaseScheduleType.
        /// </summary>
        public DateTime? ReleaseDate { get; set; }

        /// <summary>
        /// Indicates the document version's release scheduling type. E.g.: Immediate, Future Date.
        /// </summary>
        public ExtensionProperty<DocumentVersionReleaseScheduleType, string> ReleaseScheduleType { get; set; }

        /// <summary>
        /// Represents the document version's release notes.
        /// </summary>
        public string ReleaseNote { get; set; }

        /// <summary>
        /// Represents the document version's training and communication notes.
        /// </summary>
        public string TrainingNote { get; set; }

        /// <summary>
        /// Gets the Actions to be performed for the document.
        /// </summary>
        public DocumentActions Actions { get; set; }

        /// <summary>
        /// Gets or sets the archived date .
        /// </summary>
        public DateTime? ArchiveDate { get; set; }

        /// <summary>
        /// Represents the reason for changing the version.
        /// </summary>
        public string VersionComment { get; set; }

        /// <summary>
        /// Represents a collection of users that need to be notified when the document is released.
        /// </summary>
        public List<DocumentUser> NotificationUsers { get; set; }

        /// <summary>
        /// Creates a new version, or it an existing version is passed, an upgrade from that version.
        /// </summary>
        /// <param name="existingVersion"></param>
        /// <param name="versionType"></param>
        /// <returns></returns>
        public static DocumentVersion CreateNewVersion(DocumentVersion existingVersion = null, DocumentVersionType? versionType = null)
        {
            DocumentVersion version;
            if (existingVersion == null)
            {
                version = new DocumentVersion()
                {
                    VersionNumber = DEFAULT_VERSION_NUMBER,
                    IsCurrent = true,
                    VersionStatusType = DocumentVersionStatusType.Upload,
                    VersionType = DEFAULT_VERSION_TYPE,
                    Uid = Guid.NewGuid()
                };
            }
            else
            {
                var versionNumber = CalculateNextVersion(existingVersion.VersionNumber, versionType.Value);

                version = new DocumentVersion()
                {
                    VersionNumber = versionNumber,
                    IsCurrent = false,
                    VersionStatusType = DocumentVersionStatusType.Draft,
                    VersionType = versionType.Value,
                    Uid = Guid.NewGuid()
                };
            }

            return version;
        }

        /// <summary>
        /// Calculates next version number, based on whether it is Major or Minor.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="versionType"></param>
        /// <returns></returns>
        public static string CalculateNextVersion(string version, DocumentVersionType versionType)
        {
            var versionParts = version.Split('.');
            int versionPartMajor = int.Parse(versionParts[0]);
            int versionPartMinor = int.Parse(versionParts.Length > 1 ? versionParts[1] : "0");

            switch (versionType)
            {
                case DocumentVersionType.Major:
                    versionPartMajor++;
                    versionPartMinor = 0;
                    break;

                case DocumentVersionType.Minor:
                    versionPartMinor++;
                    break;
            }

            var nextVersion = string.Format("{0}.{1}", versionPartMajor, versionPartMinor);
            return nextVersion;
        }

        /// <summary>
        /// Gets or sets the document's  version control status type.
        /// </summary>
        public ControlStatus ControlStatus { get; set; }

        /// <summary>
        /// Gets a value indicating whether the document version is locked due to being 'Checked Out'.
        /// </summary>
        public bool IsLocked
        {
            get
            {
                return
                    ControlStatus != null &&
                    ControlStatus.Id == DocumentControlStatus.CheckedOut;
            }
        }

        /// <summary>
        /// Gets the UserID of the user who last Checked out the document version.
        /// </summary>
        public int? CheckedOutUser { get; set; }

        /// <summary>
        /// Gets the UserID of the user who last Checked out the document version.
        /// </summary>
        public string CheckedOutUserName { get; set; }


        /// <summary>
        /// Gets the date when the document version was last checked out.
        /// </summary>
        public DateTime? CheckedOutDate { get; set; }

        /// <summary>
        /// Gets or sets a collection of users who will be notified when Document is final.
        /// </summary>
        public List<DocumentUser> ReleaseDistributionList { get; set; }
    }
}