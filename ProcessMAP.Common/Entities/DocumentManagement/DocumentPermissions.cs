﻿namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Permissions for actions that are performed at the document level.
    /// </summary>
    public class DocumentPermissions
    {
        /// <summary>
        /// Can the user perform a periodic review for this document.
        /// </summary>
        public bool CanPerformPeriodicReview { get; set; }

        /// <summary>
        /// Can the user perform a change request for this document.
        /// </summary>
        public bool CanPerformChangeRequest { get; set; }

        /// <summary>
        /// Can the user access the document release.
        /// </summary>
        public bool CanViewDocumentRelease { get; set; }

        /// <summary>
        /// Can the user edit the document release.
        /// </summary>
        public bool CanEditDocumentRelease { get; set; }

        /// <summary>
        /// Can the user change the workflow of the document.
        /// </summary>
        public bool CanChangeWorkflow { get; set; }

        /// <summary>
        /// Can the user change the document's ownership.
        /// </summary>
        public bool CanTransferOwnership { get; set; }

        /// <summary>
        /// Can the user view the Version History (has Edit Permission).
        /// </summary>
        public bool CanViewVersionHistory { get; set; }

        /// <summary>
        /// Can the user view the Approval Workflow (Document Details Saved).
        /// </summary>
        public bool CanViewWorkflow { get; set; }
    }
}