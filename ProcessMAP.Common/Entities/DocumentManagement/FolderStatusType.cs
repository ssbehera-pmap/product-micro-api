﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Represents an enumeration of status types for a folder.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FolderStatusType : short
    {
        /// <summary>
        /// Undefined folder status type.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Active status type.
        /// </summary>
        Active = 1,

        /// <summary>
        /// Inactive status type.
        /// </summary>
        Inactive = 2
    }
}