﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum IntervalTypes  : short
    {
        /// <summary>
        /// Unsupported interval type.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Days
        /// </summary>
        Days = 1,

        /// <summary>
        /// Weeks
        /// </summary>
        Weeks = 2,

        /// <summary>
        /// Months
        /// </summary>
        Months = 3,

        /// <summary>
        /// Years
        /// </summary>
        Years = 4
    }
}