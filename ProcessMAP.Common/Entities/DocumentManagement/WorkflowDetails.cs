﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowDetails : ExtensionProperty<int, string>
    {
        /// <summary>
        /// Gets or sets a collection of commands and the number of users that can be assigned to the command.
        /// </summary>
        //public Dictionary<string, int> UsersPerCommand { get; set; }

        public List<WorkflowStage> Stages { get; set; }
     }
}