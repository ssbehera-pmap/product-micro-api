﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class ControlStatus : ExtensionProperty<DocumentControlStatus, string>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int UpdatedBy { get; set; }
    }
}