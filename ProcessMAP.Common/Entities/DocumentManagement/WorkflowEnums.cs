﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.DocumentManagement
{
    /// <summary>
    /// Known workflow state names.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum KnownWorkflowStates : short
    {
        /// <summary>
        /// State is undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Draft state. This is the first state in all workflows.
        /// All new document uploads will start in this state.
        /// </summary>
        Draft = 1,

        /// <summary>
        /// PendingApproval state.
        /// The document is ready for the approval process.
        /// </summary>
        PendingApproval = 2,

        /// <summary>
        /// Reviewed state.
        /// The document has been reviewed.
        /// </summary>
        Reviewed = 3,

        /// <summary>
        /// Evaluated state.
        /// The document has been evaluated.
        /// </summary>
        Evaluated = 4,

        /// <summary>
        /// Approved state.
        /// The document has been approved.
        /// </summary>
        Approved = 5,

        /// <summary>
        /// Final state.
        /// The document has been finalized (0r released).
        /// </summary>
        Final = 6,

        /// <summary>
        /// Archived state.
        /// The document has been archived.
        /// </summary>
        Archived,


        /// <summary>
        /// Rejected state.
        /// The document has been rejected.
        /// </summary>
        Rejected
    }

    /// <summary>
    /// Known workflow command names.
    /// </summary>
    public enum KnownWorkflowCommands : short
    {
        /// <summary>
        /// Command is undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// SubmitForAppproval command.
        /// After this command is executed, the document will enter the PendingApproval state.
        /// </summary>
        SubmitForApproval = 1,

        /// <summary>
        /// Review command.
        /// After this command is executed, the document will enter the Reviewed state.
        /// </summary>
        Review = 2,

        /// <summary>
        /// Evaluate command.
        /// After this command is executed, the document will enter the Evaluated state.
        /// </summary>
        Evaluate = 3,

        /// <summary>
        /// Approve command.
        /// After this command is executed, the document will enter the Approved state.
        /// </summary>
        Approve = 4,

        /// <summary>
        /// Finalize command.
        /// After this command is executed, the document will enter the Final state.
        /// </summary>
        Finalize = 5,

        /// <summary>
        /// Archive command.
        /// After this command is executed, the document will enter the Archived state.
        /// </summary>
        Archive = 6,

        /// <summary>
        /// Reject command.
        /// After this command is executed, the document will enter the Rejected state.
        /// </summary>
        Reject = 7,

        /// <summary>
        /// CancelApprovalRequest command.
        /// After this command is executed, the document will be placed back in the Draft state.
        /// </summary>
        CancelApprovalRequest = 8
    }
}