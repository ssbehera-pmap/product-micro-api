﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Represents an enumeration of document approval statuses.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentApprovalStatusType : short
    {
        /// <summary>
        /// The Default document approval status.
        /// </summary>
        Default = 0,

        /// <summary>
        /// The Pending document approval status.
        /// </summary>
        Pending = 1,

        /// <summary>
        /// The Approved document approval status.
        /// </summary>
        Approved = 2,

        /// <summary>
        /// The Rejected document approval status.
        /// </summary>
        Rejected = 3
    }
}