﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentActions
    {
        /// <summary>
        /// 
        /// </summary>
        public DocumentActions()
        {
            Ellipsis = new List<ExtensionProperty<DocumentActionType, string>>();
            Buttons = new List<ExtensionProperty<DocumentActionType, string>>();
        }

        /// <summary>
        /// 
        /// </summary>
        public List<ExtensionProperty<DocumentActionType, string>> Buttons { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ExtensionProperty<DocumentActionType, string>> Ellipsis { get; set; }
    }
}