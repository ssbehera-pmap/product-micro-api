﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class Subfolder : ExtensionProperty<Guid, string>
    {
        /// <summary>
        /// The id of the sub-folder.
        /// </summary>
        [JsonProperty(PropertyName = "Uid")]
        public override Guid Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        /// <summary>
        /// The date of creation.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The date of last modified.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Determines if the subfolder is private.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public bool IsPrivate { get; set; }
    }
}