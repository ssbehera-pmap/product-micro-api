﻿using System;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Represents a folder in the lookups collection.
    /// </summary>
    public class LookupFolder : ExtensionProperty<Guid, string>
    {
        /// <summary>
        /// Contains a value indicating whether the lookup folder is private.
        /// </summary>
        public bool IsPrivate { get; set; }
    }
}