﻿using Newtonsoft.Json;
using ProcessMAP.Common.Entities.Base;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Represents a user approval for a particular document.
    /// </summary>
    public class DocumentApproval : UEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int DocumentVersionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public override long Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        /// <summary>
        /// The id of the document.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int DocumentId { get; set; }

        /// <summary>
        /// The unique identifier of the target document's version.
        /// </summary>
        public Guid DocumentVersionUid { get; set; }

        /// <summary>
        /// The user id of the approver.
        /// </summary>
        public int ApprovalUserId { get; set; }

        /// <summary>
        /// The workflow command to be executed for this approval.
        /// </summary>
        public string WorkflowCommand { get; set; }

        /// <summary>
        /// The email address of the approver.
        /// </summary>
        public string ApprovalUserEmail { get; set; }

        /// <summary>
        /// Gets or sets the user's full name.
        /// </summary>
        public string ApprovalUserFullName { get; set; }

        /// <summary>
        /// The date when the document was requested for approval.
        /// </summary>
        public DateTime RequestedDate { get; set; }

        /// <summary>
        /// The date when the document is due for approval.
        /// </summary>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// The date when the document was approved.
        /// </summary>
        public DateTime? ApprovedDate { get; set; }

        /// <summary>
        /// Gets or sets the date where the document was denied.
        /// </summary>
        public DateTime? DeniedDate { get; set; }

        /// <summary>
        /// The comments of the approver.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets a value representing whether the user has approved the document.
        /// </summary>
        public bool IsApproved
        {
            get
            {
                return
                    (DeniedDate == null && ApprovedDate != null) ||
                    (ApprovedDate > DeniedDate);
            }
        }

        /// <summary>
        /// Gets a value representing whether the user has approved the document.
        /// </summary>
        public bool IsRejected
        {
            get
            {
                return
                    DeniedDate.HasValue &&
                    (!ApprovedDate.HasValue || DeniedDate.Value >= ApprovedDate.Value);
            }
        }

        /// <summary>
        /// Gets or sets the archived date.
        /// </summary>
        public DateTime? ArchiveDate { get; set; }

        /// <summary>
        /// Represents the status of the user approval.
        /// </summary>
        public DocumentApprovalStatusType StatusLabel { get; set; }
    }
}