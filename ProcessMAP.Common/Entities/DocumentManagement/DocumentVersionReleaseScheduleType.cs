﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentVersionReleaseScheduleType : short
    {
        /// <summary>
        /// Undefined version release schedule type.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Immediate version release schedule type.
        /// </summary>
        Immediate = 1,

        /// <summary>
        /// Future Date version release schedule type.
        /// </summary>
        FutureDate = 2
    }
}