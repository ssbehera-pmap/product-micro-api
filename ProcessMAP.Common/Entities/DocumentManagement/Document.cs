﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;
using ProcessMAP.Common.DocumentManagement;
using ProcessMAP.Common.Entities.WorkflowEngine;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Defines a document object and all its properties.
    /// </summary>
    public class Document : TreeElement
    {
        private DocumentFolder primaryFolder;
        private DocumentFolder[] displayFolders;
        private DocumentVersion targetVersion;

        /// <summary>
        /// The default title when a new document is uploaded.
        /// </summary>
        public const string DEFAULT_TITLE = "Untitled";

        /// <summary>
        /// The state/status of the document after it has been uploaded,
        /// but no Workflow has been assigned yet.
        /// </summary>
        public const string INITIAL_STATE = "UploadedPendingDetails";

        /// <summary>
        /// Document constructor. Instantiates a new Document object.
        /// </summary>
        public Document()
            : base()
        { }

        /// <summary>
        /// Gets the Actions to be performed for the document.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public ExtensionProperty<DocumentActionType, string>[] AllActions { get; set; }

        /// <summary>
        /// Gets or sets the internal document id.
        /// </summary>
        [JsonProperty(PropertyName = "InternalId")]
        public string SourceDocumentId { get; set; }

        /// <summary>
        /// Gets or sets the description for this document.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the current state of the document as it stands in the WFE (Workflow Engine).
        /// This property also delivers the WFE commands that the current user is allowed to execute, if any.
        /// </summary>
        public ProcessState DocumentState { get; set; }

        /// <summary>
        /// Gets or sets the business process where the document belongs.
        /// </summary>
        public ExtensionProperty<int, string> BusinessProcess { get; set; }

        /// <summary>
        /// Gets or sets the level of document.
        /// </summary>
        public ExtensionProperty<int, string> Level { get; set; }

        /// <summary>
        /// Gets or sets the type of document.
        /// </summary>
        public ExtensionProperty<string, string> Type { get; set; }

        /// <summary>
        /// Gets or sets the category of the document.
        /// </summary>
        public ExtensionProperty<string, string> Category { get; set; }

        /// <summary>
        /// Gets or sets the workflow associated to this document.
        /// </summary>
        public WorkflowDetails Workflow { get; set; }

        /// <summary>
        /// Gets or sets the document's control status type.
        /// </summary>
        public ControlStatus ControlStatus { get; set; }

        /// <summary>
        /// Gets a value indicating whether the document is locked due to being 'Checked Out'.
        /// </summary>
        public bool IsLocked
        {
            get
            {
                return
                    ControlStatus != null &&
                    ControlStatus.Id == DocumentControlStatus.CheckedOut;
            }
        }

        /// <summary>
        /// Gets a value indicating that this document is in the Draft state.
        /// </summary>
        public bool IsDraft
        {
            get
            {
                return
                    DocumentState != null &&
                    DocumentState.CurrentState.Equals(KnownWorkflowStates.Draft.ToString(), StringComparison.CurrentCultureIgnoreCase);
            }
        }

        /// <summary>
        /// Gets a value indicating that this document is in the Approved state.
        /// </summary>
        public bool IsApproved
        {
            get
            {
                return
                    DocumentState != null &&
                    DocumentState.CurrentState.Equals(KnownWorkflowStates.Approved.ToString(), StringComparison.CurrentCultureIgnoreCase);
            }
        }

        /// <summary>
        /// Gets a value indicating that this document is in the Rejected state.
        /// </summary>
        public bool IsRejected
        {
            get
            {
                return
                    DocumentState != null &&
                    DocumentState.CurrentState.Equals(KnownWorkflowStates.Rejected.ToString(), StringComparison.CurrentCultureIgnoreCase);
            }
        }

        /// <summary>
        /// Gets a value indicating that this document is in the Final state.
        /// </summary>
        public bool IsFinal
        {
            get
            {
                return
                    DocumentState != null &&
                    DocumentState.CurrentState.Equals(KnownWorkflowStates.Final.ToString(), StringComparison.CurrentCultureIgnoreCase);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this document has been archived.
        /// </summary>
        public bool IsArchived
        {
            get
            {
                return
                    DocumentState != null &&
                    DocumentState.CurrentState.Equals(KnownWorkflowStates.Archived.ToString(), StringComparison.CurrentCultureIgnoreCase);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this document has been marked private.
        /// </summary>
        public bool IsPrivate
        {
            get
            {
                return
                    VisibilityType != null &&
                    VisibilityType.Id == DocumentVisibility.Private;
            }
        }

        /// <summary>
        /// Determines if the document is pending update details.
        /// </summary>
        public bool IsPendingUpdateDetails
        {
            get
            {
                return
                    UpdatedDate.HasValue == false |
                    Owner == null |
                    string.IsNullOrWhiteSpace(Title) |
                    Title.Equals(DEFAULT_TITLE, StringComparison.CurrentCultureIgnoreCase) |
                    Title.Equals(FileName, StringComparison.CurrentCultureIgnoreCase) |
                    PrimaryFolder == null;
            }
        }

        /// <summary>
        /// Gets or sets the document's visibility type.
        /// </summary>
        public ExtensionProperty<DocumentVisibility, string> VisibilityType { get; set; }

        /// <summary>
        /// Gets or sets the id of the owner of the document.
        /// </summary>
        public DocumentUser Owner { get; set; }

        /// <summary>
        /// Gets or sets the id of the author of the document.
        /// </summary>
        public DocumentUser Coordinator { get; set; }

        /// <summary>
        /// Gets or sets a collection of users that can view this document when its visibility has been set to "Private".
        /// </summary>
        public List<DocumentUser> PrivateViewers { get; set; }

        /// <summary>
        /// Gets or sets the full file name of the document.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the version of the document.
        /// </summary>
        public List<DocumentVersion> Versions { get; set; }

        /// <summary>
        /// Gets the document's current version.
        /// </summary>
        public DocumentVersion CurrentVersion
        {
            get
            {
                if (Versions != null && Versions.Count > 0)
                {
                    return Versions.FirstOrDefault(v => v.IsCurrent);
                }

                return null;
            }
        }

        /// <summary>
        /// The version of the document that is meant to be targeted.
        /// </summary>
        public DocumentVersion TargetVersion
        {
            get
            {
                if (targetVersion == null)
                    return CurrentVersion;

                return targetVersion;
            }
            set { targetVersion = value; }
        }

        /// <summary>
        /// Represents the number of the version being targeted.
        /// If no version is being targeted, then the number of the
        /// current version will be returned.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        [Obsolete]
        public string TargetVersionNumber
        {
            get { return TargetVersion.VersionNumber; }
        }

        /// <summary>
        /// Represents the uid of the version being targeted.
        /// If no version is being targeted, then the uid of the
        /// current version will be returned.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        [Obsolete("Use TargetVersion.Uid instead.")]
        public Guid TargetVersionUid
        {
            get { return TargetVersion.Uid; }
        }

        /// <summary>
        /// Gets or sets a collection of folders in which the document should exist.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public List<DocumentFolder> Folders { get; set; }

        /// <summary>
        /// Gets or sets a collection of additional folders the document might exist in.
        /// </summary>
        public DocumentFolder[] DisplayFolders
        {
            get
            {
                if (displayFolders == null && Folders != null)
                {
                    displayFolders = Folders.Where(f => !f.IsPrimary).ToArray();
                }

                return displayFolders;
            }
            set
            {
                displayFolders = value;
                if (Folders == null) Folders = new List<DocumentFolder>();
                Folders.AddRange(displayFolders);
            }
        }


        /// <summary>
        /// Gets or sets the primary folder for the document.
        /// </summary>
        public DocumentFolder PrimaryFolder
        {
            get
            {
                if (primaryFolder == null && Folders != null)
                {
                    primaryFolder = Folders.FirstOrDefault(f => f.IsPrimary);
                }

                return primaryFolder;
            }
            set
            {
                if (value == null)
                    return;

                if (Folders == null) Folders = new List<DocumentFolder>();

                if (primaryFolder == null)
                {
                    primaryFolder = value;
                    primaryFolder.IsPrimary = true;
                    Folders.Add(primaryFolder);
                }
                else if (primaryFolder.Id != value.Id)
                {
                    Folders.Remove(primaryFolder);
                    primaryFolder = value;
                    primaryFolder.IsPrimary = true;
                    Folders.Add(primaryFolder);
                }
            }
        }

        /// <summary>
        /// Gets or sets the URL to the details page.
        /// </summary>
        public string DetailsLink { get; set; }

        /// <summary>
        /// Gets or sets the link to download the document.
        /// </summary>
        public string DownloadLink { get; set; }

        /// <summary>
        /// Gets or sets a collection of keywords associated to the document.
        /// </summary>
        public string[] Keywords { get; set; }

        /// <summary>
        /// Gets or sets a value indicating that this document is set for auto release.
        /// </summary>
        public bool IsAutoRelease { get; set; }

        /// <summary>
        /// Gets or sets the contents of the document ready to stream.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Tuple<string, byte[]> FileStream { get; set; }

        /// <summary>
        /// Gets or sets a collection of the document's regulatory references.
        /// </summary>
        public string[] RegulatoryReferences { get; set; }

        /// <summary>
        /// Gets or sets a collection of user approvals.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public List<DocumentApproval> UserApprovals { get; set; }

        /// <summary>
        /// Gets or sets a collection of Departments associated with this document.
        /// </summary>
        public Department[] Departments { get; set; }

        /// <summary>
        /// Gets or sets a value indicating that the document......
        /// </summary>
        public bool IsPublishOnlyPDFs { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the interval for the retention period.
        /// </summary>
        public int? RetentionPeriodInterval { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the retention period interval type.
        /// </summary>
        public ExtensionProperty<IntervalTypes, string> RetentionPeriodType { get; set; }

        /// <summary>
        /// The document's periodic review.
        /// </summary>
        public DocumentReview[] Reviews { get; set; }

        /// <summary>
        /// Gets or sets a collection of documents that are referenced by this document.
        /// </summary>
        public ExtensionProperty<Guid, string>[] ReferenceDocuments { get; set; }

        /// <summary>
        /// Gets the unique identifier of the primary folder of the document, if available.
        /// </summary>
        public override Guid? ParentFolderUid
        {
            get
            {
                if (PrimaryFolder != null)
                {
                    return PrimaryFolder.Id;
                }

                return null;
            }

            set { base.ParentFolderUid = value; }
        }

        /// <summary>
        /// Represents the interval type for the periodic review. E.g.: Days, Months, etc.
        /// </summary>
        public ExtensionProperty<IntervalTypes, string> PeriodicReviewIntervalType { get; set; }

        /// <summary>
        /// Represents the value of the interval for the document's periodic review.
        /// This value is used to calculate the due date of the periodic review based on the
        /// specified PeriodicReviewStartDate.
        /// </summary>
        public int? PeriodicReviewInterval { get; set; }

        /// <summary>
        /// Represents the date at which the periodic review cycle starts.
        /// </summary>
        public DateTime? PeriodicReviewStartDate { get; set; }

        /// <summary>
        /// Represents the comments that the updating user might enter as the reason for change when updating a document.
        /// </summary>
        [JsonProperty(PropertyName = "ChangeComment")]
        public string UserChangeComment { get; set; }


        /// <summary>
        /// Represents the comments that the updating user might enter as the reason for change when updating a document.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string LastChangeComment { get; set; }
        /// <summary>
        /// A set of permissions for actions that can be performed at the document level.
        /// </summary>
        public DocumentPermissions DocumentPermissions { get; set; }

        /// <summary>
        /// Gets a value indicating whether this document can be shared to non-system users.
        /// </summary>
        public bool PublicSharing { get; set; }
    }
}