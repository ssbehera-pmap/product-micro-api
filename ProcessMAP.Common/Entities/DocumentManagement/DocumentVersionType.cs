﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentVersionType : short
    {
        /// <summary>
        /// Undefined document version type.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Major version type.
        /// </summary>
        Major = 1,

        /// <summary>
        /// Minor version type.
        /// </summary>
        Minor = 2,

        /// <summary>
        /// CheckIn type.
        /// </summary>
        CheckIn = 3
    }
}