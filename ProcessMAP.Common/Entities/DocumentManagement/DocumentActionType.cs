﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Document action types.
    /// https://processmap.atlassian.net/wiki/display/PR/Landing+Page
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentActionType
    {
        /// <summary>
        /// Unsupported document action type.
        /// </summary>
        Undefined,

        /// <summary>
        /// Acknowledge action type.
        /// </summary>
        Acknowledge,

        /// <summary>
        /// Archive action type.
        /// </summary>
        Archive,

        /// <summary>
        /// Cancel check-out action type.
        /// Document Owner and Document Coordinator only.
        /// Draft and Rejected status documents only; option not available until a document is checked out.
        /// </summary>
        CancelCheckOut,

        /// <summary>
        /// Check-in action type.
        /// Document Owner and Document Coordinator only.
        /// Draft and Rejected status documents only; option not available until a document is checked out.
        /// </summary>
        CheckIn,

        /// <summary>
        /// Check-out action type.
        /// Document Owner and Document Coordinator only.
        /// Draft and Rejected status documents only; option disappears once document is checked out.
        /// </summary>
        CheckOut,

        /// <summary>
        /// Check-out action type.
        /// Document Owner and Document Coordinator only.
        /// Final status documents only.
        /// </summary>
        CreateNewVersion,

        /// <summary>
        /// Delete action type.
        /// Users with delete permission.
        /// </summary>
        Delete,

        /// <summary>
        /// Download action type.
        /// Users with view permission.
        /// </summary>
        Download,

        /// <summary>
        /// Preview action type.
        /// Users with view permission.
        /// </summary>
        Preview,

        /// <summary>
        /// Print action type.
        /// Users with view permission.
        /// </summary>
        Print,

        /// <summary>
        /// Share action type.
        /// Users with view permission.
        /// </summary>
        Share,

        /// <summary>
        /// Upload action type.
        /// Users with add permission.
        /// </summary>
        Upload,

        /// <summary>
        /// Edit action type.
        /// Users with edit permission.
        /// </summary>
        Edit,

        /// <summary>
        /// Upload without approval action type.
        /// Document Owner and Document Coordinator only.
        /// Final status documents only.
        /// </summary>
        UploadWithoutApproval,

        /// <summary>
        /// Submit Change Request action type.
        ///     
        /// </summary>
        SubmitChangeRequest,

        /// <summary>
        /// The View action type.
        /// Users with View permission.
        /// </summary>
        View,

        /// <summary>
        /// The Save action type.
        /// Users with Edit permission.
        /// </summary>
        Save
    }
}