﻿using System;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentFolder : GenericTree<Guid, string>
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsPrimary { get; set; }

        /// <summary>
        /// Creates a folder to be used as "Root" folder.
        /// </summary>
        /// <returns></returns>
        public static DocumentFolder CreateRoot()
        {
            return new DocumentFolder()
            {
                Id = Guid.Empty,
                Description = Folder.DEFAULT_ROOT_FOLDER_TITLE,
                IsPrimary = true
            };
        }

        /// <summary>
        /// Gets or sets the mame of the folder.
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// Gets or sets the archeived date .
        /// </summary>
        public DateTime? ArchiveDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that updated the entity.
        /// </summary>
        public string UpdatedByName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that created the entity.
        /// </summary>
        public string CreatedByName { get; set; }
    }
}