﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentVersionStatusType
    {
        /// <summary>
        /// Draft document status type.
        /// All documents should start with version type "Draft", until they go through the workflow.
        /// </summary>
        Draft,

        /// <summary>
        /// Final document status type.
        /// </summary>
        Final,

        /// <summary>
        ///Document Status when just Uploaded.
        /// </summary>
        Upload
    }
}