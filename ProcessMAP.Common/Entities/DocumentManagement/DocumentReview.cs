﻿using Newtonsoft.Json;
using ProcessMAP.Common.Entities.Base;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentReview : UEntity
    {
        /// <summary>
        /// Gets or sets the internal document id.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the unique document id.
        /// </summary>
        [XmlIgnore]
        public Guid DocumentUid { get; set; }

        /// <summary>
        /// Gets or sets the Periodic Review Type.
        /// </summary>
        public string ReviewType { get; set; }

        /// <summary>
        /// Gets or sets the Periodic Review Status.
        /// </summary>
        public string ReviewStatus { get; set; }

        /// <summary>
        /// Gets or sets the UserID of Completed By / Reviewed By.
        /// </summary>
        public int? ReviewedBy { get; set; }

        /// <summary>
        /// Gets or sets the Name of Completed By / Reviewed By.
        /// </summary>
        public string ReviewedByName { get; set; }

        /// <summary>
        /// Gets or sets the Interval (in units defined in Interval Type)
        /// </summary>
        public int? Interval { get; set; }

        /// <summary>
        /// Gets or sets the Interval Type (like days, weeks, months, years)
        /// </summary>
        public string IntervalType { get; set; }

        /// <summary>
        /// Gets or sets the Completed Date / Reviewed Date.
        /// </summary>
        public DateTime? ReviewedDate { get; set; }

        /// <summary>
        /// Gets or sets the Review Due Date.
        /// </summary>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// Gets or sets the Periodic Review Comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the archived date .
        /// </summary>
        public DateTime? ArchiveDate { get; set; }

        /// <summary>
        /// Gets or sets the Version Type (Major or Minor).
        /// </summary>
        public string VersionType { get; set; }

        /// <summary>
        /// Gets whether a revised version of the document is uploaded or not. Pass values as true or false .
        /// </summary>
        public Boolean UploadRevisedVersion { get; set; }
    }
}