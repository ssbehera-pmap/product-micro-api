﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Represents the document's control status type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentControlStatus
    {
        /// <summary>
        /// For more information:
        /// https://processmap.atlassian.net/wiki/pages/viewpage.action?pageId=2621727
        /// </summary>
        CheckedIn,

        /// <summary>
        /// For more information:
        /// https://processmap.atlassian.net/wiki/pages/viewpage.action?pageId=2621727
        /// </summary>
        CheckedOut
    }
}