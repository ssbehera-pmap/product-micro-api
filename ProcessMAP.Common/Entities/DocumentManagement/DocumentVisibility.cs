﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Enumeration of document visibility types.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DocumentVisibility : short
    {
        /// <summary>
        /// Undefined document visibility.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Public document visibility.
        /// </summary>
        Public = 1,

        /// <summary>
        /// Private document visibility.
        /// </summary>
        Private = 2
    }
}