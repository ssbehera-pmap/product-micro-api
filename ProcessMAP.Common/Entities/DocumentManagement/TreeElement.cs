﻿using Newtonsoft.Json;
using ProcessMAP.Common.Entities.Base;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// Defines an element that belongs to the file system tree.
    /// </summary>
    public abstract class TreeElement : UEntity
    {
        /// <summary>
        /// Tree element constructor
        /// </summary>
        public TreeElement()
            : base()
        { }

        /// <summary>
        /// Gets or sets the id of the element in the tree.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public override long Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        /// <summary>
        /// Gets or sets the name of the element in the tree.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or set the id of the parent container.
        /// </summary>
        public virtual Guid? ParentFolderUid { get; set; }

        /// <summary>
        /// Sets or gets the location id for this element.
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        /// Gets or sets the document's location code.
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// Gets or sets the document's location name.
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Gets or sets the comments for the update.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string UpdateComment { get; set; }

        /// <summary>
        /// Gets or sets the id of the tenant.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int TenantId { get; set; }

        /// <summary>
        /// Do not serialize the property 'Id'.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeId()
        {
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}:{1}", Title, Uid.ToString());
        }
    }
}