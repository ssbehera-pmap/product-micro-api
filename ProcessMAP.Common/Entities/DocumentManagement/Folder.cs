﻿using System;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class Folder : TreeElement
    {
        /// <summary>
        /// The default name for a root folder.
        /// </summary>
        public const string DEFAULT_ROOT_FOLDER_TITLE = "Root";

        /// <summary>
        /// Folder constructor
        /// </summary>
        public Folder()
            : base()
        { }

        /// <summary>
        /// Gets or sets the breadcrumbs to this folder.
        /// </summary>
        public Breadcrumb[] Breadcrumbs { get; set; }

        /// <summary>
        /// Gets or sets a collection of Folders contained in this folder.
        /// </summary>
        public Subfolder[] Subfolders { get; set; }

        /// <summary>
        /// Gets or sets a collection of Documents contained in this folder.
        /// </summary>
        public Document[] Documents { get; set; }

        /// <summary>
        /// Gets or sets the comments for this folder.
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Gets or sets the status of the tree element.
        /// </summary>
        public FolderStatusType Status { get; set; }

        /// <summary>
        /// Creates a folder to be used as "Root" folder.
        /// </summary>
        /// <returns></returns>
        public static Folder CreateRoot()
        {
            return new Folder()
            {
                Title = Folder.DEFAULT_ROOT_FOLDER_TITLE,
                Id = 0,
                Uid = Guid.Empty
            };
        }
    }
}