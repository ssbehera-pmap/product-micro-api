﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class GenericTree<TKey, TVal> : ExtensionProperty<TKey, TVal>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public override TKey Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public override TVal Description
        {
            get { return base.Description; }
            set { base.Description = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "children")]
        public List<GenericTree<TKey, TVal>> Children { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "toggled")]
        public bool Toggled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "selected")]
        public bool Selected { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GenericTree<TKey, TVal> CreateObject(TVal description)
        {
            return new GenericTree<TKey, TVal>()
            {
                Description = description
            };
        }

        internal void PopulateRootChildren(List<ExtensionProperty<TKey, TVal>>  rootChildren)
        {
            Children = rootChildren.Select(i => new GenericTree<TKey, TVal>() { Id = i.Id, Description = i.Description }).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flatStructure"></param>
        internal void PopulateChildren(List<ExtensionProperty<TKey, TVal>> flatStructure)
        {
            PopulateChildrenRecursive(this, flatStructure);
        }

        private void PopulateChildrenRecursive(GenericTree<TKey, TVal> tree, List<ExtensionProperty<TKey, TVal>> flatStructure)
        {
            tree.Children = flatStructure.Where(i => i.ParentObjectId != null && i.ParentObjectId.Equals(tree.Id)).Select(i => new GenericTree<TKey, TVal>() { Id = i.Id, Description = i.Description }).ToList();

            foreach (var child in tree.Children)
            {
                PopulateChildrenRecursive(child, flatStructure);
            }
        }
    }
}