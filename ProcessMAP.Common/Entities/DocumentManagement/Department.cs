﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ProcessMAP.Common.Entities.DocumentManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class Department : ExtensionProperty<int, string>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public override int Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public override string Description
        {
            get { return base.Description; }
            set { base.Description = value; }
        }
    }
}