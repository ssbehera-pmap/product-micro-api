﻿using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Common.Entities.Interfaces;

namespace ProcessMAP.Common.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class ConsumerContext
    {
        /// <summary>
        /// 
        /// </summary>
        public ConsumerContext() { }

        /// <summary>
        /// 
        /// </summary>
        public int ConsumerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Consumer Consumer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IDatabaseInfo DatabaseInfo { get; set; }
    }
}