﻿using System;
using System.Net;

namespace ProcessMAP.Services.REST.Common
{
    /// <summary>
    /// Represents the status of a published message.
    /// </summary>
    public class MessageStatus
    {
        /// <summary>
        /// Represents the status code of the publish response.
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Represents the resulting error message if the response is not OK.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Represents the exception thrown if any.
        /// </summary>
        public Exception Exception { get; set; }
    }
}