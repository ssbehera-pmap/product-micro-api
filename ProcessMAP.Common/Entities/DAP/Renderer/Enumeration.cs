﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP.Renderer
{
    /// <summary>
    /// Serves as the Column Type enumeration.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ColumnType
    {
        /// <summary>
        /// Represents the columns as a Data type.
        /// </summary>
        Data,

        /// <summary>
        /// Represents the column as a Date type.
        /// </summary>
        Date,

        /// <summary>
        /// Represents the column as a Label type.
        /// </summary>
        Label,

        /// <summary>
        /// Represents the column as a Title type.
        /// </summary>
        Title
    }

    /// <summary>
    /// Serves as the Column Alignment enumeration.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ColumnAlignment
    {
        /// <summary>
        /// Represents the column alignment as Center.
        /// </summary>
        Center,

        /// <summary>
        /// Represents the column alignment as Left.
        /// </summary>
        Left,

        /// <summary>
        /// Represents the column alignment as Right.
        /// </summary>
        Right
    }

    /// <summary>
    /// Serves as the Type of the Source.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SourceType
    {
        /// <summary>
        /// Represents the Source as a type Date.
        /// </summary>
        Date,

        /// <summary>
        /// Represents the Source as a type String.
        /// </summary>
        String
    }
}