﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.CustomDataAnnotations;
using System;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Base class of a DAP entity.
    /// </summary>
    public class DAPEntity
    {
        /// <summary>
        /// Entity constructor
        /// </summary>
        public DAPEntity()
        { }

        /// <summary>
        /// Represents the globally unique identifier of the record.
        /// </summary>
        [RequiredGuid]
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that created the entity.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the username of the user that created the entity.
        /// </summary>
        public string CreatedByUserName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that created the entity.
        /// </summary>
        public string CreatedByFullName { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that updated the entity.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the username of the user that updated the entity.
        /// </summary>
        public string UpdatedByUserName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that updated the entity.
        /// </summary>
        public string UpdatedByFullName { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was updated.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Represents the optional user id of the user that [soft] deleted the record.
        /// </summary>
        public int? DeletedBy { get; set; }

        /// <summary>
        /// Represents the optional date time when the record was [soft] deleted.
        /// </summary>
        public DateTime? DeletedDate { get; set; }
    }
}