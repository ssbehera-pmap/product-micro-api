﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Enumerations;
using System;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the Column entity.
    /// </summary>
    public class Column
    {
        /// <summary>
        /// Represents the Column unique identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the Column Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the Column Order.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Represents the Column Sort Order.
        /// </summary>
        public SortOrder SortOrder { get; set; }

        /// <summary>
        /// Represents the Column Filter.
        /// </summary>
        public Filter Filter { get; set; }

        /// <summary>
        /// Represents the flag that determines if the Column will be displayed.
        /// </summary>
        public bool IsVisible { get; set; }

        public bool Visible { get; set; }

        /// <summary>
        /// Represents the Data Type of the Column.
        /// </summary>
        public DataType DataType { get; set; }

        public ViewColumnType Type { get; set; } = ViewColumnType.Data;

        [BsonRepresentation(BsonType.String)]
        public Guid ControlUid { get; set; }

        /// <summary>
        /// Represents the Data Type of the View Column.
        /// </summary>
        public DataType ViewDataType { get; set; }

        /// <summary>
        /// Represents to allow sorting .
        /// </summary>
        public Boolean AllowSorting { get; set; }

        /// <summary>
        /// ShowInMobile
        /// </summary>
        public bool ShowInMobile { get; set; }
    }
}