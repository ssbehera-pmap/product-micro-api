﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessMAP.Common.Entities.DAP
{
    [BsonIgnoreExtraElements]
    public class DataSourceFilters
    {
        /// <summary>
        /// 
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        public string DataSourceColumnName { get; set; }

        [BsonRepresentation(BsonType.String)]
        public Guid? DataSourceColumnId { get; set; }

        public string DataSourceAction { get; set; }

        public List<string> DataSourceColumnValue { get; set; }
    }
}
