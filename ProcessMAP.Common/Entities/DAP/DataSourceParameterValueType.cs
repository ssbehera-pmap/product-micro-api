﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DataSourceParameterValueType
    {
        /// <summary>
        /// 
        /// </summary>
        None,

        /// <summary>
        /// 
        /// </summary>
        Reference,

        /// <summary>
        /// 
        /// </summary>
        Value
    }
}