﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the entity that manages the View.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class View : DAPEntity
    {
        /// <summary>
        /// Default page size
        /// </summary>
        public const int DefaultPageSize = 25;

        /// <summary>
        /// Represents the Name of the View.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents if the View is the default.
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Represents the unique Consumer identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the Page Size with a default set to 25.
        /// </summary>
        public int PageSize { get; set; } = DefaultPageSize;

        /// <summary>
        /// Represents the unique Form identifier of the View.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid FormUid { get; set; }

        /// <summary>
        /// Represents the unique identifier of the View.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int UserId { get; set; }

        /// <summary>
        /// Represents a flag that determines if the View is visible to everyone.
        /// </summary>
        public bool ShouldDisplayGlobally { get; set; }

        /// <summary>
        /// Represents the Columns on the View.
        /// </summary>
        public List<Column> Columns { get; set; }

        /// <summary>
        /// Represents a flag that determines if the View was originated from the Form.
        /// </summary>
        public bool OriginatedFromForm { get; set; }
    }
}