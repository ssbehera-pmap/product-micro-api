﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using ProcessMAP.Common.Entities.DAP.Authorization;
using ProcessMAP.Common.Enumerations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the main [Form] entity for managing the Dynamic Application Platform.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class FormMeta : DAPEntity
    {
        /// <summary>
        /// Represents the Title of the Form.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Represents the Description of the Form.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// A value representing a unique readable code for the form.
        /// Useful as see for other unique readable codes or IDs.
        /// </summary>
        [Required]
        [AllowedCharacterRange(3, 15)]
        public string Code { get; set; }

        /// <summary>
        /// Represents the Hash of the Form.
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// Represents the Title of the List.
        /// </summary>
        public string ListTitle { get; set; } = "Untitled";

        /// <summary>
        /// Represents the federated Form by Consumer.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the current Form  is a readonly type.
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// Represents the Status of the form.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public StatusFlags? Status { get; set; }

        /// <summary>
        /// Represents the user permissions for this form.
        /// </summary>
        public UserPermissions UserPermissions { get; set; }

        /// <summary>
        /// Represents the sharing definitions for this form.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public IEnumerable<FormSharingDefinition> SharingDefinitions { get; set; }

        /// <summary>
        /// Represents the Icon Path/Class of the Form.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Represents the Icon type of the Form.
        /// </summary>
        public string IconType { get; set; }
    }
}