﻿using ProcessMAP.Common.Entities.Interfaces;
using System;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as the implementation of the notification result.
    /// </summary>
    public class NotifyResult
    {
        /// <summary>
        /// Represents the unique notification result identifier.
        /// </summary>
        public Guid RequestUid { get; set; }

        /// <summary>
        /// Represents a list of email results.
        /// </summary>
        public EmailResult EmailResults { get; set; }

        /// <summary>
        /// Represents a list of sms results.
        /// </summary>
        public SmsResult SMSResults { get; set; }
    }
}