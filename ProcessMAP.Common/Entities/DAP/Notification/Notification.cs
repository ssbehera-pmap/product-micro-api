﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Entities.DAP.Actions;
using ProcessMAP.Common.Enumerations;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// 
    /// </summary>
    public class Notification : DAPEntity
    {
        /// <summary>
        /// NotificationName
        /// </summary>
        public string NotificationName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public StatusFlags? Status { get; set; }

        /// <summary>
        /// TemplateUid
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid TemplateUid { get; set; }

        ///// <summary>
        ///// Gets or sets a collection of actions to be performed in this form.
        ///// </summary>
        //public IEnumerable<Actions.Action> Actions { get; set; }

        /// <summary>
        ///  Gets or sets a collection of conditions to be performed for notification
        /// </summary>
        public IEnumerable<Condition> Conditions { get; set; }

        /// <summary>
        /// Receipients
        /// </summary>
        public Recipients Recipients { get; set; }
    }
}
