﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as the entity for managing the maintenance of the notification entity.
    /// </summary>
    public class Maintenance
    {
        /// <summary>
        /// Represents the notification request that is submitted.
        /// </summary>
        public List<Notifier> Request { get; set; }

        /// <summary>
        /// Represents the notification response to the request.
        /// </summary>
        public List<NotifyResult> Response { get; set; }

        /// <summary>
        /// Represents the UTC instance of the Completed Stage.
        /// </summary>
        public DateTime When { get; set; }

        /// <summary>
        /// Represents the service endpoint.
        /// </summary>
        public string ServiceEndpoint { get; set; }

        /// <summary>
        /// Represents the exception.
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Represents the HTTP Response Message
        /// </summary>
        public HttpResponseMessage HttpResponseMessage { get; set; }
    }
}