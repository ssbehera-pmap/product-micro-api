﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as an implementation of the email.
    /// </summary>
    public class Email
    {
        /// <summary>
        /// Represents the email body.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Represents the email recipient.
        /// </summary>
        public Item From { get; set; }

        /// <summary>
        /// Represents the email subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Represents the email recipients.
        /// </summary>
        public List<Item> To { get; set; }
    }
}