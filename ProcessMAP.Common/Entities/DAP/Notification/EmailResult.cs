﻿namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as the implementation for managing the email results.
    /// </summary>
    public class EmailResult
    {
        /// <summary>
        /// Represents the Email Sent Status.
        /// </summary>
        public bool EmailSent { get; set; }

        /// <summary>
        /// Represents the Email Status Code.
        /// </summary>
        public string StatusCode { get; set; }
    }
}