﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// 
    /// </summary>
    public class Recipients
    {
        /// <summary>
        /// Roles
        /// </summary>
        public List<int> Roles { get; set; }

        /// <summary>
        /// Locations
        /// </summary>
        public List<int> Locations { get; set; }

        /// <summary>
        /// Levels
        /// </summary>
        public List<int> Levels { get; set; }

        /// <summary>
        /// Users
        /// </summary>
        public List<int> Users { get; set; }

        /// <summary>
        /// ControlUIds
        /// </summary>
        public List<Guid> ControlUIds { get; set; }

        /// <summary>
        /// Receipients
        /// </summary>
        [JsonIgnore]
        public List<KeyValuePair<string, string>> AdditionalRecipients { get; set; }

        /// <summary>
        /// Others
        /// </summary>
        public List<DynamicRecipient> Others { get; set; }
    }
    /// <summary>
    /// Recipients
    /// </summary>
    public class DynamicRecipient
    {
        /// <summary>
        /// ControlUid
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid ControlUid { get; set; }
        /// <summary>
        /// ParentControlUid
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? ParentControlUid { get; set; }
        
        ///// <summary>
        ///// ParentControlType
        ///// </summary>
        //public ControlType? ParentControlType { get; set; }

        /// <summary>
        /// DataSourceField
        /// </summary>
        public string DataSourceField { get; set; }
    }
}
