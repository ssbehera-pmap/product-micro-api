﻿using ProcessMAP.Common.Entities.Interfaces;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    public class SMS : ISMS
    {
        public string Body { get; set; }

        public string From { get; set; }

        public List<string> To { get; set; }
    }
}