﻿namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as the implementation of the notifier.
    /// </summary>
    public class Notifier
    {
        /// <summary>
        /// Represents the notifier unique consumer identifier.
        /// </summary>
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the notifier email.
        /// </summary>
        public Email Email { get; set; }

        /// <summary>
        /// Represents the notifier sms.
        /// </summary>
        public SMS SMS { get; set; }
    }
}