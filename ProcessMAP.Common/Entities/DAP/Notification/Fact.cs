﻿using ProcessMAP.Common.Entities.Interfaces;
using System;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as an implementation of IData.
    /// </summary>
    /// <typeparam name="T">Represents the type of the Entity.</typeparam>
    public class Fact<T> : IData
    {
        /// <summary>
        /// Represents the Consumer identifier.
        /// </summary>
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the unique identifier.
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the typed entity.
        /// </summary>
        public T Entity { get; set; }
    }
}