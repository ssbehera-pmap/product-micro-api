﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as the implementation for managing the sms results.
    /// </summary>
    public class SmsResult
    {
        /// <summary>
        /// Represents the SMS Sent Status.
        /// </summary>
        IDictionary<string, bool> SmsResults { get; set; }
    }
}