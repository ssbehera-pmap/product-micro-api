﻿namespace ProcessMAP.Common.Entities.DAP.Notification
{
    /// <summary>
    /// Serves as the email notification item.
    /// </summary>
    public class Item
    {
        /// <summary>
        /// Represents the item name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the item email.
        /// </summary>
        public string Email { get; set; }
    }
}