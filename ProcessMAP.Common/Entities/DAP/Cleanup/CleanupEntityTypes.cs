﻿using System;

namespace ProcessMAP.Common.Entities.DAP.Cleanup
{
    /// <summary>
    /// 
    /// </summary>
    [FlagsAttribute]
    public enum CleanupEntityTypes
    {
        /// <summary>
        /// None selected.
        /// </summary>
        None = 0,

        /// <summary>
        /// The form entity.
        /// </summary>
        Form = 1,

        /// <summary>
        /// The form document/data entity.
        /// </summary>
        Document = 2,

        /// <summary>
        /// The form view entity.
        /// </summary>
        View = 4,

        /// <summary>
        /// All entities
        /// </summary>
        All = Form & Document & View
    }
}