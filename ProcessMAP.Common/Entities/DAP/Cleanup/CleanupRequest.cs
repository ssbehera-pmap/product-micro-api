﻿using System;

namespace ProcessMAP.Common.Entities.DAP.Cleanup
{
    /// <summary>
    /// Represents a request for cleanup of entities that have been marked for deletion.
    /// </summary>
    public class CleanupRequest
    {
        /// <summary>
        /// Represents the entity type(s) to target.
        /// If not specified, all entities will be cleaned regardless of which entity type it is.
        /// </summary>
        public CleanupEntityTypes EntityTypes { get; set; } = CleanupEntityTypes.All;

        /// <summary>
        /// Represents the id of the consumer to target.
        /// If not specified, all data will be cleaned regardless of which consumer it belongs to.
        /// </summary>
        public int? ConsumerId { get; set; }

        /// <summary>
        /// Represents the id of the form to target.
        /// If not specified, all data will be cleaned regardless of which form it belongs to.
        /// </summary>
        public Guid? FormUid { get; set; }

        /// <summary>
        /// Represents the date (UTC) for which older data will be the target.
        /// If not specified, all data will be cleaned regardless of how old it is.
        /// </summary>
        public DateTime? CleanDataOlderThan { get; set; }
    }
}