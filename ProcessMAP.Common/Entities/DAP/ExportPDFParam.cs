﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// ExportPDFParam
    /// </summary>
    public class ExportPDFParam
    {
        /// <summary>
        /// FormUid
        /// </summary>
        public Guid FormUid { get; set; }

        /// <summary>
        /// DocumentUid
        /// </summary>
        public Guid DocumentUid { get; set; }

        /// <summary>
        /// TimeZone
        /// </summary>
        public string TimeZone { get; set; }

        /// <summary>
        /// ExportPDFVisibleControls
        /// </summary>
        public List<ExportPDFVisibleControl> Controls { get; set; }

        /// <summary>
        /// Document
        /// </summary>
        public Data Document { get; set; }
    }

    /// <summary>
    /// ExportPDFVisibleControl
    /// </summary>
    public class ExportPDFVisibleControl
    {
        /// <summary>
        /// ControlUid
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// RepeterControls
        /// </summary>
        public dynamic RepeaterControls { get; set; }
    }
}
