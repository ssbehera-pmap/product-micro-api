﻿using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Represents the search query parameters of a data search.
    /// </summary>
    public class DataSearchRequest
    {
        private const int DEFAULT_RECORD_LIMIT = 10;

        /// <summary>
        /// The contents of the query.
        /// </summary>
        [Required]
        public string Query { get; set; }

        /// <summary>
        /// The unique identifier of the form in question.
        /// </summary>
        [Required]
        public Guid FormUid { get; set; }

        /// <summary>
        /// The id of the consumer the data belongs to.
        /// </summary>
        [JsonIgnore]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents a collection of locations ids to match data created in those locations.
        /// </summary>
        [JsonIgnore]
        public int LocationId { get; set; }

        /// <summary>
        /// Represents a value indicating the number of results to fetch.
        /// </summary>
        public int Limit { get; set; } = DEFAULT_RECORD_LIMIT;

        /// <summary>
        /// Represents a collection of element uids to be searched in this request.
        /// </summary>
        [Required]
        [CollectionRange(minCount: 1)]
        public IEnumerable<Guid> ElementsToSearch { get; set; }

        /// <summary>
        /// UsedDataSources
        /// </summary>
        public List<Lookup> UsedDataSources { get; set; }
    }
}