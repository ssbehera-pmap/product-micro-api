﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    public class Signature : DAPControl, IDAPControl
    {
        /// <summary>
        /// Creates a new instance of the Signature control.
        /// </summary>
        public Signature(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Signature";
            ControlPrefix = "signature";
            ControlType = ControlType.Signature;
            DataType = DataType.String;
            IconName = ControlIconNames.Signature;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;
    }
}
