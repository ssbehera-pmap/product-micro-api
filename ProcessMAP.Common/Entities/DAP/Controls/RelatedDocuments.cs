﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class RelatedDocuments : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultLabel"></param>
        public RelatedDocuments(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Related Data";
            ControlPrefix = "relDocs";
            ControlType = ControlType.RelatedDocuments;
            IconName = ControlIconNames.FileShare;
            DataType = DataType.RelatedDocuments;

            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);
            // Set specific properties
            properties.Add(DAPControlPropertyType.LinkedForm, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.LinkedForm));
            properties.Add(DAPControlPropertyType.LinkedFormDisplayColumns, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.LinkedFormDisplayColumns));
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;
    }
}