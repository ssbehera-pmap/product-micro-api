﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using ProcessMAP.Common.Utilities;
using System.Linq;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class NavigationControl : Button
    {
        /// <summary>
        /// 
        /// </summary>
        public NavigationControl(string defaultLabel = null)
            : base (defaultLabel)
        {
            Name = "Navigation Button";
            ControlPrefix = "nav";
            IconName = ControlIconNames.Generic;

            // Add specific properties
            var dataSource = EnumHelper.ToKeyValuePair(typeof(NavigationControlBootstrapClass)).ToList();
            properties.Add(DAPControlPropertyType.BootstrapClass, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.BootstrapClass, defaultValueDataSource: dataSource, hideFromUser: true));

            // Remove unnecessary properties
            properties.Remove(DAPControlPropertyType.SubLabel);
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;
    }
}