﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Video : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Video(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Video";
            ControlPrefix = "vid";
            ControlType = ControlType.Video;
            DataType = DataType.String;
            IconName = ControlIconNames.Video;

            properties[DAPControlPropertyType.Enabled] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Enabled, hideFromUser: true, dataType: DataType);

            properties.Add(DAPControlPropertyType.URL, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.URL));
        }
    }
}