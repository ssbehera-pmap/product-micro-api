﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Toggle : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Toggle(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Toggle";
            ControlPrefix = "tgl";
            ControlType = ControlType.Toggle;
            DataType = DataType.Boolean;
            IconName = ControlIconNames.Toggle;
            DefaultValue = false;

            // Specific properties
            properties.Add(DAPControlPropertyType.Required, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required));
            properties.Add(DAPControlPropertyType.DefaultValue, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType:DataType));
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;
    }
}