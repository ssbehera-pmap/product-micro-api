﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class AutoComplete : DataSourceControl
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="singleSelect"></param>
        /// <param name="defaultLabel"></param>
        public AutoComplete(string defaultLabel = null, bool singleSelect = true)
           : base(defaultLabel)
        {
            Name = "Auto Complete";
            ControlPrefix = "autoComplete";
            ControlType = singleSelect ? ControlType.SingleAutoComplete : ControlType.MultiAutoComplete;
            IconName = singleSelect ? ControlIconNames.SingleSelctionAutoComplete : ControlIconNames.MultiSelectionAutoComplete;

            // Set specific properties
            properties.Add(DAPControlPropertyType.SingleSelect, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.SingleSelect, singleSelect, hideFromUser: true));
            properties.Add(DAPControlPropertyType.DefaultValue, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType));
            if (singleSelect)
            {
                properties.Add(DAPControlPropertyType.HasDependency, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.HasDependency));
                properties.Add(DAPControlPropertyType.ParentControl, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ParentControl));
                properties.Add(DAPControlPropertyType.ParentControlField, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ParentControlField));
                properties.Add(DAPControlPropertyType.DataSourceField, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DataSourceField));
                properties[DAPControlPropertyType.Autopopulate] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Autopopulate);
                properties[DAPControlPropertyType.DependentColumns] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DependentColumns, hideFromUser: true);
            }
        }
    }
}