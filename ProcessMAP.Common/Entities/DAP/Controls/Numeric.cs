﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Numeric : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Numeric(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Numeric";
            ControlPrefix = "num";
            ControlType = ControlType.Numeric;
            IconName = ControlIconNames.Numeric;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);
            properties[DAPControlPropertyType.MinValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MinValue, int.MinValue, dataType: DataType.Integer);
            properties[DAPControlPropertyType.MaxValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MaxValue, int.MaxValue, dataType: DataType.Integer);

            var dataSource = new List<KeyValuePair<string, string>>();
            foreach (var format in Enum.GetNames(typeof(NumericFormat))) dataSource.Add(new KeyValuePair<string, string>(format.ToLower(), format));
            properties[DAPControlPropertyType.ControlFormat] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ControlFormat, defaultValueDataSource: dataSource);

            CalculateDataType();
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType);
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;

        /// <summary>
        /// Represents the filter comparison operators available for this control.
        /// </summary>
        public IEnumerable<ComparisonOperator> FilterOperators
        {
            get
            {
                return new ComparisonOperator[]
                {
                    ComparisonOperator.Greater,
                    ComparisonOperator.GreaterEq,
                    ComparisonOperator.Less,
                    ComparisonOperator.LessEq,
                    ComparisonOperator.Eq,
                    ComparisonOperator.Ne
                };
            }
        }
    }
}