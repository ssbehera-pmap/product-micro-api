﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ControlType
    {
        /// <summary>
        /// 
        /// </summary>
        None,

        /// <summary>
        /// 
        /// </summary>
        SingleAutoComplete,

        /// <summary>
        /// 
        /// </summary>
        MultiAutoComplete,

        /// <summary>
        /// 
        /// </summary>
        Button,

        /// <summary>
        /// 
        /// </summary>
        Date,

        /// <summary>
        /// 
        /// </summary>
        DropDownList,

        /// <summary>
        /// 
        /// </summary>
        Label,

        /// <summary>
        /// 
        /// </summary>
        MultipleChoice,

        /// <summary>
        /// 
        /// </summary>
        PickList,

        /// <summary>
        /// 
        /// </summary>
        Numeric,

        /// <summary>
        /// 
        /// </summary>
        Section,

        /// <summary>
        /// 
        /// </summary>
        TextArea,

        /// <summary>
        /// 
        /// </summary>
        TextBox,

        /// <summary>
        /// 
        /// </summary>
        Time,

        /// <summary>
        /// 
        /// </summary>
        Toggle,

        /// <summary>
        /// 
        /// </summary>
        SystemGeneratedID,

        /// <summary>
        /// 
        /// </summary>
        FileUpload,

        /// <summary>
        /// 
        /// </summary>
        ActionItem,

        /// <summary>
        /// 
        /// </summary>
        Repeater,

        /// <summary>
        /// RelatedDocuments
        /// </summary>
        RelatedDocuments,

        /// <summary>
        /// ActionItem
        /// </summary>
        MultipleActionItem,

        /// <summary>
        /// Video
        /// </summary>
        Video,

        /// <summary>
        /// Segmented
        /// </summary>
        Segmented,

        /// <summary>
        /// Signature
        /// </summary>
        Signature,

        /// <summary>
        /// QRCode
        /// </summary>
        QRCode,

        /// <summary>
        /// EmbeddedImage
        /// </summary>
        EmbeddedImage
    }
}