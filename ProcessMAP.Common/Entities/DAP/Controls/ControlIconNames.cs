﻿namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public static class ControlIconNames
    {
        /// <summary>
        /// 
        /// </summary>
        public const string Generic = "circle";

        /// <summary>
        /// 
        /// </summary>
        public const string Button = "cursor-touch-o";

        /// <summary>
        /// 
        /// </summary>
        public const string TextBox = "text-input-o";

        /// <summary>
        /// 
        /// </summary>
        public const string TextArea = "text-vector-o";

        /// <summary>
        /// 
        /// </summary>
        public const string Label = "spelling-check-o";

        /// <summary>
        /// 
        /// </summary>
        public const string Numeric = "hash-o";

        /// <summary>
        /// 
        /// </summary>
        public const string Section = "focus-o";

        /// <summary>
        /// 
        /// </summary>
        public const string Date = "calendar-o";

        /// <summary>
        /// 
        /// </summary>
        public const string Time = "clock-o";

        /// <summary>
        /// 
        /// </summary>
        public const string Toggle = "settings-o";

        /// <summary>
        /// 
        /// </summary>
        public const string DropDownList = "list-bullets-o";

        /// <summary>
        /// 
        /// </summary>
        public const string SingleSelctionAutoComplete = "file-view-1-o";

        /// <summary>
        /// 
        /// </summary>
        public const string MultiSelectionAutoComplete = "file-view-2-o";

        /// <summary>
        /// 
        /// </summary>
        public const string MultipleChoice = "checklist-o";

        /// <summary>
        /// 
        /// </summary>
        public const string PickList = "check-box-o";

        /// <summary>
        /// 
        /// </summary>
        public const string FileUpload = "upload-o";

        /// <summary>
        /// 
        /// </summary>
        public const string SystemGeneratedId = "barcode-scan-o";

        /// <summary>
        /// 
        /// </summary>
        public const string ActionItem = "flag-o";

        /// <summary>
        /// 
        /// </summary>
        public const string Repeater = "repeater-o";

        /// <summary>
        /// 
        /// </summary>
        public const string FileShare = "file-share";

        /// <summary>
        /// video icon
        /// </summary>
        public const string Video = "play-circle-o";

        /// <summary>
        /// segmented
        /// </summary>
        public const string Segmented = "tasks-o";

         /// <summary>
        /// 
        /// </summary>
        public const string Signature = "signature";

        /// <summary>
        /// QRCOde
        /// </summary>
        public const string QRCode = "qrcode";

        public const string EmbeddedImage = "EmbeddedImage";
    }
}