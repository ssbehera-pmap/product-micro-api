﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using ProcessMAP.Common.Utilities;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class MultipleChoice : DataSourceControl
    {
        /// <summary>
        /// 
        /// </summary>
        public MultipleChoice(string defaultLabel = null, string appVersion = AppVersion.DefaultVersion)
            : base(defaultLabel)
        {
            Name = "Multiple Choice";
            ControlPrefix = "multi";
            ControlType = ControlType.MultipleChoice;
            IconName = ControlIconNames.MultipleChoice;

            // Set specific properties
            properties[DAPControlPropertyType.NumOfColumns] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.NumOfColumns);
            properties[DAPControlPropertyType.SecurityField] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.SecurityField);
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType.Integer);
        }
    }
}