﻿namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Label : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Label(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Label";
            ControlPrefix = "lbl";
            ControlType = ControlType.Label;
            DataType = DataType.String;
            IconName = ControlIconNames.Label;
        }
    }
}