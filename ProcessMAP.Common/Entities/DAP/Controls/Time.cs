﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Time : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Time(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Time";
            ControlPrefix = "time";
            ControlType = ControlType.Time;
            DataType = DataType.Integer;
            IconName = ControlIconNames.Time;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);

            var dataSource = new List<KeyValuePair<string, string>>();
            foreach (var timeZone in TimeZoneInfo.GetSystemTimeZones()) dataSource.Add(new KeyValuePair<string, string>(timeZone.Id, timeZone.DisplayName));
            properties[DAPControlPropertyType.TimeZone] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.TimeZone, defaultValueDataSource: dataSource, hideFromUser: true);
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType.Seconds);
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;

        /// <summary>
        /// Represents the filter comparison operators available for this control.
        /// </summary>
        public IEnumerable<ComparisonOperator> FilterOperators
        {
            get
            {
                return new ComparisonOperator[]
                {
                    ComparisonOperator.Greater,
                    ComparisonOperator.GreaterEq,
                    ComparisonOperator.Less,
                    ComparisonOperator.LessEq,
                    ComparisonOperator.Eq,
                    ComparisonOperator.Ne
                };
            }
        }
    }
}