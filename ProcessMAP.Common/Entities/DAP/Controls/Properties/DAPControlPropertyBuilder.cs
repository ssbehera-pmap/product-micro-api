﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls.Properties
{
    /// <summary>
    /// 
    /// </summary>
    public static class DAPControlPropertyBuilder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="defaultValue"></param>
        /// <param name="defaultValueDataSource"></param>
        /// <param name="dataType"></param>
        /// <param name="hideFromUser"></param>
        /// <returns></returns>
        public static DAPControlProperty CreateControlProperty(DAPControlPropertyType type, object defaultValue = null, List<KeyValuePair<string, string>> defaultValueDataSource = null, DataType? dataType = null, bool hideFromUser = false)
        {
            DAPControlProperty property;
            switch (type)
            {
                case DAPControlPropertyType.ControlFormat:
                    property = new DAPControlProperty()
                    {
                        Text = "Format",
                        Type = DataType.String,
                        Value = defaultValue,
                        ValueDataSource = defaultValueDataSource,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DataSource:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Data Source",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DataSourceDisplayField:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Data Source Field",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DataSourceValueField:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Data Source Store Field",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Collapsed:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Collapsed",
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Collapsible:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Collapsible",
                        Value = defaultValue ?? true,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Repeatable:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Repeatable",
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.MaxNumOfRepetitions:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Integer,
                        Text = "Number of Repetitions",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.RepeaterCollapsed:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Repeater Collapsed",
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.RepeaterCollapsible:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Repeater Collapsible",
                        Value = defaultValue ?? true,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Enabled:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Enabled",
                        Value = defaultValue ?? true,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Required:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Required",
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Visible:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Visible",
                        Value = defaultValue ?? true,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Label:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Label",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.MaxCharCount:
                    property = new DAPControlProperty()
                    {
                        Text = "Max Character Count",
                        Type = DataType.Integer,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.MaxValue:
                    property = new DAPControlProperty()
                    {
                        Text = "Maximum Value",
                        Type = dataType ?? DataType.Integer,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.MinValue:
                    property = new DAPControlProperty()
                    {
                        Text = "Minimum Value",
                        Type = dataType ?? DataType.Integer,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.SubLabel:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Sub Label",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Title:
                    property = new DAPControlProperty()
                    {
                        Text = "Title",
                        Type = DataType.String,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DefaultValue:
                    property = new DAPControlProperty()
                    {
                        Type = dataType ?? DataType.String,
                        Text = "Default Value",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.TimeZone:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Time Zone",
                        Value = defaultValue,
                        ValueDataSource = defaultValueDataSource,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.SingleSelect:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Single Select",
                        Value = defaultValue ?? true,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.NumOfColumns:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Integer,
                        Text = "Number of Columns",
                        Value = defaultValue ?? 1,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DefaultViewEnabled:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Add to List View",
                        Value = defaultValue ?? false,
                        HideFromUser = true
                    };
                    break;
                case DAPControlPropertyType.BootstrapClass:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Bootstrap Class",
                        Value = defaultValue ?? NavigationControlBootstrapClass.Default.ToString().ToLower(),
                        ValueDataSource = defaultValueDataSource,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.UseCustomDataSource:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Use Custom Data Source",
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.LinkedForm:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Guid,
                        Text = "Form to Link",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.LinkedFormDisplayColumns:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.GuidArray,
                        Text = "Display Columns",
                        Value = defaultValue,
                        ValueDataSource = defaultValueDataSource,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.HasDependency:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Boolean,
                        Text = "Has Dependency",
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.ParentControl:
                    property = new DAPControlProperty()
                    {
                        Text = "Parent Control",
                        Type = DataType.Guid,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.ParentControlField:
                    property = new DAPControlProperty()
                    {
                        Text = "Parent Control Field",
                        Type = DataType.Guid,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DataSourceField:
                    property = new DAPControlProperty()
                    {
                        Text = "Data Source Field",
                        Type = DataType.Guid,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Autopopulate:
                    property = new DAPControlProperty()
                    {
                        Text = "Auto Populate",
                        Type = DataType.Boolean,
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DependentColumns:
                    property = new DAPControlProperty()
                    {
                        Text = "Dependent Columns",
                        Type = DataType.KeyValuePair,
                        Value = defaultValue,
                        ValueDataSource = defaultValueDataSource,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.SecurityField:
                    property = new DAPControlProperty()
                    {
                        Text = "Role Base",
                        Type = DataType.Boolean,
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.ActionItemControls:
                    property = new DAPControlProperty()
                    {
                        Text = "Action Item Controls",
                        Type = DataType.ActionItemControls,
                        Value = defaultValue ?? false,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.ActionItemDisplayColumns:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.GuidArray,
                        Text = "Display Columns",
                        Value = defaultValue,
                        ValueDataSource = defaultValueDataSource,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.URL:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "URL",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.MinimumSelections:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Integer,
                        Text = "Minimum Selections",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.MaximumSelections:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Integer,
                        Text = "Maximum Selections",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.MaximumSegmentsAllowed:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.Integer,
                        Text = "Maximum Segments Allowed",
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                // Commented the below line of code for DAP-2915- Image upload feature.
                case DAPControlPropertyType.ImageSourceType:
                    property = new DAPControlProperty()
                    {
                        Type = DataType.String,
                        Text = "Image Source Type",
                        Value = defaultValue,
                        ValueDataSource = defaultValueDataSource,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.Attachment:
                    property = new DAPControlProperty()
                    {
                        Text = "Attachment",
                        Type = DataType.Files,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                case DAPControlPropertyType.DataSourceFilter:
                    property = new DAPControlProperty()
                    {
                        Text = "DataSourceFilter",
                        Type = DataType.KeyValuePair,
                        Value = defaultValue,
                        HideFromUser = hideFromUser
                    };
                    break;
                default:
                    throw new NotImplementedException($"The DAPControlPropertyType '{type.ToString()}' has not been implemented.");
            }
            return property;
        }
    }
}