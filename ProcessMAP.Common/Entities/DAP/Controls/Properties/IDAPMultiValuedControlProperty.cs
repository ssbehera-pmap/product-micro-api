﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls.Properties
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDAPMultiValuedControlProperty : IDAPControlProperty
    {
        /// <summary>
        /// 
        /// </summary>
        List<KeyValuePair<string, string>> ValueDataSource { get; set; }
    }
}