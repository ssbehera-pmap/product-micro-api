﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP.Controls.Properties
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DAPControlPropertyType
    {
        /// <summary>
        /// 
        /// </summary>
        Label,

        /// <summary>
        /// 
        /// </summary>
        SubLabel,

        /// <summary>
        /// 
        /// </summary>
        Placeholder,

        /// <summary>
        /// 
        /// </summary>
        Enabled,

        /// <summary>
        /// 
        /// </summary>
        Visible,

        /// <summary>
        /// 
        /// </summary>
        DataSource,

        /// <summary>
        /// 
        /// </summary>
        DataSourceDisplayField,

        /// <summary>
        /// 
        /// </summary>
        DataSourceValueField,

        /// <summary>
        /// 
        /// </summary>
        Required,

        /// <summary>
        /// 
        /// </summary>
        ControlFormat,

        /// <summary>
        /// 
        /// </summary>
        MaxCharCount,

        /// <summary>
        /// 
        /// </summary>
        MinValue,

        /// <summary>
        /// 
        /// </summary>
        MaxValue,

        /// <summary>
        /// 
        /// </summary>
        Collapsible,

        /// <summary>
        /// 
        /// </summary>
        Collapsed,

        /// <summary>
        /// 
        /// </summary>
        Title,

        /// <summary>
        /// 
        /// </summary>
        DefaultValue,

        /// <summary>
        /// 
        /// </summary>
        TimeZone,

        /// <summary>
        /// 
        /// </summary>
        SingleSelect,

        /// <summary>
        /// 
        /// </summary>
        NumOfColumns,

        /// <summary>
        /// Represents controls that are members of the Default View.
        /// </summary>
        DefaultViewEnabled,

        /// <summary>
        /// Represents controls that are member of the List screen.
        /// </summary>
        RequiredForList,

        /// <summary>
        /// Represents the bootstrap css class to apply to a control.
        /// </summary>
        BootstrapClass,

        /// <summary>
        /// 
        /// </summary>
        UseCustomDataSource,

        /// <summary>
        /// 
        /// </summary>
        Repeatable,

        /// <summary>
        /// 
        /// </summary>
        MaxNumOfRepetitions,

        /// <summary>
        /// 
        /// </summary>
        RepeaterCollapsed,

        /// <summary>
        /// 
        /// </summary>
        RepeaterCollapsible,

        /// <summary>
        /// 
        /// </summary>
        LinkedForm,

        /// <summary>
        /// 
        /// </summary>
        LinkedFormDisplayColumns,

        /// <summary>
        /// To determine if The selected Data source shall be cascading
        /// </summary>
        HasDependency,

        /// <summary>
        /// Control trigerring the cascading filter on target Data source
        /// </summary>
        ParentControl,

        /// <summary>
        /// Parent Datasource field Name ,which shall be mapped.
        /// </summary>
        ParentControlField,

        /// <summary>
        /// Target Drop down Datasource Field Name 
        /// </summary>
        DataSourceField,

        /// <summary>
        /// Autopopulate
        /// </summary>
        Autopopulate,

        /// <summary>
        /// AutopopulateSourceControl.
        /// </summary>
        DependentFieldName,

        /// <summary>
        /// Auto populate Field
        /// </summary>
        DependentFieldColumn,

        /// <summary>
        /// Is Security Field
        /// </summary>
        SecurityField,

        /// <summary>
        /// 
        /// </summary>
        DependentColumns,

        /// <summary>
        /// Child controls of an ActionItem.
        /// </summary>
        ActionItemControls,

        /// <summary>
        /// The list of control uid which we want to dsiplay on page.
        /// </summary>
        ActionItemDisplayColumns,

        /// <summary>
        /// URL path
        /// </summary>
        URL,

        /// <summary>
        /// minimum number of items to be selected 
        /// </summary>
        MinimumSelections,

        /// <summary>
        /// maximum number of items to be selected 
        /// </summary>
        MaximumSelections,

        /// <summary>
        /// maximum number of segments allowed
        /// </summary>
        MaximumSegmentsAllowed,

        /// <summary>
        ///  Type of Embedded Image to be added.
        /// </summary>
        ImageSourceType,

        // Commented the below line of code for DAP-2915- Image upload feature.
        /// <summary>
        /// Embedded Image Attachment
        /// </summary>

        Attachment,

        /// <summary>
        /// DataSource Filter
        /// </summary>
        DataSourceFilter
    }
}