﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP.Controls.Properties
{
    /// <summary>
    /// 
    /// </summary>
    public class DAPControlProperty : IDAPControlProperty, IDAPMultiValuedControlProperty
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        public DAPControlProperty(string text = null, object value = null)
        {
            Text = text;
            Value = value;
        }

        /// <summary>
        /// 
        /// </summary>
        // DM: APB-79 [Required]
        public string Text { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public DataType Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<KeyValuePair<string, string>> ValueDataSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool HideFromUser { get; set; }
    }
}