﻿namespace ProcessMAP.Common.Entities.DAP.Controls.Properties
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDAPControlProperty
    {
        /// <summary>
        /// 
        /// </summary>
        string Text { get; set; }

        /// <summary>
        /// 
        /// </summary>
        object Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        DataType Type { get; set; }
    }
}