﻿namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class PickList : DataSourceControl
    {
        /// <summary>
        /// 
        /// </summary>
        public PickList(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "PickList";
            ControlPrefix = "pick";
            ControlType = ControlType.PickList;
            IconName = ControlIconNames.PickList;
        }
    }
}