﻿using Newtonsoft.Json;
using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        Guid GroupUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Guid Uid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        object DefaultValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        ControlType ControlType { get; }

        /// <summary>
        /// 
        /// </summary>
        string ControlPrefix { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Dictionary<DAPControlPropertyType, DAPControlProperty> Properties { get; }

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<ControlEvent> Events { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string IconName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        DataType DataType { get; }

        /// <summary>
        /// 
        /// </summary>
        void CalculateDataType();

        /// <summary>
        /// Gets a value indicating of the current controls is a data souce control.
        /// </summary>
        bool IsDataSourceControl { get; }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        bool IsSearchable { get; }

        /// <summary>
        /// Gets a value indicating if the current control is Deprecated.
        /// </summary>
        bool IsDeprecated { get; set; }

        /// <summary>
        /// Name of element in the collection's document
        /// </summary>
        string StoreName { get; set; }
    }
}