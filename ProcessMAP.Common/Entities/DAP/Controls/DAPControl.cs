﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using Newtonsoft.Json;
using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using ProcessMAP.Common.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(typeof(ActionItem), typeof(Button), typeof(DataSourceControl), typeof(Date), typeof(FileUploadControl),
        typeof(Label), typeof(NavigationControl), typeof(Numeric), typeof(Section), typeof(Repeater), typeof(SystemGeneratedId),
        typeof(TextArea), typeof(Textbox), typeof(Time), typeof(Toggle), typeof(Video))]
    public class DAPControl : IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        protected Dictionary<DAPControlPropertyType, DAPControlProperty> properties = new Dictionary<DAPControlPropertyType, DAPControlProperty>()
        {
            { DAPControlPropertyType.Label, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Label) },
            { DAPControlPropertyType.SubLabel, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.SubLabel) },
            { DAPControlPropertyType.Visible, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Visible) },
            { DAPControlPropertyType.Enabled, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Enabled) },
            { DAPControlPropertyType.DefaultViewEnabled, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultViewEnabled) }
        };

        /// <summary>
        /// 
        /// </summary>
        protected DAPControl() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultLabel"></param>
        protected DAPControl(string defaultLabel = null)
            : this()
        {
            if (!string.IsNullOrWhiteSpace(defaultLabel))
            {
                properties[DAPControlPropertyType.Label].Text = defaultLabel;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid GroupUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object DefaultValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<ControlEvent> Events { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [BsonDictionaryOptions(DictionaryRepresentation.ArrayOfArrays)]
        public Dictionary<DAPControlPropertyType, DAPControlProperty> Properties
        {
            get { return properties; }
            set { properties = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public ControlType ControlType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ControlPrefix { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IconName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public DataType DataType { get; set; }

        /// <summary>
        /// Gets a value indicating of the current controls is a data souce control.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public bool IsDataSourceControl
        {
            get
            {
                return Properties.Any(p => p.Key == DAPControlPropertyType.DataSource);
            }
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public virtual bool IsSearchable => true;

        /// <summary>
        /// 
        /// </summary>
        public void CalculateDataType()
        {
            if (ControlType == ControlType.Numeric)
            {
                if (properties.ContainsKey(DAPControlPropertyType.ControlFormat))
                {
                    DAPControlProperty formatProperty = properties[DAPControlPropertyType.ControlFormat];

                    if (formatProperty != null && formatProperty.Value != null)
                    {
                        var numericFormat = EnumHelper.StringToEnum<NumericFormat>(formatProperty.Value.ToString());
                        DataType = (numericFormat == NumericFormat.Integer) ? DataType.Integer : DataType.Decimal;
                        return;
                    }
                    DataType = DataType.Integer;
                }   
            }
        }

        /// <summary>
        /// Gets a value indicating if the current control is Deprecated.
        /// </summary>
        public bool IsDeprecated { get; set; }

        /// <summary>
        /// Name of element in the collection's document
        /// </summary>
        public string StoreName { get; set; }
    }
}