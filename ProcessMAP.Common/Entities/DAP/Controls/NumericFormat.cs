﻿namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public enum NumericFormat
    {
        /// <summary>
        /// 
        /// </summary>
        Integer,

        /// <summary>
        /// 
        /// </summary>
        Decimal,

        /// <summary>
        /// 
        /// </summary>
        Percentage,

        /// <summary>
        /// 
        /// </summary>
        Currency
    }
}