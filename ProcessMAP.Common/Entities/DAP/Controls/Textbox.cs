﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Textbox : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Textbox(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "TextBox";
            ControlPrefix = "txt";
            ControlType = ControlType.TextBox;
            DataType = DataType.String;
            IconName = ControlIconNames.TextBox;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);
            properties[DAPControlPropertyType.MaxCharCount] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MaxCharCount, 100);
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType);

            var dataSource = new List<KeyValuePair<string, string>>();
            foreach (var format in Enum.GetNames(typeof(TextboxFormat))) dataSource.Add(new KeyValuePair<string, string>(format.ToLower(), format));
            properties[DAPControlPropertyType.ControlFormat] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ControlFormat, defaultValueDataSource: dataSource);
        }
    }
}