﻿using System;
using System.Collections.Generic;
using System.Text;
using ProcessMAP.Common.Entities.DAP.Controls.Properties;
namespace ProcessMAP.Common.Entities.DAP.Controls
{
    public class EmbeddedImage : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultLabel"></param>
        /// 
        public EmbeddedImage(string defaultLabel = null)
           : base(defaultLabel)
        {
            Name = "EmbeddedImage";
            ControlPrefix = "EmbeddedImage";
            ControlType = ControlType.EmbeddedImage;
            DataType = DataType.Files;
            IconName = ControlIconNames.EmbeddedImage;

            
            properties.Add(DAPControlPropertyType.URL, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.URL, hideFromUser: true, dataType: DataType));

            properties[DAPControlPropertyType.Enabled] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Enabled, hideFromUser: true, dataType: DataType);

            // Commented the below line of code for DAP-2915- Image upload feature.
            properties.Add(DAPControlPropertyType.Attachment, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Attachment, hideFromUser: true, dataType: DataType));


            var dataSource = new List<KeyValuePair<string, string>>();
            foreach (var format in Enum.GetNames(typeof(EmbeddedImageFormat))) dataSource.Add(new KeyValuePair<string, string>(format, format));
            properties.Add(DAPControlPropertyType.ImageSourceType, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ImageSourceType, defaultValueDataSource: dataSource));
        }
    }
}
