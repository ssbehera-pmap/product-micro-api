﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    ///  This control allows the upload of files.
    /// </summary>
    public class FileUploadControl : DAPControl, IDAPControl
    {
        /// <summary>
        /// Creates a new instance of the FileUpload control.
        /// </summary>
        public FileUploadControl(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "File Upload";
            ControlPrefix = "fileUpload";
            ControlType = ControlType.FileUpload;
            DataType = DataType.Files;
            IconName = ControlIconNames.FileUpload;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;
    }
}