﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class TextArea : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public TextArea(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Text Area";
            ControlPrefix = "textarea";
            ControlType = ControlType.TextArea;
            DataType = DataType.String;
            IconName = ControlIconNames.TextArea;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);
            properties[DAPControlPropertyType.MaxCharCount] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MaxCharCount, 4000);
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType:DataType);
        }
    }
}