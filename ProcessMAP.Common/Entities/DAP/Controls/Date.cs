﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Date : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Date(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Date";
            ControlPrefix = "date";
            ControlType = ControlType.Date;
            DataType = DataType.DateTime;
            IconName = ControlIconNames.Date;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);
            properties[DAPControlPropertyType.MinValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MinValue, DateTime.Parse("1900/01/01"), dataType: DataType.DateTime);
            properties[DAPControlPropertyType.MaxValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MaxValue, DateTime.Parse("2100/01/01"), dataType: DataType.DateTime);
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType);
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;

        /// <summary>
        /// Represents the filter comparison operators available for this control.
        /// </summary>
        public IEnumerable<DateComparisonOperator> FilterOperators
        {
            get
            {
                return new DateComparisonOperator[]
                {
                    DateComparisonOperator.After,
                    DateComparisonOperator.Before,
                    DateComparisonOperator.Custom,
                    DateComparisonOperator.Last30Days,
                    DateComparisonOperator.LastMonth,
                    DateComparisonOperator.ThisMonth,
                    DateComparisonOperator.ThisWeek,
                    DateComparisonOperator.Today
                };
            }
        }
    }
}