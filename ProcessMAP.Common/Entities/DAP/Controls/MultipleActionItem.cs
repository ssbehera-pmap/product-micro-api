﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using ProcessMAP.Common.Utilities;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// ActionItems
    /// </summary>
    public class MultipleActionItem : DAPControl, IDAPControl
    {
        /// <summary>
        /// Creates a new instance of the ActionItem control.
        /// </summary>
        public MultipleActionItem(IEnumerable<dynamic> dataSourceTypes, string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Action Items";
            ControlPrefix = "multipleActionItem";
            ControlType = ControlType.MultipleActionItem;
            IconName = ControlIconNames.ActionItem;
            DataType = DataType.MultipleActionItem;


            var actionItemTitleControl = new Textbox(CommonConstants.ActionItemTitleLabel);
            actionItemTitleControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.ActionItemTitleLabel;
            actionItemTitleControl.Properties[DAPControlPropertyType.Required].Value = true;
            actionItemTitleControl.Properties[DAPControlPropertyType.Visible].Value = true;

            var actionItemDescControl = new TextArea(CommonConstants.ActionItemDescriptionTitle);
            actionItemDescControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.ActionItemDescriptionTitle;
            actionItemDescControl.Properties[DAPControlPropertyType.Required].Value = true;
            actionItemDescControl.Properties[DAPControlPropertyType.MaxCharCount].Value = 2000;
            actionItemDescControl.Properties[DAPControlPropertyType.Visible].Value = true;

            var priorityLevelControl = new AutoComplete(CommonConstants.PriorityLevelTitle, true);
            priorityLevelControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.PriorityLevelTitle;
            priorityLevelControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.ActionItemPriorityLevels.ToString();
            priorityLevelControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.Name;
            priorityLevelControl.Properties[DAPControlPropertyType.Visible].Value = true;
            priorityLevelControl.Properties[DAPControlPropertyType.DefaultValue].Value = 1;

            var actionItemDueDateControl = new Date(CommonConstants.ActionItemDueDateTitle);
            actionItemDueDateControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.ActionItemDueDateTitle;
            actionItemDueDateControl.Properties[DAPControlPropertyType.Required].Value = true;
            actionItemDueDateControl.Properties[DAPControlPropertyType.Visible].Value = true;

            //var primaryOwnersTitle = "Owners";
            //var primaryOwnersControl = new AutoComplete(primaryOwnersTitle, false);
            //primaryOwnersControl.Properties[DAPControlPropertyType.Label].Value = primaryOwnersTitle;
            var primaryOwnersControl = new AutoComplete(CommonConstants.Owners, false);
            primaryOwnersControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.Owners;
            primaryOwnersControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.Users.ToString();
            //primaryOwnersControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = "FullName";
            primaryOwnersControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.FullName;
            primaryOwnersControl.Properties[DAPControlPropertyType.Required].Value = true;
            primaryOwnersControl.Properties[DAPControlPropertyType.Visible].Value = true;

            var notifyOwnersControl = new Toggle(CommonConstants.NotifyOwnersImmediatelyTitle);
            notifyOwnersControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.NotifyOwnersImmediatelyTitle;
            notifyOwnersControl.Properties[DAPControlPropertyType.Visible].Value = true;

            var assignedByControl = new AutoComplete(CommonConstants.AssignedByTitle, true);
            assignedByControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.AssignedByTitle;
            assignedByControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.Users.ToString();
            //assignedByControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = "FullName";
            assignedByControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.FullName;
            assignedByControl.Properties[DAPControlPropertyType.Required].Value = true;
            assignedByControl.Properties[DAPControlPropertyType.DefaultValue].Value = "@@CurrentUser";
            assignedByControl.Properties[DAPControlPropertyType.Visible].Value = true;

            var requiredVerificationControl = new Toggle(CommonConstants.RequiredVerificationTitle);
            requiredVerificationControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.RequiredVerificationTitle;
            requiredVerificationControl.Properties[DAPControlPropertyType.DefaultValue].Value = false;
            requiredVerificationControl.Properties[DAPControlPropertyType.Enabled].Value = true;
            requiredVerificationControl.Properties[DAPControlPropertyType.Visible].Value = true;

            var verifyUserControl = new AutoComplete(CommonConstants.VerifyUsersTitle, false);
            verifyUserControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.VerifyUsersTitle;
            verifyUserControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.Users.ToString();
            //verifyUserControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = "FullName";
            verifyUserControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.FullName;
            verifyUserControl.Properties[DAPControlPropertyType.Required].Value = true;
            verifyUserControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var actionItemStatusControl = new DropDownList(CommonConstants.ActionItemStatusTitle);
            actionItemStatusControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.ActionItemStatusTitle;
            actionItemStatusControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.ActionItemStatus.ToString();
            actionItemStatusControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.Name;
            actionItemStatusControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            actionItemStatusControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var actionTakenControl = new TextArea(CommonConstants.ActionTakenTitle);
            actionTakenControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.ActionTakenTitle;
            actionTakenControl.Properties[DAPControlPropertyType.Required].Value = false;
            actionTakenControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            actionTakenControl.Properties[DAPControlPropertyType.Visible].Value = false;
            actionTakenControl.Properties[DAPControlPropertyType.MaxCharCount].Value = 2000;

            var completedByControl = new AutoComplete(CommonConstants.ActionItemCompletedByTitle, true);
            completedByControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.ActionItemCompletedByTitle;
            completedByControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.Users.ToString();
            //completedByControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = "FullName";
            completedByControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.FullName;
            completedByControl.Properties[DAPControlPropertyType.Required].Value = false;
            completedByControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            completedByControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var actionItemcompletedDateControl = new Date(CommonConstants.ActionItemcompletedDateTitle);
            actionItemcompletedDateControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.ActionItemcompletedDateTitle;
            actionItemcompletedDateControl.Properties[DAPControlPropertyType.Required].Value = false;
            actionItemcompletedDateControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            actionItemcompletedDateControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var verificationStatusControl = new DropDownList(CommonConstants.VerificationStatusTitle);
            verificationStatusControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.VerificationStatusTitle;
            verificationStatusControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.VerificationStatus.ToString();
            verificationStatusControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.Name;
            verificationStatusControl.Properties[DAPControlPropertyType.Required].Value = false;
            verificationStatusControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            verificationStatusControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var verificationPerformedControl = new DropDownList(CommonConstants.VerificationPerformedTitle);
            verificationPerformedControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.VerificationPerformedTitle;
            verificationPerformedControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.VerificationPerformed.ToString();
            verificationPerformedControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.Name;
            verificationPerformedControl.Properties[DAPControlPropertyType.Required].Value = false;
            verificationPerformedControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            verificationPerformedControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var verifiedByControl = new AutoComplete(CommonConstants.VerifiedByTitle, false);
            verifiedByControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.VerifiedByTitle;
            verifiedByControl.Properties[DAPControlPropertyType.DataSource].Value = DataSourceType.Users.ToString();
            //verifiedByControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = "FullName";
            verifiedByControl.Properties[DAPControlPropertyType.DataSourceDisplayField].Value = CommonConstants.FullName;
            verifiedByControl.Properties[DAPControlPropertyType.Required].Value = false;
            verifiedByControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            verifiedByControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var verificationDateControl = new Date(CommonConstants.VerificationDateTitle);
            verificationDateControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.VerificationDateTitle;
            verificationDateControl.Properties[DAPControlPropertyType.Required].Value = false;
            verificationDateControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            verificationDateControl.Properties[DAPControlPropertyType.Visible].Value = false;

            var commentsControl = new TextArea(CommonConstants.CommentsTitle);
            commentsControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.CommentsTitle;
            commentsControl.Properties[DAPControlPropertyType.Required].Value = false;
            commentsControl.Properties[DAPControlPropertyType.Enabled].Value = false;
            commentsControl.Properties[DAPControlPropertyType.Visible].Value = false;
            commentsControl.Properties[DAPControlPropertyType.MaxCharCount].Value = 2000;

            var fileUploadControl = new FileUploadControl(CommonConstants.UploadFileTitle);
            fileUploadControl.Properties[DAPControlPropertyType.Label].Value = CommonConstants.UploadFileTitle;
            fileUploadControl.Properties[DAPControlPropertyType.Visible].Value = true;

            var actionItemControls = new List<DAPControl>()
            {
                actionItemTitleControl,
                actionItemDescControl,
                priorityLevelControl,
                actionItemDueDateControl,
                primaryOwnersControl,
                notifyOwnersControl,
                assignedByControl,
                requiredVerificationControl,
                verifyUserControl,
                actionItemStatusControl,
                actionTakenControl,
                completedByControl,
                actionItemcompletedDateControl,
                verificationStatusControl,
                verificationPerformedControl,
                verifiedByControl,
                verificationDateControl,
                commentsControl,
                fileUploadControl
            };
            actionItemControls.ForEach(c =>
            {
                c.Properties[DAPControlPropertyType.Label].HideFromUser = true;
                c.Properties[DAPControlPropertyType.SubLabel].HideFromUser = true;
            });
            properties = new Dictionary<DAPControlPropertyType, DAPControlProperty>()
            {
                { DAPControlPropertyType.Label, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Label) },
                { DAPControlPropertyType.SubLabel, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.SubLabel) },
                { DAPControlPropertyType.Required, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required) },
                { DAPControlPropertyType.Visible, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Visible) },
                { DAPControlPropertyType.Enabled, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Enabled) },
                { DAPControlPropertyType.DefaultViewEnabled, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultViewEnabled) },
                { DAPControlPropertyType.ActionItemDisplayColumns, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ActionItemDisplayColumns) },
                { DAPControlPropertyType.ActionItemControls, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ActionItemControls,actionItemControls) }
            };
        }
    }
}
