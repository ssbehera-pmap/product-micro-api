﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using ProcessMAP.Common.Utilities;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// The Repeater control.
    /// </summary>
    public class Repeater : Section
    {
        /// <summary>
        /// 
        /// </summary>
        public Repeater(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Repeater";
            ControlPrefix = CommonConstants.Repeater;
            IconName = ControlIconNames.Repeater;
            DataType = DataType.Repeater;

            // Set specific properties
            properties[DAPControlPropertyType.RepeaterCollapsed] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.RepeaterCollapsed);
            properties[DAPControlPropertyType.RepeaterCollapsible] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.RepeaterCollapsible);
            properties[DAPControlPropertyType.MaxNumOfRepetitions] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MaxNumOfRepetitions);
            properties[DAPControlPropertyType.DefaultViewEnabled] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultViewEnabled);
            properties[DAPControlPropertyType.SubLabel] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.SubLabel);

            // Set action controls
            var addButton = new NavigationControl();
            addButton.Properties[DAPControlPropertyType.Label].Value = "+ Add";
            addButton.Properties[DAPControlPropertyType.BootstrapClass].Value = NavigationControlBootstrapClass.Primary.ToString().ToLower();
            ActionControls = new NavigationControl[] { addButton };
        }
    }
}