﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// This control represents an auto generated system id.
    /// </summary>
    public class SystemGeneratedId : DAPControl, IDAPControl
    {
        /// <summary>
        /// Creates a new instance of the SystemGeneratedId control.
        /// </summary>
        public SystemGeneratedId(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "System Generated ID";
            ControlPrefix = "sysId";
            ControlType = ControlType.SystemGeneratedID;
            DataType = DataType.String;
            IconName = ControlIconNames.SystemGeneratedId;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required, hideFromUser: true);
            properties[DAPControlPropertyType.Enabled] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Enabled, false, hideFromUser: true);
        }

        /// <summary>
        /// Represents the filter comparison operators available for this control.
        /// </summary>
        public IEnumerable<ComparisonOperator> FilterOperators
        {
            get
            {
                return new ComparisonOperator[]
                {
                    ComparisonOperator.Contain,
                    ComparisonOperator.DoesNotContain,
                    ComparisonOperator.Eq,
                    ComparisonOperator.Ne
                };
            }
        }
    }
}