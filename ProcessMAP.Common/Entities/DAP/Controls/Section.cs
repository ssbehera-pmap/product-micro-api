﻿using ProcessMAP.Common.CustomDataAnnotations;
using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class Section : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Section(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Section";
            ControlPrefix = "section";
            ControlType = ControlType.Section;
            DataType = DataType.Section;
            IconName = ControlIconNames.Section;

            // Set specific properties
            properties = new Dictionary<DAPControlPropertyType, DAPControlProperty>()
            {
                { DAPControlPropertyType.Title, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Label) },
                { DAPControlPropertyType.Visible, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Visible) },
                { DAPControlPropertyType.Collapsed, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Collapsed) },
                { DAPControlPropertyType.Collapsible, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Collapsible) }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [CollectionRange(minCount: 1, maxCount: 1000)]
        public IEnumerable<DAPControl> Controls { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<NavigationControl> ActionControls { get; set; }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;

        /// <summary>
        /// Gets a value indicating if the current control is system defined.
        /// </summary>
        public bool IsSystemDefined { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public void RemoveInnerControlsFromDefaultView()
        {
            if (Controls != null && Controls.Any())
            {
                foreach (var control in Controls)
                {
                    if (control.Properties.ContainsKey(DAPControlPropertyType.DefaultViewEnabled))
                    {
                        control.Properties[DAPControlPropertyType.DefaultViewEnabled].Value = false;
                    }
                }
            }
        }
    }
}