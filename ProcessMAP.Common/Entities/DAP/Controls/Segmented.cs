﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    public class Segmented : DataSourceControl
    {
        public Segmented(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Segmented Button";
            ControlPrefix = "segmented";
            ControlType = ControlType.Segmented;
            IconName = ControlIconNames.Segmented;

            // Set specific properties
            properties[DAPControlPropertyType.MaximumSegmentsAllowed] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MaximumSegmentsAllowed, 5);
            properties[DAPControlPropertyType.MinimumSelections] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MinimumSelections, 0);
            properties[DAPControlPropertyType.MaximumSelections] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.MaximumSelections, 1);

            //properties[DAPControlPropertyType.SecurityField] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.SecurityField);
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType.Integer);
        }
    }
}
