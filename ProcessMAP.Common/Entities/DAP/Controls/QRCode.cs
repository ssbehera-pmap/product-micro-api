﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    public class QRCode : DAPControl, IDAPControl
    {

        public QRCode(string defaultLabel = null) : base(defaultLabel)
        {
            Name = "QR Code";
            ControlPrefix = "qrcode";
            ControlType = ControlType.QRCode;
            DataType = DataType.String;
            IconName = ControlIconNames.QRCode;

            // Set specific properties
            properties[DAPControlPropertyType.Required] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required);

        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;

    }
}
