﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class ControlEvent
    {
        /// <summary>
        /// Represents the Action id of the Field.
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the UIDs of the dependent controls.
        /// </summary>    
        public IEnumerable<Guid> DependentControlUids { get; set; }

        /// <summary>
        /// Represents the UIDs of the controls.
        /// </summary>    
        public IEnumerable<Guid> ControlUids { get; set; }

        /// <summary>
        /// Represents the event name.
        /// </summary>      
        public EventType EventType { get; set; }

        /// <summary>
        /// Represents the callback on the event.
        /// </summary>   
        public string Callback { get; set; }

        /// <summary>
        /// Represents the DatasourceName.
        /// </summary>  
        public string DataSourceName { get; set; }

        /// <summary>
        /// Represents the data source with parameters.
        /// </summary>  
        public DataSource DataSource { get; set; }
    }
}