﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TextboxFormat
    {
        /// <summary>
        /// 
        /// </summary>
        Email,

        /// <summary>
        /// 
        /// </summary>
        Password,

        /// <summary>
        /// 
        /// </summary>
        PhoneNumber,

        /// <summary>
        /// 
        /// </summary>
        Url
    }

    public enum VideoLinkFormat
    {
        /// <summary>
        /// 
        /// </summary>
        Url
    }
}