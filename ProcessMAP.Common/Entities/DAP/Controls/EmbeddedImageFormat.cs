﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace ProcessMAP.Common.Entities.DAP.Controls
{
    [JsonConverter(typeof(StringEnumConverter))]

    public enum EmbeddedImageFormat
    {
        /// <summary>
        /// 
        /// </summary>
        URL,

        /// <summary>
        /// 
        /// </summary>
        Attachment
    }
}
