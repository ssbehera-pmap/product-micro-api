﻿using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(typeof(NavigationControl))]
    public class Button : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Button(string defaultLabel = null)
            : base(defaultLabel)
        {
            Name = "Button";
            ControlPrefix = "btn";
            ControlType = ControlType.Button;
            DataType = DataType.String;
            IconName = ControlIconNames.Button;

            // Remove unnecessary properties
            properties.Remove(DAPControlPropertyType.DefaultViewEnabled);
        }

        /// <summary>
        /// Gets a value indicating if the current control is searchable.
        /// </summary>
        public override bool IsSearchable => false;
    }
}