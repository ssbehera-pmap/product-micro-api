﻿using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Entities.DAP.Controls.Properties;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(typeof(AutoComplete), typeof(DropDownList), typeof(MultipleChoice), typeof(PickList))]
    public class DataSourceControl : DAPControl, IDAPControl
    {
        /// <summary>
        /// 
        /// </summary>
        public DataSourceControl(string defaultLabel = null)
            : base(defaultLabel)
        {
            DataType = DataType.Integer;

            // Set specific properties
            properties.Add(DAPControlPropertyType.Required, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Required));
            properties.Add(DAPControlPropertyType.DataSource, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DataSource));
            properties.Add(DAPControlPropertyType.DataSourceDisplayField, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DataSourceDisplayField));
            properties.Add(DAPControlPropertyType.UseCustomDataSource, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.UseCustomDataSource));
            properties.Add(DAPControlPropertyType.DataSourceFilter, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DataSourceFilter));
        }
    }
}