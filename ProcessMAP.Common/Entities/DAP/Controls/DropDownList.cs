﻿using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using ProcessMAP.Common.Utilities;

namespace ProcessMAP.Common.Entities.DAP.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class DropDownList : DataSourceControl
    {
        /// <summary>
        /// 
        /// </summary>
        public DropDownList(string defaultLabel = null, string appVersion = AppVersion.DefaultVersion)
            : base(defaultLabel)
        {
            Name = "Drop Down List";
            ControlPrefix = "ddl";
            ControlType = ControlType.DropDownList;
            IconName = ControlIconNames.DropDownList;

            properties.Add(DAPControlPropertyType.HasDependency, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.HasDependency));
            properties.Add(DAPControlPropertyType.ParentControl, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ParentControl));
            properties.Add(DAPControlPropertyType.ParentControlField, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.ParentControlField));
            properties.Add(DAPControlPropertyType.DataSourceField, DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DataSourceField));
            properties[DAPControlPropertyType.Autopopulate] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.Autopopulate);
            properties[DAPControlPropertyType.DependentColumns] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DependentColumns, hideFromUser: true);
            properties[DAPControlPropertyType.DefaultValue] = DAPControlPropertyBuilder.CreateControlProperty(DAPControlPropertyType.DefaultValue, hideFromUser: false, dataType: DataType);
        }
    }
}