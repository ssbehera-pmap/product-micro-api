﻿using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Object to encapsulate all parameters needed to fetch the form data from the database.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class FormDataRequest
    {
        /// <summary>
        /// The unique identifier of the form in question.
        /// </summary>
        public Guid? FormUid { get; set; }

        /// <summary>
        /// The unique identifier of the form document in question.
        /// </summary>
        public Guid? DocumentUid { get; set; }

        /// <summary>
        /// The unique identifier of the form view in question.
        /// </summary>
        public Guid? ViewUid { get; set; }

        /// <summary>
        /// The unique identifier of the Action item collection.
        /// </summary>
        public Guid? ActionItemUid { get; set; }
        /// <summary>
        /// The id of the consumer the data belongs to.
        /// </summary>
        [Required]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents a collection of locations ids to match data created in those locations.
        /// </summary>
        [CollectionRange(minCount: 1)]
        public IEnumerable<int> LocationIds { get; set; }

        /// <summary>
        /// Represents the id of the level for which to rollup all the data.
        /// </summary>
        public int? LevelId { get; set; }

        /// <summary>
        /// Represents the id of the user. Useful to load the form views.
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Represents a value indicating the index at which to start matching results.
        /// </summary>
        public int? Skip { get; set; }

        /// <summary>
        /// Represents a value indicating the number of results to fetch.
        /// </summary>
        public int? Take { get; set; }

        /// <summary>
        /// Represents the optional date from time reference of the returned Form Data.
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Represents the optional date to time reference of the returned Form Data.
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Represents the Application Type of the consumer that generated the request.
        /// </summary>
        public Enumerations.ApplicationType ApplicationType { get; set; }

        /// <summary>
        /// Represents the lists of sorting expression applied in UI.
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// Represents the lists of sorting expression applied in UI.
        /// </summary>
        public string SortOrder { get; set; }

        /// <summary>
        /// Represens the optional date when data was last synchronized.
        /// </summary>
        public DateTime? LastSynced { get; set; }

        /// <summary>
        /// Represents if the Location Column is  .
        /// </summary>

        public bool? AddLocationToList { get; set; }

        /// <summary>
        /// Represents the location control id.
        /// </summary>
        public Guid? LocationControlUid { get; set; }
    }
}