﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LogicalOperator
    {
        /// <summary>
        /// 
        /// </summary>
        And,

        /// <summary>
        /// 
        /// </summary>
        Or
    }
}