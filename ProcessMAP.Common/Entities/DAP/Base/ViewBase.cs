﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.Base
{
    /// <summary>
    /// Serves as the base View, for both Desktop and Mobile consumers.
    /// </summary>
    public class ViewBase
    {
        /// <summary>
        /// Represents the unique identifier of the View.
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the Name of the View.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the Description of the View.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Represents the Type of the View.
        /// </summary>
        public ViewType Type { get; set; }

        /// <summary>
        /// Represents if the View is the default.
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Represents the unique identifier of the user who created the View.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Represents the User Name of the Creator of the View.
        /// </summary>
        public string CreatedByUserName { get; set; }

        /// <summary>
        /// Represents the Full Name of the Creator of the View.
        /// </summary>
        public string CreatedByFullName { get; set; }

        /// <summary>
        /// Represents the date/time that the View was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Represents the unique identifier of the user who updated the View.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Represents the User Name of the Updater of the View.
        /// </summary>
        public string UpdatedByUserName { get; set; }

        /// <summary>
        /// Represents the Full Name of the Updater of the View.
        /// </summary>
        public string UpdatedByFullName { get; set; }

        /// <summary>
        /// Represents the date/time that the View was updated.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Represents the Application unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid AppUid { get; set; }

        /// <summary>
        /// Represents the unique Form identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid FormUid { get; set; }
    }
}