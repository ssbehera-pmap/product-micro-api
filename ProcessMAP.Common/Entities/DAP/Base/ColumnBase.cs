﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.Base
{
    /// <summary>
    /// Serves as the base class for Column.
    /// </summary>
    public class ColumnBase
    {
        /// <summary>
        /// Represents the unique identifier of the Column.
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the Name of the Column.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the Type of the Column.
        /// </summary>
        public ViewColumnType Type { get; set; }

        /// <summary>
        /// Represents the type of the data being bound to the column.
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        /// Represents the unique View identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid ViewUid { get; set; }

        /// <summary>
        /// Represents the Order of the Column.
        /// </summary>
        public int Order { get; set; }
    }
}