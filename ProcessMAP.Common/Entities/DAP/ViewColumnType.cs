﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ViewColumnType
    {
        /// <summary>
        /// Indicates that the value should be displayed in a non-interactive way.
        /// </summary>
        Data,

        /// <summary>
        /// Indicates that the value should be displayed as a hyperlink.
        /// </summary>
        Hyperlink
    }
}