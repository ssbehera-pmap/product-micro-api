﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the entity responsible for managing access key.
    /// </summary>
    public class AccessKey
    {
        /// <summary>
        /// Represents the unique Access Key identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the collection of role identifiers.
        /// </summary>
        public IEnumerable<int> Roles { get; set; }

        /// <summary>
        /// Represents the action associated with the access key.
        /// </summary>
        public Actions.ActionType Action { get; set; }

        /// <summary>
        /// Represents the collection of control identifiers.
        /// </summary>
        public IEnumerable<Guid> Controls { get; set; }

        /// <summary>
        /// Represents the user type associated with the form.
        /// </summary>
        public IEnumerable<Enumerations.UserTypes> UserTypes { get; set; }
    }
}