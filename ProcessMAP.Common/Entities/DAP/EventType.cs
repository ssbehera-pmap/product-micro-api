﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EventType
    {
        /// <summary>
        /// 
        /// </summary>
        None,

        /// <summary>
        /// 
        /// </summary>
        OnChange,

        /// <summary>
        /// 
        /// </summary>
        OnSelect,

        /// <summary>
        /// 
        /// </summary>
        OnSearch,

        /// <summary>
        /// 
        /// </summary>
        OnCascading,

        /// <summary>
        /// 
        /// </summary>
        OnGridLoad,

        /// <summary>
        /// 
        /// </summary>
        AddClick,

        /// <summary>
        /// 
        /// </summary>
        OnDataLoad,

        /// <summary>
        /// 
        /// </summary>
        OnDataSave,

        /// <summary>
        /// 
        /// </summary>
        OnSave,

        /// <summary>
        /// 
        /// </summary>
        OnAttachmentDelete,

        /// <summary>
        /// 
        /// </summary>
        OnSuccess,

        /// <summary>
        /// 
        /// </summary>
        OnSubmit
    }
}