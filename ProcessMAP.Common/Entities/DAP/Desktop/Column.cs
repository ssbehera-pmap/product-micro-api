﻿using System;

namespace ProcessMAP.Common.Entities.DAP.Desktop
{
    /// <summary>
    /// Serves as the Column, as defined for the Desktop consumer.
    /// </summary>
    public class Column : Base.ColumnBase
    {
        /// <summary>
        /// Represents the Width of the Column.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Represents the property that will be used for binding of the View.
        /// </summary>
        public string BindProperty { get; set; }

        /// <summary>
        /// Represents a flag that determines if the Column is Visible.
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Represents a flag that determines if the Control will allow exporting.
        /// </summary>
        public bool AllowExport { get; set; }

        /// <summary>
        /// Represents a flag that determines if the Column will allow sorting.
        /// </summary>
        public bool AllowSorting { get; set; }

        /// <summary>
        /// Represents a flag that determines if the Column will allow filtering.
        /// </summary>
        public bool AllowFiltering { get; set; }

        /// <summary>
        /// Represents a flag that determines if the Column will allow grouping.
        /// </summary>
        public bool AllowGrouping { get; set; }

        /// <summary>
        /// Represents the navigation path that can be used for the Column.
        /// </summary>
        public string NavigateURL { get; set; }

        /// <summary>
        /// Represents the Sorting of the Column.
        /// </summary>
        public string Sorting { get; set; }

        /// <summary>
        /// Represents the URL of the Column Image.
        /// </summary>
        public string ImageURL { get; set; }

        /// <summary>
        /// Represents the Column Filter.
        /// </summary>
        public Filter Filter { get; set; }

        /// <summary>
        /// Represents the Sort of the Column.
        /// </summary>
        public Sort Sort { get; set; }

        /// <summary>
        /// Represents the Control Uid to which this Column binds its data to.
        /// </summary>
        public Guid ControlUid { get; set; }
    }
}