﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.Desktop
{
    /// <summary>
    /// Serves as the View that will be used for Desktop consumption.
    /// </summary>
    public class View : Base.ViewBase
    {
        /// <summary>
        /// Represents the Filters of the View.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public IEnumerable<Filter> Filters { get; set; }

        /// <summary>
        /// Represents the Columns of the View.
        /// </summary>
        public List<Column> Columns { get; set; }
    }
}