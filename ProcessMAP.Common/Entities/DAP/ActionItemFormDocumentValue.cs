﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    public class ActionItemFormDocumentValue : FormDocumentValue
    {
        /// <summary>
        /// ActionItemKey
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string ActionItemKey { get; set; }
    }
}
