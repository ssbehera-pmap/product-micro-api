﻿namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves ass the Layout entity of the Form.
    /// </summary>
    public class Layout
    {
        /// <summary>
        /// Represents the X-Coordinate of the Card. X is equivalent to [left].
        /// </summary>
        public int x { get; set; }

        /// <summary>
        /// Represents the Y-Coordinate of the Card. Y is equivalent to [top].
        /// </summary>
        public int y { get; set; }

        /// <summary>
        /// Represents the Width of the Card.
        /// </summary>
        public int w { get; set; }

        /// <summary>
        /// Represents the Height of the Card.
        /// </summary>
        public int h { get; set; }
    }
}