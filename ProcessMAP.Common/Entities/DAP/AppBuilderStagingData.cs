﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// DatumData
    /// </summary>
    public class AppBuilderStagingData
    {
        /// <summary>
        /// FormUid
        /// </summary>
        public Guid FormUid { get; set; }

        /// <summary>
        /// FormUid
        /// </summary>

        public Guid DocumentUid { get; set; }

        /// <summary>
        /// FormUid
        /// </summary>

        public Guid ParentControlUid { get; set; }

        /// <summary>
        /// FormUid
        /// </summary>
        public Guid ControlUid { get; set; }

        /// <summary>
        /// FormUid
        /// </summary>
        public int ModuleId { get; set; }
        
        /// <summary>
        /// EntityType
        /// </summary>
        public string EntityType { get; set; }
        
        /// <summary>
        /// EntityType
        /// </summary>
        public string DataType { get; set; }
        /// <summary>
        /// ScreenName
        /// </summary>
        public string ScreenName { get; set; }

        /// <summary>
        /// Schema Name
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// Table Name
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Key (Sql Column Name)
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// CreatedBy
        /// </summary>
        public int CreatedBy { get; set; }
    }

    /// <summary>
    /// AppBuilderStagingReferenceData
    /// </summary>
    public class AppBuilderStagingReferenceData
    {
        /// <summary>
        /// FormUid
        /// </summary>
        public Guid DocumentUid { get; set; }

        /// <summary>
        /// FormUid
        /// </summary>
        public Guid ParentControlUid { get; set; }

        /// <summary>
        /// FormUid
        /// </summary>
        public Guid ControlUid { get; set; }

        /// <summary>
        /// EntityType
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// EntityType
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// DataSequence
        /// </summary>
        public int DataSequence { get; set; }

        /// <summary>
        /// Schema Name
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// Table Name
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Key (Sql Column Name)
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// AppBuilderStaging
        /// </summary>
        public class AppBuilderStaging
        {
            /// <summary>
            /// AppBuilderStagingData
            /// </summary>
            public List<AppBuilderStagingData> AppBuilderStagingData { get; set; }

            /// <summary>
            /// AppBuilderStagingReferenceData
            /// </summary>
            public List<AppBuilderStagingReferenceData> AppBuilderStagingReferenceData { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public List<ProcessMAP.Common.Entities.FileUpload.File> Files { get; set; }

            /// <summary>
            /// ProcessingSP
            /// </summary>
            public string ProcessingSP { get; set; }
        }
    }
}
