﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace ProcessMAP.Common.Entities.DAP.Authorization
{
    /// <summary>
    /// The DAP user permissions.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Permissions : int
    {
        /// <summary>
        /// No permission.
        /// </summary>
        None = 0,

        /// <summary>
        /// Permission to share forms.
        /// </summary>
        [Description("Share Form")]
        CanShareForm = 750001,

        /// <summary>
        /// Permission to create forms.
        /// </summary>
        [Description("Create Form")]
        CanCreateForm = 750002,

        /// <summary>
        /// Permission to delete forms.
        /// </summary>
        [Description("Delete Form")]
        CanDeleteForm = 750003,

        /// <summary>
        /// Permission to update forms.
        /// </summary>
        [Description("Update Form")]
        CanUpdateForm = 750004,

        /// <summary>
        /// Permission to read/view forms.
        /// </summary>
        [Description("Read Form")]
        CanReadForm = 750005,

        /// <summary>
        /// Permission to create form data/documents.
        /// </summary>
        [Description("Create Data")]
        CanCreateData = 750007,

        /// <summary>
        /// Permission to delete form data/documents.
        /// </summary>
        [Description("Delete Data")]
        CanDeleteData = 750008,

        /// <summary>
        /// Permission to update form data/documents.
        /// </summary>
        [Description("Update Data")]
        CanUpdateData = 750009,

        /// <summary>
        /// Permission to read/view form data/documents.
        /// </summary>
        [Description("Read Data")]
        CanReadData = 750010
    }
}