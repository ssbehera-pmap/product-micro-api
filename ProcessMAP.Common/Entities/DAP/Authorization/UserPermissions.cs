﻿using ProcessMAP.Common.Enumerations;
using ProcessMAP.Security.Authorization;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Authorization
{
    /// <summary>
    /// 
    /// </summary>
    public class UserPermissions : Dictionary<Permissions, bool>
    {
        /// <summary>
        /// 
        /// </summary>
        public UserPermissions()
            : base()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userSystemPermissionsMask"></param>
        public UserPermissions(int userSystemPermissionsMask)
            : this()
        {
            SetSystemPermissions(userSystemPermissionsMask);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userPermissionsMask"></param>
        public void SetSystemPermissions(int userPermissionsMask)
        {
            base[Permissions.CanCreateData] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanCreateData, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanCreateForm] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanCreateForm, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanDeleteData] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanDeleteData, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanDeleteForm] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanDeleteForm, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanReadData] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanReadData, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanReadForm] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanReadForm, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanShareForm] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanShareForm, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanUpdateData] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanUpdateData, (int)Module.DAP, userPermissionsMask);
            base[Permissions.CanUpdateForm] = AuthorizationTools.CheckUserHasPermission((int)Permissions.CanUpdateForm, (int)Module.DAP, userPermissionsMask);
        }
    }
}