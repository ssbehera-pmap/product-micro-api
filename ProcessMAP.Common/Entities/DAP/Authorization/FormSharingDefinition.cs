﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.Authorization
{
    /// <summary>
    /// Represents the sharing definition of a form.
    /// </summary>
    public class FormSharingDefinition
    {
        /// <summary>
        /// The unique identifier of the sharing definition.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// The unique identifier of the form.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        [RequiredGuid]
        public Guid FormUid { get; set; }

        /// <summary>
        /// A collection of user ids the form will be shared with.
        /// </summary>
        public IEnumerable<int> Users { get; set; }

        /// <summary>
        /// A collection of location ids the form will be shared with.
        /// </summary>
        public IEnumerable<int> Locations { get; set; }

        /// <summary>
        /// A collection of level ids the form will be shared with.
        /// </summary>
        public IEnumerable<int> Levels { get; set; }

        /// <summary>
        /// A collection of role ids the form will be shared with.
        /// </summary>
        public IEnumerable<int> Roles { get; set; }

        /// <summary>
        /// A value representing the permissions mask for the specified user and module.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int PermissionsMask { get; set; }

        /// <summary>
        /// A collection of user permissions and their values.
        /// </summary>
        [Required]
        public SharingPermissions Permissions { get; set; }
    }
}