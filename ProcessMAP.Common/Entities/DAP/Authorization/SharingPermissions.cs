﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProcessMAP.Common.Entities.DAP.Authorization
{
    /// <summary>
    /// 
    /// </summary>
    public class SharingPermissions
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "form")]
        public IEnumerable<FormSharingPermissions> Form { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "data")]
        public IEnumerable<DataSharingPermissions> Data { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FormSharingPermissions
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Create Form")]
        create,

        /// <summary>
        /// 
        /// </summary>
        [Description("Read Form")]
        read,

        /// <summary>
        /// 
        /// </summary>
        [Description("Update Form")]
        update,

        /// <summary>
        /// 
        /// </summary>
        [Description("Delete Form")]
        delete,

        /// <summary>
        /// 
        /// </summary>
        [Description("Share Form")]
        share
    }

    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DataSharingPermissions
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Create Data")]
        create,

        /// <summary>
        /// 
        /// </summary>
        [Description("Read Data")]
        read,

        /// <summary>
        /// 
        /// </summary>
        [Description("Update Data")]
        update,

        /// <summary>
        /// 
        /// </summary>
        [Description("Delete Data")]
        delete
    }
}