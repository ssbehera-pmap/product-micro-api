﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the Template entity.
    /// </summary>
    public class Template : ITemplate, ICacheableEntity
    {
        /// <summary>
        /// Represents the consumer identifier where the template is associated with.
        /// </summary>
        public int? ConsumerId { get; set; }

        /// <summary>
        /// Represents the identifier of the user who created the template.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Represents the date when the template was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Represents the definition of the template.
        /// </summary>
        [Required]
        public string Definition { get; set; }

        /// <summary>
        /// Represents the description of the template.
        /// </summary>
        /// <summary>
        public string Description { get; set; }

        /// <summary>
        /// Represents the E-mail.
        /// </summary>
        /// <summary>
        public string EmailSubject { get; set; }

        /// <summary>
        /// Represents the template activity flag.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Represents the date/time the template was cached.
        /// </summary>
        public DateTime LastCached { get; set; }

        /// <summary>
        /// Represents the template name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Represents the template unique identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// SystemTemplateUid
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? SystemTemplateUid { get; set; }
        /// <summary>
        /// Represents the optional template updated user id.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Represents the optional template updated date.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Badges
        /// </summary>
        public List<Badge> Badges { get; set; }
    }
}