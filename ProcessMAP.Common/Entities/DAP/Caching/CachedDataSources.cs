﻿using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Entities.DAP.DataSources;
using ProcessMAP.Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Common.Entities.DAP.Caching
{
    /// <summary>
    /// Singleton to cache data sources with two keys, TenantId and LocationId.
    /// </summary>
    public class CachedDataSources : TwoKeyDictionary<int, int, Dictionary<string, DataSourceTable>>
    {
        /// <summary>
        /// Lazy, thread-safe way to access the instance of the singleton.
        /// </summary>
        private static readonly Lazy<CachedDataSources> instance = new Lazy<CachedDataSources>(() => new CachedDataSources());

        /// <summary>
        /// 
        /// </summary>
        private int? ExpirationInMinutes;

        /// <summary>
        /// Private constructor
        /// </summary>
        private CachedDataSources() { }

        /// <summary>
        /// 
        /// </summary>
        public static CachedDataSources Instance
        {
            get { return instance.Value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expirationInMinutes"></param>
        public void SetExpiration(int expirationInMinutes)
        {
            ExpirationInMinutes = expirationInMinutes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        public void Remove(int key1, int key2)
        {
            if (!ContainsKey(key1, key2))
                return;

            Clear(key1, key2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <param name="values"></param>
        public void Set(int key1, int key2, Dictionary<string, DataSourceTable> values)
        {
            if (values != null)
            {
                var utcNow = DateTime.UtcNow;
                PmapLogger<CachedDataSources>.DebugFormat("Adding DataSources with keys [{0}, {1}] @ {2}.", key1, key2, utcNow);
                values.Values.AsParallel().ForAll(v => v.LastCached = utcNow);
                base[key1, key2] = values;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="value"></param>
        public void Set(int key1, int key2, string dataSourceName, DataSourceTable value)
        {
            if (value != null)
            {
                var utcNow = DateTime.UtcNow;
                PmapLogger<CachedDataSources>.DebugFormat("Adding DataSource with keys [{0}, {1}] for type [{2}] @ {3}.", key1, key2, dataSourceName, utcNow);
                value.LastCached = utcNow;

                if (!ContainsKey(key1, key2))
                    base[key1, key2].Add(dataSourceName, value);
                else
                    base[key1, key2] = new Dictionary<string, DataSourceTable>() { { dataSourceName, value } };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <param name="dataSourceName"></param>
        /// <returns></returns>
        public DataSourceTable Get(int key1, int key2, string dataSourceName)
        {
            if (CheckIsExpired(key1, key2, dataSourceName))
                return null;

            var result = default(DataSourceTable);

            if (ContainsKey(key1, key2))
                result = base[key1, key2][dataSourceName];

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public Dictionary<string, DataSourceTable> Get(int key1, int key2)
        {
            if (CheckIsExpired(key1, key2))
                return null;

            if (ContainsKey(key1, key2))
                return base[key1, key2];

            return null;
        }

        private bool CheckIsExpired(int key1, int key2, string dataSourceName = null)
        {
            if (!ContainsKey(key1, key2))
                return true;

            if (!ExpirationInMinutes.HasValue)
                return true;

            var values = new List<DataSourceTable>();

            if (!string.IsNullOrWhiteSpace(dataSourceName))
                values.Add(base[key1, key2][dataSourceName]);
            else
                values.AddRange(base[key1, key2].Values);

            foreach (var value in values)
            {
                if (value != null)
                {
                    var utcNow = DateTime.UtcNow;
                    var cacheDelta = utcNow - value.LastCached;
                    if (cacheDelta.TotalMinutes > ExpirationInMinutes)
                    {
                        PmapLogger<CachedDataSources>.DebugFormat("Hey, as of {0}, DataSources with keys [{1}, {2}] have expired. Let's remove them.", utcNow, key1, key2);
                        Remove(key1, key2);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}