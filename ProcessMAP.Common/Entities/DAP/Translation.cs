﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the entity for managing Files for uploading and downloading.
    /// </summary>
    public class TranslationList
    {
        /// <summary>
        /// Represents the List of Key and Translations 
        /// <summary>
        public IDictionary<string, object> Translations { get; set; }
    }
}