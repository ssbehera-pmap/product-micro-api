﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP.Actions
{
    /// <summary>
    /// Represents the definition of an action on a form.
    /// </summary>
    public class Action
    {
        /// <summary>
        /// Gets or sets a value indicating the unique identifier of the action.
        /// </summary>
        [RequiredGuid]
        [JsonProperty("uid")]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Gets or sets a collection of subactions to execute.
        /// </summary>
        [Required]
        // DM: APB-69: [CollectionRange(minCount: 1)] // DM: APB-69:
        [CollectionRange(minCount: 0)]
        [JsonProperty("actions")]
        public IEnumerable<Subaction> Actions { get; set; }

        /// <summary>
        /// Gets or sets a collection of conditions to test for the action.
        /// </summary>
        [Required]
        [CollectionRange(minCount: 1)]
        [JsonProperty("conditions")]
        public IEnumerable<Condition> Conditions { get; set; }

        /// <summary>
        /// IsSystemDefined
        /// </summary>
        public bool IsSystemDefined { get; set; } = false;
    }
}