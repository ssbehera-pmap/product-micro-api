﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP.Actions
{
    /// <summary>
    /// Represents the definition of a subaction.
    /// </summary>
    public class Subaction
    {
        /// <summary>
        /// Gets or set the unique identifier for the subaction.
        /// </summary>
        [RequiredGuid]
        [JsonProperty("uid")]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Gets or sets the action type to execute.
        /// </summary>
        [Required]
        [JsonProperty("action")]
        public ActionType Action { get; set; }

        /// <summary>
        /// Gets or sets a collection of unique identifiers that correspond to the controls involved in the subaction.
        /// </summary>
        [Required]
        [CollectionRange(minCount: 1)]
        [JsonProperty("elements")]
        public IEnumerable<Guid> Elements { get; set; }

        /// <summary>
        /// Gets or sets the value to set in the subaction.
        /// This property is optional and should be used in conjunction with the 'Set' ActionType.
        /// </summary>
        [JsonProperty("value")]
        public dynamic Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the themplate to be sent in a "Notify" action.
        /// </summary>
        [JsonProperty("templateUid")]
        [BsonRepresentation(BsonType.String)]
        public Guid TemplateUid { get; set; }

        /// <summary>
        /// Gets or sets a collection of roles from which users will become the recipients of the notification.
        /// </summary>
        [JsonProperty("roleRecipients")]
        public IEnumerable<int> RoleRecipients { get; set; }
    }
}