﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace ProcessMAP.Common.Entities.DAP.Actions
{
    /// <summary>
    /// Available action types.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ActionType
    {
        /// <summary>
        /// The Hide action type.
        /// </summary>
        [Description("Hide")]
        Hide,

        /// <summary>
        /// The Show action type.
        /// </summary>
        [Description("Show")]
        Show,

        /// <summary>
        /// The Enable action type.
        /// </summary>
        [Description("Enable")]
        Enable,

        /// <summary>
        /// The Disable action type.
        /// </summary>
        [Description("Disable")]
        Disable,

        ///// <summary>
        ///// The Set action type.
        ///// </summary>
        //Set,

        /// <summary>
        /// The Require action type.
        /// </summary>
        [Description("Require")]
        Require,

        /// <summary>
        /// The Optional action type.
        /// </summary>
        [Description("Optional")]
        Optional,

        /// <summary>
        /// Represents the Set action type.
        /// </summary>
        [Description("Set")]
        Set
    }
}