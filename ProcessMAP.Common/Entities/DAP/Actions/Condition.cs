﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP.Actions
{
    /// <summary>
    /// Represents a condition for an action to be executed.
    /// </summary>
    public class Condition
    {
        /// <summary>
        /// Gets or sets the unique identifier for this condition.
        /// </summary>
        [RequiredGuid]
        [JsonProperty("uid")]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Gets or sets the unique identifier for the control
        /// </summary>
        [RequiredGuid]
        [JsonProperty("control")]
        [BsonRepresentation(BsonType.String)]
        public Guid Control { get; set; }

        /// <summary>
        /// Gets or sets the type of comparison operator to be used in this condition.
        /// </summary>
        [Required]
        [JsonProperty("comparison")]
        public ComparisonOperator Comparison { get; set; }

        /// <summary>
        /// Gets or sets the value for the comparison.
        /// </summary>
        //DM: APB-69 [Required]
        [JsonProperty("value")]
        public dynamic Value { get; set; }

        /// <summary>
        /// Gets or sets the logical operator to be used if a previous condition exists.
        /// This property is optional (for example, if only one condition exists).
        /// </summary>
        [JsonProperty("operator")]
        public LogicalOperator? Operator { get; set; }
    }
}