﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// ActionItemRemove
    /// </summary>
    public class ActionItemRemove
    {
        /// <summary>
        /// Uids
        /// </summary>
        public List<Guid> Uids { get; set; }

        /// <summary>
        /// UserID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// IsForm
        /// </summary>
        public bool IsForm { get; set; }
    }
}
