﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    public class Lookup
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool  IsSystemDefined { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LookupResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, dynamic> SystemDefined { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, dynamic> CustomDefined { get; set; }
    }
}
