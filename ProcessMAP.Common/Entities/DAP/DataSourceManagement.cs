﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.Entities.DAP.Controls;
using ProcessMAP.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    public class DataSourceManagement
    {
        /// <summary>
        /// Represents the unique Custom Data Source identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the Consumer unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int? ConsumerId { get; set; }

        /// <summary>
        /// Represents the unique Name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Columns
        /// </summary>
        public IEnumerable<DataSourceColumn> Columns { get; set; }

        /// <summary>
        /// ColumnValue
        /// </summary>
        public IEnumerable<DataSourceValues> ColumnValues { get; set; }

        /// <summary>
        /// Represents the order to which the records will be listed.
        /// </summary>
        public SortOrder SortOrder { get; set; }

        /// <summary>
        /// Represents the flag to determine if the record is active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that created the entity.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was created.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that updated the entity.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was updated.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// DeletedBy
        /// </summary>
        public int? DeletedBy { get; set; }
        /// <summary>
        /// DeletedDate
        /// </summary>
        public DateTime? DeletedDate { get; set; }

    }
    [BsonIgnoreExtraElements]
    public class DataSourceColumn
    {
        /// <summary>
        /// Represents the unique Custom Data Source identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the unique Name.
        /// </summary>
        public string ColumnName { get; set; }

        ///// <summary>
        ///// ReferenceId
        ///// </summary>
        //[BsonRepresentation(BsonType.String)]
        //public Guid? ReferenceId { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }
    }
    [BsonIgnoreExtraElements]
    public class DataSourceValues
    {
        /// <summary>
        /// Represents the unique DataSource ColumnValue identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        [BsonRepresentation(BsonType.String)]
        public Guid DatasourceId { get; set; }
        /// <summary>
        /// Represents the Consumer unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int? ConsumerId { get; set; }
        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        ///// <summary>
        ///// Gets or sets the id of the user that created the entity.
        ///// </summary>
        //public int? CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the DateTime object in UTC when the entity was created.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the id of the user that updated the entity.
        ///// </summary>
        //public int? UpdatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the DateTime object in UTC when the entity was updated.
        ///// </summary>
        //public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// DeletedBy
        /// </summary>
        public int? DeletedBy { get; set; }
        /// <summary>
        /// DeletedDate
        /// </summary>
        public DateTime? DeletedDate { get; set; }


        public IEnumerable<DataSourceColumnValue> Values { get; set; }
    }
    [BsonIgnoreExtraElements]
    public class DataSourceColumnValue
    {
        /// <summary>
        /// Represents the unique DataSource ColumnValue identifier.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid ColumnUid { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

    }
}
