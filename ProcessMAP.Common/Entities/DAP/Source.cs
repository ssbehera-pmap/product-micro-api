﻿using Newtonsoft.Json;
using ProcessMAP.Common.Entities.DAP.Renderer;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the origin of column information.
    /// </summary>
    public class Source
    {
        /// <summary>
        /// Represents the data source-field to bind to.
        /// </summary>
        public string Bind { get; set; }

        /// <summary>
        /// Represents the Type of the column.
        /// </summary>
        public SourceType Type { get; set; }

        /// <summary>
        /// Represents the Column unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid ColumnUid { get; set; }
    }
}