﻿using System;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// BadgeVersion
    /// </summary>
    public class BadgeVersion : Badge
    {
        /// <summary>
        /// BackupBy
        /// </summary>
        public int BackupBy { get; set; }
        /// <summary>
        /// BackupDate
        /// </summary>
        public DateTime BackupDate { get; set; }
    }
}
