﻿using System;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// TemplateVersion
    /// </summary>
    public class TemplateVersion : Template
    {
        /// <summary>
        /// BackupBy
        /// </summary>
        public int BackupBy { get; set; }
        /// <summary>
        /// BackupDate
        /// </summary>
        public DateTime BackupDate { get; set; }
    }
}
