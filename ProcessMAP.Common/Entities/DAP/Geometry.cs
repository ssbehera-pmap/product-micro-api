﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the entity for managing the geometric layout.
    /// </summary>
    public class Geometry
    {
        /// <summary>
        /// Represents the geometric row.
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// Represents the geometric Width.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Represents the Column unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid ColumnUid { get; set; }
    }
}