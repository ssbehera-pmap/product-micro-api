﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Export Form Model
    /// </summary>
    /// 
    public class FormExport
    {
        /// <summary>
        /// 
        /// </summary>
        public Form FormDefinition { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<View> Views { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Template> Templates { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public IEnumerable<CustomDataSource> CustomDataSources { get; set; }


        public IEnumerable<DataSourceManagement> CustomDataSources { get; set; }

        /// <summary>
        /// ExportTemplate
        /// </summary>
        public ExportTemplate ExportTemplate { get; set; }

        /// <summary>
        /// shows current version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// To Show Icon after import
        /// </summary>
        public string IconUrl { get; set; }
    }
}
