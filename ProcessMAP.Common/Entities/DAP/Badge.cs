﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.Enumerations;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the Badge entity.
    /// </summary>
    public class Badge : ICacheableEntity
    {
        /// <summary>
        /// Represents the badge unique identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the consumer identifier where the badge is associated with.
        /// </summary>
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the badge activity flag.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Represents the date/time the badge was cached.
        /// </summary>
        public DateTime LastCached { get; set; }

        /// <summary>
        /// Represents the badge name.
        /// </summary>
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// Represents the unique template identifier.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid TemplateUid { get; set; }        

        /// <summary>
        /// Represents the unique control identifier.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? ControlUid { get; set; }

        /// <summary>
        /// Represents the Tyoe
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public BadgeType Type { get; set; }

        ///// <summary>
        ///// Represents the identifier of the user who created the badge.
        ///// </summary>
        //public int CreatedBy { get; set; }

        ///// <summary>
        ///// Represents the date when the badge was created.
        ///// </summary>
        //public DateTime CreatedDate { get; set; }

        ///// <summary>
        ///// Represents the optional badge updated user id.
        ///// </summary>
        //public int? UpdatedBy { get; set; }

        ///// <summary>
        ///// Represents the optional badge updated date.
        ///// </summary>
        //public DateTime? UpdatedDate { get; set; }

        ///// <summary>
        ///// Represents the value of the badge.
        ///// </summary>
        //public string Value { get; set; }

        ///// <summary>
        ///// Represents the dislay value of the badge.
        ///// </summary>
        //public string Display { get; set; }
    }
}