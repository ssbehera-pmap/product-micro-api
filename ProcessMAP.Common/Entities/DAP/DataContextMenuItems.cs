﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DataContextMenuItems
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Add")]
        Add,

        /// <summary>
        /// 
        /// </summary>
        [Description("Modify")]
        Edit,

        /// <summary>
        /// 
        /// </summary>
        [Description("Delete")]
        Delete
    }
}