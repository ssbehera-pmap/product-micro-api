﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// ModuleDataSourceValueLabel
    /// </summary>
    public class ModuleDataSourceValueLabel
    {
        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Lebel
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// Gets or sets the id of the user that created the entity.
        /// </summary>
        public int? CreatedBy { get; set; }
        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was created.
        /// </summary>
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// Gets or sets the id of the user that updated the entity.
        /// </summary>
        public int? UpdatedBy { get; set; }
        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was updated.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }
        /// <summary>
        /// IsCustomDataSource
        /// </summary>
        public bool IsCustomDataSource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ModuleDataSourceValueLabels> Fields { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ModuleDataSourceValueLabels
    {
        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Lebel
        /// </summary>
        public string Label { get; set; }
    }
}
