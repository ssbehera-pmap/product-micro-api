﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DataType : int
    {
        /// <summary>
        /// 
        /// </summary>
        String = 0,

        /// <summary>
        /// 
        /// </summary>
        Integer = 1,

        /// <summary>
        /// 
        /// </summary>
        Decimal = 2,

        /// <summary>
        /// 
        /// </summary>
        DateTime = 3,

        /// <summary>
        /// 
        /// </summary>
        Boolean = 4,

        /// <summary>
        /// 
        /// </summary>
        Guid = 5,

        /// <summary>
        /// Represents a type for a Section control.
        /// </summary>
        Section = 6,

        /// <summary>
        /// Represents a type for a Repeater control.
        /// </summary>
        Repeater = 7,

        /// <summary>
        /// Represents a type that references a Data Source.
        /// </summary>
        DataSource = 8,

        /// <summary>
        /// Represents a type that references a Data Source.
        /// </summary>
        Files = 9,

        /// <summary>
        /// Represents the data type for an ActionItem control.
        /// </summary>
        ActionItem = 10,

        /// <summary>
        /// Represent the data type for Time Control
        /// </summary>
        Time = 11,

        /// <summary>
        /// Represents a data type for an array of guids.
        /// </summary>
        GuidArray,

        /// <summary>
        /// Represents an DAP action type.
        /// </summary>
        ActionTypeEnum,

        /// <summary>
        /// Represents a data type concerned with a collection of integers.
        /// </summary>
        IntegerArray,

        /// <summary>
        /// Represents a data type that signals the 
        /// </summary>
        RelatedDocuments,
 
        /// <summary>
        /// Represents a data type that type of KeyValuePair 
        /// </summary>
        KeyValuePair,
        
        /// <summary>
        /// Controls of an action item
        /// </summary>
        ActionItemControls,
        
        /// <summary>
        /// Represents the data type for an ActionItem control.
        /// </summary>
        MultipleActionItem,
        
        /// <summary>
        /// Represents the data type for an Time control.
        /// </summary>
        Seconds,
        
        /// <summary>
        /// Represents the data type for a single sytem data sourcce control.
        /// </summary>
        SingleSystemDatasource,
        
        /// <summary>
        /// Represents the data type for an single custom data source control.
        /// </summary>
        SingleCustomDatasource,

        /// <summary>
        /// Represents the data type for a mult sytem data sourcce control.
        /// </summary>
        MultiSystemDatasource,

        /// <summary>
        /// Represents the data type for a multi custom data sourcce control.
        /// </summary>
        MultiCustomDatasource,
        
        /// <summary>
        /// Represents the data type for a Video Control.
        /// </summary>
        VideoLinks,

        /// <summary>
        /// Represents the data type for an Embedded Image Control
        /// </summary>
        EmbeddedImage
    }
}