﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.Entities.WorkflowEngine;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// This class represents the data for a particular form.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class DynData : Data, IWorkflowEntity
    {

        [BsonExtraElements]
        public BsonDocument Metadata { get; set; }

        public static Data CreateFrom(DynData dyndata)
        {
            Data data = new Data()
            {
                ActionItemId = dyndata.ActionItemId,
                ApplicationType = dyndata.ApplicationType,
                AttachmentCount = dyndata.AttachmentCount,
                ConsumerId = dyndata.ConsumerId, // [JsonIgnore]
                ContextMenuItems = dyndata.ContextMenuItems,
                CreatedBy = dyndata.CreatedBy, // DAPEntity
                CreatedByFullName = dyndata.CreatedByFullName, // DAPEntity
                CreatedByUserName = dyndata.CreatedByUserName, // DAPEntity
                CreatedDate = dyndata.CreatedDate, // DAPEntity
                CreatedPoint = dyndata.CreatedPoint,
                DeletedBy = dyndata.DeletedBy, // DAPEntity
                DeletedDate =dyndata.DeletedDate, // DAPEntity
                FormUid = dyndata.FormUid,
                FormVersion = dyndata.FormVersion,
                IsSynced = dyndata.IsSynced,
                LocationId = dyndata.LocationId, // [JsonIgnore]
                RecordCount = dyndata.RecordCount,
                RelatedDocuments = dyndata.RelatedDocuments,
                SyncDate = dyndata.SyncDate,
                SystemGeneratedControlUid = dyndata.SystemGeneratedControlUid,
                Uid = dyndata.Uid, // DAPEntity
                UpdatedBy = dyndata.UpdatedBy, // DAPEntity
                UpdatedByFullName = dyndata.UpdatedByFullName, // DAPEntity
                UpdatedByUserName = dyndata.UpdatedByUserName, // DAPEntity
                UpdatedDate = dyndata.UpdatedDate, // DAPEntity
                UpdatedPoint = dyndata.UpdatedPoint,
                Uploads = dyndata.Uploads,
                UserPermissions = dyndata.UserPermissions,
                ValueContent = dyndata.ValueContent, // [JsonIgnore]
                WorkflowState = dyndata.WorkflowState
            };
            return data;
        }

        public static DynData CreateFrom(Data data)
        {
            DynData dyndata = new DynData()
            {
                ActionItemId = data.ActionItemId,
                ApplicationType = data.ApplicationType,
                AttachmentCount = data.AttachmentCount,
                ConsumerId = data.ConsumerId, // [JsonIgnore]
                ContextMenuItems = data.ContextMenuItems,
                CreatedBy = data.CreatedBy, // DAPEntity
                CreatedByFullName = data.CreatedByFullName, // DAPEntity
                CreatedByUserName = data.CreatedByUserName, // DAPEntity
                CreatedDate = data.CreatedDate, // DAPEntity
                CreatedPoint = data.CreatedPoint,
                DeletedBy = data.DeletedBy, // DAPEntity
                DeletedDate = data.DeletedDate, // DAPEntity
                FormUid = data.FormUid,
                FormVersion = data.FormVersion,
                IsSynced = data.IsSynced,
                LocationId = data.LocationId, // [JsonIgnore]
                RecordCount = data.RecordCount,
                RelatedDocuments = data.RelatedDocuments,
                SyncDate = data.SyncDate,
                SystemGeneratedControlUid = data.SystemGeneratedControlUid,
                Uid = data.Uid, // DAPEntity
                UpdatedBy = data.UpdatedBy, // DAPEntity
                UpdatedByFullName = data.UpdatedByFullName, // DAPEntity
                UpdatedByUserName = data.UpdatedByUserName, // DAPEntity
                UpdatedDate = data.UpdatedDate, // DAPEntity
                UpdatedPoint = data.UpdatedPoint,
                Uploads = data.Uploads,
                UserPermissions = data.UserPermissions,
                ValueContent = data.ValueContent, // [JsonIgnore]
                WorkflowState = data.WorkflowState
            };
            return dyndata;
        }
    }
}