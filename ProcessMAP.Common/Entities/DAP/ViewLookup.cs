﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    public class ViewLookup
    {
        public List<ColumnLookup> Columns { get; set; }
        public List<Column> FormColumns { get; set; }
        public List<Filter> FormFilters { get; set; }
        public Dictionary<DataType, List<Filter>> DataTypeFilterMapping { get; set; }

        public ViewLookup()
        {
            Columns = new List<ColumnLookup>();
        }
    }

    public class ColumnLookup
    {
        public List<Filter> Filters { get; set; }
        public string Name { get; set; }
        public Guid Uid { get; set; }
        public DataType DataType { get; set; }
    }
}