﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Form Backup Entity.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class FormVersion : Form
    {
        /// <summary>
        /// Represents the unique id of the Form.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid FormUid { get; set; }
    }
}