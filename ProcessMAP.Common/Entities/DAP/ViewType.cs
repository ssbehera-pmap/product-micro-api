﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ViewType
    {
        /// <summary>
        /// 
        /// </summary>
        None,

        /// <summary>
        /// 
        /// </summary>
        System,

        /// <summary>
        /// 
        /// </summary>
        Link,

        /// <summary>
        /// 
        /// </summary>
        Numeric,

        /// <summary>
        /// 
        /// </summary>
        DateTime,

        /// <summary>
        /// 
        /// </summary>
        Image,

        /// <summary>
        /// 
        /// </summary>
        String,

        /// <summary>
        /// 
        /// </summary>
        Date,

        /// <summary>
        /// 
        /// </summary>
        Data
    }
}