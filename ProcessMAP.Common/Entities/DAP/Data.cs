﻿using BAMCIS.GeoJSON;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using ProcessMAP.Common.Entities.DAP.Authorization;
using ProcessMAP.Common.Entities.WorkflowEngine;
using ProcessMAP.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// This class represents the data for a particular form.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class Data : DAPEntity, IWorkflowEntity
    {
        /// <summary>
        /// The unique identifier of the form to which this record belongs to.
        /// </summary>
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid FormUid { get; set; }

        /// <summary>
        /// Unique Identifier for Action item Record
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? ActionItemId { get; set; }

        /// <summary>
        /// The unique identifier for system generated Uid.
        /// </summary>
        //[RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid? SystemGeneratedControlUid { get; set; }

        /// <summary>
        /// Key value pair collection of the control's unique identifiers and its values
        /// </summary>
        [Required]
        [CollectionRange(minCount: 1)]
        public IEnumerable<FormDocumentValue> Values { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the date that the data was synchronized.
        /// </summary>
        public DateTime? SyncDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        public int LocationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ProcessState WorkflowState { get; set; }

        /// <summary>
        /// Represents the count of records, required for pagination.
        /// </summary>
        public long RecordCount { get; set; }

        /// <summary>
        /// List of Uploads
        /// </summary>
        public IEnumerable<FileUpload.File> Uploads { get; set; }

        /// <summary>
        /// Represents the count of attachments in this form document,
        /// not including attachments in Action Items.
        /// </summary>
        public int AttachmentCount { get; set; }

        /// <summary>
        /// The data's context menu items. These items are secured following the user's permissions.
        /// </summary>
        public IDictionary<DataContextMenuItems, string> ContextMenuItems { get; set; }

        /// <summary>
        /// Represents the user permissions for this document.
        /// </summary>
        public UserPermissions UserPermissions { get; set; }

        /// <summary>
        /// List of related documents.
        /// </summary>
        public IEnumerable<RelatedDocument> RelatedDocuments { get; set; }

        /// <summary>
        /// Represents the count of entities when required.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public dynamic ValueContent { get; set; }

        /// <summary>
        /// Represents the form Version to which the data belongs to.
        /// </summary>
        public int? FormVersion { get; set; }

        /// <summary>
        /// Represents the Status of the Data.
        /// </summary>
        public bool IsSynced { get; set; }

        /// <summary>
        /// Represents application type of the request
        /// </summary>
        public ApplicationType ApplicationType { get; set; }

        /// <summary>
        /// Represents GeoJSON point where document was created
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Position CreatedPoint { get; set; }

        /// <summary>
        /// Represents GeoJSON point where document was edited
        /// </summary>
        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public Position UpdatedPoint { get; set; }

    }
}