﻿using Newtonsoft.Json;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// SyncTemplate
    /// </summary>
    public class SyncTemplate : DAPEntity, ICacheableEntity
    {
        /// <summary>
        /// Represents the Name of the SyncTemplate.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Represents the Linked FormUid
        /// </summary>
        [Required]
        [RequiredGuid]
        public string FormUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ConsumerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ModuleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<SyncField> Fields { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int SourceType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string Source { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastCached { get; set; }

        /// <summary>
        /// ProcessingSP
        /// </summary>
        public string ProcessingSP { get; set; }
    }

    /// <summary>
    /// SyncField
    /// </summary>
    public class SyncField : Field
    {
        /// <summary>
        /// TableName
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Schema Name
        /// </summary>
        public string SchemaName { get; set; }
    }
}
