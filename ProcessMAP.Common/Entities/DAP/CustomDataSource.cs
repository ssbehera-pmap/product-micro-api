﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.Entities.DAP.Controls;
using ProcessMAP.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the Custom Data Source entity.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class CustomDataSource
    {
        /// <summary>
        /// Represents the unique Custom Data Source identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the Consumer unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int? ConsumerId { get; set; }

        /// <summary>
        /// Represents the unique Name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Represents the Value.
        /// </summary>
        [Required]
        public IEnumerable<Vlu> Values { get; set; }

        /// <summary>
        /// Represents the order to which the records will be listed.
        /// </summary>
        public SortOrder? SortOrder { get; set; }

        /// <summary>
        /// Represents the flag to determine if the record is active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that created the entity.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was created.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that updated the entity.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was updated.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [BsonIgnoreExtraElements]
    public class Vlu
    {
        /// <summary>
        /// 
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; set; }

        //public IEnumerable<LinkedField> LinkedFields { get; set; }
        /// <summary>
        /// LinkedFieldID
        /// </summary>
        public Guid? LinkedFieldID { get; set; }

        ///// <summary>
        ///// LinkedFieldValue
        ///// </summary>
        ////public string LinkedFieldValue { get; set; }

        /// <summary>
        /// This prop is used for get the default value options for different controls.
        /// </summary>
        public ControlType? ControlType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [BsonIgnoreExtraElements]
    public class LinkedField
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }
    }
}