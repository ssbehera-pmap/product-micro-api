﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.Caching;
using ProcessMAP.Common.CustomDataAnnotations;
using ProcessMAP.Common.Entities.DAP.Authorization;
using ProcessMAP.Common.Entities.DAP.Controls;
using ProcessMAP.Common.Entities.DAP.Controls.Properties;
using ProcessMAP.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the main [Form] entity for managing the Dynamic Application Platform.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class Form : DAPEntity, ICacheableEntity
    {
        private List<IDAPControl> allControls;

        /// <summary>
        /// Represents the Title of the Form.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Represents the Description of the Form.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// A value representing a unique readable code for the form.
        /// Useful as see for other unique readable codes or IDs.
        /// </summary>
        [Required]
        [AllowedCharacterRange(3, 15)]
        public string Code { get; set; }

        /// <summary>
        /// Represents the Help of the Form.
        /// </summary>
        public string Help { get; set; }

        /// <summary>
        /// Represents the Sections of the Form.
        /// </summary>
        [Required]
        [CollectionRange(minCount: 1, maxCount: 100)]
        public IEnumerable<Section> Sections { get; set; }

        /// <summary>
        /// Represents the Controls of the Form.
        /// </summary>
        [Required]
        [CollectionRange(minCount: 1)]
        public IEnumerable<NavigationControl> NavigationControls { get; set; }

        /// <summary>
        /// Represents the Hash of the Form.
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// Represents the Layout of the Form.
        /// </summary>
        public Layout Layout { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public IEnumerable<IDAPControl> AllControls
        {
            get
            {
                allControls = null; //Added for DAP-2601
                if (allControls == null && Sections != null)
                {
                    allControls = new List<IDAPControl>();

                    foreach (var section in Sections)
                    {
                        allControls.Add(section);
                        allControls.AddRange(section.Controls);
                    }
                }

                return allControls;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public IEnumerable<IDAPControl> ViewControls
        {
            get
            {
                return AllControls.Where(c => c.DataType != DataType.Section);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public IEnumerable<IDAPControl> DefaultViewControls
        {
            get
            {
                return ViewControls.Where(c => c.Properties.ContainsKey(DAPControlPropertyType.DefaultViewEnabled) && (bool)c.Properties[DAPControlPropertyType.DefaultViewEnabled].Value == true);
            }
        }

        /// <summary>
        /// Represents the Application unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid AppUid { get; set; }

        /// <summary>
        /// Represents the Title of the List.
        /// </summary>
        public string ListTitle { get; set; } = "Untitled";

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastCached { get; set; }

        /// <summary>
        /// Represents the federated Form by Consumer.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the serialized representation of the Form.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string Definition { get; set; }

        /// <summary>
        /// Gets or sets a collection of actions to be performed in this form.
        /// </summary>
        public IEnumerable<Actions.Action> Actions { get; set; }

        /// <summary>
        /// The form's default view based on which controls in the form were flaged [DefaultViewEnabled = true].
        /// </summary>
        public View DefaultView { get; set; }

        /// <summary>
        /// The form's context menu items. These items are secured following the user's permissions.
        /// </summary>
        public IDictionary<FormContextMenuItems, string> ContextMenuItems { get; set; }

        /// <summary>
        /// The form's context menu items. These items are secured following the user's permissions.
        /// </summary>
        public IDictionary<FormContextMenuItems, string> Buttons { get; set; }

        /// <summary>
        /// Represents the sharing definitions for this form.
        /// </summary>
        public IEnumerable<FormSharingDefinition> SharingDefinitions { get; set; }

        /// <summary>
        /// Represents the user permissions for this form.
        /// </summary>
        public UserPermissions UserPermissions { get; set; }

        /// <summary>
        /// Represents the collection of access keys.
        /// </summary>
        public IEnumerable<AccessKey> AccessKeys { get; set; }

        /// <summary>
        /// Represents the current Form  is a readonly type.
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// Represents the source for the readonly form.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? ReadOnlyDataSource { get; set; }

        /// <summary>
        /// Represents the Status of the form.
        /// </summary>
        public StatusFlags? Status { get; set; }

        /// <summary>
        /// Represents the Icon Path/Class of the Form.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Represents the Icon type of the Form.
        /// </summary>
        public string IconType { get; set; }

        /// <summary>
        /// Represents if the Location Column is  .
        /// </summary>
        public bool? AddLocationToList { get; set; }

        /// <summary>
        /// Represents the location control id.
        /// </summary>
        public Guid? LocationControlUid { get; set; }

        /// <summary>
        /// Represents the current version of the Form.
        /// </summary>
        [System.ComponentModel.DefaultValue(1)]
        public int? Version { get; set; }

        /// <summary>
        /// Gets or sets a collection of Controls affected on Form change.
        /// </summary>
        public List<Guid> DeltaControls { get; set; }

        // added to prevent errors while calling from web-client
        public List<Guid> DeltaControlsForAddition { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Notification.Notification> Notifications { get; set; }

        /// <summary>
        /// EnableSynchronization
        /// </summary>
        public bool EnableSynchronization { get; set; }

        /// <summary>
        /// SynchronizationTemplate
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? SynchronizationTemplate { get; set; }

        /// <summary>
        /// RestrictSpecialCharacters
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public bool RestrictSpecialCharacters { get; set; }

        /// <summary>
        /// Represents if the Form is available for Drill down Export.
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public bool ListDrillDownExport { get; set; }

        /// <summary>
        /// ExportTemplate
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid? FormExportTemplateUid { get; set; }

        /// <summary>
        /// Decides whether or not  to show the deprecated controls
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public bool ShowEarlierVersionControl { get; set; }

        /// <summary>
        /// RollBackVersion
        /// </summary>
        public int? RollBackVersion { get; set; }

        /// <summary>
        /// Represents the columns to display in mobile.
        /// </summary>
        [System.ComponentModel.DefaultValue(4)]
        public int? MaxAllowedColumnsInMobile { get; set; }

        /// <summary>
        /// Represents whether form versioning is enabled or not
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public bool EnableFormVersioning { get; set; }

        /// <summary>
        /// Represents whether form uses New Persistence Structure
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public bool NewPersistence { get; set; }

        /// <summary>
        /// Represents whether form uses New Persistence Structure
        /// </summary>
        public string CollectionName { get; set; }
    }
   

}

