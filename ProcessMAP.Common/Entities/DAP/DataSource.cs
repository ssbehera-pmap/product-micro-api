﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    public class DataSource
    {
        /// <summary>
        /// Represents the SP Name.
        /// </summary>  
        public string Source { get; set; }

        /// <summary>
        /// Represents the Parameters Required for SP.
        /// </summary>  
        public List<DataSourceParameter> Parameters { get; set; }

        /// <summary>
        /// Represents the data.
        /// </summary>  
        public List<dynamic> Data { get; set; }
    }
}