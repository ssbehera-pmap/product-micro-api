﻿using ProcessMAP.Common.Caching;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// PdfTemplate
    /// </summary>
    public class ExportTemplate : DAPEntity, ICacheableEntity
    {
        /// <summary>
        /// Represents the date/time the template was cached.
        /// </summary>
        public DateTime LastCached { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Header
        /// </summary>
        public string HeaderDefinition { get; set; }

        /// <summary>
        /// Footer
        /// </summary>
        public string FooterDefinition { get; set; }

        /// <summary>
        /// Body
        /// </summary>
        public string BodyDefinition { get; set; }

        /// <summary>
        /// SystemTemplateUid
        /// </summary>
        public Guid? SystemTemplateUid { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }        
        
        /// <summary>
        /// ConsumerId
        /// </summary>
        public int? ConsumerId { get; set; }

        /// <summary>
        /// IsHeaderSystemDefined
        /// </summary>
        public bool IsHeaderSystemDefined { get; set; }

        /// <summary>
        /// IsBodySystemDefined
        /// </summary>
        public bool IsBodySystemDefined { get; set; }

        /// <summary>
        /// IsFooterSystemDefined
        /// </summary>
        public bool IsFooterSystemDefined { get; set; }

        /// <summary>
        /// Badges
        /// </summary>
        public List<Badge> Badges { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ExportTemplateVersions : ExportTemplate
    {
        /// <summary>
        /// BackupBy
        /// </summary>
        public int BackupBy { get; set; }

        /// <summary>
        /// BackupDate
        /// </summary>
        public DateTime BackupDate { get; set; }
    }
}
