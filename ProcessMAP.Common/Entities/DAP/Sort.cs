﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the Column Sort.
    /// </summary>
    public class Sort
    {
        /// <summary>
        /// Represents the Sort Value.
        /// </summary>
        public string Name { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        /// <summary>
        /// Represents the Column unique identifier.
        /// </summary>
        public Guid ColumnUid { get; set; }
    }
}