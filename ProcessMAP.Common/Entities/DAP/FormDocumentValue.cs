﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [BsonIgnoreExtraElements]
    public class FormDocumentValue
    {
        /// <summary>
        /// 
        /// </summary>
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid ControlUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //[Required] //APB-69 null values bug fixed
        public dynamic Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public DataType ValueDataType { get; set; }

        /// <summary>
        /// InActiveData
        /// </summary>
        public List<InActiveData> InActiveData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsArray()
        {
            return (Value is IEnumerable) && !(Value is String);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class InActiveData
    {
        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; set; }
    }
}