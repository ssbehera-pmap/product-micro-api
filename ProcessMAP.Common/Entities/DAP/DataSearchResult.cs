﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Represents the results of a data search.
    /// </summary>
    public class DataSearchResult
    {
        /// <summary>
        /// The unique identifier of the document found.
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// The unique identifier of the parent form.
        /// </summary>
        public Guid FormUid { get; set; }

        /// <summary>
        /// Key value pair collection of the control's unique identifiers and its values
        /// </summary>
        public IEnumerable<FormDocumentValue> Values { get; set; }
    }
}