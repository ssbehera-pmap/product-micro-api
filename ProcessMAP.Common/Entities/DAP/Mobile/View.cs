﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP.Mobile
{
    /// <summary>
    /// Serves as the View that will be used for Mobile consumption.
    /// </summary>
    public class View : Base.ViewBase
    {
        /// <summary>
        /// Represents the Columns of the View.
        /// </summary>
        public List<Column> Columns { get; set; }
    }
}