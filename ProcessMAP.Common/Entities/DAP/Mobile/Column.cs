﻿using System;

namespace ProcessMAP.Common.Entities.DAP.Mobile
{
    /// <summary>
    /// Serves as the Column, as defined in Mobile.
    /// </summary>
    public class Column : Base.ColumnBase
    {
        /// <summary>
        /// Represents the Column Alignment.
        /// </summary>
        public string Align { get; set; }

        /// <summary>
        /// Represents the Column Geometry, where the Row and Width are defined.
        /// </summary>
        public Geometry Geometry { get; set; }

        /// <summary>
        /// Represents the Column Source, where the Bind and Type are defined.
        /// </summary>
        public Source Source { get; set; }

        public Guid ControlUid { get; set; }
    }
}