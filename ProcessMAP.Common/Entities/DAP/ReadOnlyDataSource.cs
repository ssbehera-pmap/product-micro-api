﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadOnlyDataSource
    {
        /// <summary>
        /// 
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ConsumerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ModuleId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<Field> Fields { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int SourceType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string Source { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public dynamic Definition { get; set; }
    }

    public class Field
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid? ControlUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ColumnName { get; set; }
    }

    public class RequestParam
    {
        /// <summary>
        /// 
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? FromDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ToDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FilterExpression { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string  Id { get; set; }

        /// <summary>
        /// Represents a value indicating the number of results to fetch
        /// </summary>
        public int? Take { get; set; }

        /// <summary>
        /// Represents a value indicating the index at which to start matching results.
        /// </summary>
        public int? Skip { get; set; }
    }
}