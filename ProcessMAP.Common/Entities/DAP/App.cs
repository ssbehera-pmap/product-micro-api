﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the model of the Application.
    /// The App has can have 0-to-many Forms.
    /// </summary>
    public class App
    {
        /// <summary>
        /// Represents the unique identfier of the App.
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the Name of the App.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the Description of the App.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Represents the unique identifier of the Creator of the App.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Represents the User Name of the Creator of the App.
        /// </summary>
        public string CreatedByUserName { get; set; }

        /// <summary>
        /// Represents the Full Name of the Creator of the App.
        /// </summary>
        public string CreatedByFullName { get; set; }

        /// <summary>
        /// Represents the Date and Time of the Creation of the App.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Represents the unique identifier of the Updator of the App.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Represents the User Name of the Updator of the App.
        /// </summary>
        public string UpdatedByUserName { get; set; }

        /// <summary>
        /// Represents the Full Name of the Updator of the App.
        /// </summary>
        public string UpdatedByFullName { get; set; }

        /// <summary>
        /// Represents the Date and Time of the Updation of the App.
        /// </summary>
        public DateTime UpdatedDate { get; set; }

        /// <summary>
        /// Represents the Hash of the App.
        /// </summary>
        public byte[] Hash { get; set; }

        /// <summary>
        /// Represents the Forms associated with the App.
        /// </summary>
        public IEnumerable<Form> Forms { get; set; }
    }
}