﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the entity for managing Files for uploading and downloading.
    /// </summary>
    public class File
    {
        /// <summary>
        /// Represents the unique identifier of the File.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public byte[] Content { get; set; }
        public string Name { get; set; }
        public ObjectId Id { get; set; }
        public DateTime CreatedDate { get; set; }

        [BsonRepresentation(BsonType.String)]
        public Guid FormUid { get; set; }
        public long Size { get; set; }
        public int ConsumerId { get; set; }
    }
}