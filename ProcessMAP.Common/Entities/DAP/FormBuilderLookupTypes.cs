﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FormBuilderLookupTypes
    {
        /// <summary>
        /// 
        /// </summary>
        ControlGroups,

        /// <summary>
        /// 
        /// </summary>
        ControlTypes,

        /// <summary>
        /// 
        /// </summary>
        ControlPropertyTypes,

        /// <summary>
        /// 
        /// </summary>
        ControlEvents,

        /// <summary>
        /// 
        /// </summary>
        ComparisonOperators,

        /// <summary>
        /// 
        /// </summary>
        DateComparisonOperators,

        /// <summary>
        /// 
        /// </summary>
        DataSourceTypes,

        /// <summary>
        /// 
        /// </summary>
        LogicalOperators,

        /// <summary>
        /// 
        /// </summary>
        DataTypes,

        /// <summary>
        /// 
        /// </summary>
        NavigationControls,

        /// <summary>
        /// 
        /// </summary>
        CustomDataSourceSortingTypes,

        /// <summary>
        /// 
        /// </summary>
        ActionTypes,

        /// <summary>
        /// 
        /// </summary>
        DAPPermissions,

        /// <summary>
        /// 
        /// </summary>
        Templates,

        /// <summary>
        /// 
        /// </summary>
        Users,

        /// <summary>
        /// 
        /// </summary>
        Roles,

        /// <summary>
        /// 
        /// </summary>
        UserTypes,

        /// <summary>
        /// ExportTemplate
        /// </summary>
        FormExportTemplates
    }
}