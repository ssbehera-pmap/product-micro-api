﻿using ProcessMAP.Common.Caching;
using System;

namespace ProcessMAP.Common.Entities.DAP.DataSources
{
    /// <summary>
    /// 
    /// </summary>
    public class Employee : DataSourceBase, ICacheableEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmploymentStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Suffix { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MaritalStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? NumberOfDependents { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PrimaryLanguage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SecondaryLanguage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Country { get; set; }

        public string CountryName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PhoneNumberDay { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PhoneNumberNight { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmergencyContactDetails { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>

        public string Department { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DateOfHire { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Wage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string WageUnit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string WageFrequency { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? WorkingHoursPerDay { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? DaysPerWeek { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PersonalArea { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SupervisorEmployeeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DateJobStarted { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DateOfTermination { get; set; }

        /// <summary>
        /// SupervisorId
        /// </summary>
        public int SupervisorId { get; set; }

        /// <summary>
        /// EmployeeType
        /// </summary>
        public int EmployeeType { get; set; }

        /// <summary>
        /// DOBAvailable
        /// </summary>
        public string DOBAvailable { get; set; }

        /// <summary>
        /// PersonnelType
        /// </summary>
        public string PersonnelType { get; set; }
        /// <summary>
        /// 
        /// </summary>

        public string SupervisorName { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public string EmployeeTypeName { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public string EmployeeRole { get; set; }
    }
}