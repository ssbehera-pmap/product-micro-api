﻿using ProcessMAP.Common.Caching;

namespace ProcessMAP.Common.Entities.DAP.DataSources
{
    /// <summary>
    /// 
    /// </summary>
    public class State : DataSourceBase, ICacheableEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }
    }
}