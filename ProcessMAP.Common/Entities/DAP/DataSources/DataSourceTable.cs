﻿using Newtonsoft.Json;
using ProcessMAP.Common.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.DataSources
{
    /// <summary>
    /// 
    /// </summary>
    public class DataSourceTable : DataTable, ICacheableEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dataTable"></param>
        public DataSourceTable(string tableName, DataTable dataTable = null)
            : base(tableName)
        {
            if (dataTable != null)
            {
                Merge(dataTable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DataSourceTable()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dataRows"></param>
        /// <param name="excludeFields"></param>
        public DataSourceTable(string tableName, IEnumerable<DataRow> dataRows, IEnumerable<string> excludeFields = null)
            : base(tableName)
        {
            var firstRow = dataRows.FirstOrDefault();

            if (firstRow != null)
            {
                foreach(var column in firstRow.Table.Columns.Cast<DataColumn>())
                {
                    if (excludeFields == null || !excludeFields.Any(f => f == column.ColumnName))
                        Columns.Add(column.ColumnName, column.DataType);
                }

                if (dataRows != null)
                {
                    foreach (var row in dataRows)
                    {
                        ImportRow(row);
                    }
                }
            }
        }

        /// <summary>
        /// Represents the last time the entity was cached.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public DateTime LastCached { get; set; }
    }
}