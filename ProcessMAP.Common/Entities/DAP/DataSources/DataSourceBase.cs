﻿using Newtonsoft.Json;
using ProcessMAP.Common.Caching;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP.DataSources
{
    /// <summary>
    /// 
    /// </summary>
    public class DataSourceBase : ICacheableEntity
    {
        /// <summary>
        /// Represents the last time the entity was cached.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public DateTime LastCached { get; set; }
        /// <summary>
        /// Represents the Unique identifier .
        /// </summary>
      
        public int Id { get; set; }
    }
}