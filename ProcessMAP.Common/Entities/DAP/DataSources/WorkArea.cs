﻿using ProcessMAP.Common.Caching;

namespace ProcessMAP.Common.Entities.DAP.DataSources
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkArea : DataSourceBase, ICacheableEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? BusinessTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BusinessType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? OperationTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OperationType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? StatusTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ModuleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }
    }
}