﻿using ProcessMAP.Common.Caching;

namespace ProcessMAP.Common.Entities.DAP.DataSources
{
    /// <summary>
    /// 
    /// </summary>
    public class Department : DataSourceBase, ICacheableEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ParentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ParentDepartmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OrganizationComponentType { get; set; }

        public string DESCRIPTION { get; set; }
    }
}