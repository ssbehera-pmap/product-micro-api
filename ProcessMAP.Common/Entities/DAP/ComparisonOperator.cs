﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ComparisonOperator
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Empty")]
        Empty,

        /// <summary>
        /// 
        /// </summary>
        [Description("Not Empty")]
        NotEmpty,

        /// <summary>
        /// 
        /// </summary>
        [Description("Equals")]
        Eq,

        /// <summary>
        /// 
        /// </summary>
        [Description("Not Equals")]
        Ne,

        /// <summary>
        /// 
        /// </summary>
        [Description("Contains")]
        Contain,

        /// <summary>
        /// 
        /// </summary>
        [Description("Does Not Contain")]
        DoesNotContain,

        /// <summary>
        /// 
        /// </summary>
        [Description("Greater Than")]
        Greater,

        /// <summary>
        /// 
        /// </summary>
        [Description("Less Than")]
        Less,

        /// <summary>
        /// 
        /// </summary>
        [Description("Greater Than or Equal")]
        GreaterEq,

        /// <summary>
        /// 
        /// </summary>
        [Description("Less Than or Equal")]
        LessEq
    }

    /// <summary>
    /// Operators to be used to filter Date control types.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DateComparisonOperator
    {
        /// <summary>
        /// Match values after a specific date.
        /// </summary>
        [Description("After")]
        After,

        /// <summary>
        /// Match values before a specific date.
        /// </summary>
        [Description("Before")]
        Before,

        /// <summary>
        /// Match values for last month.
        /// </summary>
        [Description("Last Month")]
        LastMonth,

        /// <summary>
        /// Match values within the last 30 days.
        /// </summary>
        [Description("Last 30 Days")]
        Last30Days,

        /// <summary>
        /// Match values for this month.
        /// </summary>
        [Description("This Month")]
        ThisMonth,

        /// <summary>
        /// Match values this week.
        /// </summary>
        [Description("This Week")]
        ThisWeek,

        /// <summary>
        /// Match values for today.
        /// </summary>
        [Description("Today")]
        Today,

        /// <summary>
        /// Match values within a specific date range.
        /// </summary>
        [Description("Custom")]
        Custom
    }
}