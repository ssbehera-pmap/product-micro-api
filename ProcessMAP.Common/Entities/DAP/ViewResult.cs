﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// ViewResult
    /// </summary>
    public class ViewResult
    {
        /// <summary>
        /// View
        /// </summary>
        public View View { get; set; }

        /// <summary>
        /// Skip
        /// </summary>

        public int? Skip { get; set; }
        /// <summary>
        /// Take
        /// </summary>
        public int? Take { get; set; }

        /// <summary>
        /// SortBy
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// SortOrder
        /// </summary>
        public string SortOrder { get; set; }

        /// <summary>
        /// TimeZone
        /// </summary>
        public string TimeZone { get; set; }

        /// <summary>
        /// FormUid
        /// </summary>
        public Guid? FormUid { get; set; }

        /// <summary>
        /// ViewUid
        /// </summary>
        public Guid? ViewUid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Lookup> UsedDataSources { get; set; }
    }
}
