﻿using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DataSourceType
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Body Parts")]
        BodyParts,

        /// <summary>
        /// 
        /// </summary>
        [Description("Custom")]
        Custom,

        /// <summary>
        /// 
        /// </summary>
        [Description("Countries")]
        Countries,

        /// <summary>
        /// 
        /// </summary>
        [Description("Employees")]
        Employees,

        /// <summary>
        /// 
        /// </summary>
        [Description("Departments")]
        Departments,

        /// <summary>
        /// 
        /// </summary>
        [Description("States")]
        States,

        /// <summary>
        /// 
        /// </summary>
        [Description("Supervisors")]
        Supervisors,

        /// <summary>
        /// 
        /// </summary>
        [Description("Users")]
        Users,

        /// <summary>
        /// 
        /// </summary>
        [Description("Work Areas")]
        WorkAreas,

        /// <summary>
        /// 
        /// </summary>
        [Description("Work Shifts")]
        WorkShifts,

        /// <summary>
        /// 
        /// </summary>
        [Description("Action Item Priority Levels")]
        ActionItemPriorityLevels,

        /// <summary>
        /// 
        /// </summary>
        [Description("Action Item Status")]
        ActionItemStatus,

        /// <summary>
        /// 
        /// </summary>
        [Description("Verification Status")]
        VerificationStatus,

        /// <summary>
        /// 
        /// </summary>
        [Description("Verification Performed")]
        VerificationPerformed
    }
}