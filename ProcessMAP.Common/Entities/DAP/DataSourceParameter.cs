﻿namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    public class DataSourceParameter
    {
        /// <summary>
        /// Represents the Parameter Name for DataSource.
        /// </summary>        
        public string Name { get; set; }
        
        /// <summary>
        /// Represents the Parameter Value.
        /// </summary>        
        public string Value { get; set; }

        /// <summary>
        /// Represents the Parameter Type for data source.
        /// </summary>    
        public DataSourceParameterValueType ParameterValueType { get; set; }
    }
}
