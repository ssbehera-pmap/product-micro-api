﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using ProcessMAP.Common.CustomDataAnnotations;
using ProcessMAP.Common.Entities.WorkflowEngine;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Action Item class 
    /// </summary>
    public class ActionItems : DAPEntity, IWorkflowEntity
    {

        /// <summary>
        /// This Id represents form defination uid
        /// </summary>
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid FormUid { get; set; }

        /// <summary>
        /// This ID linked with the DataID 
        /// </summary>
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid ParentUid { get; set; }

        /// <summary>
        /// ControlUid
        /// </summary>
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid ControlUid { get; set; }

        /// <summary>
        /// Key value pair collection of the control's unique identifiers and its values
        /// </summary>
        [CollectionRange(minCount: 1)]
        public IEnumerable<ActionItemFormDocumentValue> Values { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int ConsumerId { get; set; }

        /// <summary>
        /// Represents the date that the data was synchronized.
        /// </summary>
        public DateTime? SyncDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int LocationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ProcessState WorkflowState { get; set; }

        /// <summary>
        /// Represents the count of records, required for pagination.
        /// </summary>
        public long RecordCount { get; set; }
    }
}
