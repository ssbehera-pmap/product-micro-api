﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProcessMAP.Common.CustomDataAnnotations;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Represents a document to be linked to another.
    /// </summary>
    public class RelatedDocument
    {
        /// <summary>
        /// Represents the globally unique identifier of the form the related document belongs to.
        /// </summary>
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid FormUid { get; set; }

        /// <summary>
        /// Represents the globally unique identifier of the document being linked.
        /// </summary>
        [RequiredGuid]
        [BsonRepresentation(BsonType.String)]
        public Guid DocumentUid { get; set; }

        /// <summary>
        /// Represents the column names of the document.
        /// </summary>
        public IEnumerable<string> ColumnNames { get; set; }

        /// <summary>
        /// Represents the values to be displayed.
        /// </summary>
        public IEnumerable<FormDocumentValue> Values { get; set; }

        /// <summary>
        /// Represents the user's notes.
        /// </summary>
        public string Notes { get; set; }
    }
}