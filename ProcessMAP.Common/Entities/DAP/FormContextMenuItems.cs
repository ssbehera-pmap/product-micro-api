﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FormContextMenuItems
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("List")]
        List,

        /// <summary>
        /// 
        /// </summary>
        [Description("Modify")]
        Modify,

        /// <summary>
        /// 
        /// </summary>
        [Description("Delete")]
        Delete,

        /// <summary>
        /// 
        /// </summary>
        [Description("Share")]
        Share,

        /// <summary>
        /// 
        /// </summary>
        [Description("Complete Form")]
        CompleteForm,

        /// <summary>
        /// 
        /// </summary>
        [Description("Get Versions")]
        Versions
    }
}