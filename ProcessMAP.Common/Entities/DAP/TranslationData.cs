﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the entity for managing Files for uploading and downloading.
    /// </summary>
    public class TranslationData
    {
        /// <summary>
        /// Represents the unique identifier of the File.
        /// </summary>
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        //[JsonIgnore]
        //[XmlIgnore]
        //public ObjectId id { get; set; }

        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        public string Key { get; set; }
        public int? ConsumerId { get; set; }
        public Guid? AppUid { get; set; }
        public Guid? FormUid { get; set; }
        public IEnumerable<ValueEntity> Values { get; set; }
    }

    /// <summary>
    /// Serves as the entity for managing translation values.
    /// </summary>
    public class ValueEntity
    {
        public string LanguageCode { get; set; }
        public string Value { get; set; }
    }
}