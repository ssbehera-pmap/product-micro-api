﻿using ProcessMAP.Common.Enumerations;

namespace ProcessMAP.Common.Entities.DAP
{
    /// <summary>
    /// Serves as the type of filter.
    /// </summary>
    public class Filter
    {
        /// <summary>
        /// Represents the enumerated option.
        /// </summary>
        public FilterOption FilterOption { get; set; }

        /// <summary>
        /// Represents the value.
        /// </summary>
        public object Value { get; set; }
    }
}