﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.Calendar
{
    /// <summary>
    /// Represents the Event Owner.
    /// </summary>
    public class EventOwner : Owner
    {
        /// <summary>
        /// Represents the Event unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int EventId { get; set; }
    }
}