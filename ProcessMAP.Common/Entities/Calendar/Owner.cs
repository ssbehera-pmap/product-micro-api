﻿using ProcessMAP.Common.Entities.Base;

namespace ProcessMAP.Common.Entities.Calendar
{
    /// <summary>
    /// Represents an Owner lookup.
    /// </summary>
    public class Owner : PMAPLookup { }
}