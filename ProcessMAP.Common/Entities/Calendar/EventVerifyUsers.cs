﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.Calendar
{
    /// <summary>
    /// Represents the Calendar Verify Users entity.
    /// </summary>
    public class EventVerifyUsers : VerifyUsersLookup {
        /// <summary>
        /// Represents the Event unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int EventId { get; set; }
    }
}
