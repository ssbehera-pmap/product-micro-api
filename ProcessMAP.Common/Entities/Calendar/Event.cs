﻿using ProcessMAP.Common.Entities.Base;
using ProcessMAP.Common.Entities.Foundation;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.Calendar
{
    /// <summary>
    /// Represents the Calendar Event entity.
    /// </summary>
    public class Event : Entity
    {
        /// <summary>
        /// Represents the event name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the event description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Represents the event module identifier.
        /// </summary>
        public int ModuleId { get; set; }

        /// <summary>
        /// Represents the id of the Location of the event.
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        /// Represents the event calendar category identifier.
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Represents the event assigned by identifier.
        /// </summary>
        public int AssignedById { get; set; }

        /// <summary>
        /// Represents the event assigned by user.
        /// </summary>
        public string AssignedBy { get; set; }

        /// <summary>
        /// Represents the event priority identifier.
        /// </summary>
        public int? PriorityId { get; set; }

        /// <summary>
        /// Represents the owner's notification.
        /// </summary>
        public bool NotifyOwnersImmediately { get; set; }

        /// <summary>
        /// Represents the event due date.
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Represents the action taken on the event.
        /// </summary>
        public string ActionTaken { get; set; }

        /// <summary>
        /// Represents the action item status identifier.
        /// </summary>
        public int? ActionItemStatusId { get; set; }

        /// <summary>
        /// Represents the action item completed by identifier.
        /// </summary>
        public int? ActionItemCompletedById { get; set; }

        /// <summary>
        /// Represents the user who completed the action item.
        /// </summary>
        public string ActionItemCompletedBy { get; set; }

        /// <summary>
        /// Represents the action item completion date.
        /// </summary>
        public DateTime? ActionItemCompletionDate { get; set; }

        /// <summary>
        /// Represents a list of event owners.
        /// </summary>
        public List<EventOwner> Owners { get; set; }

        /// <summary>
        /// Represents a list of event attachments.
        /// </summary>
        public List<Attachment> Attachments { get; set; }

        /// <summary>
        /// Represents the [Update] Permission of the Event.
        /// </summary>
        public bool CanUpdate { get; set; }

        /// <summary>
        /// Represents the [Delete] Permission of the Event.
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// Represents the [status Change] Permission of the Event.
        /// </summary>
        public bool CanStatusChange { get; set; }

        /// <summary>
        /// Represents the [Extend Due Date] Permission of the Event.
        /// </summary>
        public bool CanExtendDueDate { get; set; }

        /// <summary>
        /// Represents the [Verify] Permission of the Event.
        /// </summary>
        public bool CanVerify { get; set; }

        /// <summary>
        /// Represents the [Cost] Permission of the Event.
        /// </summary>
        public bool CanChangeCost { get; set; }

        /// <summary>
        /// Represents the Reference/Citation property of the Event.
        /// </summary>
        public string ReferenceCitation { get; set; }

        /// <summary>
        /// Represents the IsVerificationRequired property of the Event.
        /// </summary>
        public bool IsVerificationRequired { get; set; }

        /// <summary>
        /// Represents a list of Verify Users
        /// </summary>
        public List<EventVerifyUsers> VerifyUsers { get; set; }

        /// <summary>
        /// Represents VerificationStatusId
        /// </summary>
        public int? VerificationStatus { get; set; }

        /// <summary>
        /// Represents Source Id(Unique Identification of an action item)
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// VerifiedByUsers
        /// </summary>
        public List<EventOwner> VerifiedByUsers { get; set; }

        /// <summary>
        /// Verificationdate
        /// </summary>
        public DateTime? Verificationdate { get; set; }

        /// <summary>
        /// VerificationPerformedID
        /// </summary>
        public int? VerificationPerformedID { get; set; }

        /// <summary>
        /// Represents Verification comments
        /// </summary>
        public string VerificationComments { get; set; }
    }
}