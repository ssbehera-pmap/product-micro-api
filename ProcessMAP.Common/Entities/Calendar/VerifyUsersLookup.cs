﻿using ProcessMAP.Common.Entities.Base;

namespace ProcessMAP.Common.Entities.Calendar
{  /// <summary>
   /// Represents the VerifyUsers lookup.
   /// </summary>
    public class VerifyUsersLookup : PMAPLookup { }
}
