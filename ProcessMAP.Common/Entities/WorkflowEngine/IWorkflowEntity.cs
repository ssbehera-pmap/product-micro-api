﻿namespace ProcessMAP.Common.Entities.WorkflowEngine
{
    interface IWorkflowEntity
    {
        /// <summary>
        /// Gets or sets the current state of the entity as it stands in the WFE (Workflow Engine).
        /// This property should also deliver the WFE commands that the current user is allowed to execute, if any.
        /// </summary>
        ProcessState WorkflowState { get; set; }
    }
}