﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.WorkflowEngine
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcessState
    {
        /// <summary>
        /// It describes the state of a particular process (document) as it stands in the WFE (Workflow Engine).
        /// </summary>
        public ProcessState() { }

        /// <summary>
        /// Gets or sets the current WFE state.
        /// </summary>
        public string CurrentState { get; set; }

        /// <summary>
        /// Gets or sets the current WFE state localized description.
        /// </summary>
        public string CurrentStateDescription { get; set; }

        /// <summary>
        /// Gets or sets the previous WFE state (if available).
        /// </summary>
        public string PreviousState { get; set; }

        /// <summary>
        /// Gets or sets the previous WFE state localized description (if available).
        /// </summary>
        public string PreviousStateDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Command[] AvailableCommands { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Command[] PossibleCommands { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return CurrentState ?? "Undefined";
        }
    }
}