﻿namespace ProcessMAP.Common.Entities.WorkflowEngine
{
    /// <summary>
    /// 
    /// </summary>
    public class Command
    {
        /// <summary>
        /// Instantiates a new Command object.
        /// </summary>
        public Command() { }

        /// <summary>
        /// The name of the command.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The localized description for this command.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The direction of the command. E.g.: Forward, Reverse.
        /// </summary>
        public CommandDirection Direction { get; set; }

        /// <summary>
        /// To string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}:{1}", Name, Direction.ToString());
        }
    }
}