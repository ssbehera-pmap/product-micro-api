﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.WorkflowEngine
{
    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CommandDirection
    {
        /// <summary>
        /// 
        /// </summary>
        Forward,

        /// <summary>
        /// 
        /// </summary>
        Reverse
    }
}