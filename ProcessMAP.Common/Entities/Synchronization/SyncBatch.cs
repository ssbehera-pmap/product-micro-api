﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// Synchronization Statuses
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SyncStatus
    {
            /// <summary>
            /// The Completed synchronization batch status.
            /// </summary>
            Completed,

            /// <summary>
            /// The Deleted synchronization batch status.
            /// </summary>
            Deleted,

            /// <summary>
            /// The InProgress synchronization batch status.
            /// </summary>
            InProgress,

            /// <summary>
            /// The Received synchronization batch status.
            /// Initial state of the synchronization batch.
            /// </summary>
            Received,

            /// <summary>
            /// The NotFound synchronization batch status.
            /// </summary>
            NotFound
    }

    /// <summary>
    /// Constant values to be used as synchronization actions.
    /// For example, Add, Update, Delete.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SyncAction
    {
        /// <summary>
        /// This action has not been defined.
        /// </summary>
        None = 0,

        /// <summary>
        /// The Add synchronization action.
        /// </summary>
        Add = 1,

        /// <summary>
        /// The Delete synchronization action.
        /// </summary>
        Delete = 2,

        /// <summary>
        /// The Update synchronization action.
        /// </summary>
        Update = 3,

        /// <summary>
        /// Adds a child entity.
        /// </summary>
        AddChild    =   4,

        /// <summary>
        /// Deletes a child entity.
        /// </summary>
        DeleteChild =   5
    }

    /// <summary>
    /// Enumeration values to be used as synchronization entity types.
    /// For example, Observation, WellnessCheck.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SyncEntity
    {
        /// <summary>
        /// Undefined sync entity.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The document of a DAP form.
        /// </summary>
        FormData = 1,

        /// <summary>
        /// The ActionItem of a DAP form.
        /// </summary>
        ActionItem = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class SyncBatch
    {
        /// <summary>
        /// Represents the unique Custom Data Source identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Uid { get; set; }

        /// <summary>
        /// Represents the Consumer unique identifier.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public int? ConsumerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid BatchId { get; set; }
    }
}