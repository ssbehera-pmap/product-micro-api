﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// 
    /// </summary>
    public class SyncBatchStatus : SyncBatch
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public SyncStatus Status { get; set; }
    }
}