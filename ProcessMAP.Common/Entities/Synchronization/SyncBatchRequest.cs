﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using ProcessMAP.Common.Entities.DAP;
using ProcessMAP.Common.ErrorHandling;

namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// This class represents the structure of a synchronization request.
    /// </summary>
    public class SyncBatchRequest : SyncBatch
    {
        /// <summary>
        /// Creates an instance of the SynchronizationRequest class.
        /// </summary>
        public SyncBatchRequest()
        {
            // Empty default constructor needed for auto generated
            // Web API Help documentation.
        }

        /// <summary>
        /// Creates an instance of the SynchronizationRequest class.
        /// </summary>
        /// <param name="batch">Optional entity object.</param>
        public SyncBatchRequest(List<SyncBatchEntryRequest> batch)
            : this()
        {
            Batch = batch;
            AssignBatchIdToEntries();
        }

        /// <summary>
        /// 
        /// </summary>
        public List<SyncBatchEntryRequest> Batch { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public void AssignBatchIdToEntries()
        {
            foreach (var entry in Batch)
            {
                entry.BatchId = BatchId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serializerSettings"></param>
        /// <returns></returns>
        public ValidationResult ValidateEntries(JsonSerializerSettings serializerSettings)
        {
            var validation = new ValidationResult();

            if (Batch != null)
            {
                try
                {
                    foreach (var entry in Batch)
                    {
                        if (entry.Action == SyncAction.None)
                        {
                            var errorMessage = string.Format("Unknown synchronization action '{0}'.", entry.Action);
                            validation.Errors.Add(new Error((int)HttpStatusCode.NotImplemented, errorMessage));
                            return validation;
                        }

                        if (entry.EntityType == SyncEntity.Undefined)
                        {
                            var errorMessage = String.Format("Unknown entity type '{0}'.", entry.EntityType);
                            validation.Errors.Add(new Error((int)HttpStatusCode.NotImplemented, errorMessage));
                            return validation;
                        }

                        var jsonString = entry.Entity.ToString();

                        switch(entry.Action)
                        {
                            case SyncAction.Delete:
                                JsonConvert.DeserializeObject<SyncDeleteObject>(jsonString, serializerSettings);
                                continue;
                        }

                        switch (entry.EntityType)
                        {
                            case SyncEntity.FormData:
                                JsonConvert.DeserializeObject<Data>(jsonString, serializerSettings);
                                continue;

                            case SyncEntity.ActionItem:
                                JsonConvert.DeserializeObject<ActionItems>(jsonString, serializerSettings);
                                continue;
                        }
                    }
                }
                catch (JsonReaderException jsonReadEx)
                {
                    validation.Errors.Add(new Error((int)HttpStatusCode.BadRequest, jsonReadEx.Message));
                    validation.Exception = jsonReadEx;
                }
                catch (JsonSerializationException jsonEx)
                {
                    validation.Errors.Add(new Error((int)HttpStatusCode.BadRequest, jsonEx.Message));
                    validation.Exception = jsonEx;
                }
                catch (Exception ex)
                {
                    validation.Errors.Add(new Error((int)HttpStatusCode.InternalServerError, ex.Message));
                    validation.Exception = ex;
                }
            }

            return validation;
        }
    }
}