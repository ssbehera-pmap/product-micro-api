﻿using System;

namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// Class that represents the minimum required properties of an entity that needs to be deleted.
    /// </summary>
    public class SyncDeleteObject
    {
        /// <summary>
        /// Gets or sets the Id of the entity to delete.
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets the unique identifier of the entity to delete.
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Gets or sets the id of the user requesting the delete operation.
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Gets or sets the id of the user requesting the delete operation.
        /// </summary>
        public Guid? FormUid { get; set; }
    }
}