﻿namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// 
    /// </summary>
    public class SyncBatchEntryResponse : SyncBatchEntry
    {
        /// <summary>
        /// 
        /// </summary>
        public SyncBatchEntryResponse()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entry"></param>
        public SyncBatchEntryResponse(SyncBatchEntryRequest entry = null)
        {
            if (entry != null)
            {
                EntryId = entry.EntryId;
                EntityId = entry.EntityId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ResponseCode { get; set; }

        /// <summary>
        /// Gets or sets an error message when the synchronization can't succeed.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a dynamic object with ad hoc properties that can be returned to the caller.
        /// </summary>
        public dynamic UpdatedJsonEntity { get; set; }
    }
}