﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// This class represents the structure of a synchronization response.
    /// </summary>
    public class SyncBatchResponse : SyncBatch
    {
        /// <summary>
        /// Creates an instance of the SynchronizationResponse class.
        /// </summary>
        public SyncBatchResponse()
        {
            // Empty default constructor needed for auto generated
            // Web Api Help documentation.
        }

        /// <summary>
        /// Creates an instance of the SynchronizationResponse class.
        /// </summary>
        /// <param name="request">Optional SynchronizationRequest input parameter.</param>
        public SyncBatchResponse(SyncBatchRequest request)
            : this()
        {
            Batch = new List<SyncBatchEntryResponse>();

            if (request != null)
            {
                BatchId = request.BatchId;

                foreach (var entry in request.Batch)
                {
                    Batch.Add(new SyncBatchEntryResponse(entry));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public SyncStatus Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<SyncBatchEntryResponse> Batch { get; set; }
    }
}