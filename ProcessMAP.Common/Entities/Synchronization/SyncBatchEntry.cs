﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SyncBatchEntry
    {
        /// <summary>
        /// 
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid EntryId { get; set; }

        /// <summary>
        /// Gets or sets a Guid that represents a unique identifier for the entity.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public Guid EntityId { get; set; }
    }
}