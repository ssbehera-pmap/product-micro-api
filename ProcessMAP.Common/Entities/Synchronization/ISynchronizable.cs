﻿namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISynchronizable
    {
        /// <summary>
        /// Gets or sets the synchronization action.
        /// </summary>
        SyncAction SyncAction { get; set; }
    }
}