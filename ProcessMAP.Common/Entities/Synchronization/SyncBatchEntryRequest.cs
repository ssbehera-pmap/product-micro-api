﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.Synchronization
{
    /// <summary>
    /// 
    /// </summary>
    public class SyncBatchEntryRequest : SyncBatchEntry
    {
        /// <summary>
        /// Gets or sets the synchronization action.
        /// </summary>
        public SyncAction Action { get; set; }

        /// <summary>
        /// Gets or sets the id of the parent batch.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public Guid BatchId { get; set; }

        /// <summary>
        /// Gets or sets the type of entity to be synchronized.
        /// </summary>
        public SyncEntity EntityType { get; set; }

        /// <summary>
        /// Gets or sets the entity object.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the LocationId for entity (APB-64).
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        /// Gets a string representation of the JSON object.
        /// </summary>
        public string JsonEntity
        {
            get
            {
                if (Entity == null)
                    return string.Empty;

                return JsonConvert.SerializeObject(Entity);
            }
        }
    }
}