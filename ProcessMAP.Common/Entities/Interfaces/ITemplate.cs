﻿using System;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// Serves as the interface that defines the behavior of the Template.
    /// </summary>
    public interface ITemplate : ICRUD
    {
        /// <summary>
        /// Represents the consumer identifier where the template is associated with.
        /// </summary>
        int? ConsumerId { get; set; }

        /// <summary>
        /// Represents the template unique identifier.
        /// </summary>
        Guid Uid { get; set; }
    }
}