﻿using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Security.Authentication;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUserInfo
    {
        /// <summary>
        /// Gets or sets the user's id.
        /// </summary>
        int UserId { get; set; }

        /// <summary>
        /// Gets or sets the user's username.
        /// </summary>
        string Username { get; set; }

        /// <summary>
        /// Gets or sets the user's first name.
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the user's first last name.
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// Gets the user's full name.
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// Gets or sets the user's email.
        /// </summary>
        string Email { get; set; }

        /// <summary>
        /// Gets or sets the user's phone number.
        /// </summary>
        string Phone { get; set; }

        /// <summary>
        /// Gets or sets the type of user.
        /// </summary>
        string UserType { get; set; }

        /// <summary>
        /// Gets or sets the user's access level.
        /// </summary>
        string AccessLevel { get; set; }

        /// <summary>
        /// Gets or sets the user's default location
        /// </summary>
        LocationInfo DefaultLocation { get; set; }

        /// <summary>
        /// Indicates the type of authentication to be used for this user.
        /// This is used on hybrid configurations.
        /// </summary>
        AuthType AuthType { get; set; }
    }
}