﻿using System;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// Serves as the interface that defines the Create/Read/Update/Delete (CRUD) operations.
    /// </summary>
    public interface ICRUD
    {
        /// <summary>
        /// Represents the identifier of the user who created the template.
        /// </summary>
        int CreatedBy { get; set; }

        /// <summary>
        /// Represents the date when the template was created.
        /// </summary>
        DateTime CreatedDate { get; set; }

        /// <summary>
        /// Represents the template activity flag.
        /// </summary>
        bool IsActive { get; set; }

        /// <summary>
        /// Represents the optional template updated user id.
        /// </summary>
        int? UpdatedBy { get; set; }

        /// <summary>
        /// Represents the optional template updated date.
        /// </summary>
        DateTime? UpdatedDate { get; set; }
    }
}