﻿using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.Interfaces
{
    public interface ISMS
    {
        string Body { get; set; }
        string From { get; set; }
        List<string> To { get; set; }
    }
}