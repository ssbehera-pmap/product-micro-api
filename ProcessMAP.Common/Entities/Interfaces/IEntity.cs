﻿using System;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// Entity interface.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Represents the unique identifier of the entity.
        /// </summary>
        long Id { get; set; }
        
        /// <summary>
        /// Represents the identifier of the user that created the entity.
        /// </summary>
        int CreatedBy { get; set; }

        /// <summary>
        /// Represents the full name of the user that created the entity.
        /// </summary>
        string CreatedByName { get; set; }

        /// <summary>
        /// Represents the date that the entity was created.
        /// </summary>
        DateTime CreatedDate { get; set; }

        /// <summary>
        /// Represents the optional identifier of the user that updated the entity.
        /// </summary>
        int? UpdatedBy { get; set; }

        /// <summary>
        /// Represents the optional date that the entity was updated.
        /// </summary>
        DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Represents the optional full name of the user that updated the entity.
        /// </summary>
        string UpdatedByName { get; set; }
    }
}