﻿using ProcessMAP.Common.Enumerations;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// Contract for entities that need to carry database information.
    /// </summary>
    public interface IDatabaseInfo
    {
        /// <summary>
        /// Gets or sets the type of database.
        /// </summary>
        DatabaseType DatabaseType { get; set; }

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the name of the database.
        /// </summary>
        string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        string ConnString { get; set; }

        /// <summary>
        /// Gets or sets the name of the database instance.
        /// </summary>
        string InstanceName { get; set; }

        /// <summary>
        /// Gets or sets the id of the tenant.
        /// </summary>
        string TenantId { get; set; }

        /// <summary>
        /// Connection string in format needed for the Entity Framework.
        /// </summary>
        string EntityModelConnectionString { get; }
    }
}