﻿using System;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// Serves as the entity that is passed between layers.
    /// </summary>
    public interface IData
    {
        /// <summary>
        /// Represents the Consumer identifier.
        /// </summary>
        int ConsumerId { get; set; }

        /// <summary>
        /// Represents the unique identifier.
        /// </summary>
        Guid Uid { get; set; }
    }
}