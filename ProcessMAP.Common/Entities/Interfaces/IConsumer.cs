﻿using Newtonsoft.Json;
using ProcessMAP.Security.Authentication;
using ProcessMAP.Security.Cryptography;
using ProcessMAP.Security.Entities;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// Defines the consumer contract.
    /// </summary>
    public interface IConsumer
    {
        /// <summary>
        /// Gets or sets the consumer id.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //[JsonIgnore]
        //[XmlIgnore]
        bool IsActive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets a collection of claim settings.
        /// </summary>
        //[JsonIgnore]
        //[XmlIgnore]
        List<Claim> ClaimSettings { get; set; }

        /// <summary>
        /// Gets or sets a collection of allowed endpoints.
        /// </summary>
        //[JsonIgnore]
        //[XmlIgnore]
        List<string> AllowedEndpoints { get; set; }

        /// <summary>
        /// Represents a collection of the consumer's authentication info.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        IEnumerable<IAuthenticationInfo> AuthenticationInfoList { get; set; }

        /// <summary>
        /// Represents the consumer's database info.
        /// </summary>
        DatabaseInfo DatabaseInfo { get; set; }

        /// <summary>
        /// Gets or sets information about the consumer's encryption mechanisms.
        /// </summary>
        ICryptographyInfo CryptographyInfo { get; set; }

        /// <summary>
        /// Represents the Application unique identifier.
        /// </summary>
        Guid ApplicationId { get; set; }
    }
}