﻿using ProcessMAP.Common.Caching;

namespace ProcessMAP.Common.Entities.Interfaces
{
    /// <summary>
    /// Interface for cache-able consumers.
    /// </summary>
    public interface ICacheableConsumer : IConsumer, ICacheableEntity
    {
        /// <summary>
        /// Represents the duration of the cache in seconds.
        /// </summary>
        //[JsonIgnore]
        //[XmlIgnore]
        int ApplicationCacheExpiration { get; set; }
    }
}