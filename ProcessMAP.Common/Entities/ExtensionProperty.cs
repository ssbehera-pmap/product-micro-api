﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ProcessMAP.Common.Entities
{
    /// <summary>
    /// Holds key/value pairs of data objects.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class ExtensionProperty<TKey, TValue>
    {
        /// <summary>
        /// The id of the object.
        /// </summary>
        public virtual TKey Id { get; set; }
        
        /// <summary>
        /// The object.
        /// </summary>
        public virtual TValue Description { get; set; }

        /// <summary>
        /// The id of the parent object.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public dynamic ParentObjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}: {1}", Id, Description);
        }
    }
}