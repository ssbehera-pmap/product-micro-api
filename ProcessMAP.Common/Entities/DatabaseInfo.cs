﻿using ProcessMAP.Common.Entities.Interfaces;
using ProcessMAP.Common.Enumerations;
using System;

namespace ProcessMAP.Common.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class DatabaseInfo : IDatabaseInfo
    {
        /// <summary>
        /// Creates an instance of the DatabaseInfo class.
        /// </summary>
        public DatabaseInfo()
        {
            // Empty constructor needed for serialization.
        }

        /// <summary>
        /// Creates an instance of the DatabaseInfo class.
        /// </summary>
        /// <param name="entityFramewworkDbContextName"></param>
        public DatabaseInfo(string entityFramewworkDbContextName)
            : this()
        {
            EntityFrameworkDbContextName = entityFramewworkDbContextName;
        }

        /// <summary>
        /// Gets or sets the name of the entity framework database context.
        /// </summary>
        public string EntityFrameworkDbContextName { get; set; }

        /// <summary>
        /// Gets or sets the type of the database.
        /// </summary>
        //[JsonIgnore]
        //[XmlIgnore]
        public DatabaseType DatabaseType { get; set; }

        /// <summary>
        /// Gets or sets the connection string to the database.
        /// </summary>
        //[JsonIgnore]
        //[XmlIgnore]
        public string ConnString { get; set; }

        /// <summary>
        /// Gets or sets the name of the database catalog.
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the name of the database server.
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the name of the database instance.
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// Gets or sets the id of the tenant.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Gets the connection string in the format needed for the Entity Framework.
        /// </summary>
        public string EntityModelConnectionString
        {
            get
            {
                /* todo: mega
                 * 
                 * if (string.IsNullOrWhiteSpace(EntityFrameworkDbContextName))
                                    EntityFrameworkDbContextName = "DataAccess.WorkflowDataModel";

                                var entityStringBuilder = new EntityConnectionStringBuilder();
                                entityStringBuilder.ProviderConnectionString = ConnString;
                                entityStringBuilder.Metadata = string.Format("res:/QQ/QQ*QQ/{0}.csdl|res:/QQ/QQ*QQ/{0}.ssdl|res:/QQ/QQ*QQ/{0}.msl", EntityFrameworkDbContextName);

                                return entityStringBuilder.ConnectionString;
             */
                return "MEGASTUB for EntityModelConnectionString!";
            }
        } 
    } 
}