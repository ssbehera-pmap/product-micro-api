﻿namespace ProcessMAP.Common.Entities.Base
{
    /// <summary>
    /// Responsible for managing the PMAP Lookups.
    /// </summary>
    public class PMAPLookup
    {
        /// <summary>
        /// Represents the unique identifier of a PMAP Lookup.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Represents the Description of a PMAP Lookup.
        /// </summary>
        public string Description { get; set; }
    }
}