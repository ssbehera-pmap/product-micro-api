﻿using System;

namespace ProcessMAP.Common.Entities.Base
{
    /// <summary>
    /// Entity with additional unique identifier (Guid)
    /// </summary>
    public class UEntity : Entity
    {
        /// <summary>
        /// Gets or sets a unique identifier for the element in the tree.
        /// </summary>
        public new virtual Guid Uid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the date in which the entity was deleted.
        /// </summary>
        public DateTime? DeleteDate { get; set; }
    }
}