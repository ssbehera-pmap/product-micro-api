﻿using ProcessMAP.Common.Entities.Interfaces;
using System;

namespace ProcessMAP.Common.Entities.Base
{
    /// <summary>
    /// Represents an entity in its basic form.
    /// It contains properties that all entities should inherit.
    /// </summary>
    public abstract class Entity : IEntity
    {
        /// <summary>
        /// Entity constructor
        /// </summary>
        public Entity()
        { }

        /// <summary>
        /// Gets or sets the id of the user that created the entity.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that created the entity.
        /// </summary>
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the id of the entity.
        /// </summary>
        public virtual long Id { get; set; }

        /// <summary>
        /// Gets or sets the id of the user that updated the entity.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that updated the entity.
        /// </summary>
        public string UpdatedByName { get; set; }

        /// <summary>
        /// Gets or sets the DateTime object in UTC when the entity was updated.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }
        /// <summary>
        /// Represents the globally unique identifier of the record.
        /// </summary> 
        public Guid? Uid { get; set; }
    }
}