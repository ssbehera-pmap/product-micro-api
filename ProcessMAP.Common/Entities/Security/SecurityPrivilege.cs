﻿namespace ProcessMAP.Common.Entities.Security
{
    /// <summary>
    /// Represents the Security Privilege.
    /// </summary>
    public class SecurityPrivilege
    {
        /// <summary>
        /// Represents the Privilege unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Represents the Privilege Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Represents the Privilege Permission unique identifier.
        /// </summary>
        public int PermissionId { get; set; }

        /// <summary>
        /// Represents the Privilege.
        /// </summary>
        public string Privilege { get; set; }

        /// <summary>
        /// Represents the Privilege Section unique identifier.
        /// </summary>
        public int SectionId { get; set; }

        /// <summary>
        /// Represents the Privilege Created By unique identifier.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Represents the Privilege Updated By optional unique identifier.
        /// </summary>
        public int? UpdatedBy { get; set; }

        /// <summary>
        /// Represents the Privilege default constructor.
        /// </summary>
        public SecurityPrivilege() { }
    }
}