﻿using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Security.Entities;
using System.Collections.Generic;

namespace ProcessMAP.Common.Entities.Security
{
    /// <summary>
    /// Represents a security entity that binds Users, Roles and Groups.
    /// </summary>
    public class Bond
    {
        /// <summary>
        /// Represents a list of Bond Users.
        /// </summary>
        public List<UserInfo> Users { get; set; }

        /// <summary>
        /// Represents a list of Bond Roles.
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// Represents a list of Bond Groups.
        /// </summary>
        public List<Group> Groups { get; set; }
    }
}