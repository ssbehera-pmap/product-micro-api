﻿using NLog;
using ProcessMAP.Common.Utilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace ProcessMAP.Common.Logging
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [ExcludeFromCodeCoverage]
    public static class PmapLogger<T>
    {
        #region Private Members
        /// <summary>
        /// Internal dictionary of loggers.
        /// </summary>
        private static readonly Dictionary<Type, ILogger> loggers;

        /// <summary>
        /// Logger lock object.
        /// </summary>
        private static readonly object loggerLock = new object();

        /// <summary>
        /// Private queue of LogMessage objects to ensure that the messages
        /// are logged in the order in which they were received.
        /// ConcurrentQueue is thread safe.
        /// </summary>
        private static readonly ConcurrentQueue<LogMessage> messageQueue;

        /// <summary>
        /// Event that is raised when new items are added to the queue.
        /// </summary>
        private static event EventHandler<EventArgs> NewMessageEnqueued;
        #endregion

        #region Constructor
        static PmapLogger()
        {
            loggers = new Dictionary<Type, ILogger>();
            messageQueue = new ConcurrentQueue<LogMessage>();
            NewMessageEnqueued += HandleNewMessageEnqueued;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Determines whether the logger is enabled for the specified Level.
        /// </summary>
        public static bool IsEnabledForLevel(LogLevel level)
        {
            var isEnabled = false;
            ILogger log = GetLogger(typeof(T));

            if (level == LogLevel.Debug)
                isEnabled = log.IsDebugEnabled;
            if (level == LogLevel.Info)
                isEnabled = log.IsInfoEnabled;
            if (level == LogLevel.Error)
                isEnabled = log.IsErrorEnabled;
            if (level == LogLevel.Fatal)
                isEnabled = log.IsFatalEnabled;
            if (level == LogLevel.Warn)
                isEnabled = log.IsWarnEnabled;

            return isEnabled;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerIdValue"></param>
        public static void AddConsumerIdContextProperty(string consumerIdValue = null)
        {
            var propertyKey = "consumerId";

            if (!string.IsNullOrWhiteSpace(consumerIdValue))
            {
                MappedDiagnosticsLogicalContext.Set(propertyKey, consumerIdValue);
                return;
            }

            var context = GetContext.Current;

            if (context != null)
            {
                if (context.Items.ContainsKey(PmapRequestHeaders.ConsumerId))
                {
                    if (int.TryParse(context.Items[PmapRequestHeaders.ConsumerId].ToString(), out int consumerId))
                    {
                        if (consumerId > 0)
                        {
                            //LogicalThreadContext.Properties[propertyKey] = consumerId;
                            MappedDiagnosticsLogicalContext.Set(propertyKey, consumerId);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method adds or replaces an entry to the LogicalThreadContext.Properties of the Logger.
        /// It adds the value passed and if no value is passed, it tries to fetch the value from the
        /// HttpContext.Items dictionary.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        public static void AddContextProperty<TVal>(string propertyName, TVal value = default)
        {
            //
            // If no value was passed
            // try to fetch it from the HttpContext.Items collection.
            //
            if (value == null || value.Equals(default(TVal)))
            {
                var context = GetContext.Current;
                if (context != null)
                {
                    // If HttpContext.Items doesn't contain the entry, exit the method.
                    // A value should have been added in the Global.asax file.
                    if (!context.Items.ContainsKey(propertyName)) return;

                    // Assign the value found
                    value = (TVal)context.Items[propertyName];
                }
            }

            //
            // Add value
            //
            //LogicalThreadContext.Properties[propertyName] = value;
            MappedDiagnosticsLogicalContext.Set(propertyName, value);
        }
        #endregion

        #region Debug
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void Debug(string message)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Debug,
                Message = message
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void DebugFormat(string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Debug,
                Message = string.Format(format, args)
            };

            EnqueueNewMessage(logMessage);
        }
        #endregion

        #region Info
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void Info(string message)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Info,
                Message = message
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void InfoFormat(string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Info,
                Message = string.Format(format, args)
            };

            EnqueueNewMessage(logMessage);
        }
        #endregion

        #region Warning
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void Warning(string message)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Warn,
                Message = message
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Warning(string message, Exception exception)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Warn,
                Message = message,
                Exception = exception
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void WarningFormat(string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Warn,
                Message = string.Format(format, args)
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void WarningFormat(Exception exception, string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Warn,
                Message = string.Format(format, args),
                Exception = exception
            };

            EnqueueNewMessage(logMessage);
        }
        #endregion

        #region Error
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void Error(string message)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Error,
                Message = message
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Error(string message, Exception exception)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Error,
                Message = message,
                Exception = exception
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void ErrorFormat(string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Error,
                Message = string.Format(format, args)
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void ErrorFormat(Exception exception, string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Error,
                Message = string.Format(format, args),
                Exception = exception
            };

            EnqueueNewMessage(logMessage);
        }
        #endregion

        #region Fatal
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void Fatal(string message)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Fatal,
                Message = message
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Fatal(string message, Exception exception)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Fatal,
                Message = message,
                Exception = exception
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void FatalFormat(string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Fatal,
                Message = string.Format(format, args)
            };

            EnqueueNewMessage(logMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void FatalFormat(Exception exception, string format, params object[] args)
        {
            var logMessage = new LogMessage()
            {
                Level = LogLevel.Fatal,
                Message = string.Format(format, args),
                Exception = exception
            };

            EnqueueNewMessage(logMessage);
        }
        #endregion

        #region Private Methods
        static void EnqueueNewMessage(LogMessage logMessage)
        {
            if (logMessage != null)
            {
                messageQueue.Enqueue(logMessage);
                OnNewMessageEnqueued();
            }
        }

        static void OnNewMessageEnqueued()
        {
            NewMessageEnqueued?.Invoke(null, EventArgs.Empty);
        }

        static void HandleNewMessageEnqueued(object sender, EventArgs e)
        {
            // Add ConsumerId to the logger's context properties.
            // Get it from the HttpContext.Items.
            AddContextProperty<int>(PmapRequestHeaders.ConsumerId);

            // Add UserId to the logger's context properties.
            // Get it from the HttpContext.Items.
            AddContextProperty<int>(PmapRequestHeaders.UserId);

            // Add the request unique identifier to the logger's context properties.
            // Get it from the HttpContext.Items.
            AddContextProperty<Guid>(PmapRequestKeys.RequestId);

            // Add the endpoint to the logger's context properties.
            // Get it from the HttpContext.Items.
            AddContextProperty<string>(PmapRequestKeys.RequestEndpoint);

            Task.Factory.StartNew(() =>
            {
                WriteLogMessage();
            });
        }

        private static void WriteLogMessage()
        {
            if (!messageQueue.IsEmpty)
            {
                ILogger log = GetLogger(typeof(T));

                while (messageQueue.TryDequeue(out LogMessage logMessage))
                {
                    if (logMessage != null)
                    {
                        if (logMessage.Level == LogLevel.Debug)
                        {
                            if (log.IsDebugEnabled) log.Debug(logMessage.Exception, logMessage.Message);
                        }
                        else if (logMessage.Level == LogLevel.Info)
                        {
                            if (log.IsInfoEnabled) log.Info(logMessage.Exception, logMessage.Message);
                        }
                        else if (logMessage.Level == LogLevel.Warn)
                        {
                            if (log.IsWarnEnabled) log.Warn(logMessage.Exception, logMessage.Message);
                        }
                        else if (logMessage.Level == LogLevel.Error)
                        {
                            if (log.IsErrorEnabled) log.Error(logMessage.Exception, logMessage.Message);
                        }
                        else if (logMessage.Level == LogLevel.Fatal)
                        {
                            if (log.IsFatalEnabled) log.Fatal(logMessage.Exception, logMessage.Message);
                        }
                    }
                }
            }
        }

        private static ILogger GetLogger(Type type)
        {
            lock (loggerLock)
            {
                if (loggers.ContainsKey(type) == false)
                {
                    ILogger log = NLogManager.LogFactory.GetCurrentClassLogger();

                    loggers.Add(type, log);
                }
            }

            return loggers[type];
        }
        #endregion
    }
}