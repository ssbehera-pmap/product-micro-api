﻿using NLog;
using ProcessMAP.Common.Utilities;
using System.IO;
using System.Reflection;

namespace ProcessMAP.Common.Logging
{
    /// <summary>
    /// NLog related operations and configurations.
    /// </summary>
    public static class NLogManager
    {
        private static LogFactory _logFactory;
        private static readonly object obj = new object();

        /// <summary>
        /// NLogFactory object based on the nlog.config file in the /bin directory.
        /// </summary>
        public static LogFactory LogFactory
        {
            get
            {
                if (_logFactory == null)
                {
                    lock(obj)
                    {
                        if (_logFactory == null)
                        {
                            var assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                            //var filePath = Path.Combine(assemblyFolder, "conf", "nlog.config");
                            var filePath = Path.Combine(assemblyFolder, CommonConstants.Conf, CommonConstants.NlogConfig);
                            _logFactory = LogManager.LoadConfiguration(filePath);
                        }
                    }
                }
                return _logFactory;
            }
        }
    }
}