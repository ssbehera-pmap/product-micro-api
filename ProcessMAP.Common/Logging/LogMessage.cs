﻿using NLog;
using System;

namespace ProcessMAP.Common.Logging
{
    internal class LogMessage
    {
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public LogLevel Level { get; set; }
    }
}
