﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.ErrorHandling
{
    /// <summary>
    /// Exception that is thrown when one or more validations fail.
    /// </summary>
    public class ValidationException : ApiException
    {
        /// <summary>
        /// Initializes a new instance of the ValidationException class with the results of one validation.
        /// </summary>
        /// <param name="validationResult"></param>
        /// <param name="innerException"></param>
        /// <param name="message"></param>
        public ValidationException(System.ComponentModel.DataAnnotations.ValidationResult validationResult, string message = null, Exception innerException = null)
            : this(new[] { validationResult }, message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationResults"></param>
        /// <param name="innerException"></param>
        /// <param name="message"></param>
        public ValidationException(IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> validationResults, string message = null, Exception innerException = null)
            : base(message, innerException)
        {
            if (ValidationResults == null)
                ValidationResults = new List<System.ComponentModel.DataAnnotations.ValidationResult>();

            ValidationResults = validationResults;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> ValidationResults { get; set; }
    }
}