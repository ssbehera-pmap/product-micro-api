﻿using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.ErrorHandling
{
    /// <summary>
    /// Represents the results of a given validation.
    /// </summary>
    public class ValidationResult
    {
        /// <summary>
        /// Creates an instance of the <see cref="ValidationResult"/> class.
        /// </summary>
        public ValidationResult()
        {
            Errors = new List<Error>();
        }

        /// <summary>
        /// Gets a value indicating the state of the validation object.
        /// </summary>
        public bool Passed
        {
            get
            {
                return (Errors.Count == 0) && (Exception == null);
            }
        }

        /// <summary>
        /// Gets or sets a collection of error messages.
        /// </summary>
        public List<Error> Errors { get; set; }

        /// <summary>
        /// Gets or sets an <see cref="Exception"/> object.
        /// </summary>
        public Exception Exception { get; set; }
    }
}