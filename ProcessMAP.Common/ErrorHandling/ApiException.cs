﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace ProcessMAP.Common.ErrorHandling
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class ApiException : Exception, IErrorMessage
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="innerException"></param>
        public ApiException(Error error, Exception innerException = null)
            : this(error.Message, innerException)
        {
            Error = error;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public ApiException(string message, Exception innerException = null)
            : base(message, innerException)
        { }

        /// <summary>
        /// 
        /// </summary>
        public Error Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}