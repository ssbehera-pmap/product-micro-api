﻿using System.Diagnostics.CodeAnalysis;
namespace ProcessMAP.Common.ErrorHandling
{
    /// <summary>
    /// Represents an Error with a code and a message.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public struct Error
    {
        /// <summary>
        /// An integer value that represents the error code.
        /// </summary>
        public int Code;
        
        /// <summary>
        /// A string value that represents the error message.
        /// </summary>
        public string Message;

        /// <summary>
        /// Creates an instance of the Error structure.
        /// </summary>
        /// <param name="code">The error code.</param>
        /// <param name="message">The error message.</param>
        public Error(int code, string message)
        {
            Code = code;
            Message = message;
        }

        /// <summary>
        /// Gets an empty instance of the Error class.
        /// </summary>
        public static Error Empty
        {
            get
            {
                return new Error(0, string.Empty);
            }
        }

        /// <summary>
        /// Determines if the instance of the Error structure is empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return (Code == 0) && (string.IsNullOrWhiteSpace(Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}: {1}", Code, Message);
        }
    }
}