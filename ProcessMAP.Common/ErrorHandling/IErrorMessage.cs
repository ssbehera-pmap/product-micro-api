﻿namespace ProcessMAP.Common.ErrorHandling
{
    /// <summary>
    /// This interface defines an ErrorMessage property contract.
    /// </summary>
    public interface IErrorMessage
    {
        /// <summary>
        /// Gets or set an Error element.
        /// </summary>
        Error Error { get; set; }
    }
}