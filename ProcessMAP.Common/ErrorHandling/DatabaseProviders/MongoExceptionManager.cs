﻿using System;

namespace ProcessMAP.Common.ErrorHandling.DatabaseProvides
{
    public class MongoExceptionManager : Exception
    {
        public string ErrorMessage { get; set; }
    }
}