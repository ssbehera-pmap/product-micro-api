﻿using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Entities.Foundation;
using ProcessMAP.Common.Entities.Interfaces;
using ProcessMAP.Common.Logging;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Caching
{
    /// <summary>
    /// Singleton Dictionary that maintains a list of Tenants in application memory.
    /// </summary>
    public class CachedConsumers : Dictionary<int, ICacheableConsumer>
    {
        /// <summary>
        /// Lazy, thread-safe way to access the instance of the singleton.
        /// </summary>
        private static readonly Lazy<CachedConsumers> instance = new Lazy<CachedConsumers>(() => new CachedConsumers());

        /// <summary>
        /// 
        /// </summary>
        private CachedConsumers() {}

        /// <summary>
        /// Gets an instance of the ConnectionStrings dictionary.
        /// </summary>
        public static CachedConsumers Elements
        {
            get { return instance.Value; }
        }

        /// <summary>
        /// Gets a value indicating id the specified consumer id is part of the cache.
        /// </summary>
        /// <param name="consumerId"></param>
        /// <returns></returns>
        public static bool Contains(int consumerId)
        {
            var containsKey = Elements.ContainsKey(consumerId);
            PmapLogger<CachedConsumers>.DebugFormat("Cache Elements contains consumer id [{0}] = [{1}].", consumerId, containsKey);
            return containsKey;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumer"></param>
        public static void Add(ICacheableConsumer consumer)
        {
            lock (Elements)
            {
                if (consumer != null)
                {
                    if (Elements.ContainsKey(consumer.Id) == false)
                    {
                        PmapLogger<CachedConsumers>.DebugFormat("Adding Consumer of id [{0}].", consumer.Id);
                        consumer.LastCached = DateTime.UtcNow;
                        Elements.Add(consumer.Id, consumer);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the connection string associated with the specified tenant URL, if it exists.
        /// </summary>
        /// <param name="consumerId">The unique id that identifies the customer.</param>
        /// <returns>A string representing the connection string.</returns>
        public static string GetConsumerConnectionString(int consumerId)
        {
            var dbInfo = GetConsumerDatabaseInfo(consumerId);
            PmapLogger<CachedConsumers>.DebugFormat("Getting Consumer Connection String for Consumer of Id [{0}].", consumerId);

            if (dbInfo == null)
                return null;

            return dbInfo.ConnString;
        }

        /// <summary>
        /// Gets the database info associated with the specified tenant, if it exists.
        /// </summary>
        /// <param name="consumerId">The unique id that identifies the customer.</param>
        /// <returns>An IDatabaseInfo object representing the consumer's database information.</returns>
        public static IDatabaseInfo GetConsumerDatabaseInfo(int consumerId)
        {
            if (consumerId <= 0)
                return null;

            if (!Elements.ContainsKey(consumerId))
                return null;

            PmapLogger<CachedConsumers>.DebugFormat("Getting Consumer Database Info for Consumer of id [{0}].", consumerId);

            var consumer = Elements[consumerId];

            if (consumer.DatabaseInfo == null)
                return null;

            consumer.DatabaseInfo.TenantId = consumerId.ToString();

            return consumer.DatabaseInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerId"></param>
        /// <returns></returns>
        public static ConsumerContext GetConsumerContext(int consumerId)
        {
            if (consumerId <= 0)
                return null;

            if (!Elements.ContainsKey(consumerId))
                return null;

            PmapLogger<CachedConsumers>.DebugFormat("Getting Consumer Database Info for Consumer of id [{0}].", consumerId);

            var consumer = Elements[consumerId] as Consumer;

            consumer.DatabaseInfo.TenantId = consumerId.ToString();

            return new ConsumerContext()
            {
                ConsumerId = consumerId,
                DatabaseInfo = consumer.DatabaseInfo,
                Consumer = consumer
            };
        }
    }
}