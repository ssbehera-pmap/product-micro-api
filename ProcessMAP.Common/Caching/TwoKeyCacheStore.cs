﻿using ProcessMAP.Common.Logging;
using System;
using System.Collections.Generic;

namespace ProcessMAP.Common.Caching
{
    /// <summary>
    /// Singleton to cache entities with two keys.
    /// </summary>
    public class TwoKeyCacheStore<Key1, Key2, Val>
        : TwoKeyDictionary<Key1, Key2, Val>
        where Val : ICacheableEntity
    {
        /// <summary>
        /// Lazy, thread-safe way to access the instance of the singleton.
        /// </summary>
        private static readonly Lazy<TwoKeyCacheStore<Key1, Key2, Val>> instance = new Lazy<TwoKeyCacheStore<Key1, Key2, Val>>(() => new TwoKeyCacheStore<Key1, Key2, Val>());

        /// <summary>
        /// 
        /// </summary>
        private int? ExpirationInMinutes;

        /// <summary>
        /// Private constructor
        /// </summary>
        private TwoKeyCacheStore() { }

        /// <summary>
        /// 
        /// </summary>
        public static TwoKeyCacheStore<Key1, Key2, Val> Instance
        {
            get { return instance.Value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expirationInMinutes"></param>
        public void SetExpiration(int expirationInMinutes)
        {
            ExpirationInMinutes = expirationInMinutes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <param name="entity"></param>
        public void Set(Key1 key1, Key2 key2, Val entity)
        {
            if (entity != null)
            {
                var utcNow = DateTime.UtcNow;
                PmapLogger<TwoKeyCacheStore<Key1, Key2, Val>>.Debug($"Adding entity with keys [{key1}, {key2}] @ {utcNow}.");
                entity.LastCached = utcNow;
                base[key1, key2] = entity;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public Val Get(Key1 key1, Key2 key2)
        {
            var result = default(Val);

            if (ContainsKey(key1, key2))
                result = base[key1, key2];

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public override bool ContainsKey(Key1 key1, Key2 key2)
        {
            if (!base.ContainsKey(key1, key2))
                return false;

            return !CheckIsExpired(key1, key2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        public void RemoveValues(Key1 key1)
        {
            if (base.ContainsKey(key1))
            {
                Clear(key1);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        public void RemoveValue(Key1 key1, Key2 key2)
        {
            if (ContainsKey(key1, key2))
            {
                Clear(key1, key2);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <returns></returns>
        public IEnumerable<Val> GetAllValues(Key1 key1)
        {
            return AllValues(key1);
        }

        private bool CheckIsExpired(Key1 key1, Key2 key2)
        {
            if (!ExpirationInMinutes.HasValue)
                return true;

            var entity = base[key1, key2];
            var utcNow = DateTime.UtcNow;
            var cacheDelta = utcNow - entity.LastCached;
            if (cacheDelta.TotalMinutes > ExpirationInMinutes)
            {
                PmapLogger<TwoKeyCacheStore<Key1, Key2, Val>>.Debug($"Hey, as of {utcNow}, the entity with keys [{key1}, {key2}] has expired. Let's remove it.");
                Clear(key1, key2);
                return true;
            }

            return false;
        }
    }
}