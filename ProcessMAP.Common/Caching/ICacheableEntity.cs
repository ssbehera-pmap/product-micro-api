﻿using System;

namespace ProcessMAP.Common.Caching
{
    /// <summary>
    /// Interface that enforces a cache-able entity.
    /// </summary>
    public interface ICacheableEntity
    {
        /// <summary>
        /// Represents the last time the entity was cached.
        /// </summary>
        //[JsonIgnore]
        //[XmlIgnore]
        DateTime LastCached { get; set; }
    }
}