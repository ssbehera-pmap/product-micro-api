﻿using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Common.Caching
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="K1"></typeparam>
    /// <typeparam name="K2"></typeparam>
    /// <typeparam name="V"></typeparam>
    public class TwoKeyDictionary<K1, K2, V>
    {
        //EE private Dictionary<K1, List<KeyValuePair<K2, V>>> dict = new Dictionary<K1, List<KeyValuePair<K2, V>>>();
        private readonly Dictionary<K1, List<KeyValuePair<K2, V>>> dict = new Dictionary<K1, List<KeyValuePair<K2, V>>>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        protected V this[K1 key1, K2 key2]
        {
            get
            {
                if (!ContainsKey(key1))
                    //EE return default(V);
                    return default;

                if (dict[key1].Count == 0)
                    //EE return default(V);
                    return default;

                var item = dict[key1].FirstOrDefault(i => i.Key.Equals(key2));
                if (item.Equals(default(KeyValuePair<K2, V>)))
                    //EE return default(V);
                    return default;

                return item.Value;
            }

            set
            {
                if (!ContainsKey(key1))
                    dict[key1] = new List<KeyValuePair<K2, V>>();

                if (dict[key1].Any(i => i.Key.Equals(key2)))
                {
                    var oldItem = dict[key1].FirstOrDefault(i => i.Key.Equals(key2));
                    if (!oldItem.Equals(default(KeyValuePair<K2, V>)))
                    {
                        dict[key1].Remove(oldItem);
                    }
                }
                if (!dict.ContainsKey(key1))
                    dict[key1].Add(new KeyValuePair<K2, V>(key2, value));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public virtual bool ContainsKey(K1 key1, K2 key2)
        {
            if (!ContainsKey(key1))
                return false;

            return dict[key1].Any(i => i.Key.Equals(key2));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key1"></param>
        /// <returns></returns>
        public virtual bool ContainsKey(K1 key1)
        {
            return dict.ContainsKey(key1);
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Clear(K1 key1 = default(K1), K2 key2 = default(K2))
        {
            if ((key1 == null || key1.Equals(default(K1))) && (key2 == null || key2.Equals(default(K2))))
            {
                dict.Clear();
                return;
            }

            if (key1 != null && !key1.Equals(default(K1)) && (key2 == null || key2.Equals(default(K2))))
            {
                dict.Remove(key1);
                return;
            }

            if (key1 != null && !key1.Equals(default(K1)) && key2 != null && !key2.Equals(default(K2)))
            {
                var item = dict[key1].FirstOrDefault(i => i.Key.Equals(key2));
                if (!item.Equals(default(KeyValuePair<K2, V>)))
                {
                    dict[key1].Remove(item);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected IEnumerable<V> AllValues(K1 key1)
        {
            if (dict == null || dict.Count == 0)
                return null;

            if (!ContainsKey(key1))
                return null;

            return dict[key1].Select(i => i.Value);
        }
    }
}