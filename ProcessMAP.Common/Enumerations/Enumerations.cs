﻿using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Common.Enumerations
{
    /// <summary>
    /// Enumeration of the ProcessMAP modules available for synchronization.
    /// </summary>
    public enum Module : ushort
    {
        /// <summary>
        /// 
        /// </summary>
        None = 0,

        /// <summary>
        /// 
        /// </summary>
        Foundation = 9,

        /// <summary>
        /// 
        /// </summary>
        DocumentManagement = 36,

        /// <summary>
        /// The BBS Module
        /// </summary>
        BBS = 21,

        /// <summary>
        /// The Wellness Check Module
        /// </summary>
        WellnessCheck = 27,

        /// <summary>
        /// The DAP "module".
        /// </summary>
        DAP = 75
    }

    /// <summary>
    /// Application types enumeration.
    /// </summary>
    public enum ApplicationType
    {
        /// <summary>
        /// None selected.
        /// </summary>
        None = 0,

        /// <summary>
        /// Web Application
        /// </summary>
        Web = 1,

        /// <summary>
        /// Mobile Application (Web)
        /// </summary>
        Mobile = 2,

        /// <summary>
        /// Mobile Plus Application (Hybrid)
        /// </summary>
        MobilePlus = 3,

        /// <summary>
        /// iOS Applications
        /// </summary>
        iOS = 4,

        /// <summary>
        /// Integration applications.
        /// </summary>
        Integrations = 5,

        /// <summary>
        /// Android applications
        /// </summary>
        Android = 6,

        /// <summary>
        /// Windows applications
        /// </summary>
        Windows = 7
    }

    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ChangeAction
    {
        /// <summary>
        /// 
        /// </summary>
        None,

        /// <summary>
        /// 
        /// </summary>
        Add,

        /// <summary>
        /// 
        /// </summary>
        Update,

        /// <summary>
        /// 
        /// </summary>
        Delete
    }

    /// <summary>
    /// Enumeration of database types.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DatabaseType
    {
        /// <summary>
        /// Microsoft SQL Server
        /// </summary>
        SqlServer = 1,

        /// <summary>
        /// MongoDb
        /// </summary>
        MongoDb = 2
    }

    /// <summary>
    /// Represents the name of the Collection.
    /// </summary>
    public enum CollectionName
    {
        /// <summary>
        /// Serves as the Custom Data Source collection.
        /// </summary>
        CustomDataSources,

        /// <summary>
        /// Serves as the Data collection.
        /// </summary>
        Data,

        /// <summary>
        /// Serves as the FIle collection.
        /// </summary>
        File,

        /// <summary>
        /// Serves as the File Info collectio.
        /// </summary>
        FileInfo,

        /// <summary>
        /// Serves as the Form collection.
        /// </summary>
        Form,

        /// <summary>
        /// Serves as the Sevron collection.
        /// </summary>
        Sevron,

        /// <summary>
        /// Serves as the View collection.
        /// </summary>
        View,
        /// <summary>
        /// Serves as the View Archive collection.
        /// </summary>
        ViewArchive,
        /// <summary>
        /// Serves as the ActionItems collection.
        /// </summary>
        ActionItems,

        /// <summary>
        /// Serves as the Attachment collection.
        /// </summary>
        Uploads,

        /// <summary>
        /// Serves as the System Translations collection.
        /// </summary>
        SystemTranslations,

        /// <summary>
        /// Serves as the Consumer Translations collection.
        /// </summary>
        ConsumerTranslations,

        /// <summary>
        /// Serves as the App Translations collection.
        /// </summary>
        AppTranslations,

        /// <summary>
        /// Serves as the Form Translations collection.
        /// </summary>
        FormTranslations,

        /// <summary>
        /// Serves as the Template collection.
        /// </summary>
        Template,

        /// <summary>
        /// Serves as the Badge collection.
        /// </summary>
        Badge,

        /// <summary>
        /// Serves as the ReadOnly Data Source collection.
        /// </summary>
        FormTemplates,

        /// <summary>
        /// Serves as the Form Backup collection.
        /// </summary>
        FormVersions,

        /// <summary>
        /// Serves as the SyncTemplate
        /// </summary>
        SyncTemplate,

        /// <summary>
        /// Serves as the PdfTemplate
        /// </summary>
        ExportTemplate,

        /// <summary>
        /// TemplateVersion
        /// </summary>
        TemplateVersion,

        /// <summary>
        /// BadgeVersion
        /// </summary>
        BadgeVersion,

        /// <summary>
        /// ExportTemplateVersion
        /// </summary>
        ExportTemplateVersion,

        /// <summary>
        /// Synchronization Batch Entry
        /// </summary>
        SynchronizationBatchEntry,

        /// <summary>
        /// Synchronization Batch Status
        /// </summary>
        SynchronizationBatchStatus,

        /// <summary>
        /// ScreenLayout
        /// </summary>
        ScreenLayout,

        /// <summary>
        /// Statistics
        /// </summary>
        Statistics,
        /// <summary>
        /// CustomDataSources
        /// </summary>
        DataSources,
        /// <summary>
        /// DataSourceValue
        /// </summary>
        DataSourceValues
    }

    /// <summary>
    /// Serves as the enumeration that defines the sort orders.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SortOrder
    {
        /// <summary>
        /// Represents the default Sort Order. The order will be defined by created.
        /// </summary>
        None = 0,

        /// <summary>
        /// Represents the Ascending Sort Order. The order increases alphabetically.
        /// </summary>
        Ascending = 1,

        /// <summary>
        /// Represents the Descending Sort Order. The order decreases alphabetically.
        /// </summary>
        Descending = 2
    }

    /// <summary>
    /// Serves as the enumeration that defines the Filter Options.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FilterOption
    {
        /// <summary>
        /// Represents the default option.
        /// </summary>
        None = 0,

        /// <summary>
        /// Represents the After option.
        /// </summary>
        After = 1,

        /// <summary>
        /// Represents the Before option.
        /// </summary>
        Before = 2,

        /// <summary>
        /// Represents the Last Month option.
        /// </summary>
        LastMonth = 3,

        /// <summary>
        /// Represents the Last 30 Day option.
        /// </summary>
        Last30Days = 4,

        /// <summary>
        /// Represents the This Month option.
        /// </summary>
        ThisMonth = 5,

        /// <summary>
        /// Represents the This Week option.
        /// </summary>
        ThisWeek = 6,

        /// <summary>
        /// Represents the Today option.
        /// </summary>
        Today = 7,

        /// <summary>
        /// Represents the Custom option.
        /// </summary>
        Custom = 8,

        /// <summary>
        /// Represents the Equal To option
        /// </summary>
        EqualTo = 9,

        /// <summary>
        /// Represents the Not Equal To option.
        /// </summary>
        NotEqualTo = 10,

        /// <summary>
        /// Represents the Less Than option.
        /// </summary>
        LessThan = 11,

        /// <summary>
        /// Represents the Less Than Or Equal To option.
        /// </summary>
        LessThanOrEqualTo = 12,

        /// <summary>
        /// Represents the Greater Than option.
        /// </summary>
        GreaterThan = 13,

        /// <summary>
        /// Represents the Greater Than Or Equal To option.
        /// </summary>
        GreaterThanOrEqualTo = 14,

        /// <summary>
        /// Represents the Contains option.
        /// </summary>
        Contains = 15
    }

    /// <summary>
    /// 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum UserTypes
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Record Creator")]
        RecordCreator
    }


    /// <summary>
    /// Serves as the enumeration that defines the status.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StatusFlags
    {
        /// <summary>
        /// Active Status.
        /// </summary>
        Active = 1,

        /// <summary>
        /// InActive Status.
        /// </summary>
        InActive = 2,


        /// <summary>
        /// Draft Status.
        /// </summary>
        Draft = 3,


        /// <summary>
        /// Delete Status.
        /// </summary>
        Delete = 4,

        /// <summary>
        /// Archieve Status.
        /// </summary>
        Archive = 5

    }

    /// <summary>
    /// 
    /// </summary>
    /// 
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BadgeType
    {
        /// <summary>
        /// Active Lebel.
        /// </summary>
        Lebel = 1,

        /// <summary>
        /// Data.
        /// </summary>
        Data = 2,

        /// <summary>
        /// Data.
        /// </summary>
        Dynamic = 3
    }
}