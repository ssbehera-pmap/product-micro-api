﻿using System;
namespace ProcessMAP.Common.PubSub
{
    public class MessageBusConfiguration
    {
        /// <summary>
        /// The connection string to the message queue.
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
