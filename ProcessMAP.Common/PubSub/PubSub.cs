﻿using System;
using System.Threading.Tasks;

namespace ProcessMAP.Common.PubSub
{
    
        /// <summary>
        /// Interface that represents the shape of the messages being published on any channel.
        /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Represents the unique identifier for this request.
        /// </summary>
        Guid RequestUid { get; set; }

        /// <summary>
        /// Represents the timeat which the message was published.
        /// </summary>
        DateTime When { get; set; }

        /// <summary>
        /// Represents a dynamic object as the payload of the message.
        /// </summary>
        dynamic Payload { get; set; }

        /// <summary>
        /// Represents the message Channel and all associated error messages.
        /// </summary>
        string Channel { get; set; }
    }
    public interface IMessageHandler
    {
        /// <summary>
        /// Handles a de-queued message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task HandleMessage(IMessage message);
    }

}
