﻿using System;
using Newtonsoft.Json;

namespace ProcessMAP.Common.PubSub
{
    
        /// <summary>
        /// Configuration settings for subscribers.
        /// </summary>
        public class SubscriberConfiguration : MessageBusConfiguration
        {
            /// <summary>
            /// The channel or topic to listen to.
            /// </summary>
            public string Channel { get; set; }

            /// <summary>
            /// The name of the queue. 
            /// Forms a round-robin between Subscribers when the queue name is the same.
            /// </summary>
            public string QueueName { get; set; }

            /// <summary>
            /// The message handler for this configuration.
            /// </summary>
            [JsonIgnore]
            public IMessageHandler MessageHandler { get; set; }
        }
    
}
