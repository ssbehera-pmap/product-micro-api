﻿using Microsoft.AspNetCore.Http;
using ProcessMAP.Common.ErrorHandling;
using ProcessMAP.Common.Logging;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessMAP.Common.Utilities
{
    /// <summary>
    /// Utility methods
    /// </summary>
    public static class Tools
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly DateTime EpochStartDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Gets a value representing the epoch in milliseconds for the current DateTime.Now object.
        /// </summary>
        /// <returns></returns>
        public static ulong Epoch()
        {
            return Epoch(DateTime.UtcNow);
        }

        /// <summary>
        /// Gets a value representing the epoch in milliseconds for the specified DateTime object.
        /// </summary>
        /// <returns></returns>
        public static ulong Epoch(DateTime dateTime)
        {
            var deltaUTC = dateTime - EpochStartDate;

            if (deltaUTC.TotalSeconds <= 0)
            {
                var error = string.Format("An error occurred trying to generate an EPOCH from DateTime: {0}.", dateTime.ToString());
                PmapLogger<DateTime>.Error(error);
                throw new ApiException(error);
            }

            return (ulong)deltaUTC.TotalSeconds;
        }

        /// <summary>
        /// Gets a value representing a DateTime object from the specified epoch in milliseconds.
        /// </summary>
        /// <param name="epoch"></param>
        /// <returns></returns>
        public static DateTime DateTimeFromEpoch(ulong epoch)
        {
            return EpochStartDate.AddSeconds(epoch);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="headers"></param>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public static T GetValueFromHeaders<T>(IHeaderDictionary headers, string headerName)
        {
            T value = default;

            if (headers.Keys.Select(key => key.ToLower()).Contains(headerName.ToLower()))
            {
                //old: var headerValue = headers.GetValues(headerName).FirstOrDefault();
                var headerValue = headers[headerName].FirstOrDefault();

                if (typeof(T).IsEnum)
                {
                    if (Enum.IsDefined(typeof(T), headerValue))
                    {
                        return (T)Enum.Parse(typeof(T), headerValue);
                    }
                    else
                    {
                        int.TryParse(headerValue, out int intValue);
                        if (Enum.IsDefined(typeof(T), intValue))
                        {
                            return (T)Enum.Parse(typeof(T), headerValue);
                        }
                    }
                }

                value = (T)Convert.ChangeType(headerValue, typeof(T));
            }

            return value;
        }

        public static FileStream Base64StringToBitmap(string base64String, string fileName)
        {
            byte[] imgBytes = Convert.FromBase64String(base64String);

            using (var imageFile = new FileStream(fileName, FileMode.Create))
            {
                imageFile.WriteAsync(imgBytes, 0, imgBytes.Length).GetAwaiter().GetResult();
                imageFile.FlushAsync().GetAwaiter().GetResult();
                return imageFile;
            }
        }

        public static void DeleteDirectoryRecursively(string destinationDir)
        {
            const int magicDust = 10;
            for (var gnomes = 1; gnomes <= magicDust; gnomes++)
            {
                try
                {
                    Directory.Delete(destinationDir, true);
                }
                catch (DirectoryNotFoundException)
                {
                    return;
                }
                catch (IOException)
                { 
                    Thread.Sleep(50);
                    continue;
                }
                return;
            }
        }

        public async static Task<string> ConvertImageURLToBase64(string url)
        {
            var credentials = new NetworkCredential("", "");
            using (var handler = new HttpClientHandler { Credentials = credentials })
            using (var client = new HttpClient(handler))
            {
                var bytes = await client.GetByteArrayAsync(url);
                return Convert.ToBase64String(bytes);
            }
        }
    }
}