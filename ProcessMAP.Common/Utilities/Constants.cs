﻿namespace ProcessMAP.Common.Utilities
{
    /// <summary>
    /// Represents header names used in our requests.
    /// </summary>
    public class PmapRequestHeaders
    {
        /// <summary>
        /// Header to represent the value of the consumer id.
        /// </summary>
        public const string ConsumerId = "ConsumerId";

        /// <summary>
        /// Header to represent the value of the authentication token.
        /// </summary>
        public const string Authorization = "Authorization";

        /// <summary>
        /// Header to represent the value of the user id.
        /// </summary>
        public const string UserId = "UserId";

        /// <summary>
        /// Header to represent the value of the location id.
        /// </summary>
        public const string LocationId = "LocationId";

        /// <summary>
        /// Header to represent the value of the level id.
        /// </summary>
        public const string LevelId = "LevelId";

        /// <summary>
        /// Header to represent the value of the module id.
        /// </summary>
        public const string ModuleId = "ModuleId";

        /// <summary>
        /// Header to represent the value of the desired language for translations.
        /// </summary>
        public const string AcceptLanguage = "Accept-Language";

        /// <summary>
        /// Header to represent the value of the application type of the consumer.
        /// </summary>
        public const string ApplicationType = "ApplicationType";

        /// <summary>
        /// Header to represent debug mode for the APIs.
        /// </summary>
        public const string Debug = "Debug";

        /// <summary>
        /// Represents the unique identifier of the source.
        /// </summary>
        public const string SourceId = "SourceId";

        /// <summary>
        /// Represents the unique identifier of the type of the source.
        /// </summary>
        public const string SourceTypeId = "SourceTypeId";

        /// <summary>
        /// Represents the unique identifier of the type of the source of the parent.
        /// </summary>
        public const string ParentSourceId = "ParentSourceId";

        /// <summary>
        /// Represents the name with extension of the file to be uploaded.
        /// </summary>
        public const string FileName = "FileName";

        /// <summary>
        /// Represents the Time-Zone header name.
        /// </summary>
        public const string TimeZone = "Time-Zone";

        /// <summary>
        /// Represents the Content-Type
        /// </summary>
        public const string ContentType = "Content-Type";

        /// <summary>
        /// Sets the File upload path if provided. Eg : For calendarModule: "locationId\MODULES\ACTIONITEM\SourceId". Sample :"8790\MODULES\ACTIONITEM\US-Westlake-16-CA-1973".
        /// </summary>
        public const string DestinationFolder = "destinationFolder";

        /// <summary>
        /// Sets the File Category /parent. Eg: Form/ActionItem/ControlNAme etc.
        /// </summary>
        public const string Source = "source";

        public const string CacheControl = "Cache-Control";
    }

    /// <summary>
    /// 
    /// </summary>
    public class PmapRequestKeys
    {
        /// <summary>
        /// 
        /// </summary>
        public const string RequestId = "RequestId";

        /// <summary>
        /// 
        /// </summary>
        public const string RequestEndpoint = "RequestEndpoint";
    }

    /// <summary>
    /// Serves as the parameters of the http headers.
    /// </summary>
    public class PmapHeaderParam
    {
        /// <summary>
        /// Represents the application/json
        /// </summary>
        public const string ApplicationJson = "application/json";
    }


    /// <summary>
    /// Serves as the configuration settings for Redis.
    /// </summary>
    public class RedisSetting
    {
        /// <summary>
        /// Represents the Redis Host.
        /// </summary>
        public const string Host = "RedisHost";

        /// <summary>
        /// Represents the Redis Port.
        /// </summary>
        public const string Port = "RedisPort";

        /// <summary>
        /// Represents the Redis Expiration.
        /// </summary>
        public const string ExpiresIn = "RedisExpiration";
    }

    /// <summary>
    /// Serves as the role notification information parameters.
    /// </summary>
    public class RoleNotifyInfoParam
    {
        /// <summary>
        /// Represents the Role Notification Information Endpoint.
        /// </summary>
        public const string Endpoint = "RoleNotifyInfoEndpoint";
    }

    /// <summary>
    /// Serves as the Template Parameters.
    /// </summary>
    public class TemplateParam
    {
        /// <summary>
        /// Represents the Location Code
        /// </summary>
        //public const string LocationCode = "__locationCode__";
        public const string LocationCode = "@@locationCode";

        /// <summary>
        /// lbl_locationCode__
        /// </summary>
        public const string LblLocationCode = "lbl_locationCode__";

        /// <summary>
        /// Represents the Event Date
        /// </summary>
        public const string EventDate = "__eventDate__";

        /// <summary>
        /// Represents the Security Class
        /// </summary>
        public const string SecurityClass = "__securityClass__";

        /// <summary>
        /// Represents the Security Type
        /// </summary>
        public const string SecurityType = "__securityType__";

        /// <summary>
        /// Represents the Event Description
        /// </summary>
        public const string DescriptionOfEvent = "__descriptionOfEvent__";

        /// <summary>
        /// Represents the Status
        /// </summary>
        public const string Status = "__status__";

        /// <summary>
        /// Represents the Disclaimer
        /// </summary>
        public const string Disclaimer = "__disclaimer__";

        /// <summary>
        /// Represents the Form Name
        /// </summary>
        //public const string FormName = "__formName__";
        public const string FormName = "@@formTitle";

        /// <summary>
        /// Represents the Action Condition
        /// </summary>
        public const string ActionCondition = "@@actionCondition";

        /// <summary>
        /// Represents the Detail Report
        /// </summary>
        public const string DetailReport = "__detailReport__";

        /// <summary>
        /// Represents the Detail Report
        /// </summary>
        public const string LblDetailReport = "lbl_DetailReport__";

        /// <summary>
        /// Represents the Document Link
        /// </summary>
        public const string DocumentLink = "@@documentLink";

        /// <summary>
        /// Represents the template location code
        /// </summary>
        public const string TmpLocationCode = "tmpLocationCode__";

        /// <summary>
        /// Represents the template event date
        /// </summary>
        public const string TmpEventDate = "tmpEventDate__";

        /// <summary>
        /// Represents the template security class
        /// </summary>
        public const string TmpSecurityClass = "tmpSecurityClass__";

        /// <summary>
        /// Represents the template security type
        /// </summary>
        public const string TmpSecurityType = "tmpSecurityType__";

        /// <summary>
        /// Represents the template description of event
        /// </summary>
        public const string TmpDescriptionOfEvent = "tmpDescriptionOfEvent__";

        /// <summary>
        /// Represents the template status
        /// </summary>
        public const string TmpStatus = "tmpStatus__";

        /// <summary>
        /// Represents the template disclaimer.
        /// </summary>
        //public const string TmpDisclaimer = "tmpDisclaimer__";
        public const string TmpDisclaimer = "lbl_Disclaimer__";
        /// <summary>
        /// Represents the template disclaimer.
        /// </summary>
        //public const string TmpDisclaimer = "tmpDisclaimer__";
        public const string DisclaimerValue = "Disclaimer: This email is confidential and is intended only for the valid users of lbl_Disclaimer__. Since this email is system generated, do not reply to this message.";

        /// <summary>
        /// Represents the template action condition.
        /// </summary>
        public const string TmpActionCondition = "tmpActionCondition__";

        /// <summary>
        /// Represents the template detail report.
        /// </summary>
        public const string TmpDetailReport = "tmpDetailReport__";
        /// <summary>
        /// recordUpdatedBy
        /// </summary>
        public const string RecordUpdatedBy = "@@recordUpdatedby";

        /// <summary>
        /// recordCreatedBy
        /// </summary>
        public const string RecordCreatedBy = "@@recordCreatedby";

        /// <summary>
        /// recordCreatedBy
        /// </summary>
        public const string RecordUpdatedDate = "@@recordUpdateddate";

        /// <summary>
        /// recordCreatedBy
        /// </summary>
        public const string RecordCreatedDate = "@@recordCreateddate";

        /// <summary>
        /// CompanyLogo
        /// </summary>
        public const string CompanyLogo = "@@companyLogo";

        /// <summary>
        /// FormIcon
        /// </summary>
        public const string FormIcon = "@@formIcon";

        /// <summary>
        /// ReportName
        /// </summary>
        public const string ReportName = "@@reportName";

        /// <summary>
        /// LocalDateTimeStamp
        /// </summary>
        public const string LocalDateTimeStamp = "@@localDatetimestamp";

        /// <summary>
        /// UTCDateTimeStamp
        /// </summary>
        public const string UTCDateTimeStamp = "@@utcDatetimestamp";

        /// <summary>
        /// Pagination
        /// </summary>
        public const string Pagination = "@@pagination";

        /// <summary>
        /// Pagination
        /// </summary>
        public const string CurrentUser = "@@currentUser";
    }

    /// <summary>
    /// Serves a general formatter.
    /// </summary>
    public class Formatter
    {
        /// <summary>
        /// Represents the template date format.
        /// </summary>
        public const string TemplateDate = "{0:MMMM dd, yyyy}";
    }

    /// <summary>
    /// Serves as the Custom Settings.
    /// </summary>
    public class CustomSettings
    {
        /// <summary>
        /// Represents the Custom Settings Module Endpoint
        /// </summary>
        public const string ModuleEndpoint = "papi/v1/settings/module/0";

        /// <summary>
        /// Represents the Client Key.
        /// </summary>
        public const string ClientKey = "CLIENT";


        /// <summary>
        /// Represents the SITE_URL.
        /// </summary>
        public const string SiteUrl = "SITE_URL";
    }

    /// <summary>
    /// 
    /// </summary>
    public class AppVersion
    {
        /// <summary>
        /// Version 1.0
        /// </summary>
        public const string DefaultVersion = "1.0";
    }

    public class CommonConstants
    {
        public const string Space = " ";
        public const string AccessToTheEndpointIsForbidden = "Access to the endpoint is forbidden.";
        public const string ActionItemCompletedBy = "ActionItemCompletedBy";
        public const string ActionItemCompletedByTitle = "Action Item Completed By";
        public const string ActionItemCompletedDate = "ActionItemCompletedDate";
        public const string ActionItemcompletedDateTitle = "Action Item Completed Date";
        public const string ActionItemDescription = "ActionItemDescription";
        public const string ActionItemDescriptionTitle = "Action Item Description";
        public const string ActionItemDueDate = "ActionItemDueDate";
        public const string ActionItemDueDateTitle = "Action Item Due Date";
        public const string ActionItemStatus = "ActionItemStatus";
        public const string ActionItemStatusTitle = "Action Item Status";
        public const string ActionTaken = "ActionTaken";
        public const string ActionTakenTitle = "Action Taken";
        public const string ActionItemTitle = "ActionItemTitle";
        public const string ActionItemTitleLabel = "Action Item Title";
        public const string ADataAccessRelatedIssueOccurredWhileTryingToFulfillYourRequest = "A data access related issue occurred while trying to fulfill your request.";
        public const string AnErrorOccurredIn  = "An error occurred in ";
        public const string AppConfig = "AppConfig";
        public const string AppBuilderNotification = "AppBuilderNotification";
        public const string ActionItemSync = "ActionItemSync";
        public const string ApplicationJson = "application/json";
        public const string AssignedBy = "AssignedBy";
        public const string AssignedByTitle = "Assigned By";
        public const string BatchId = "batchId";
        public const string CannotBeNull = " cannot be null.";
        public const string Conf = "conf";
        public const string CouldNotBeFound = " could not be found.";
        public const string CommentsTitle = "Comments";
        public const string CompleteanActionItem = "CompleteanActionItem";
        public const string DataSync = "DataSync";
        public const string DateTime = "DateTime";
        public const string DefaultViewDescription = "Default view description...";
        public const string DocumentsForTheSpecifiedFormUid = " Documents for the specified formUid: ";
        public const string Error = " Error";
        public const string Endpoint = " endpoint";
        public const string EndpointWithTheFollowingParameter = " endpoint with the following parameter(s): ";
        public const string EndpointWithAValidDocumentWith  = " endpoint with a valid document with ";
        public const string EnteringIntoThe = "Entering into the ";
        public const string FormData = "form-data";
        public const string FullName = "FullName";
        public const string GMTStandardTime = "GMT Standard Time";
        public const string InvalidInputParameter = "Invalid input parameter ";
        public const string InvalidRequest = "Invalid Request";
        public const string IsInvalid = " is invalid.";
        public const string LeavingThe = "Leaving the ";
        public const string ListView = "List View";
        public const string MicroserviceLaunched = "microservice launched";
        public const string MissingRequiredHeader = "Missing required header ";
        public const string MethodWith = " method with ";
        public const string Name = "Name";
        public const string NlogConfig = "nlog.config";
        public const string No = "No";
        public const string NoRecipientsErr = "NO-RECIPIENTS ERR!";
        public const string NotifyOwnersImmediately = "NotifyOwnersImmediately";
        public const string NotifyOwnersImmediatelyTitle = "Notify Owners Immediately";
        public const string Null = "null";
        public const string Owners = "Owners";
        public const string Password = "password";
        public const string PDF = "PDF";
        public const string PriorityLevel = "PriorityLevel";
        public const string PriorityLevelTitle = "Priority Level";
        public const string Repeater = "repeater";
        public const string RestServiceHostUrlIsNotSet = "RestServiceHostUrl is not set.";
        public const string RequiredVerification = "RequiredVerification";
        public const string RequiredVerificationDisable = "RequiredVerificationDisable";
        public const string RequiredVerificationTitle = "Required Verification";
        public const string SeeDocumentation = "See documentation.";
        public const string TemplateBasedPDF = "TemplateBasedPDF";
        public const string TemplateForTheSpecifiedUid = "Template for the specified Uid: ";
        public const string TheDocumentWithUid = "The document with uid ";
        public const string TheDocumentYouHaveRequestedDoesNotExist = "The document you have requested does not exist.";
        public const string TheInputParameter = "The input parameter ";
        public const string TheFormWithUid  = "The form with uid ";
        public const string ThePayloadForThisEndpointCannotBeNull = "The payload for this endpoint cannot be null. Please, check the documentation pages for more information about the definition of the payload for this endpoint.";
        public const string TheUserDoesNotHaveAccessToDeleteFormData = "The user does not have access to delete form data.";
        public const string UploadFile = "UploadFile";
        public const string UrlLink = "URL";
        public const string UploadFileTitle = "Upload File";
        public const string Values = "Values";
        public const string VerifiedBy = "VerifiedBy";
        public const string VerifiedByTitle = "Verified By";
        public const string VerificationDateTitle = "Verification Date";
        public const string VerificationPerformedTitle = "Verification Performed";
        public const string VerificationStatus = "VerificationStatus";
        public const string VerificationStatusTitle = "Verification Status";
        public const string VerifyUsers = "VerifyUsers";
        public const string VerifyUsersTitle = "Verify Users";
        public const string WasNotFound = " was not found.";
        public const string WasNotFoundOrWasAlreadyDeleted = " was not found or was already deleted.";
        public const string WasThrownWhileTryingToSynchronizeAnEntity = "was thrown while trying to synchronize an entity.";
        public const string WithAFormUidOf = " with a FormUid of ";
        public const string WhileGettingAllTheFormVersionsFromTheDatabase = " while getting all the form versions from the database.";
        public const string Yes = "Yes";

        //Controllers & services Names
        public const string ActionItemsSvcController = "[ActionItemsSvcController].";
        public const string ActionItemsService = "DAP.ActionItemsSvc ";
        public const string DAPServiceHostUrlIsNotSet = "DAPServiceHostUrl is not set.";
        public const string DataSvcController = "[DataSvcController].";
        public const string DataService = "DAP.DataSvc ";
        public const string FilesSvcController = "[FilesSvcController].";
        public const string FilesService = "DAP.FilesSvc ";
        public const string FormsSvcController = "[FormsSvcController].";
        public const string FormsService = "DAP.FormsSvc ";
        public const string LocationsSvcController = "[LocationsSvcController].";
        public const string LocationsService = "DAP.LocationsSvc ";
        public const string LookupsSvcController = "[LookupsSvcController].";
        public const string LookupsService = "DAP.LookupsSvc ";
        public const string MaintenanceSvcController = "[MaintenanceSvcController].";
        public const string MaintenanceService = "DAP.MaintenanceSvc ";
        public const string PermissionsSvcController = "[PermissionsSvcController].";
        public const string PermissionsService = "DAP.PermissionsSvc ";
        public const string RolesSvcController = "[RolesSvcController].";
        public const string RolesService = "DAP.RolesSvc ";
        public const string SourcesSvcController = "[SourcesSvcController].";
        public const string SourcesService = "DAP.SourcesSvc ";
        public const string StatsSvcController = "[StatsSvcController].";
        public const string StatsService = "DAP.StatsSvc ";
        public const string SyncSvcController = "[SyncSvcController].";
        public const string SyncService = "DAP.SyncSvc ";
        public const string TemplatesSvcController = "[TemplatesSvcController].";
        public const string TemplatesService = "DAP.TemplatesSvc ";
        public const string TranslationsSvcController = "[TranslationsSvcController].";
        public const string TranslationsService = "DAP.TranslationsSvc ";
        public const string UsersSvcController = "[UsersSvcController].";
        public const string UsersService = "DAP.UsersSvc ";
        public const string UTCStandardTime = "Etc/GMT";
        public const string ViewsSvcController = "[ViewsSvcController].";
        public const string ViewsService = "DAP.ViewsSvc ";
    }
}