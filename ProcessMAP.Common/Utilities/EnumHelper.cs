﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ProcessMAP.Common.Utilities
{
    /// <summary>
    /// Helper class for Enums
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// Attempts to convert a string value to an Enum of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T StringToEnum<T>(string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception)
            {
                throw new Exception(string.Format("Can't convert {0} to {1}", value, typeof(T)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public static bool TryStringToEnum<T>(string value, out T output)
        {
            try
            {
                output = (T)Enum.Parse(typeof(T), value, true);
                return true;
            }
            catch
            {
                //output = default(T);
                output = default;
            }

            return false;
        }

        /// <summary>
        /// Returns the value of an enum's description attribute.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(Enum value)
        {
            DescriptionAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }

        /// <summary>
        /// Returns an enum based on value of a description attribute.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="description"></param>
        /// <returns></returns>
        public static T GetEnumValueFromDescription<T>(string description)
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new ArgumentException();
            }

            FieldInfo[] fields = type.GetFields();

            var field = fields
                .SelectMany(f => f.GetCustomAttributes(
                    typeof(DescriptionAttribute), false), (
                        f, a) => new { Field = f, Att = a })
                .Where(a => ((DescriptionAttribute)a.Att)
                    .Description == description).SingleOrDefault();

            //return field == null ? default(T) : (T)field.Field.GetRawConstantValue();
            return field == null ? default : (T)field.Field.GetRawConstantValue();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> ToKeyValuePair(Type enumType, bool useNameAsKey = true)
        {
            var pairs = new List<KeyValuePair<string, string>>();
            var enumNames = Enum.GetNames(enumType);
            var enumValues = Enum.GetValues(enumType);

            for (var i = 0; i < enumNames.Length; i++)
            {
                var key = useNameAsKey ? enumNames[i] : enumValues.OfType<string>().ElementAt(i);

                pairs.Add(new KeyValuePair<string, string>(key.ToLower(), enumNames[i]));
            }

            return pairs;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<dynamic> ToValueLabelPair(Type enumType, bool useNameAsKey = true)
        {
            var pairs = new List<dynamic>();
            var enumNames = Enum.GetNames(enumType);
            var enumValues = Enum.GetValues(enumType);

            for (var i = 0; i < enumNames.Length; i++)
            {
                var key = useNameAsKey ? enumNames[i] : enumValues.OfType<string>().ElementAt(i);
                var desc = GetDescription(enumValues.OfType<Enum>().ElementAt(i));

                pairs.Add(
                    new { Value = key, Label = desc ?? enumNames[i] }
                );
            }
            //if (appVersion == AppVersion.DefaultVersion
            //       && enumType.Name == "ActionType")
            //{
            //    pairs.Remove(new { Value = "Notify", Label = "Notify" });
            //}
            return pairs;
        }
    }
}