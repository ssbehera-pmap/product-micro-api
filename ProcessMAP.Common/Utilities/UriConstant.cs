﻿namespace ProcessMAP.Common.Utilities
{
    /// <summary>
    /// This class use to store the Uri info.
    /// </summary>
    public class UriConstant
    {
        /// <summary>
        /// 
        /// </summary>
        public static class Calendar
        {
            /// <summary>
            /// SyncActionItem
            /// </summary>
            public const string SyncActionItem = "papi/v1/calendar/SyncActionItem";

            /// <summary>
            /// Removeactionitem
            /// </summary>
            public const string RemoveActionitem = "papi/v1/calendar/removeactionitem";

            /// <summary>
            /// RemoveActionitems
            /// </summary>
            public const string RemoveActionitems = "papi/v1/calendar/removeactionitems";
        }

        /// <summary>
        /// 
        /// </summary>
        public static class Foundation
        {
            /// <summary>
            /// Endpoint to get child levels and locations.
            /// </summary>
            public const string GET_ChildLevelsAndLocations = "papi/v1/locations/allbyrootlevel/{LevelId}";

            /// <summary>
            /// CustomSettings
            /// </summary>
            public const string CustomSettings = "papi/v1/utils/customsettings";

            /// <summary>
            /// 
            /// </summary>
            public const string GetEmployeesByLocation = "papi/v1/employees/location/";

            /// <summary>
            /// 
            /// </summary>
            public const string GetLevelLocationsByRootLevelId = "papi/v1/locations/allbyrootlevel/{rootLevelId}";

            /// <summary>
            /// 
            /// </summary>
            public const string GetLevelLocationsNestedByRootLevelId = "papi/v1/locations/allnested/{rootLevelId}";

            /// <summary>
            /// 
            /// </summary>
            public const string GetLocation = "papi/v1/locations/{locationId}";

            /// <summary>
            /// 
            /// </summary>
            public const string GetTopLevels = "papi/v1/locations/toplevels/";

            /// <summary>
            /// 
            /// </summary>
            public const string GetUser = "papi/v1/user/getuser/{userId}";

            /// <summary>
            /// 
            /// </summary>
            public const string GetAllUsersByModule = "papi/v1/user/all/{moduleId}";

            /// <summary>
            /// 
            /// </summary>
            public const string GetUsersInformation = "papi/v1/user/getusersinformation";

            /// <summary>
            /// 
            /// </summary>
            public const string GetAllRoles = "papi/v1/role/all";

            /// <summary>
            /// 
            /// </summary>
            public const string GetRolesByModule = "papi/v1/role/rolesByModule/{moduleId}";
        }

        /// <summary>
        /// 
        /// </summary>
        public static class DataSource
        {
            /// <summary>
            /// Endpoint to get child levels and locations.
            /// </summary>
            public const string GetLookUpData = "papi/v1/lookups";

            /// <summary>
            /// Endpoint to get child levels and locations.
            /// </summary>
            public const string GetLookUpByNames = "papi/v1/lookups/datasourceByNames";

            /// <summary>
            /// moduledatasource
            /// </summary>
            public const string ModuleDataSource = "papi/v1/lookups/moduledatasource";

            /// <summary>
            /// Endpoint to get child levels and locations.
            /// </summary>
            public const string GetReadOnlyData = "papi/v1/ReadOnlyDataSource";

            /// <summary>
            /// Endpoint to get file from SQL.
            /// </summary>
            public const string GetFile = "papi/v1/utils/fileinformation/";
        }

        /// <summary>
        /// 
        /// </summary>
        public static class Tenant
        {
            /// <summary>
            /// Endpoint to get child levels and locations.
            /// </summary>
            public const string GetTalent = "/papi/v1/tenant/";
        }

        /// <summary>
        /// 
        /// </summary>
        public static class Notify
        {
            /// <summary>
            /// 
            /// </summary>
            public const string NotifyInfo = "/papi/v1/utils/notifyinfo";
        }

        /// <summary>
        /// 
        /// </summary>
        public static class StageData
        {
            /// <summary>
            /// Stage
            /// </summary>
            public const string Stage = "/papi/v1/utils/stagedata";
        }
    }
}