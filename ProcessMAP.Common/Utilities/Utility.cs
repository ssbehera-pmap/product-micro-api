﻿using ProcessMAP.Business.Authentication;
using ProcessMAP.Common.Logging;
using ProcessMAP.Security.Authentication;
using System;
using System.Net.Http;

namespace ProcessMAP.Common.Utilities
{
    internal static class Utility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="request"></param>
        public static void LogJWTUsage(string token, HttpRequestMessage request)
        {
            try
            {
                var tokenClaims = JWTManager.GetTokenClaims(token);
                var jwt = JWTManager.ExtractJWT(token);
                var audience = string.Empty;
                if (tokenClaims.ContainsKey(RegisteredClaimNames.Audience))
                {
                    audience = tokenClaims[RegisteredClaimNames.Audience] as string;
                }

                double.TryParse(tokenClaims[RegisteredClaimNames.Expiration].ToString(), out double exp);

                Guid.TryParse(jwt.Id, out Guid jti);
            }
            catch (Exception ex)
            {
                PmapLogger<Exception>.Warning("An exception was thrown while logging the JWT usage to the database.", ex);
                throw;
            }
        }
    }
}