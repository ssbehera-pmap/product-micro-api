﻿using Microsoft.AspNetCore.Http;

namespace ProcessMAP.Common.Utilities
{
    public class GetContext
    {
        public static HttpContext Current => new HttpContextAccessor().HttpContext;
        public string CurrentContext()
        {
            return Current.ToString();
        }
    }
}
