﻿using Newtonsoft.Json;
using ProcessMAP.Common.Entities;
using ProcessMAP.Common.Entities.Base;
using ProcessMAP.Common.Entities.DocumentManagement;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProcessMAP.Common.Utilities
{
    /// <summary>
    /// Common Extension Methods
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Transforms an IDictionary object into a NameValueCollection.
        /// </summary>
        /// <param name="dict">The IDictionary object to be transformed.</param>
        /// <returns>A NameValueCollection object.</returns>
        public static NameValueCollection ToNameValueCollection<TKey, TVal>(this IDictionary<TKey, TVal> dict)
        {
            var nameValueCol = new NameValueCollection();

            foreach (var item in dict)
            {
                nameValueCol.Add(item.Key.ToString(), item.Value.ToString());
            }

            return nameValueCol;
        }

        /// <summary>
        /// Checks if the specified NameValueCollection object contains a specific key.
        /// </summary>
        /// <param name="collection">The NameValueCollection that potentially contains the specified key.</param>
        /// <param name="key">The key to be checked.</param>
        /// <returns>A value (bool) representing whether the key is present in the collection.</returns>
        public static bool ContainsKey(this NameValueCollection collection, string key)
        {
            if (collection == null)
                return false;

            if (string.IsNullOrWhiteSpace(key))
                return false;

            if (collection.Keys.Count == 0)
                return false;

            var existingKey = collection.Keys.Cast<string>().FirstOrDefault(k => k.ToLowerInvariant() == key.ToLowerInvariant());

            return (existingKey != null);
        }

        /// <summary>
        /// Gets a string with a list of comma separated ids.
        /// </summary>
        /// <returns></returns>
        public static string ToCommaSeparated<T>(this IEnumerable<T> list)
            where T : PMAPLookup
        {
            var ids = list.Select(i => i.Id).ToArray();
            var commaSeparatedIds = string.Join(",", ids);
            return commaSeparatedIds;
        }

        /// <summary>
        /// Returns a value indicating whether a substring occurs within this string.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="comparisonType"></param>
        /// <returns></returns>
        public static bool Contains(this string source, string value, StringComparison comparisonType)
        {
            return source.IndexOf(value, comparisonType) >= 0;
        }

        /// <summary>
        /// Converts a collection of LookupFolders to ExtensionProperty.
        /// </summary>
        /// <param name="lookupFolders"></param>
        /// <returns></returns>
        public static List<ExtensionProperty<Guid, string>> ToExtensionPropertyList(this IEnumerable<LookupFolder> lookupFolders)
        {
            return lookupFolders.Select(l => new ExtensionProperty<Guid, string>() { Description = l.Description, Id = l.Id, ParentObjectId = l.ParentObjectId }).ToList();
        }

        /// <summary>
        /// Calculates whether a particular date time value is past the current UtcNow.
        /// Or, from a particular date if a from date is passed.
        /// </summary>
        /// <param name="theDate"></param>
        /// <param name="dueDate"></param>
        /// <returns></returns>
        public static bool IsOverdue(this DateTime? theDate, DateTime? dueDate = null)
        {
            if (!dueDate.HasValue)
                dueDate = DateTime.UtcNow;

            return theDate.HasValue && dueDate > theDate;
        }

        /// <summary>
        /// Calculates whether a particular date time value is past the current UtcNow.
        /// Or, from a particular date if a from date is passed.
        /// </summary>
        /// <param name="theDate"></param>
        /// <param name="dueDate"></param>
        /// <returns></returns>
        public static bool IsOverdue(this DateTime theDate, DateTime? dueDate = null)
        {
            DateTime? date = theDate;
            return IsOverdue(date, dueDate);
        }

        /// <summary>
        /// Returns the current date set to 11:59:999 PM.
        /// </summary>
        /// <param name="theDate"></param>
        /// <returns></returns>
        public static DateTime? Ceiling(this DateTime? theDate)
        {
            if (theDate.HasValue)
            {
                return theDate.Value.Date.AddDays(1);
            }

            return theDate;
        }

        /// <summary>
        /// Returns the current date set to 11:59:999 PM.
        /// </summary>
        /// <param name="theDate"></param>
        /// <returns></returns>
        public static DateTime Ceiling(this DateTime theDate)
        {
            DateTime? date = theDate;
            return date.Ceiling().Value;
        }

        /// <summary>
        /// Replace an exact match of an instance inside a string.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="find"></param>
        /// <param name="replaceWith"></param>
        /// <param name="matchWholeWord"></param>
        /// <returns></returns>
        public static string SafeReplace(this string input, string find, string replaceWith, bool matchWholeWord)
        {
            string textToFind = matchWholeWord ? string.Format(@"\b{0}\b", find) : find;
            return Regex.Replace(input, textToFind, replaceWith);
        }

        /// <summary>
        /// Converts a string into a version # formated string.
        /// Example: 3    --> "3.0"
        ///          3.0  --> "3.0"
        ///          3.24 --> "3.24"
        ///          3abc --> ""
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToVersionNumberFormat(this string input)
        {
            decimal.TryParse(input, out decimal value);

            return value > 0 ? string.Format("{0:0.0############}", value) : string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumeric(this string input)
        {
            bool isNum = double.TryParse(input, NumberStyles.Any, NumberFormatInfo.InvariantInfo, out double num);

            return isNum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Key"></typeparam>
        /// <typeparam name="Val"></typeparam>
        /// <param name="target"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static Dictionary<Key, Val> Merge<Key, Val>(this IDictionary<Key, Val> target, IDictionary<Key, Val> other)
        {
            return target.Concat(other).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        /// <summary>
        /// Converts a dynamic or ExpandoObject to the specified type using serialization.
        /// </summary>
        /// <typeparam name="T">The type of the result object.</typeparam>
        /// <param name="obj">The original object to be converted.</param>
        /// <returns>An object converted to the specified type.</returns>
        public static T ToObject<T>(this IDictionary<string, object> obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Performs an OR bitwise operation.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns></returns>
        public static bool Or(this bool value1, bool value2)
        {
            return value1 || value2;
        }

        /// <summary>
        /// Performs an AND bitwise operation.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns></returns>
        public static bool And(this bool value1, bool value2)
        {
            return value1 && value2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool IsEmpty<T>(this IEnumerable<T> source)
        {
            return !source.Any();
        }
    }
}