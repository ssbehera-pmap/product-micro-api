﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Microsoft.IdentityModel.Tokens;
using ProcessMAP.Security.Authentication;
using ProcessMAP.Security.Authorization;

namespace ProcessMAP.Business.Authentication
{
    /// <summary>
    /// This class provides methods to interact with existing tokens or to create new ones.
    /// </summary>
    public class JWTManager
    {
        #region Public Methods

        /// <summary>
        /// Validates a given token against the claim settings specified.
        /// </summary>
        /// <param name="token">The token to be validated.</param>
        /// <param name="claimSettings">The set of ClaimSetting objects to help the validation process.</param>
        /// <param name="consumerClaim"></param>
        /// <param name="applicationId">Represents the Application Id from the Cached Consumer which is used for validation.</param>
        public static void ValidateToken(string token, List<Security.Entities.Claim> claimSettings, KeyValuePair<string, int> consumerClaim, Guid applicationId = new Guid())
        {
            ValidateNullOrEmpty(token);
            ValidateTokenParts(token);
            ValidateLifetime(token);
            ValidateApplication(token, applicationId, consumerClaim.Value);
            ValidateSignature(token, claimSettings);
            AuthorizeRequest(token, consumerClaim);
        }

        /// <summary>
        /// Issues a new JWT based on the payload and claim settings specified.
        /// </summary>
        /// <param name="payload">A collection of key, value pairs that represent the claims to be included in the token.</param>
        /// <param name="claimSettings">The set of ClaimSetting objects by customer.</param>
        /// <param name="userAuthorization">Optional parameter to indicate whether to include the user authorization info in the JWT or not.</param>
        /// <returns>A string representing a new JWT.</returns>
        public static string IssueToken(NameValueCollection payload, List<Security.Entities.Claim> claimSettings, UserAuthorization userAuthorization = null)
        {
            // Validate Required Secret Key
            if (claimSettings.Exists(x => x.Key.Equals(RequiredClaim.SecretKey, StringComparison.InvariantCultureIgnoreCase)) == false)
            {
                throw new InvalidPayloadException(PmapClaimNames.SecretKey);
            }

            var secret = claimSettings.Find(x => x.Key.Equals(RequiredClaim.SecretKey, StringComparison.InvariantCultureIgnoreCase)).Value.ToString();
            var secretKeyBytes = new System.Text.ASCIIEncoding().GetBytes(secret);

            if (secretKeyBytes.Length < 64) Array.Resize(ref secretKeyBytes, 64);

            var createdMinutesFromNow = Convert.ToInt64(claimSettings.Find(x => x.Key.Equals(RequiredClaim.CreatedMinutesFromNow, StringComparison.InvariantCultureIgnoreCase)).Value);
            var expirationMinutes = Convert.ToInt64(claimSettings.Find(x => x.Key.Equals(RequiredClaim.ExpirationMinutes, StringComparison.InvariantCultureIgnoreCase)).Value);
            var createdDate = DateTime.UtcNow.AddMinutes(createdMinutesFromNow);
            var expirationDate = DateTime.UtcNow.AddMinutes(expirationMinutes);

            List<string> claimsToExclude = new List<string>() { PmapClaimNames.Password };

            var claims = new List<Claim>()
            {
                new Claim(RegisteredClaimNames.JWTID, Guid.NewGuid().ToString())
            };

            try
            {
                foreach (var key in payload.AllKeys)
                {
                    if (claimsToExclude.Contains(key) == false)
                    {
                        if (payload[key] != null)
                        {
                            claims.Add(new Claim(key, payload[key]));
                        }
                    }
                }

                var jwtPayload = new JwtPayload(null, null, claims, createdDate, expirationDate);

                if (userAuthorization != null)
                {
                    jwtPayload.Add(PmapClaimNames.Authorization, userAuthorization);
                }

                var signatureAlgorithm = claimSettings.Find(x => x.Key.Equals(RequiredClaim.SignatureAlgorithm, StringComparison.InvariantCultureIgnoreCase)).Value.ToString();
                var digestAlgorithm = claimSettings.Find(x => x.Key.Equals(RequiredClaim.DigestAlgorithm, StringComparison.InvariantCultureIgnoreCase)).Value.ToString();
                var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKeyBytes), signatureAlgorithm, digestAlgorithm);
                var jwtToken = new JwtSecurityToken(new JwtHeader(signingCredentials), jwtPayload);
                var jwtTokenHandler = new JwtSecurityTokenHandler();
                var token = jwtTokenHandler.WriteToken(jwtToken);

                return token;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Extracts the token object.
        /// </summary>
        /// <param name="jwt"></param>
        /// <returns></returns>
        public static SecurityToken ExtractJWT(string jwt)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.ReadToken(jwt);
            return securityToken;
        }

        /// <summary>
        /// Gets the list of claims in a given token.
        /// </summary>
        /// <param name="token">A string that represents the token in question.</param>
        /// <returns>A JwtPayload object containing a list of claims in the specified token.</returns>
        public static JwtPayload GetTokenClaims(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.ReadToken(token);
            var claims = ((JwtSecurityToken)(securityToken)).Payload;
            return claims;
        }
        #endregion

        #region Private Methods

        internal static void ValidateLifetime(string jwt)
        {
            var securityToken = ExtractJWT(jwt);
            var now = DateTime.UtcNow;

            if (securityToken.ValidFrom > now)
            {
                var error = "Token validation failed because the specified token's start date is in the future.";
                throw new InvalidTokenException(error);
            }

            if (securityToken.ValidTo < now)
            {
                var error = "Token validation failed because the specified token has expired.";
                throw new InvalidTokenException(error);
            }
        }

        /// <summary>
        /// Validates the token isn't null or empty.
        /// </summary>
        /// <param name="token"></param>
        private static void ValidateNullOrEmpty(string token)
        {
            if (String.IsNullOrWhiteSpace(token))
            {
                var error = "Token validation failed because the specified token was null or empty.";
                throw new InvalidTokenException(error);
            }
        }

        /// <summary>
        /// Validates the token has the necessary parts.
        /// </summary>
        /// <param name="token"></param>
        private static void ValidateTokenParts(string token)
        {
            if (token.Split('.').Length != 3)
            {
                var error = "Token validation failed because the specified token was malformed.";
                throw new InvalidTokenException(error);
            }
        }

        /// <summary>
        /// Validates the token's signature.
        /// </summary>
        /// <param name="jwt"></param>
        /// <param name="claimSettings"></param>
        private static void ValidateSignature(string jwt, List<Security.Entities.Claim> claimSettings)
        {
            try
            {
                ValidateSignature(claimSettings, jwt);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Validates the token's signature.
        /// </summary>
        /// <param name="claimSettings"></param>
        /// <param name="jwt"></param>
        private static void ValidateSignature(List<Security.Entities.Claim> claimSettings, string jwt, string urlReferrerAuthority = null)
        {
            var secret = claimSettings.Find(x => x.Key.Equals(RequiredClaim.SecretKey, StringComparison.InvariantCultureIgnoreCase)).Value.ToString();
            var audienceClaims = claimSettings.FindAll(cs => cs.Key.Equals(RequiredClaim.Audience, StringComparison.InvariantCultureIgnoreCase)).Select(a => a.Value);
            var audience = string.Join(",", audienceClaims);
            var issuer = claimSettings.Find(x => x.Key.Equals(RequiredClaim.Issuer, StringComparison.InvariantCultureIgnoreCase)).Value.ToString();
            var secretKey = new System.Text.ASCIIEncoding().GetBytes(secret);
            var validationParameters = new TokenValidationParameters()
            {
                RequireExpirationTime = true,
                IssuerSigningKey = new SymmetricSecurityKey(secretKey),
                ValidAudience = audience,
                ValidIssuer = issuer
            };
            var isValidated = false;

            try
            {
                var principal = new JwtSecurityTokenHandler().ValidateToken(jwt, validationParameters, out SecurityToken validatedToken);
                isValidated = principal.Identity.IsAuthenticated;
            }
            catch (Exception ex)
            {
                var error = "Token validation failed because the specified token's signature is invalid.";
                throw new InvalidOperationException(error, ex);
            }

            if (!string.IsNullOrWhiteSpace(urlReferrerAuthority))
            {
                if (audience.Contains(urlReferrerAuthority) == false)
                {
                    var error = $"Token validation failed because the specified token's audience does not allow the current request. RequestAuthority:{urlReferrerAuthority} -- TokenAudience:{audience}";
                    throw new InvalidOperationException(error);
                }
            }

            // TODO: Validate the value of Issuer in the JWT against the value in the claim settings.
        }

        /// <summary>
        /// Validates the Application Id that is derived from the security token
        /// with the Application Id extracted from the cache consumer.
        /// </summary>
        /// <param name="token">Represents the security token.</param>
        /// <param name="appId">The Application Id extracted from the cache consumer</param>
        /// <param name="consumerId">Represents the Consumer Id extracted from the Cached Consumer.</param>
        private static void ValidateApplication(string token, Guid appId, int consumerId)
        {
            var tokenClaims = GetTokenClaims(token);
            var aid = new object();
            var cid = -1;

            try
            {
                aid = tokenClaims[PmapClaimNames.ApplicationId];
                cid = int.Parse(tokenClaims[PmapClaimNames.ConsumerId].ToString());
            }
            catch (KeyNotFoundException knf) { throw new InvalidTokenException(knf.Message); }

            if (aid == null)
                throw new InvalidTokenException("Unable to retrieve Cached Consumer");

            if (aid.ToString() != appId.ToString())
                throw new InvalidTokenException("ApplicationId Mismatch");

            if (cid == -1)
                throw new InvalidTokenException("Unable to retriee Consumer Id");

            if (cid != consumerId)
                throw new InvalidTokenException("Consumer Id Mismatch");
        }

        /// <summary>
        /// Validates the token is authorized.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="consumerClaim"></param>
        private static void AuthorizeRequest(string token, KeyValuePair<string, int> consumerClaim)
        {
            if (consumerClaim.Value < 0)
            {
                var error = "Token validation failed because a consumer could not be found in the headers of the request.";
                throw new InvalidTokenException(error);
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.ReadToken(token);
            var claims = (securityToken as JwtSecurityToken).Payload;

            try
            {
                var idInClaim = claims[consumerClaim.Key];

                if (!consumerClaim.Value.ToString().Equals(idInClaim))
                {
                    var error = string.Format("Token validation failed because the consumer in the request does not match the consumer in the claim. Id:{0} -- IdInClaim:{1}", consumerClaim.Value, idInClaim);
                    //PmapLogger<JWTManager>.DebugFormat(error);
                    throw new InvalidTokenException(error);
                }
            }
            catch (KeyNotFoundException)
            {
                throw;
            }
        }
        #endregion
    }
}