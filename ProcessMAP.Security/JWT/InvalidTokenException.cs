﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace ProcessMAP.Business.Authentication
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class InvalidTokenException : Exception
    {
        /// <summary>
        /// Used to provide a consistent reporting for exceptions of type Invalid Token.
        /// </summary>
        /// <param name="message"></param>
        public InvalidTokenException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}