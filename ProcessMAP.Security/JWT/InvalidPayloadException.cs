﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace ProcessMAP.Business.Authentication
{
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class InvalidPayloadException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="payloadItemName"></param>
        public InvalidPayloadException(string payloadItemName)
            : base(String.Format("Payload item '{0}' is invalid.", payloadItemName))
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}