﻿using System;
using System.Security;

namespace ProcessMAP.Security
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SecurityBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public SecurityBase(string connectionString = null)
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Gets the connection string for the customer based on the request's URL.
        /// </summary>
        public string ConnectionString { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        protected void HandleException<T>(Exception ex, string message = null)
        {
            var errorMessage = message ?? "A data access related issue occurred while trying to fulfill your request.";
            throw new SecurityException(errorMessage, ex);
        }
    }
}