﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Security.Authorization
{
    /// <summary>
    /// 
    /// </summary>
    public static class AuthorizationTools
    {
        /// <summary>
        /// Gets the user's permissions bit array based on a PermissionsMask.
        /// </summary>
        /// <returns></returns>
        public static BitArray GetPermissionsBitArray(int permissionsMask)
        {
            var binaryStringValue = Convert.ToString(permissionsMask, 2).PadLeft(ModulePermissionsBitArrays.PermissionsArraySize, '0');
            var bitValues = binaryStringValue.ToCharArray().Select(c => c == '1' ? true : false).ToArray();
            var permissions = new BitArray(bitValues);

            return permissions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="permissionId"></param>
        /// <param name="permissionsMask"></param>
        /// <param name="permissionsBitArray"></param>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public static bool CheckUserHasPermission(int permissionId, int moduleId, int? permissionsMask = null, BitArray permissionsBitArray = null)
        {
            if (permissionsBitArray == null && !permissionsMask.HasValue)
            {
                throw new ArgumentException($"Both parameters '{nameof(permissionsMask)}' and '{nameof(permissionsBitArray)}' cannot be null.");
            }

            var permsBitArray = permissionsBitArray ?? GetPermissionsBitArray(permissionsMask.Value);
            var permissionsDefinition = ModulePermissionsBitArrays.BitArrayMapping[moduleId];

            for (var i = 0; i < permissionsDefinition.Count; i++)
            {
                if (permissionsDefinition.Keys.ElementAt(i) == permissionId)
                {
                    return permsBitArray.Get(i);
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="permissionsBitArray"></param>
        /// <returns></returns>
        public static int CalculatePermissionsMask(BitArray permissionsBitArray)
        {
            if (permissionsBitArray.Length < ModulePermissionsBitArrays.PermissionsArraySize)
            {
                throw new ArgumentException($"The parameter '{nameof(permissionsBitArray)}' has size '{permissionsBitArray.Length}' which is less than the required size of '{ModulePermissionsBitArrays.PermissionsArraySize}'");
            }

            var binaryStringValue = string.Join(string.Empty, permissionsBitArray.OfType<bool>().Select(b => b ? "1" : "0"));

            return Convert.ToInt32(binaryStringValue, 2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="permissions"></param>
        /// <returns></returns>
        public static int CalculatePermissionsMask(int moduleId, IEnumerable<int> permissions)
        {
            var permissionsDefinition = new Dictionary<int, bool>(ModulePermissionsBitArrays.BitArrayMapping[moduleId]);

            foreach(var permissionId in permissions)
            {
                if (permissionsDefinition.ContainsKey(permissionId))
                {
                    permissionsDefinition[permissionId] = true;
                }
            }

            var bitArray = new BitArray(permissionsDefinition.Select(p => p.Value).ToArray());

            return CalculatePermissionsMask(bitArray);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="permissionsMasks"></param>
        /// <param name="performAndOperation"></param>
        /// <returns></returns>
        public static BitArray FlattenPermissionsMasks(IEnumerable<int> permissionsMasks, bool performAndOperation = false)
        {
            var resultBitArray = new BitArray(ModulePermissionsBitArrays.PermissionsArraySize, performAndOperation);

            foreach(var mask in permissionsMasks)
            {
                var bitArray = GetPermissionsBitArray(mask);
                resultBitArray = performAndOperation ? resultBitArray.And(bitArray) : resultBitArray.Or(bitArray);
            }

            return resultBitArray;
        }
    }
}