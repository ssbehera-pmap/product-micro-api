﻿namespace ProcessMAP.Security.Authorization
{
    /// <summary>
    /// Represents an enumeration of form sharing modes.
    /// </summary>
    public enum SharingPermissionsMode : byte
    {
        /// <summary>
        /// Sharing and System permissions are combined with AND bitwise operation.
        /// Therefore, the lesser permission will be honored.
        /// </summary>
        Realistic = 0,

        /// <summary>
        /// Sharing Permissions override System Permissions.
        /// </summary>
        Optimistic = 1,

        /// <summary>
        /// System Permissions override Sharing Permissions.
        /// </summary>
        Pessimistic = 2
    }
}