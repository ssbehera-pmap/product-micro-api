﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ProcessMAP.Security.Authorization
{
    /// <summary>
    /// Represents the permission masks and roles for a particular user for a particular module.
    /// </summary>
    public class UserModulePermissionMasksAndRoles
    {
        /// <summary>
        /// The id of the module.
        /// </summary>
        public int ModuleId { get; set; }

        /// <summary>
        /// A value representing the permissions mask for the specified user and module.
        /// </summary>
        public int PermissionsMask { get; set; }

        /// <summary>
        /// A collection of values representing the roles the specified user belongs to, for the specified module.
        /// </summary>
        public IEnumerable<int> Roles { get; set; }

        /// <summary>
        /// Calculates whether the user has the specified permission.
        /// </summary>
        /// <param name="permissionId"></param>
        /// <returns></returns>
        public bool UserHasPermission(int permissionId)
        {
            return AuthorizationTools.CheckUserHasPermission(permissionId, ModuleId, PermissionsMask);
        }

        /// <summary>
        /// It revokes a user permission in memory.
        /// </summary>
        /// <param name="permissionId"></param>
        public void RevokeUserPermission(int permissionId)
        {
            if (UserHasPermission(permissionId))
            {
                var permissionsBitArray = AuthorizationTools.GetPermissionsBitArray(PermissionsMask);
                var moduleBitArrayDefinition = ModulePermissionsBitArrays.BitArrayMapping[ModuleId];
                var allTrueBitArray = new BitArray(ModulePermissionsBitArrays.PermissionsArraySize, true);

                for(var i = 0; i< moduleBitArrayDefinition.Count; i++)
                {
                    if (moduleBitArrayDefinition.ElementAt(i).Key == permissionId)
                    {
                        allTrueBitArray.Set(i, false);
                        break;
                    }
                }

                var resultsBitArray = permissionsBitArray.And(allTrueBitArray);
                var permissionsMask = AuthorizationTools.CalculatePermissionsMask(resultsBitArray);

                PermissionsMask = permissionsMask;
            }
        }
    }
}