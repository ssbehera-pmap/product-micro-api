﻿using System.Collections.Generic;

namespace ProcessMAP.Security.Authorization
{
    internal class ModulePermissionsBitArrays
    {
        /// <summary>
        /// The size of the definition for user permissions.
        /// </summary>
        public const byte PermissionsArraySize = 16;

        /// <summary>
        /// DAP bit array.
        /// </summary>
        public static readonly Dictionary<int, bool> DAP = new Dictionary<int, bool>(PermissionsArraySize)
        {
            { 750099, false },  // Spare
            { 750098, false },  // Spare
            { 750097, false },  // Spare
            { 750001, false },  // Form Share
            { 750002, false },  // Form Create
            { 750003, false },  // Form Delete
            { 750004, false },  // Form Update
            { 750005, false },  // Form Read
            { 750096, false },  // Spare
            { 750095, false },  // Spare
            { 750094, false },  // Spare
            { 750093, false },  // Spare
            { 750007, false },  // Data Create
            { 750008, false },  // Data Delete
            { 750009, false },  // Data Update
            { 750010, false }   // Data Read
        };

        /// <summary>
        /// Mappings of modules to their respective bit arrays.
        /// </summary>
        public static readonly Dictionary<int, Dictionary<int, bool>> BitArrayMapping = new Dictionary<int, Dictionary<int, bool>>()
        {
            { 75, DAP }
        };
    }
}