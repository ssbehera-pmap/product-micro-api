﻿using System.Collections.Generic;

namespace ProcessMAP.Security.Authorization
{
    /// <summary>
    /// Represents the permissions of a user.
    /// </summary>
    public class UserAuthorization
    {
        /// <summary>
        /// The id of the user in question.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// The id of the location that the permissions apply to.
        /// </summary>
        public int? LocationId { get; set; }

        /// <summary>
        /// The id of the level that the permissions apply to.
        /// </summary>
        public int? LevelId { get; set; }

        /// <summary>
        /// The ids of the levels that the specified location belongs to.
        /// </summary>
        public IEnumerable<int> ParentLevelIds { get; set; }

        /// <summary>
        /// The user permissions mask by module.
        /// </summary>
        public IEnumerable<UserModulePermissionMasksAndRoles> ModulePermissions { get; set; }
    }
}