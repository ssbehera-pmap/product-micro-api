﻿namespace ProcessMAP.Security.Entities
{
    /// <summary>
    /// The Claim class.
    /// </summary>
    public class Claim
    {
        /// <summary>
        /// Creates an instance of the Claim class.
        /// </summary>
        public Claim()
        {
            // Empty constructor needed for serialization
        }

        /// <summary>
        /// The identity of the claim.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The id of the consumer for which this claim has been configured.
        /// </summary>
        public int ConsumerId { get; set; }

        /// <summary>
        /// The key of the claim.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// The value of the claim.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// The application type. Example, System.String.
        /// </summary>
        public string ApplicationType { get; set; }

        /// <summary>
        /// Represents a value indicating whether this claim is required for validation.
        /// </summary>
        public bool RequiredForValidation { get; set; }

        /// <summary>
        /// Outputs a display of the claim.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}: {1}", Key, Value.ToString());
        }
    }
}