﻿namespace ProcessMAP.Security.Entities
{
    /// <summary>
    /// Represents the Bond Role entity.
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Represents the unique identifier of the Role.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Represents the Name of the Role.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Represents the Description of the Role.
        /// </summary>
        public string Description { get; set; }
    }
}