﻿namespace ProcessMAP.Security.Authentication
{
    /// <summary>
    /// Contract for entities that need to carry authentication information.
    /// </summary>
    public interface IAuthenticationInfo
    {
        /// <summary>
        /// Determines if the Tenant is SSO Enabled.
        /// </summary>
        bool SSOEnabled { get; set; }

        /// <summary>
        /// The id of the authorization type.
        /// </summary>
        int AuthTypeId { get; set; }

        /// <summary>
        /// The id of the consumer.
        /// </summary>
        int ConsumerId { get; set; }

        /// <summary>
        /// Gets the type of SSO.
        /// </summary>
        AuthType AuthType { get; }

        /// <summary>
        /// Gets or sets the user's domain.
        /// </summary>
        string DomainName { get; set; }

        /// <summary>
        /// Gets or sets the url to the SSO service.
        /// </summary>
        string AuthServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets the url to the relay party.
        /// </summary>
        string AuthRelayPartyUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating that authentication
        /// must be performed against an on-premise server.
        /// </summary>
        bool AuthenticateOnPremise { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the connection string
        /// to the database where the authentication should be performed.
        /// </summary>
        string DatabaseConnectionString { get; set; }

        /// <summary>
        /// Gets or sets a regular expression that extracts a domain from a given input.
        /// </summary>
        string DomainRegEx { get; set; }

        /// <summary>
        /// Gets or sets a regular expression that extracts a user name from a given input.
        /// </summary>
        string UsernameRegEx { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the format in which to send the user name for authentication.
        /// </summary>
        string UsernameFormat { get; set; }

        /// <summary>
        /// Calculates the format of the username as it should be sent
        /// for authentication based on the consumer's UsernameFormat value.
        /// </summary>
        /// <param name="authRequest"></param>
        /// <returns></returns>
        string GetFormattedUsername(AuthRequest authRequest);

        /// <summary>
        /// Calculated whether the input argument matches the domain in the configuration.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        bool MatchesConfigurationDomain(string input);
    }
}