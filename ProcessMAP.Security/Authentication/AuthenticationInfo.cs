﻿using System;
using System.Text.RegularExpressions;

namespace ProcessMAP.Security.Authentication
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationInfo : IAuthenticationInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public AuthenticationInfo() { }

        /// <summary>
        /// 
        /// </summary>
        public string DomainName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool SSOEnabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AuthServiceUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AuthRelayPartyUrl { get; set; }

        /// <summary>
        /// The id of the consumer.
        /// </summary>
        public int ConsumerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int AuthTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AuthType AuthType
        {
            get
            {
                return (AuthType)AuthTypeId;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating that authentication
        /// must be performed against an on-premise server.
        /// </summary>
        public bool AuthenticateOnPremise { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the connection string
        /// to the database where the authentication should be performed.
        /// </summary>
        public string DatabaseConnectionString { get; set; }

        /// <summary>
        /// Gets or sets a regular expression that extracts a domain from a given input.
        /// </summary>
        public string DomainRegEx { get; set; }

        /// <summary>
        /// Gets or sets a regular expression that extracts a user name from a given input.
        /// </summary>
        public string UsernameRegEx { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the format in which to send the user name for authentication.
        /// </summary>
        public string UsernameFormat { get; set; }

        /// <summary>
        /// Calculates the format of the username as it should be sent
        /// for authentication based on the consumer's UsernameFormat value.
        /// </summary>
        /// <param name="authRequest"></param>
        /// <returns></returns>
        public string GetFormattedUsername(AuthRequest authRequest)
        {
            if (string.IsNullOrWhiteSpace(UsernameFormat))
                return authRequest.Username;

            if (string.IsNullOrWhiteSpace(UsernameRegEx))
                return authRequest.Username;

            var extractedUsername = Regex.Match(authRequest.Username, UsernameRegEx).Groups["user"].Value;

            if (string.IsNullOrWhiteSpace(extractedUsername))
                return authRequest.Username;

            var formattedUsername = UsernameFormat
                .Replace("{domain}", DomainName)
                .Replace("{username}", extractedUsername);

            return formattedUsername;
        }

        /// <summary>
        /// Determines whether the specified email address matches the domain name of the configuration.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool MatchesConfigurationDomain(string input)
        {
            if (string.IsNullOrWhiteSpace(DomainName))
                return false;

            var extractedDomain = Regex.Match(input, DomainRegEx).Groups["domain"].Value;

            return DomainName.Equals(extractedDomain, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}