﻿namespace ProcessMAP.Security.Authentication
{
    /// <summary>
    /// 
    /// </summary>
    public class PmapClaimNames
    {
        /// <summary>
        /// 
        /// </summary>
        public const string ConsumerId = "cid";

        /// <summary>
        /// 
        /// </summary>
        public const string UserId = "uid";

        /// <summary>
        /// 
        /// </summary>
        public const string UserName = "usr";

        /// <summary>
        /// 
        /// </summary>
        public const string Password = "pwd";

        /// <summary>
        /// The name of the user.
        /// </summary>
        public const string Name = "name";

        /// <summary>
        /// Email of the user.
        /// </summary>
        public const string Email = "email";

        /// <summary>
        ///
        /// </summary>
        public const string SecretKey = "sek";

        /// <summary>
        /// 
        /// </summary>
        public const string DomainName = "dom";

        /// <summary>
        /// Represents the Consumer Key.
        /// </summary>
        public const string ConsumerKey = "csk";

        /// <summary>
        /// Represents the Application unique identifier.
        /// </summary>
        public const string ApplicationId = "aid";

        /// <summary>
        /// Represents the authorization claim in the JWT.
        /// </summary>
        public const string Authorization = "authz";
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegisteredClaimNames
    {
        /// <summary>
        /// Issuer:
        /// The iss claim identifies the principal that issued the JWT.
        /// The processing of this claim is generally application specific.
        /// The iss value is a case-sensitive string containing a StringOrURI value.
        /// Use of this claim is OPTIONAL.
        /// </summary>
        public const string Issuer = "iss";

        /// <summary>
        /// Subject:
        /// The sub claim identifies the principal that is the subject of the JWT.
        /// The Claims in a JWT are normally statements about the subject. The subject
        /// value MUST either be scoped to be locally unique in the context of the issuer
        /// or be globally unique. The processing of this claim is generally application specific.
        /// The sub value is a case-sensitive string containing a StringOrURI value.
        /// Use of this claim is OPTIONAL.
        /// </summary>
        public const string Subject = "sub";

        /// <summary>
        /// Audience:
        /// The aud claim identifies the recipients that the JWT is intended for. Each principal
        /// intended to process the JWT MUST identify itself with a value in the audience claim.
        /// If the principal processing the claim does not identify itself with a value in the aud
        /// claim when this claim is present, then the JWT MUST be rejected. In the general case,
        /// the aud value is an array of case-sensitive strings, each containing a StringOrURI value.
        /// In the special case when the JWT has one audience, the aud value MAY be a single
        /// case-sensitive string containing a StringOrURI value. The interpretation of audience
        /// values is generally application specific.
        /// Use of this claim is OPTIONAL.
        /// </summary>
        public const string Audience = "aud";

        /// <summary>
        /// Expiration:
        /// The exp claim identifies the expiration time on or after which the JWT MUST NOT be accepted
        /// for processing. The processing of the exp claim requires that the current date/time MUST be
        /// before the expiration date/time listed in the exp claim. Implementers MAY provide for some
        /// small leeway, usually no more than a few minutes, to account for clock skew. Its value MUST
        /// be a number containing a NumericDate value.
        /// Use of this claim is OPTIONAL.
        /// </summary>
        public const string Expiration = "exp";

        /// <summary>
        /// Not Before:
        /// The nbf claim identifies the time before which the JWT MUST NOT be accepted for processing.
        /// The processing of the nbf claim requires that the current date/time MUST be after or equal
        /// to the not-before date/time listed in the nbf claim. Implementers MAY provide for some small
        /// leeway, usually no more than a few minutes, to account for clock skew. Its value MUST be a
        /// number containing a NumericDate value.
        /// Use of this claim is OPTIONAL.
        /// </summary>
        public const string NotBefore = "nbf";

        /// <summary>
        /// Issued At:
        /// The iat claim identifies the time at which the JWT was issued.
        /// This claim can be used to determine the age of the JWT. Its value MUST
        /// be a number containing a NumericDate value.
        /// Use of this claim is OPTIONAL.
        /// </summary>
        public const string IssuedAt = "iat";

        /// <summary>
        /// JWT ID:
        /// The jti claim provides a unique identifier for the JWT. The identifier value MUST
        /// be assigned in a manner that ensures that there is a negligible probability that the
        /// same value will be accidentally assigned to a different data object; if the
        /// application uses multiple issuers, collisions MUST be prevented among values
        /// produced by different issuers as well. The jti claim can be used to prevent the
        /// JWT from being replayed. The jti value is a case-sensitive string.
        /// Use of this claim is OPTIONAL.
        /// </summary>
        public const string JWTID = "jti";
    }

    public class RequiredClaim
    {
        /// <summary>
        /// Represents the Audience. Example, http://www.pmapconnect.com
        /// </summary>
        public const string Audience = "Audience";

        /// <summary>
        /// Represents the Auth Grant Type. Examples, Bearer
        /// </summary>
        public const string AuthGrantType = "AuthGrantType";

        /// <summary>
        /// Represents the Auth Head Name. Example, Authorization
        /// </summary>
        public const string AuthHeaderName = "AuthHeaderName";

        /// <summary>
        /// Represents the Created Minutes From Now. Example, 0.
        /// </summary>
        public const string CreatedMinutesFromNow = "CreatedMinutesFromNow";

        /// <summary>
        /// Represents the Digest Algorithm. Example, http://www.w3.org/2001/04/xmlenc#sha256
        /// </summary>
        public const string DigestAlgorithm = "DigestAlgorithm";

        /// <summary>
        /// Represents the Expiration Minutes. Example, 123
        /// </summary>
        public const string ExpirationMinutes = "ExpirationMinutes";

        /// <summary>
        /// Represents the Issuer. Example, http://www.processmap.com
        /// </summary>
        public const string Issuer = "Issuer";

        /// <summary>
        /// Represents the Secret Key. Example, [super safe password].
        /// </summary>
        public const string SecretKey = "SecretKey";

        /// <summary>
        /// Represents the Signature Algorithm. Example, http://www.w3.org/2001/04/xmldsig-more#hmac-sha256
        /// </summary>
        public const string SignatureAlgorithm = "SignatureAlgorithm";

        /// <summary>
        /// Flag that contains whether the customer wants sliding expiration or not.
        /// </summary>
        public const string IsSlidingExpiration = "IsSliding";
    }
}