﻿using System.Text.Json.Serialization;

namespace ProcessMAP.Security.Authentication
{
    /// <summary>
    /// Enumeration of authentication types.
    /// It must map to the types in TenantSettings AuthType table.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AuthType : int
    {
        /// <summary>
        /// AuthType not applicable
        /// </summary>
        None = 0,

        /// <summary>
        /// Database
        /// </summary>
        Database = 1,

        /// <summary>
        /// ADFS 2.0
        /// </summary>
        ADFS = 2,

        /// <summary>
        /// CA SiteMinder
        /// </summary>
        SiteMinder = 3
    }
}