﻿using ProcessMAP.Security.Cryptography;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ProcessMAP.Security.Authentication
{
    /// <summary>
    /// Represents a request for authentication.
    /// </summary>
    public struct AuthRequest
    {
        /// <summary>
        /// Creates an instance of the AuthRequest structure.
        /// </summary>
        /// <param name="username">The username of the user to be authenticated.</param>
        /// <param name="password">The password of the user to be authenticated.</param>
        /// <param name="isSSOEnabled">The SSO status of the request.</param>
        /// <param name="algorithm">The hash algorithm used to secure the password.</param>
        /// <param name="cryptoInfo">The consumer's cryptography information</param>
        public AuthRequest(string username, string password, bool isSSOEnabled, EncryptionAlgorithm algorithm = EncryptionAlgorithm.None, ICryptographyInfo cryptoInfo = null)
        {
            Username = username;
            Password = password;
            IsSSOEnabled = isSSOEnabled;
            Algorithm = algorithm;
            CryptographyInfo = cryptoInfo;
        }

        /// <summary>
        /// A value that represents the username.
        /// </summary>
        [Required]
        public string Username { get; set; }

        /// <summary>
        /// A value that represents the user's password.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// The hash algorithm used to secure the password.
        /// </summary>
        public EncryptionAlgorithm Algorithm { get; set; }

        /// <summary>
        /// The consumer's cryptography information.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public ICryptographyInfo CryptographyInfo { get; set; }

        /// <summary>
        /// Represents the SSO status of the request.
        /// </summary>
        public bool IsSSOEnabled { get; set; }

        /// <summary>
        /// Provides a string formatted version of the AuthRequest.
        /// </summary>
        /// <returns>Returns the AuthRequest as a formatted string.</returns>
        public new string ToString()
        {
            const string Undefined = "Undefined";
            var u = Username;
            var p = Password;
            var i = IsSSOEnabled.ToString();
            var a = Algorithm.ToString();
            var ca = CryptographyInfo == null ? Undefined : CryptographyInfo.Algorithm.ToString();
            var ck = CryptographyInfo == null ? Undefined : CryptographyInfo.KeySize.ToString();
            var cpr = CryptographyInfo == null ? Undefined : CryptographyInfo.PrivateKey;
            var cpue = CryptographyInfo == null ? Undefined : CryptographyInfo.PublicKey.Exponent.ToString();
            var cpum = CryptographyInfo == null ? Undefined : CryptographyInfo.PublicKey.Modulus.ToString();
            var s = string.Format("Username:{0}\nPassword:{1}\nIsSSoEnabled:{2}\nAlgorithm:{3}\nCrytographyAlgorithm:{4}\nCrytographyKeySize:{5}\nCryptographyInfoPrivateKey:{6}\nCryptographyInfoPublicKeyExponent:{7}\nCryptographyInfoPublicKeyModulus:{8}",
                u, p, i, a, ca, ck, cpr, cpue, cpum);

            return s;
        }
    }
}