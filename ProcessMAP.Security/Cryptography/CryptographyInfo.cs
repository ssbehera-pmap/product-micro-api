﻿using System;
using System.Xml.Serialization;

namespace ProcessMAP.Security.Cryptography
{
    /// <summary>
    /// 
    /// </summary>
    public class CryptographyInfo : ICryptographyInfo
    {
        /// <summary>
        /// 
        /// </summary>
        private string privateKeyXml;

        /// <summary>
        /// 
        /// </summary>
        public CryptographyInfo()
        {
            KeySize = Constants.RSAKeySize;
            Algorithm = EncryptionAlgorithm.RSA;
        }

        /// <summary>
        /// 
        /// </summary>
        public EncryptionAlgorithm Algorithm { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int KeySize { get; set; }

        /// <summary>
        /// Gets or sets the xml representation of the private key.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string PrivateKey
        {
            get { return privateKeyXml; }
            set { privateKeyXml = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public PublicKey PublicKey
        {
            get
            {
                switch(Algorithm)
                {
                    case EncryptionAlgorithm.RSA:
                        return RSACryptoUtility.GetPublicKey(PrivateKey, KeySize);

                    default:
                        throw new NotImplementedException();
                }
            }
        }
    }
}