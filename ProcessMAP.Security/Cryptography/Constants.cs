﻿namespace ProcessMAP.Security.Cryptography
{
    /// <summary>
    /// Security constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The size of the private/public RSA keys.
        /// </summary>
        public const int RSAKeySize = 2048;
    }
}