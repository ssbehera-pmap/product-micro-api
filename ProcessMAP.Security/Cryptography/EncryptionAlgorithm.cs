﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProcessMAP.Security.Cryptography
{
    /// <summary>
    /// Enumeration of hash algorithms.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EncryptionAlgorithm : int
    {
        /// <summary>
        /// No hash algorithm defined.
        /// </summary>
        None = 0,

        /// <summary>
        /// SHA256 Hash Algorithm
        /// </summary>
        SHA256 = 1,

        /// <summary>
        /// RSA Encryption Algorithm
        /// </summary>
        RSA = 2
    }
}