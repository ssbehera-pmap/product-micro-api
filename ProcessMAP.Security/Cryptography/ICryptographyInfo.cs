﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ProcessMAP.Security.Cryptography
{
    /// <summary>
    /// This interface ensures encryption capabilities.
    /// </summary>
    public interface ICryptographyInfo
    {
        /// <summary>
        /// Gets or sets the size of the encryption key.
        /// </summary>
        int KeySize { get; set; }

        /// <summary>
        /// Gets or sets the type of encryption algorithm.
        /// </summary>
        EncryptionAlgorithm Algorithm { get; set; }

        /// <summary>
        /// Gets or sets the private key for decryption.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        string PrivateKey { get; }

        /// <summary>
        /// Gets the public key for encryption.
        /// </summary>
        PublicKey PublicKey { get; }
    }
}