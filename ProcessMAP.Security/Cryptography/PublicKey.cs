﻿namespace ProcessMAP.Security.Cryptography
{
    /// <summary>
    /// 
    /// </summary>
    public class PublicKey
    {
        /// <summary>
        /// 
        /// </summary>
        public byte[] Exponent;

        /// <summary>
        /// 
        /// </summary>
        public byte[] Modulus;

        /// <summary>
        /// 
        /// </summary>
        public PublicKey() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exponent"></param>
        /// <param name="modulus"></param>
        public PublicKey(byte[] exponent, byte[] modulus)
            : this()
        {
            Exponent = exponent;
            Modulus = modulus;
        }
    }
}