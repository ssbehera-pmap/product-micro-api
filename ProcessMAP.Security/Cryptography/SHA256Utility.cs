﻿using System.Security.Cryptography;
using System.Text;

namespace ProcessMAP.Security.Cryptography
{
    /// <summary>
    /// 
    /// </summary>
    public static class SHA256Utility
    {
        /// <summary>
        /// Generates a SHA256 hashed string based on the input string value.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GenerateHash(string input)
        {
            var hash = default(byte[]);
            var bytes = Encoding.ASCII.GetBytes(input);

            using (var sha256 = SHA256.Create())
            {
                hash = sha256.ComputeHash(bytes);
            }

            var stringHash = string.Empty;

            for (int i = 0; i < hash.Length; i++)
            {
                stringHash += string.Format("{0:X2}", hash[i]);
            }

            return stringHash.ToLower();
        }
    }
}