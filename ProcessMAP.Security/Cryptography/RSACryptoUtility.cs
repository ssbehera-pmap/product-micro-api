﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace ProcessMAP.Security.Cryptography
{
    /// <summary>
    /// 
    /// </summary>
    public static class RSACryptoUtility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string GenerateNewRSAKeyXml(int size)
        {
            var rsaParams = default(RSAParameters);

            using (var rsa = RSA.Create())
            {
                rsa.KeySize = size;
                rsaParams = rsa.ExportParameters(true);
            }

            var xmlSerializer = new XmlSerializer(rsaParams.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, rsaParams);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cryptographyInfo"></param>
        /// <returns></returns>
        public static RSA LoadRSAKeys(ICryptographyInfo cryptographyInfo)
        {
            using (var rsa = RSA.Create())
            {
                rsa.KeySize = cryptographyInfo.KeySize;

                XmlRootAttribute xmlRoot = new XmlRootAttribute() { ElementName = "RSAKeyValue" };
                var xmlSerializer = new XmlSerializer(typeof(RSAParameters), xmlRoot);
                using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(cryptographyInfo.PrivateKey)))
                {
                    var rsaParams = (RSAParameters)xmlSerializer.Deserialize(memStream);
                    rsa.ImportParameters(rsaParams);
                }

                return rsa;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static CryptographyInfo GenerateDefaultCryptographyInfo()
        {
            return new CryptographyInfo()
            {
                Algorithm = EncryptionAlgorithm.RSA,
                KeySize = Constants.RSAKeySize,
                PrivateKey = GenerateNewRSAKeyXml(Constants.RSAKeySize)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static RSA LoadRSAXmlKeys(string xml, int size = Constants.RSAKeySize)
        {
            var cryptoInfo = new CryptographyInfo()
            {
                Algorithm = EncryptionAlgorithm.RSA,
                KeySize = size,
                PrivateKey = xml
            };

            return LoadRSAKeys(cryptoInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="privateKeyXml"></param>
        /// <param name="keySize"></param>
        /// <returns></returns>
        public static PublicKey GetPublicKey(string privateKeyXml, int keySize)
        {
            var rsaPrivateKey = new RSAParameters();

            if (rsaPrivateKey.D == null || rsaPrivateKey.D.Length == 0)
            {
                if (!string.IsNullOrWhiteSpace(privateKeyXml))
                {
                    rsaPrivateKey = RSACryptoUtility.
                        LoadRSAXmlKeys(privateKeyXml, keySize).
                        ExportParameters(true);
                }
            }

            return new PublicKey(rsaPrivateKey.Exponent, rsaPrivateKey.Modulus);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cypher"></param>
        /// <param name="cryptographyInfo"></param>
        /// <returns></returns>
        public static string Decrypt(string cypher, ICryptographyInfo cryptographyInfo)
        {
            using (var rsa = LoadRSAKeys(cryptographyInfo))
            {
                var originalCypher = Convert.FromBase64String(cypher);
                byte[] decryptedCypher = rsa.Decrypt(originalCypher, RSAEncryptionPadding.Pkcs1);

                return Encoding.UTF8.GetString(decryptedCypher);
            }
        }
    }
}